##
## Makefile for  in /home/lecoq_m/studio
## 
## Made by Maxime LECOQ
## Login   <lecoq_m@epitech.net>
## 
## Started on  Sun Apr 24 17:37:52 2016 Maxime LECOQ
## Last update Sun Jun  5 07:32:03 2016 Maxime LECOQ
##

S_PATH		=	srcs/

I_PATH		=	includes/

SRCS		=	$(S_PATH)main.cpp				\
			$(S_PATH)Controller.cpp				\
			$(S_PATH)Loader.cpp				\
			$(S_PATH)Data.cpp				\
			$(S_PATH)Scores.cpp				\
									\
			$(S_PATH)model/Model.cpp			\
			$(S_PATH)model/ErrorOccured.cpp			\
			$(S_PATH)model/ErrorData.cpp			\
									\
			$(S_PATH)model/menu/Element.cpp			\
			$(S_PATH)model/menu/MenuModel.cpp		\
			$(S_PATH)model/menu/AMenuData.cpp		\
			$(S_PATH)model/menu/MenuDataFactory.cpp		\
			$(S_PATH)model/menu/MenuDataHome.cpp		\
			$(S_PATH)model/menu/MenuDataHomeOld.cpp		\
			$(S_PATH)model/menu/MenuDataOption.cpp		\
			$(S_PATH)model/menu/MenuDataControl.cpp		\
			$(S_PATH)model/menu/MenuDataSound.cpp		\
			$(S_PATH)model/menu/MenuDataCredit.cpp		\
			$(S_PATH)model/menu/MenuDataGame.cpp		\
			$(S_PATH)model/menu/MenuDataScores.cpp		\
			$(S_PATH)model/menu/MenuDataGameParam.cpp	\
									\
			$(S_PATH)model/gloader/ManageLauncher.cpp	\
			$(S_PATH)model/gloader/DataLauncher.cpp		\
			$(S_PATH)model/gloader/TeamNameMaker.cpp	\
									\
			$(S_PATH)model/worms/Worms.cpp			\
			$(S_PATH)model/worms/WormsInventory.cpp		\
			$(S_PATH)model/worms/ManageWaterUp.cpp		\
			$(S_PATH)model/worms/ManagePlayer.cpp		\
			$(S_PATH)model/worms/Cursor.cpp			\
			$(S_PATH)model/worms/Projectile.cpp		\
			$(S_PATH)model/worms/IA.cpp			\
			$(S_PATH)model/worms/Map.cpp			\
			$(S_PATH)model/worms/MapCreator.cpp		\
			$(S_PATH)model/worms/Team.cpp			\
			$(S_PATH)model/worms/Player.cpp			\
			$(S_PATH)model/worms/View.cpp			\
			$(S_PATH)model/worms/Png.cpp			\
			$(S_PATH)model/worms/Pixels.cpp			\
			$(S_PATH)model/worms/Pause.cpp			\
			$(S_PATH)model/worms/Turn.cpp			\
			$(S_PATH)model/worms/Clock.cpp			\
									\
			$(S_PATH)model/generatemap/GeneratorMap.cpp	\
			$(S_PATH)model/generatemap/GeneratorData.cpp	\
			$(S_PATH)model/generatemap/GeneratorPng.cpp	\
									\
			$(S_PATH)gui/EventReceiver.cpp			\
			$(S_PATH)gui/IrrlichtGUI.cpp			\
			$(S_PATH)gui/AViewGUI.cpp			\
			$(S_PATH)gui/LauncherGUI.cpp			\
			$(S_PATH)gui/WormsGUI.cpp			\
			$(S_PATH)gui/MenuGUI.cpp			\
			$(S_PATH)gui/ErrorGUI.cpp			\
			$(S_PATH)gui/GeneratorGUI.cpp			\
			$(S_PATH)gui/TextDisplayer.cpp			\
			$(S_PATH)gui/ImageDisplayer.cpp			\
			$(S_PATH)gui/Sound.cpp				\
			$(S_PATH)gui/MeshPlayer.cpp			\
									\
			$(S_PATH)tools/AError.cpp			\
			$(S_PATH)tools/Epur.cpp				\
			$(S_PATH)tools/AThread.cpp			\
			$(S_PATH)tools/File.cpp				\
			$(S_PATH)tools/ScopedLock.cpp			\
			$(S_PATH)tools/Mutex.cpp			\
			$(S_PATH)tools/Vector.cpp			\
									\
			$(S_PATH)Weapons/AFireWeapon.cpp          	\
                        $(S_PATH)Weapons/ALaunchWeapon.cpp              \
                        $(S_PATH)Weapons/AMeleeWeapon.cpp               \
                        $(S_PATH)Weapons/AThrownWeapon.cpp              \
                        $(S_PATH)Weapons/AWeapon.cpp                    \
                        $(S_PATH)Weapons/Bazooka.cpp                    \
                        $(S_PATH)Weapons/Decorator.cpp                  \
                        $(S_PATH)Weapons/DecoratorStat.cpp              \
                        $(S_PATH)Weapons/FirePunch.cpp                  \
                        $(S_PATH)Weapons/Grenade.cpp                    \
                        $(S_PATH)Weapons/ManageInventory.cpp            \
                        $(S_PATH)Weapons/Shotgun.cpp			\
									\
			$(S_PATH)drop/ADropable.cpp			\
			$(S_PATH)drop/HealPlayer.cpp			\
			$(S_PATH)drop/HealTeam.cpp			\
			$(S_PATH)drop/PoisonPlayer.cpp			\
			$(S_PATH)drop/PoisonTeam.cpp			\
			$(S_PATH)drop/PistolPlus.cpp			\
			$(S_PATH)drop/TeleportPlus.cpp			\
			$(S_PATH)drop/DropFactory.cpp			\
			$(S_PATH)drop/Drop.cpp				\
			$(S_PATH)drop/ManageDrop.cpp			\
									\

OBJS		=	$(SRCS:.cpp=.o)

NAME		=	cpp_indie_studio

CC		=	g++

RM		=	rm -f

CPPFLAGS	=	-I./$(I_PATH)					\
			-W						\
			-Wall						\
			-Werror						\
			-Wextra						\
			-std=c++11


LDFLAGS		=	-lpthread					\
			-L./Install/Irrlicht/lib/Linux/			\
			-lIrrlicht					\
			Install/Irrlicht/lib/Linux/libikpFlac.so	\
			Install/Irrlicht/lib/Linux/libikpMP3.so		\
			Install/Irrlicht/lib/Linux/libIrrKlang.so	\
			-lGL						\
			-lXxf86vm					\
			-lXext						\
			-lGLU						\
			-lX11						\
			-lXcursor					\
			LUA/lib/liblua52.a				\
			-ldl						\

all		:	$(NAME)

$(NAME)		:	$(OBJS)
			$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

install		:
			./Install/Irrlicht/install

clean		:
			$(RM) Makefile~
			$(RM) $(OBJS)

fclean		:	clean
			$(RM) $(NAME)

re		:	fclean all

.PHONY		:	all clean fclean re
