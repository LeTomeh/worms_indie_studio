//
// Position.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May  5 14:21:54 2016 Maxime LECOQ
// Last update Mon May 30 17:16:36 2016 Maxime LECOQ
//

#ifndef		__POSITION_HPP__
# define	__POSITION_HPP__

class		Position
{
private:
  int		_x;
  int		_y;
public:
  Position(const int &x = 0, const int &y = 0) : _x(x), _y(y) {}
  ~Position() {}
  Position(const Position &o) : _x(o._x), _y(o._y) {}
  Position &operator=(const Position &o)
  {
    if (this != &o)
      {
	_x = o._x;
	_y = o._y;
      }
    return (*this);
  }
  int		getX() const
  {
    return (_x);
  }
  int		getY() const
  {
    return (_y);
  }
  void		setX(const int &x)
  {
    _x = x;
  }
  void		setY(const int &y)
  {
    _y = y;
  }
  void		reset(const int &xm, const int &xM, const int &ym, const int &yM)
  {
    if (_x < xm)
      _x = xm;
    else
      if (_x > xM)
	_x = xM;
    if (_y < ym)
      _y = ym;
    else
      if (_y > yM)
	_y = yM;
  }
};

# endif
