//
// Drop.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Thu Jun  2 20:19:46 2016 Maxime LECOQ
//

#ifndef		__DROP_HH__
# define	__DROP_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"Team.hh"
# include	"IDropable.hh"
# include	"Position.hpp"
# include	"imagedefine.h"

class		Drop
{
private:
  IDropable	*_content;
  Position	_pos;
  std::string	_sprite;
public:
  Drop(IDropable *, const Position &);
  Drop(const Drop &);
  Drop &operator=(const Drop &);
  ~Drop();
  IDropable	*getContent() const;
  bool		in(const Position &) const;
  bool		inBullet(const Position &) const;
  Position	getPosition() const;
  void		setPosition(const Position &);
  const std::string	&getSprite() const;
};

#endif
