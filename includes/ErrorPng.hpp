//
// ErrorRead.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Thu May  5 14:57:45 2016 julien dufrene
//

#ifndef		__ERRORPNG_HH__
# define	__ERRORPNG_HH__

# include	"AError.hh"

class ErrorPng : public AError
{
public:
  ErrorPng(std::string const & err) : AError(err, "ErrorPng") {};
  virtual ~ErrorPng() throw() {}
};

#endif		/*__ERROR_HH__ */
