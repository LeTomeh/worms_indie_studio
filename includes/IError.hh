//
// IError.hh for  in /home/lecoq_m/cpp_plazza
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon Apr 18 13:12:05 2016 Maxime LECOQ
// Last update Sun Apr 24 18:19:02 2016 Maxime LECOQ
//

#ifndef		__IERROR_HH__
# define	__IERROR_HH__

class		IError
{
public:
  virtual const char		*what() const throw() = 0;
  virtual void			criticalError() const = 0;
  virtual void		        notCriticalError() const = 0;
  virtual			~IError() throw() {}
};

#endif
