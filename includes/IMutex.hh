//
// IMutex.hh for  in /home/lecoq_m/cpp_plazza
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sat Apr 23 15:34:53 2016 Maxime LECOQ
// Last update Sat Apr 23 15:36:28 2016 Maxime LECOQ
//

#ifndef		__IMUTEX_HH__
# define	__IMUTEX_HH__

class	IMutex
{
 public:
  virtual~IMutex(void) {}
  virtual void	lock(void) = 0;
  virtual void	unlock(void) = 0;
  virtual bool	trylock(void) = 0;
};

#endif
