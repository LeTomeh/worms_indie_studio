//
// AWeapon.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:02:42 2016 Maxence Gaumont
// Last update Sat Jun  4 09:28:15 2016 Maxime LECOQ
//

#ifndef AWEAPON_HH_
# define AWEAPON_HH_

# include	<iostream>
# include	<string>
# include	"IWeapon.hh"

class			AWeapon : public IWeapon
{
public:
  AWeapon(int, int , const std::string &, const std::string &, const std::string &, const std::string &);
  AWeapon(const AWeapon &);
  AWeapon		&operator=(const AWeapon &o);
  virtual ~AWeapon() {}

public:
  virtual void		attack() const = 0;
  const std::string	&getName() const;
  int			getDamage() const;
  int			getMun() const;
  AWeapon		*clone();
  void			setMun(const int &);
  virtual void		proMode() = 0;
  const std::string	&getPictureOff() const;
  const std::string	&getPictureOn() const;
  const std::string	&getDescription() const;
  void			setDescription(const std::string &);
  void			setPictureOn(const std::string &);
  void			setPictureOff(const std::string &);
  const std::string     &getLeftSprite() const;
  const std::string     &getRightSprite() const;
  void                  setLeftSprite(const std::string &);
  void                  setRightSprite(const std::string &);
  const std::string     &getBulletLeft() const;
  const std::string     &getBulletRight() const;
  void                  setBulletLeft(const std::string &);
  void                  setBulletRight(const std::string &);
  void			setPow(const std::string &);
  const std::string	&getPow() const;
  void                  setMesh(const std::string &);
  void                  setTexture(const std::string &);
  void                  setSound(const std::string &);
  const std::string	&getMesh() const;
  const std::string	&getTexture() const;
  const std::string	&getMeshSound() const;
  void			setDamage(const int &);
protected:
  int			_Dmg;
  int			_Mun;
  std::string		_Name;
  std::string		_on;
  std::string		_off;
  std::string		_description;
  std::string		_leftSp;
  std::string		_rightSp;
  std::string		_leftBul;
  std::string		_rightBul;
  std::string		_pow;
  std::string		_mesh;
  std::string		_texture;
  std::string		_meshSound;
};

#endif /* AWEAPON_HH_ */
