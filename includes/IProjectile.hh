//
// IProjectile.hh for oui in /home/thoma_f/rendu/cpp/cpp_indie_studio/includes
// 
// Made by François-Malo Thomas
// Login   <thoma_f@epitech.net>
// 
// Started on  Mon May  9 11:21:56 2016 François-Malo Thomas
// Last update Thu May 19 10:24:46 2016 thoma_f
//

#ifndef         __IPROJECTILE_HH__

# define        __IPROJECTILE_HH__

# include	"ProtoProjectile.hh"

enum		status
  {
    NOTCREATED = 0,
    CREATED,
    MOVING,
    DESTROYED
  };

class IProjectile : public Prototype<IProjectile>
{
public:
  virtual	~IProjectile() {}

public:
  virtual float getAngle() const = 0;
  virtual float getPosX() const = 0;
  virtual float getPosY() const = 0;
  virtual float getSpeed() const = 0;
  virtual float getTargetX() const = 0;
  virtual float getTargetY() const = 0;
  virtual std::string getName() const = 0;
  virtual status getStatus() const = 0;

public:
  virtual void setAngle(float) = 0;
  virtual void setPosX(float) = 0;
  virtual void setPosY(float) = 0;
  virtual void setSpeed(float) = 0;
  virtual void setTargetX(float) = 0;
  virtual void setTargetY(float) = 0;
  virtual void setName(const std::string &) = 0;
  virtual void setStatus(status) = 0;

public:
  virtual void	Appear() = 0;
};

#endif          // ! __IPROJECTILE_HH__
