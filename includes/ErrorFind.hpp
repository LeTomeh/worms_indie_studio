//
// ErrorRead.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Sun May 22 14:07:25 2016 Maxime LECOQ
//

#ifndef		__ERRORFIND_HH__
# define	__ERRORFIND_HH__

# include	"AError.hh"

class ErrorFind : public AError
{
public:
  ErrorFind(std::string const & err) : AError(err, "ErrorFind") {};
  virtual ~ErrorFind() throw() {}
};

#endif		/*__ERROR_HH__ */
