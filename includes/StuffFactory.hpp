
//
// StuffFactory.hpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:38:52 2016 Maxence Gaumont
// Last update Sun May 22 16:15:39 2016 thomas alexandre
//

#ifndef STUFFFACTORY_HPP_
#define STUFFFACTORY_HPP_

#include <map>
#include "Bazooka.hh"
#include "Grenade.hh"
#include "Shotgun.hh"
#include "FirePunch.hh"
#include <typeinfo>

template <class T, class Name=std::string>
class						StuffFactory
{
public:
  StuffFactory() {}
  ~StuffFactory() {}

public:
  void			save(const Name &key, T stuff)
  {
    if (m_map.find(key) == m_map.end())
      m_map[key] = stuff;
  }
  T			create(const Name &key) const
  {
    T		tmp = 0;
    typename std::map<std::string, T>::const_iterator	it = m_map.find(key);

    if (it != m_map.end())
      tmp = ((*it).second)->clone();
    return (tmp);
  }

private:
  std::map<std::string, T>	m_map;
};

#endif /* STUFFACTORY_HPP_ */
