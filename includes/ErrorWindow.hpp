//
// ErrorWindow.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Thu Apr 28 14:22:24 2016 Maxime LECOQ
//

#ifndef		__ERRORWIN_HH__
# define	__ERRORWIN_HH__

# include	"AError.hh"

class ErrorWindow : public AError
{
public:
  ErrorWindow(std::string const & err) : AError(err, "ErrorWindow") {};
  virtual ~ErrorWindow() throw() {}
};

#endif		/*__ERROR_HH__ */
