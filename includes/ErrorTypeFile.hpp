//
// ErrorRead.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Thu May  5 12:13:44 2016 julien dufrene
//

#ifndef		__ERRORTYPEFILE_HH__
# define	__ERRORTYPEFILE_HH__

# include	"AError.hh"

class ErrorTypeFile : public AError
{
public:
  ErrorTypeFile(std::string const & err) : AError(err, "ErrorTypeFile") {};
  virtual ~ErrorTypeFile() throw() {}
};

#endif		/*__ERROR_HH__ */
