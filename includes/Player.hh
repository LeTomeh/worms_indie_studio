//
// ITeam.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:19:35 2016 Maxime LECOQ
// Last update Sun Jun  5 04:06:38 2016 Maxime LECOQ
//

#ifndef		__PLAYER_HH__
# define	__PLAYER_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	"irr/irrlicht.h"
# include	"IPlayer.hh"
# include	"MemberData.hh"
# include	"Position.hpp"
# include	"ManageInventory.hh"
# include	"imagedefine.h"
# include	"Cursor.hh"

class		Player : public IPlayer
{
private:
  std::string	_name;
  int		_life;
  Position	_pos;
  int		_fall;
  std::string	_sprite;
  std::string	_jump;
  int		_cptJ;
  int		_saveY;
  int		_saveCpt;
  Inventory	*_inventory;
  IWeapon	*_current;
  Cursor	*_cursor;
  bool		_dir;
  bool		_tele;
  bool		_myTurn;
public:
  Player(const std::string &, const MemberData *, const Inventory *);
  ~Player();
  Player(const Player &);
  Player &operator=(const Player &);
  void	setLife(const int &);
  int	getLife() const;
  const std::string	&getTeamName() const;
  Position getPosition() const;

  void setPosition(const Position &);
  void	endFall();
  void	fall();
  int	getFall() const;
  void	setSprite(const std::string &);
  const std::string	&getSprite() const;
  void			setJump(const std::string &);
  void			endJump();
  const std::string	&getJump() const;
  void			jump();
  int			getJumpCpt() const;
  void			setWeapon(IWeapon *);
  void			unsetWeapon();
  IWeapon		*getCurrentWeapon() const;
  void			changeDir(const bool &);
  Cursor                *getCursor() const;
  bool			getDir() const;
  bool			teleUse() const;
  void			setTele(const bool &);
  bool			getOk() const;
  void			take(const bool &);
  Inventory		*getInventory() const;
};

#endif
