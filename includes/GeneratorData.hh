//
// MenuGUI.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:14:42 2016 Maxime LECOQ
// Last update Sat May 28 14:09:45 2016 Maxime LECOQ
//

#ifndef		__GENERATORDATA_HH__
# define	__GENERATORDATA_HH__

# include       <iostream>
# include       <string>
# include       <list>
# include       <vector>
# include	"sounddefine.h"
# include       "Convert.hpp"
# include       "Color.hpp"
# include	"Png.hh"

# define COMMAND_G1	"Arrows to move your object"
# define COMMAND_G2	"Return to save object position"
# define COMMAND_G3	"A and Z to switch forms"
# define COMMAND_G4	"Q to QUIT"

class		GeneratorData
{
public:
  GeneratorData();
  ~GeneratorData();
  GeneratorData(const GeneratorData &);
  GeneratorData &operator=(const GeneratorData &);
  void			setNamePng(const std::string &);
  const std::string	&getNamePng() const;
  void			setX(const int &);
  void			setY(const int &);
  int			getX() const;
  int			getY() const;
  const std::string	&getSound() const;
  bool			soundIsOn() const;
  void			setSound(const bool &);
  const std::string     &getEffect() const;
  bool                  effectIsOn() const;
  void                  setEffectOn(const bool &);
  void                  setEffect(const std::string &);
  bool			getReload() const;
  void			reload(const bool &);
private:
  std::string		_pngName;
  int			x;
  int			y;
  std::string		_sound;
  bool			_soundOn;
  std::string		_effect;
  bool			_effectOn;
  bool			_reload;
};

#endif
