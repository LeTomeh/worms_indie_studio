//
// MemberData.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May  5 14:26:24 2016 Maxime LECOQ
// Last update Thu May  5 14:49:17 2016 Maxime LECOQ
//

#ifndef		__MEMBERDATA_HPP__
# define	__MEMBERDATA_HPP__

# include	"Position.hpp"

class		MemberData
{
  Position	_pos;
  int		_life;
public:
  MemberData(const int &l, const Position &p) : _pos(p), _life(l) {}
  ~MemberData() {}
  MemberData(const MemberData &o) : _pos(o._pos), _life(o._life) {}
  MemberData &operator=(const MemberData &o)
  {
    if (this != &o)
      {
	_pos = o._pos;
	_life = o._life;
      }
    return (*this);
  }
  int		getLife() const
  {
    return (_life);
  }
  Position	getPos() const
  {
    return (_pos);
  }
  
};

#endif
