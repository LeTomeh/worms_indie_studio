//
// HealPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Thu Jun  2 16:58:05 2016 Maxime LECOQ
//

#ifndef		__HEALPLAYER_HH__
# define	__HEALPLAYER_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"Team.hh"
# include	"ADropable.hh"

class		HealPlayer : public ADropable
{
public:
  HealPlayer();
  HealPlayer(const HealPlayer &);
  HealPlayer &operator=(const HealPlayer &);
  ~HealPlayer();
  void		doEffect(Player *, Team *);
};

#endif
