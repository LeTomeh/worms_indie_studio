//
// ADropable.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Fri Jun  3 14:01:00 2016 Maxime LECOQ
//

#ifndef		__ADROPABLE_HH__
# define	__ADROPABLE_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"sounddefine.h"
# include	"Team.hh"
# include	"IDropable.hh"

class		ADropable : public IDropable
{
protected:
  std::string	_name;
  std::string	_description;
  std::string	_desc;
public:
  ADropable(const std::string &, const std::string &, const std::string &);
  ADropable(const ADropable &);
  ADropable &operator=(const ADropable &);
  virtual ~ADropable();
  const std::string	&getName() const;
  const std::string	&getDescription() const;
  const std::string	&getDesc() const;
  virtual void		doEffect(Player *, Team *) = 0;
};

#endif
