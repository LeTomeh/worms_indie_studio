//
// AThread.hh for  in /home/lecoq_m/cpp_plazza/bootstrap
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed Apr 13 10:31:21 2016 Maxime LECOQ
// Last update Sat May 21 12:08:32 2016 Maxime LECOQ
//

#ifndef THREAD_HH_
# define THREAD_HH_

# include	<iostream>
# include	<thread>
# include	"ErrorThread.hpp"
# include	"IThread.hh"

class		AThread : public IThread
{
private:
  IThread::ThreadStatus	_status;
  std::thread		*_thread;
public:
  AThread();
  AThread(const AThread &);
  AThread &operator=(const AThread &);
  ~AThread();
  IThread::ThreadStatus		getStatus() const;
  void				setStatus(const IThread::ThreadStatus &);
  void				run(void *(*)(void *), void *);
  template<class T>
  void				run(void *(T::*)(void *), void *);
  void				waitDeath();
};

#endif
