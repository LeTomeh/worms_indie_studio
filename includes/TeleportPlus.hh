//
// PoisonPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Thu Jun  2 17:30:54 2016 Maxime LECOQ
//

#ifndef		__TELEPLUS_HH__
# define	__TELEPLUS_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"Team.hh"
# include	"ADropable.hh"

class		TeleportPlus : public ADropable
{
public:
  TeleportPlus();
  TeleportPlus(const TeleportPlus &);
  TeleportPlus &operator=(const TeleportPlus &);
  ~TeleportPlus();
  void		doEffect(Player *, Team *);
};

#endif
