//
// AProjectile.hh for oui in /home/thoma_f/Rendu/Cpp/cpp_indie_studio
// 
// Made by thoma_f
// Login   <thoma_f@epitech.net>
// 
// Started on  Wed May 18 17:19:23 2016 thoma_f
// Last update Mon May 30 17:57:36 2016 thoma_f
//

#ifndef		__APROJECTILE_HH__
# define	__APROJECTILE_HH__

# include	<iostream>
# include	<string>
# include	<irr/vector2d.h>
# include	"IProjectile.hh"

class		AProjectile : public IProjectile
{
public:
  AProjectile(float angle, float posX, float poxY, float speed, float targetX, float targetY, const std::string &name);
  AProjectile(const AProjectile &);
  AProjectile & operator=(const AProjectile &);
  virtual ~AProjectile() {}
  
private:
  float		_angle;
  float		_posX;
  float		_posY;
  float		_speed;
  float		_targetX;
  float		_targetY;
  std::string	_name;
  status	_status;

public:
  virtual float getAngle() const;
  virtual float getPosX() const;
  virtual float getPosY() const;
  virtual float getSpeed() const;
  virtual float getTargetX() const;
  virtual float getTargetY() const;
  virtual std::string getName() const;
  virtual status getStatus() const;

public:
  virtual void setAngle(float);
  virtual void setPosX(float);
  virtual void setPosY(float);
  virtual void setSpeed(float);
  virtual void setTargetX(float);
  virtual void setTargetY(float);
  virtual void setName(const std::string &);
  virtual void setStatus(status);

};

#endif		// ! __APROJECTILE_HH__
