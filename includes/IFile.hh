//
// ILogFile.hh for  in /home/lecoq_m/basics_cpp
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 18:51:06 2016 Maxime LECOQ
// Last update Tue Apr 26 17:18:57 2016 Maxime LECOQ
//

#ifndef		__IFILE_HH__
# define	__IFILE_HH__

# include	<unistd.h>

class		IFile
{
public:
  virtual		~IFile() {};
  virtual void		setFile(const std::string &) = 0;
  virtual void		writeAppend(const std::string &) = 0;
  virtual void		writeTronc(const std::string &) = 0;
  virtual std::string	read() = 0;
  virtual void		destroy() = 0;
  virtual void		clear() = 0;
};

#endif
