//
// Inventory.hpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Sun May  1 20:31:27 2016 Maxence Gaumont
// Last update Sun Jun  5 04:10:46 2016 Maxime LECOQ
//

#ifndef INVENTORY_HPP_
# define INVENTORY_HPP_

# include <vector>
# include "StuffFactory.hpp"

class			Inventory
{
public:
  Inventory() : _pos(0) {}
  ~Inventory() {}

public:
  std::vector<std::string>	getWeapons() const
  {
    std::vector<std::string>	ret;
    for (std::map<std::string, IWeapon *>::const_iterator it=mWeapon.begin(); it!=mWeapon.end(); ++it)
      ret.push_back(it->first);
    return (ret);
  }
  template<typename T> void	fillStuff(const T stuff)
  {
    IWeapon			*weapon = new Bazooka();

    if (dynamic_cast<T>(weapon) != 0)
      mWeapon[stuff->getName()] = stuff;
    delete (weapon);
  }
  template<typename T> T	getStuff(const std::string &stuff)
  {
    IWeapon			*weapon = new Bazooka();

    if (dynamic_cast<T>(weapon) != 0)
      {
	typename std::map<std::string, IWeapon *>::const_iterator	it = mWeapon.find(stuff);
	if (it != mWeapon.end())
	  {
	    delete (weapon);
	    return (*it).second;
	  }
      }
    delete (weapon);
    return (0);
  }
  template<typename T> std::map<std::string, T>	getTypeStuff()
  {
    IWeapon			*weapon = new Bazooka();

    if (dynamic_cast<T>(weapon) != 0)
      {
	delete (weapon);
	return (mWeapon);
      }
    delete (weapon);
    return ("");
  }
  IWeapon			*getWeapon(const std::string &s)
  {
    return (mWeapon[s]);
  }
  void				setPos(const int &p)
  {
    _pos = p;
  }
  int				getPos() const
  {
    return (_pos);
  }
private:
  std::map<std::string, IWeapon *>	mWeapon;
  int					_pos;
};

#endif /* INVENTORY_HPP_ */
