//
// ALaunchWeapon.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 17:40:03 2016 Maxence Gaumont
// Last update Fri Jun  3 01:10:22 2016 Maxime LECOQ
//

#ifndef ALAUNCHWEAPON_HH_
#define ALAUNCHWEAPON_HH_

#include "AWeapon.hh"

class			ALaunchWeapon : public AWeapon
{
public:
  ALaunchWeapon(int, int, const std::string &, const std::string &, const std::string &, const std::string &);
  virtual ~ALaunchWeapon() {}

public:
  virtual void		attack() const;
  virtual void		proMode() = 0;
};

#endif /* ALAUNCHWEAPON_HH_ */
