//
// Turn.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue May  3 13:05:44 2016 Maxime LECOQ
// Last update Sat Jun  4 02:52:32 2016 Maxime LECOQ
//

# ifndef	__TURN_HH__
# define	__TURN_HH__

# include	<vector>
# include	<iostream>
# include	<string>
# include	<map>
# include	<ctime>
# include	"imagedefine.h"
# include	"CatchIt.hpp"
# include	"enumEvent.hh"
# include	"Pixels.hh"
# include	"Team.hh"
# include	"Map.hh"
# include	"enumEvent.hh"

class	Turn
{
public:
  typedef void		(Turn::*f)(Player *, Map *);
 private:
  Pixels                _pixels;
  size_t		_pTeam;
  std::map<int, std::string> _converter;
  std::clock_t		_clock;
  std::map<std::string, f> _dispatch;
  int		_pause;
  std::clock_t		_clockPause;
  bool		_isPause;
  std::vector<Team *>	_team;
  bool			_move;
  bool			_jump;
  int			_decal;
public:
  Turn(std::map<int, std::string>);
  ~Turn();
  Turn(const Turn &);
  Turn &operator=(const Turn &);
  void	playTurn(std::vector<Team *> &, Map *, const int &);
  void	checkTime(int, const std::clock_t &);
  int	getTeamTurn() const;
  void	startPause(const std::clock_t &);
  void	endPause(const std::clock_t &);
  void	resetPause();
  void	setTurn(const int &);
  void	checkJump(std::vector<Team *> &, Map *);
  std::clock_t getClock() const;
  void		nextTurn(const std::vector<Team *> &);
  bool		getMove() const;
  bool		getJump() const;
  void		setDecal(const int &);
private:
  void			right(Player *, Map *);
  void			left(Player *, Map *);
  void			jumpRight(Player *, Map *);
  void			jumpLeft(Player *, Map *);
  void			continueJump(Player *, Map *);
  bool			wormsIn(const Position &);
  void			moveTo(Position &, const Position &, Map *);
};

#endif
