//
// PoisonPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Thu Jun  2 16:58:33 2016 Maxime LECOQ
//

#ifndef		__POISONTEAM_HH__
# define	__POISONTEAM_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"Team.hh"
# include	"ADropable.hh"

class		PoisonTeam : public ADropable
{
public:
  PoisonTeam();
  PoisonTeam(const PoisonTeam &);
  PoisonTeam &operator=(const PoisonTeam &);
  ~PoisonTeam();
  void		doEffect(Player *, Team *);
};

#endif
