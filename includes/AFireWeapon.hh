//
// AFireWeapon.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 17:40:03 2016 Maxence Gaumont
// Last update Fri Jun  3 01:10:06 2016 Maxime LECOQ
//

#ifndef AFIREWEAPON_HH_
#define AFIREWEAPON_HH_

#include "AWeapon.hh"

class			AFireWeapon : public AWeapon
{
public:
  AFireWeapon(int, int, const std::string &, const std::string &, const std::string &, const std::string &);
  virtual ~AFireWeapon() {}

public:
  void			attack() const;
  virtual void		proMode() = 0;
};

#endif /* AFIREWEAPON_HH_ */
