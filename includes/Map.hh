//
// Map.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:11:29 2016 Maxime LECOQ
// Last update Wed Jun  1 16:22:42 2016 Maxime LECOQ
//

#ifndef		__MAP_HH__
# define	__MAP_HH__

# include	<vector>
# include	<iostream>
# include	<string>
# include	"Vector.hh"
# include	"Png.hh"
# include	"Position.hpp"
# include	"CatchIt.hpp"
# include	"ThrowErrorData.hpp"
# include	"Team.hh"
# include	"Player.hh"

class		Map
{
private:
  Png		*_png;
  std::vector<Position>	_pos;
  int		_width;
  int		_height;
  png_bytep	*_rowPointers;
  Png		*_wa;
  public:
  Map(const std::string &);
  ~Map();
  Map(const Map &);
  Map &operator=(const Map &);
  bool	save(const bool &);
  void	setPixels(const int &, const int &, png_byte *);
  png_byte	*getPixels(const int &, const int &) const;
  bool		ground(const int &, const int &) const;
  int		luminosity(const int &, const int &) const;
  int		getWidth() const;
  int		getHeight() const;
  bool		isEdit() const;
  bool		isSave() const;
  void		setSave(const bool &);
  std::vector<Position>	getPosition() const;
  Position	getPosition(const int &) const;
  void		deletePosition(const int &, const int &);
  void		deletePosition(const int &);
  void		deletePosition(const Position &);
  void		resetPosition(const int &, const Position &);
  void		resetPosition(const Position &, const Position &);
  void		addPosition(const Position &);
  void		destroyMapWater(const int &);
  bool		getWaterAt(const Position &, const int &);
  void		destroy(const int &, const int &, const Png &);
};

#endif
