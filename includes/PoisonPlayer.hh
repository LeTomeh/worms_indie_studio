//
// PoisonPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Thu Jun  2 16:58:25 2016 Maxime LECOQ
//

#ifndef		__POISPLAYER_HH__
# define	__POISPLAYER_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"Team.hh"
# include	"ADropable.hh"

class		PoisonPlayer : public ADropable
{
public:
  PoisonPlayer();
  PoisonPlayer(const PoisonPlayer &);
  PoisonPlayer &operator=(const PoisonPlayer &);
  ~PoisonPlayer();
  void		doEffect(Player *, Team *);
};

#endif
