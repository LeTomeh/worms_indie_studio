//
// Scores.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue May  3 13:05:44 2016 Maxime LECOQ
// Last update Tue May  3 17:29:47 2016 Maxime LECOQ
//

#ifndef		__SCORES_HH__
# define	__SCORES_HH__

# include	<vector>
# include	<iostream>
# include	<string>
# include	<map>
# include	"String.hpp"
# include	"Epur.hh"
# include	"Convert.hpp"

class		Scores
{
private:
  std::vector<std::string>	_nameWin;
  std::vector<std::string>	_nameDef;
  std::vector<std::string>	_nameScores;
  std::vector<std::string>	_nameRatio;
  std::map<std::string, int>	_win;
  std::map<std::string, int>	_def;
  std::map<std::string, int>	_scores;
  std::map<std::string, float>	_ratio;
public:
  Scores();
  ~Scores();
  Scores(const Scores &);
  Scores &operator=(const Scores &);
  void	 setScores(std::vector<std::string> &);
  std::vector<std::string>	getNameWin() const;
  std::vector<std::string>	getNameDefeat() const;
  std::vector<std::string>	getNameScores() const;
  std::vector<std::string>	getNameRatio() const;
  std::map<std::string, int>	getWin() const;
  std::map<std::string, int>	getDefeat() const;
  std::map<std::string, int>	getScores() const;
  std::map<std::string, float>	getRatio() const;
  std::vector<std::string>	getNameSc(const int &) const;
private:
  bool	checkIntegrity(const std::string &);
  void	setScoresSnd(std::vector<std::string> &);
  std::vector<std::string>        bestPlus(std::map<std::string, float>, std::vector<std::string>);
  std::vector<std::string>        best(std::map<std::string, int>, std::vector<std::string>);
};

#endif
