//
// IGraphic.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:27:11 2016 Maxime Lecoq
// Last update Mon May 16 12:22:26 2016 Maxime LECOQ
//

#ifndef		__IGRAPHIC_HH__
# define	__IGRAPHIC_HH__

# include	<iostream>
# include	"Rectangle.hpp"
# include	"Color.hpp"

class		IGraphic
{
public:
  virtual void		refresh() = 0;
  virtual void		clear() = 0;
  virtual void		quit() = 0;
  virtual bool		run() const = 0;
  virtual void		putOff() = 0;
  virtual ~IGraphic() {};
};

#endif
