//
// ManageWaterUp.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:48:37 2016 Maxime LECOQ
// Last update Sun Jun  5 15:35:24 2016 Maxime LECOQ
//

#ifndef		__IA_HH__
# define	__IA_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	<ctime>
# include	"Team.hh"
# include	"Map.hh"
# include	"lua/lua.hpp"
# include	"enumEvent.hh"
# include	"CatchIt.hpp"

class		IA
{
private:
  float		_next;
  std::clock_t	_cl;
  std::string	_dir;
  Position	_save;
  lua_State	*_L;
  Player	*_last;
  bool		_switchInv;
  bool		_teleActif;
  bool		_selTele;
  bool		_dbg;
  bool		_launch;
public:
  IA();
  ~IA();
  IA(const IA &);
  IA &operator=(const IA &);
  std::string		playTurn(std::vector<Team *> &, const int &, Map *);
private:
  int			returnDif(const int &, const int &);
  void			checkShoot(Player *, Map *, const Position &);
  void			checkTp(Player *, const Position &);
  void			manageTeleporter(Player *, const Position &);
};

#endif
