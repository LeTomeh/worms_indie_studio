//
// IEpur.hh for  in /home/lecoq_m/studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 18:24:17 2016 Maxime LECOQ
// Last update Tue Apr 26 18:06:57 2016 Maxime LECOQ
//

#ifndef		__IEPUR_HH__
# define	__IEPUR_HH__

class		IEpur
{
public:
  virtual			~IEpur() {};
  virtual const std::string	&getString() const = 0;
  virtual std::string		epur(const std::string &, const char &) = 0;
};

#endif
