/*
** SoundDefine.h for  in /home/lecoq_m/cpp_indie_studio
** 
** Made by Maxime LECOQ
** Login   <lecoq_m@epitech.net>
** 
** Started on  Fri May 27 18:46:24 2016 Maxime LECOQ
** Last update Thu Jun  2 22:44:48 2016 Maxime LECOQ
*/

#ifndef		__SOUNDDEFINE_H__
# define	__SOUNDDEFINE_H__

# define	GAME	"assets/sounds/game.wav"
# define	MENU	"assets/sounds/menu.wav"
# define	SEL	"assets/sounds/selection.wav"
# define	SWI	"assets/sounds/switch.wav"
# define	WALK	"assets/sounds/walk.wav"
# define	ENDTURN	"assets/sounds/endturn.wav"
# define	CLOCKTURN "assets/sounds/clockturn.wav"
# define	ENDGAME	"assets/sounds/endgame.wav"
# define	EXCELLENT "assets/sounds/excellent.wav"
# define	DEAD	"assets/sounds/death.wav"
# define	JUMP	"assets/sounds/jump.wav"
# define	SAVE	"assets/sounds/save.wav"
# define	FALL	"assets/sounds/fall.wav"
# define	ERROR	"assets/sounds/error.wav"
# define	DEL	"assets/sounds/delete.wav"
# define	BYEBYE	"assets/sounds/byebye.wav"
# define	HELLO	"assets/sounds/hello.wav"
# define	FAIL	"assets/sounds/fail.wav"
# define	SELWE	"assets/sounds/selectWeapon.wav"

# define	TELEPORTER "assets/sounds/teleport.wav"
# define	SHOTGUN	"assets/sounds/shotgun.wav"
# define	PISTOL	"assets/sounds/pistol.wav"
# define	GRECOL	"assets/sounds/grenadecol.wav"
# define	GREEXP	"assets/sounds/grenadeexp.wav"
# define	BAZOEXP	"assets/sounds/bazookaexp.wav"
# define	LAUNCH	"assets/sounds/launch.wav"
# define	ROLAU	"assets/sounds/roquetlaunch.wav"
# define	FIREPUNCH "assets/sounds/firePunch.wav"

# define	S1	"assets/sounds/begin/amazing.wav"
# define	S2	"assets/sounds/begin/excellent.wav"
# define	S3	"assets/sounds/begin/illgetyou.wav"
# define	S4	"assets/sounds/begin/incoming.wav"
# define	S5	"assets/sounds/begin/perfect.wav"
# define	S6	"assets/sounds/begin/runaway.wav"
# define	S7	"assets/sounds/begin/watchthis.wav"
# define	S8	"assets/sounds/begin/yessir.wav"
# define	S9	"assets/sounds/begin/yessir2.wav"
# define	S10	"assets/sounds/begin/youllregretthat.wav"
# define	START	"assets/sounds/begin/start.wav"

# define	HEAL	"assets/sounds/heal.wav"
# define	POISON	"assets/sounds/poison.wav"

#endif
