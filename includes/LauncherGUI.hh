//
// LauncherGUI.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:14:42 2016 Maxime LECOQ
// Last update Fri May 27 18:49:32 2016 Maxime LECOQ
//

#ifndef		__LAUNCHGUI_HH__
# define	__LAUNCHGUI_HH__

# include       <iostream>
# include       <string>
# include       <list>
# include       <vector>
# include	"sounddefine.h"
# include       "irr/irrlicht.h"
# include       "TextDisplayer.hh"
# include       "Element.hh"
# include       "Convert.hpp"
# include       "AMenuData.hh"
# include       "Rectangle.hpp"
# include       "Color.hpp"
# include	"enumEvent.hh"
# include	"TeamNameMaker.hh"
# include	"AViewGUI.hh"
# include	"ImageDisplayer.hh"
# include	"Sound.hh"

class		LauncherGUI : public AViewGUI
{
private:
  TeamNameMaker			*_team;
public:
  LauncherGUI(irr::IrrlichtDevice *, irr::video::IVideoDriver *, irr::scene::ISceneManager *, irr::gui::IGUIEnvironment *, TextDisplayer *, ImageDisplayer *, const int &, const int &, Sound *);
  ~LauncherGUI();
  LauncherGUI(const LauncherGUI &);
  LauncherGUI &operator=(const LauncherGUI &);
  void			setTeamNameMaker(TeamNameMaker *);
  void			launch();
private:
  void                          drawMenu(const int &);
  void                          drawContinue(const int &);
};

#endif
