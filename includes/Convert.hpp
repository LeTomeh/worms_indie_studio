//
// Convert.hh for  in /home/lecoq_m/basics_cpp
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 19:04:44 2016 Maxime LECOQ
// Last update Tue May  3 23:17:07 2016 Maxime LECOQ
//

#ifndef		__CONVERT_HPP__
# define	__CONVERT_HPP__

# include	<fstream>
# include	<sstream>

template<typename T>
class		Convert
{
public:
  Convert() {}
  ~Convert() {}
private:
  Convert(const Convert &cop) { (void)cop; }
  Convert &operator=(const Convert &cop) { (void)cop; return (*this); }
public:
  std::string		toString(const T &nb)
  {
    std::ostringstream s;
    s << nb;
    return (s.str());
  }
  T			toNumber(const std::string &s)
  {
    T			i;

    std::istringstream (s) >> i;
    return (i);
  }
  T			toNumber(const char *s)
  {
    std::string		tmp(s);
    T			i;

    std::istringstream (tmp) >> i;
    return (i);
  }
};

#endif
