/*
** ImageDefine.h for  in /home/lecoq_m/cpp_indie_studio
** 
** Made by Maxime LECOQ
** Login   <lecoq_m@epitech.net>
** 
** Started on  Fri May 27 18:46:24 2016 Maxime LECOQ
** Last update Sun May 29 20:14:08 2016 Maxime LECOQ
*/

#ifndef		__OTHEREFINE_H__
# define	__OTHERDEFINE_H__

# define	DBAZOOKA	"Bazooka is a launcher weapon : 45 damages"
# define	DFIREPUNCH	"Firepunch is a melee attack : 30 damages"
# define	DGRENADE	"Grenade is a throw weapon : 40 damages"
# define	DTELE		"Teleporter permit someone to move where he wants"
# define	DPISTOL		"Pistol is a fire weapon with long range : 25 damages"
# define	DSHOTGUN	"Shotgun is a fire weapon with small range : 50 damages"

#endif
