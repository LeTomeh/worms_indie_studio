//
// TeamNameMaker.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 09:32:11 2016 Maxime LECOQ
// Last update Sat May 28 19:13:00 2016 Maxime LECOQ
//

#ifndef		__TEAMNAMEMAKER_HH__
# define	__TEAMNAMEMAKER_HH__

# include	<iostream>
# include	<stdlib.h>
# include	<vector>
# include	<map>
# include	"sounddefine.h"
# include	"Convert.hpp"

class		TeamNameMaker
{
private:
  int		_id;
  std::string	_name;
  std::string	_basic;
  int		_pos;
  unsigned int	_max;
  std::map<int, std::vector<std::string> >	_map;
  bool		_status;
  int		_turn;
  std::string	_sound;
  bool		_soundOn;
  std::string   _effect;
  bool		_effectOn;
  int		_pistol;
  int		_missile;
public:
  TeamNameMaker(const int &, const std::vector<std::string> &n, const bool &);
  ~TeamNameMaker();
  TeamNameMaker(const TeamNameMaker &);
  TeamNameMaker &operator=(const TeamNameMaker &);
  const std::string	&getName() const;
  const std::string	&getBasic() const;
  void			setName(const std::string &);
  void			setPos(const int &);
  int			getPos() const;
  unsigned int		getMax() const;
  void			setMax(const size_t &);
  std::map<int, std::vector<std::string> > getMap();
  void			setMap(std::map<int, std::vector<std::string> >);
  bool			getStatus() const;
  void			setTurn(const int &);
  int			getTurn() const;
  const std::string	&getSound() const;
  bool			soundIsOn() const;
  void			setSound(const bool &);
  const std::string     &getEffect() const;
  bool                  effectIsOn() const;
  void			setEffectOn(const bool &);
  void			setEffect(const std::string &);
  int			getPistol() const;
  int			getMissile() const;
  void			setPistol(const int &);
  void			setMissile(const int &);
};

#endif
