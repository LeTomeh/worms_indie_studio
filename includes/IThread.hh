//
// IThread.hh for IThread in /home/lecoq_m/cpp_plazza/bootstrap
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@lecoq-Epitech>
// 
// Started on  Wed Apr 13 10:32:14 2016 Maxime LECOQ
// Last update Sun Apr 24 18:40:07 2016 Maxime LECOQ
//

#ifndef ITHREAD_HH_
# define ITHREAD_HH_

#include        <pthread.h>

class		IThread
{
public:

  enum ThreadStatus
    {
      RUN = 0,
      WAIT = 1
    };
  virtual ~IThread() {};

  virtual ThreadStatus	getStatus() const = 0;
  virtual void		setStatus(const ThreadStatus &) = 0;
  virtual void		run(void *(*)(void *), void *) = 0;
  virtual void		waitDeath() = 0;
};

#endif /* !ITHREAD_HH_ */
