//
// ManageWaterUp.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:48:37 2016 Maxime LECOQ
// Last update Mon May 30 16:57:16 2016 Maxime LECOQ
//

#ifndef		__CURSOR_HH__
# define	__CURSOR_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	"imagedefine.h"
# include	"Position.hpp"

class		Cursor
{
private:
  Position	_pos;
  std::string	_sprite;
  bool		_active;
  int		_angle;
public:
  Cursor();
  ~Cursor();
  Cursor(const Cursor &);
  Cursor &operator=(const Cursor &);
  void			setSprite(const std::string &);
  void			setPosition(const Position &);
  void			setX(const int &);
  void			setY(const int &);
  const std::string	&getSprite() const;
  Position		getPosition() const;
  int			getX() const;
  int			getY() const;
  void			setActive(const bool &);
  bool			getActive() const;
  int			getAngle() const;
  void			setAngle(const int &);
};

#endif
