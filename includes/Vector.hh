//
// Vector.hh for  in /home/lecoq_m/basics_cpp
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 21:20:40 2016 Maxime LECOQ
// Last update Sun Apr 24 21:36:24 2016 Maxime LECOQ
//

#ifndef		__VECTOR_HH__
# define	__VECTOR_HH__

# include       <iostream>
# include       <string>
# include       <vector>

class		Vector
{
public:
  class           Vectorize
  {
  private:
    std::vector<std::string>      _vec;
  public:
    Vectorize();
    Vectorize(const std::string &, const char &);
    ~Vectorize();
    Vectorize(const Vectorize &);
    Vectorize &operator=(const Vectorize &);
  public:
    std::vector<std::string>      getVector() const;
    std::vector<std::string>      getVector(const std::string &, const char &);
    std::vector<std::string>      getVector(const char *, const char &);
  };
  class           Devectorize
  {
  private:
    std::string           _str;
  public:
    Devectorize();
    Devectorize(const std::vector<std::string> &, const char &);
    ~Devectorize();
    Devectorize(const Devectorize &);
    Devectorize &operator=(const Devectorize &);
  public:
    const std::string     &getString() const;
    std::string           getString(std::vector<std::string> &, const char &);
  };
private:
  Vectorize		_v;
  Devectorize		_d;
public:
  Vector();
  ~Vector();
  Vector(const Vector &);
  Vector &operator=(const Vector &);
  std::vector<std::string>	getVector(const std::string &, const char &);
  std::vector<std::string>	getVector(const char *, const char &);
  std::string			getString(std::vector<std::string> &, const char &);
};

#endif
