//
// MeshPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 23:56:09 2016 Maxime LECOQ
// Last update Fri Jun  3 00:51:23 2016 Maxime LECOQ
//

#ifndef		__MESHPLAYER_HH__
# define	__MESHPLAYER_HH__

# include	<iostream>
# include	<unistd.h>
# include	"irr/irrlicht.h"

class		MeshPlayer
{
public:
  MeshPlayer(const std::string &, irr::video::IVideoDriver *, irr::scene::ISceneManager *);
  MeshPlayer(const char *, irr::video::IVideoDriver *, irr::scene::ISceneManager *);
  MeshPlayer(const MeshPlayer &);
  MeshPlayer &operator=(const MeshPlayer &);
  ~MeshPlayer() {};
  void		deleteIt();
  void          play(irr::IrrlichtDevice *);
  void          loadTexture(const std::string &);
  void          loadTexture(const char *);
private:
  void          add_camera();
private:
  irr::video::IVideoDriver              *_driver;
  irr::scene::ISceneManager             *_scenemgr;
  irr::scene::IAnimatedMeshSceneNode    *node;
};

#endif
