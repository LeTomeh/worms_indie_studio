/*
** ImageDefine.h for  in /home/lecoq_m/cpp_indie_studio
** 
** Made by Maxime LECOQ
** Login   <lecoq_m@epitech.net>
** 
** Started on  Fri May 27 18:46:24 2016 Maxime LECOQ
** Last update Thu Jun  2 17:09:04 2016 Maxime LECOQ
*/

#ifndef		__IMAGEDEFINE_H__
# define	__IMAGEDEFINE_H__

# define	WSEL		"assets/selection.png"
# define	WBAZOOKA	"assets/weapons/Bazooka.png"
# define	WFIREPUNCH	"assets/weapons/Firepunch.png"
# define	WGRENADE	"assets/weapons/Grenade.png"
# define	WTELEON		"assets/weapons/TeleOn.png"
# define	WTELEOFF	"assets/weapons/TeleOff.png"
# define	WPISTOLON	"assets/weapons/PistolOn.png"
# define	WPISTOLOFF	"assets/weapons/PistolOff.png"
# define	WSHOTGUN	"assets/weapons/Shotgun.png"

# define	MOVELEFT	"assets/worms/wormsLeft.png"
# define	MOVERIGHT	"assets/worms/wormsRight.png"
# define	FRONT		"assets/worms/front.png"
# define	BAZOOL		"assets/worms/bazooLeft.png"
# define	BAZOOR		"assets/worms/bazooRight.png"
# define	GRENADEL	"assets/worms/grenadeLeft.png"
# define	GRENADER	"assets/worms/grenadeRight.png"
# define	SHOTGUNL	"assets/worms/shotLeft.png"
# define	SHOTGUNR	"assets/worms/shotRight.png"
# define	PISTOLL		"assets/worms/pistolLeft.png"
# define	PISTOLR		"assets/worms/pistolRight.png"
# define	TELEL		"assets/worms/teleLeft.png"
# define	TELER		"assets/worms/teleRight.png"
# define	FIREL		"assets/worms/fireLeft.png"
# define	FIRER		"assets/worms/fireRight.png"

# define	CURSOR		"assets/cursor.png"
# define	CURSORERR	"assets/cursorerr.png"

# define	BULLETSHOT	"assets/weapons/bulletshotgun.png"
# define	BULLETSHOTREV	"assets/weapons/bulletshotgunrev.png"
# define	BULLETBAZOOSHOT	"assets/weapons/bulletbazoo.png"
# define	BULLETBAZOOREV	"assets/weapons/bulletbazoorev.png"
# define	BULLETGRESHOT	"assets/weapons/bulletgrenade.png"
# define	BULLETGREREV	"assets/weapons/bulletgrenaderev.png"

# define	BAZOOKAPOCHOIR	"assets/weapons/bazookapo.png"
# define	GRENADEPOCHOIR	"assets/weapons/grenadepo.png"
# define	SHOTGUNPOCHOIR	"assets/weapons/shotgunpo.png"
# define	PISTOLPOCHOIR	"assets/weapons/pistolpo.png"

# define		WINDR		"assets/windR.png"
# define		WINDL		"assets/windL.png"
# define		WINDR2		"assets/windR2.png"
# define		WINDL2		"assets/windL2.png"
# define		NOWIND		"assets/windNo.png"

# define	POS		"assets/pos.png"
# define	DROP		"assets/drop.png"

#endif
