//
// Map.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:11:29 2016 Maxime LECOQ
// Last update Sat May 28 09:41:07 2016 Maxime LECOQ
//

#ifndef		___GENERATORPNG_HH__
# define	___GENERATORPNG_HH__

# include	<iostream>
# include	<vector>
# include	<stdlib.h>
# include	<time.h>
# include	<sys/time.h>
# include	"Png.hh"
# include	"File.hh"
# include	"CatchIt.hpp"
# include	"AError.hh"
# include	"png.h"
# include	"ThrowErrorData.hpp"

class		GeneratorPng
{
private:
  png_bytep	*_rowPointers;
  int		_width;
  int		_height;
  Png		*png;
  bool		_save;
public:
  GeneratorPng();
  ~GeneratorPng();
  GeneratorPng(const GeneratorPng &);
  GeneratorPng &operator=(const GeneratorPng &);
  void		setRowPointers();
  void		copyBlock(const int &, const int &, Png);
  void		save();
private:
  void		deletePoint(const int &, const int &, png_bytep *);
  void		firstDelete(const int &, const int &, png_bytep *);
};

#endif
