//
// MenuDataSound.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Wed Apr 27 09:52:36 2016 Maxime LECOQ
//

#ifndef		__MENUSOUND_HH__
# define	__MENUSOUND_HH__

# include	"AMenuData.hh"

class		MenuDataSound : public AMenuData
{
private:
public:
  MenuDataSound();
  ~MenuDataSound();
};

#endif
