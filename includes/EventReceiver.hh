//
// EventReceiver.hh for EventReceiver in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <andre_o@epitech.eu>
// 
// Started on  Wed Apr 27 18:31:24 2016 Thomas André
// Last update Wed Jun  1 18:49:48 2016 Maxime LECOQ
//

#ifndef EVENTRECEIVER_HH_
# define EVENTRECEIVER_HH_

# include	<map>
# include	<vector>
# include	<iostream>
# include	<thread>
# include	<chrono>
# include	"enumEvent.hh"
# include	"irr/irrlicht.h"
# include	"IObservable.hh"

class	EventReceiver : public irr::IEventReceiver, IObservable
{
private:
  bool				_keyIsDown[irr::KEY_KEY_CODES_COUNT];
  bool				_keyIsShift[irr::KEY_KEY_CODES_COUNT];
  bool				_keyIsControl[irr::KEY_KEY_CODES_COUNT];
  std::vector<irr::EKEY_CODE>   _vec;
  std::map<irr::EKEY_CODE, int> _serial;
  bool				_observe;
  IObserver			*_observer;
  EventReceiver(const EventReceiver &);
  EventReceiver&	operator=(const EventReceiver &);
public:
  EventReceiver();
  virtual ~EventReceiver();

  virtual bool	OnEvent(const irr::SEvent&);
  void		runObservable();
  void		setObserver(IObserver *);
  void		unSetObserver();
  void		resetAll();
private:
  bool		isKeyDown(irr::EKEY_CODE) const;
  bool		isKeyShift(irr::EKEY_CODE) const;
  bool		isKeyControl(irr::EKEY_CODE) const;
  void		reset(irr::EKEY_CODE);
};

#endif /* !EVENTRECEIVER_HH_ */
