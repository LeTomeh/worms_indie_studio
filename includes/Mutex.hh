//
// Mutex.hh for  in /home/lecoq_m/cpp_plazza/bootstrap
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed Apr 13 10:44:28 2016 Maxime LECOQ
// Last update Sat Apr 23 15:42:06 2016 Maxime LECOQ
//

#ifndef		__MUTEX_HH__
# define	__MUTEX_HH__

# include	<pthread.h>
# include	"IMutex.hh"

class	Mutex : public IMutex
{
 private:
  pthread_mutex_t	_mu;
 public:
  Mutex();
  Mutex(const Mutex &);
  Mutex &operator=(const Mutex &);
  ~Mutex();
public:
  void	lock();
  void	unlock();
  bool	trylock();
};

#endif// MUTEX_HH_
