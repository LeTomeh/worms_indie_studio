//
// ErrorClose.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Sun Apr 24 19:26:25 2016 Maxime LECOQ
//

#ifndef		__ERRORCLOSE_HH__
# define	__ERRORCLOSE_HH__

# include	"AError.hh"

class ErrorClose : public AError
{
public:
  ErrorClose(std::string const & err) : AError(err, "ErrorClose") {};
  virtual ~ErrorClose() throw() {}
};

#endif		/*__ERROR_HH__ */
