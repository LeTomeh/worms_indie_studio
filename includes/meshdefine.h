/*
** ImageDefine.h for  in /home/lecoq_m/cpp_indie_studio
** 
** Made by Maxime LECOQ
** Login   <lecoq_m@epitech.net>
** 
** Started on  Fri May 27 18:46:24 2016 Maxime LECOQ
** Last update Fri Jun  3 09:08:12 2016 Maxime LECOQ
*/

#ifndef		__OTHEREFINE_H__
# define	__OTHERDEFINE_H__

# define	ANIMBAZOOKA	"assets/mesh/bazooka.b3d"
# define	ANIMFIREPUNCH	"assets/mesh/firepunch.b3d"
# define	ANIMGRENADE	"assets/mesh/grenade.b3d"
# define	ANIMTELE	"assets/mesh/teleport.b3d"
# define	ANIMPISTOL	"assets/mesh/pistol.b3d"
# define	ANIMSHOTGUN	"assets/mesh/shotgun.b3d"

# define	TEBAZOOKA	"assets/mesh/bazooka.bmp"
# define	TEFIREPUNCH	"assets/mesh/firepunch.bmp"
# define	TEGRENADE	"assets/mesh/grenade.bmp"
# define	TETELE		"assets/mesh/teleport.bmp"
# define	TEPISTOL	"assets/mesh/pistol.bmp"
# define	TESHOTGUN	"assets/mesh/shotgun.bmp"

# define	SOUNDBAZOOKA	"assets/mesh/bazooka.wav"
# define	SOUNDFIREPUNCH	"assets/mesh/firepunch.wav"
# define	SOUNDGRENADE	"assets/mesh/grenade.wav"
# define	SOUNDTELE	"assets/mesh/teleport.wav"
# define	SOUNDPISTOL	"assets/mesh/pistol.wav"
# define	SOUNDSHOTGUN	"assets/mesh/shotgun.wav"

#endif
