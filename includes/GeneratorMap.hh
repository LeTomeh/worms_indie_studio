//
// GeneratorMap.hh for GeneratorMap.hh in /home/dufren_b/teck2/rendu/CPP/cpp_indie_studio/includes
// 
// Made by julien dufrene
// Login   <dufren_b@epitech.net>
// 
// Started on  Fri May 20 14:13:37 2016 julien dufrene
// Last update Sat May 28 14:32:10 2016 Maxime LECOQ
//

#ifndef		_GENERATORMAP_HH_
# define	_GENERATORMAP_HH_

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	<sys/types.h>
# include	<dirent.h>
# include	<ctime>
# include	"sounddefine.h"
# include	"Png.hh"
# include	"Convert.hpp"
# include	"enumEvent.hh"
# include	"CatchIt.hpp"
# include	"GeneratorData.hh"
# include	"GeneratorPng.hh"
# include	"ThrowErrorData.hpp"

class		GeneratorMap
{
public:
  typedef void		(GeneratorMap::*f)();
private:
  std::vector<std::string>	srcs;
  bool				_init;
  GeneratorData			_data;
  int				num_png;
  GeneratorPng			*png;
  int				posX;
  int				posY;
  std::map<int, f>		_exec;
  int				_last;
  int				_cpt;
  int				_emptyEv;
  std::clock_t			mclock;
public:
  GeneratorMap();
  ~GeneratorMap();
  GeneratorMap(const GeneratorMap &);
  GeneratorMap &operator=(const GeneratorMap &);
  void			manageEvent(const int &);
  void			resetInit();
  void			enterIn();
  void			setSound(const bool &);
  void			setEffect(const bool &);
private:
  void			checkExistence();
  void			checkSize();

  void			getSources();

  void			save();
  void			moveRight();
  void			moveLeft();
  void			moveUp();
  void			moveDown();
  void			nextObj();
  void			prevObj();
  void			quit();
};

#endif
