//
// Controller.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 16:05:43 2016 Maxime LECOQ
// Last update Sat Jun  4 00:59:53 2016 Maxime LECOQ
//

#ifndef		__CONTROLLER_HH__
# define	__CONTROLLER_HH__

# include	<iostream>
# include	<string>
# include	<list>
# include	<chrono>
# include	"IThread.hh"
# include	"AThread.hh"
# include	"AMenuData.hh"
# include	"CatchIt.hpp"
# include	"IrrlichtGUI.hh"
# include	"Model.hh"
# include	"View.hh"
# include       "EventReceiver.hh"
# include       "IObserver.hh"
# include	"GeneratorData.hh"
# include	"ErrorData.hh"

class		Controller : public IObserver
{
private:
  IrrlichtGUI			*_gui;
  IThread			*_th;
  IThread			*_ev;
  Model				*_model;
  EventReceiver                 *_event;
  std::list<int>                _list;
  View				*_view;
public:
  Controller();
  ~Controller();
private:
  Controller(const Controller &);
  Controller &operator=(const Controller &);
public:
  void		start();
  void		notifier(const int &);
  void		notifier(View *);
  int		getEvent();
};

void		*runCatchingEvent(void *);

#endif
