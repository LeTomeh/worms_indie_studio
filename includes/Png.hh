//
// Png.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:24:02 2016 Maxime LECOQ
// Last update Tue May 31 10:44:34 2016 Maxime LECOQ
//

#ifndef		__PNG_HH__
# define	__PNG_HH__

# include	<iostream>
# include       "png.h"
# include	"ErrorPng.hpp"
# include	"File.hh"

class			Png
{
private:
  int		_width;
  int		_height;
  png_byte	_colorType;
  png_byte	_bitDepth;
  int		_numberOfPasses;
  png_bytep	*_rowPointers;
  bool		_run;
  bool		_edit;
  bool		_save;
  std::string	_name;
public:
  Png(const std::string &);
  Png(const char *);
  ~Png();
  Png(const Png &);
  Png			&operator=(const Png &);
  void			save();
  bool			saveMap(const bool &);
  void			save(const std::string &);
  png_byte		*getPixels(const int &, const int &) const;
  int			getWidth() const;
  int			getHeight() const;
  png_bytep		*getRowPointers() const;
  void		        setRowPointers(png_bytep *);
  void			setPixels(const int &, const int &, png_byte *);
  void			putOff();
  void			putOn();
  bool			run() const;
  bool			edit() const;
  bool			isSave() const;
  void			setSave(const bool &);
  void			clean();
  const std::string	&getName() const;
  void			destroy(const int &, const int &, const std::string &);
private:
  void			construct(const std::string &);
};

#endif
