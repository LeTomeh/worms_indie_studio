//
// Rectangle.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun May  1 13:26:06 2016 Maxime LECOQ
// Last update Sun May  1 13:41:24 2016 Maxime LECOQ
//

#ifndef		__RECTANGLE_HPP__
# define	__RECTANGLE_HPP__

class		Rectangle
{
private:
  int		_x;
  int		_y;
  int		_x2;
  int		_y2;
  bool		_center;
public:
  Rectangle(const int &x, const int &y, const int &x2, const int &y2, const bool &center = true) : _x(x), _y(y), _x2(x2), _y2(y2), _center(center) {}
  ~Rectangle() {}
  Rectangle(const Rectangle &c) : _x(c._x), _y(c._y), _x2(c._x2), _y2(c._y2), _center(c._center) {}
  Rectangle &operator=(const Rectangle &c)
  {
    if (this != &c)
      {
	_x = c._x;
	_y = c._y;
	_x2 = c._x2;
	_y2 = c._y2;
	_center = c._center;
      }
    return (*this);
  }
  int		getX() const { return _x; }
  int		getY() const { return _y; }
  int		getXEnd() const { return _x2; }
  int		getYEnd() const { return _y2; }
  bool		getCenter() const { return _center; }
};

#endif
