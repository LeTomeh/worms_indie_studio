//
// Worms.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:00:11 2016 Maxime LECOQ
// Last update Sat May 28 13:53:06 2016 Maxime LECOQ
//

#ifndef		__ERRORDATA_HH__
# define	__ERRORDATA_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	"sounddefine.h"

class		ErrorData
{
  int		_i;
  std::string	_sound;
  bool		_soundOn;
  std::vector<std::string>	_error;
public:
  ErrorData();
  ~ErrorData();
  ErrorData(const ErrorData &);
  ErrorData &operator=(const ErrorData &);
  void	setI(const int &);
  int	getI() const;
  const std::string &getSound() const;
  bool	getSoundOn() const;
  void	setSoundOn(const bool &);
  void	setSound(const std::string &);
  void	setError(const std::vector<std::string> &);
  std::vector<std::string> getError() const;
};

#endif
