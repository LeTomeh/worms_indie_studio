//
// Projectile.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 18:14:15 2016 Maxime LECOQ
// Last update Tue May 31 23:32:24 2016 Maxime LECOQ
//

#ifndef		__PROJECTILE_HH__
# define	__PROJECTILE_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	"Position.hpp"

class		Projectile
{
private:
  Position	_pos;
  std::vector<Position>	_list;
  bool		_active;
  std::string	_sprite;
  public:
  Projectile();
  ~Projectile();
  Projectile(const Projectile &);
  Projectile &operator=(const Projectile &);
  void		active(const bool &);
  bool		getActive() const;
  void		setDirection(const Position &, const int &, const int &, const int &, const int &, const int &, const int &, const int &, const int &);
  void		affectPos();
  Position	getPosition() const;
  void		freeProjectile();
  void		setSprite(const std::string &);
  const std::string &getSprite() const;
  void		setPosition(const Position &);
};

#endif
