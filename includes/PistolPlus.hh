//
// PoisonPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Thu Jun  2 16:58:51 2016 Maxime LECOQ
//

#ifndef		__PISTOLPLUS_HH__
# define	__PISTOLPLUS_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"Team.hh"
# include	"ADropable.hh"

class		PistolPlus : public ADropable
{
public:
  PistolPlus();
  PistolPlus(const PistolPlus &);
  PistolPlus &operator=(const PistolPlus &);
  ~PistolPlus();
  void		doEffect(Player *, Team *);
};

#endif
