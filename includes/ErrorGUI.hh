//
// MenuGUI.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:14:42 2016 Maxime LECOQ
// Last update Sat May 28 09:48:58 2016 Maxime LECOQ
//

#ifndef		__ERRORGUI_HH__
# define	__ERRORGUI_HH__

# include       <iostream>
# include       <string>
# include       <list>
# include       <vector>
# include       "irr/irrlicht.h"
# include       "TextDisplayer.hh"
# include       "Element.hh"
# include       "Convert.hpp"
# include       "AMenuData.hh"
# include       "Rectangle.hpp"
# include       "Color.hpp"
# include	"enumEvent.hh"
# include	"AViewGUI.hh"
# include	"ImageDisplayer.hh"
# include	"Sound.hh"
# include	"ErrorData.hh"

class		ErrorGUI : public AViewGUI
{
private:
  ErrorData	*_data;
public:
  ErrorGUI(irr::IrrlichtDevice *, irr::video::IVideoDriver *, irr::scene::ISceneManager *, irr::gui::IGUIEnvironment *, TextDisplayer *, ImageDisplayer *, const int &, const int &, Sound *);
  ~ErrorGUI();
  ErrorGUI(const ErrorGUI &);
  ErrorGUI &operator=(const ErrorGUI &);
  void			error();
  void			setData(ErrorData *);
};

#endif
