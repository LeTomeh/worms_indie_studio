//
// IObserver.hh for  in /home/lecoq_m/basics_cpp
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 20:31:22 2016 Maxime LECOQ
// Last update Wed May 18 08:59:12 2016 Maxime LECOQ
//

#ifndef		__IOBSERVER_HH__
# define	__IOBSERVER_HH__

# include	"View.hh"

class		IObserver
{
public:
  virtual ~IObserver() {}
  virtual void		notifier(const int &) = 0;
  virtual void		notifier(View *) = 0;
};

#endif
