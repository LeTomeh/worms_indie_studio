//
// MenuDataHome.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Mon May  2 22:03:05 2016 Maxime LECOQ
//

#ifndef		__MENUSCORES_HH__
# define	__MENUSCORES_HH__

# include	"AMenuData.hh"

class		MenuDataScores : public AMenuData
{
private:
public:
  MenuDataScores();
  ~MenuDataScores();
};

#endif
