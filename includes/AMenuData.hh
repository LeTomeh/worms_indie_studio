//
// MenuData.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Sat Jun  4 01:09:35 2016 Maxime LECOQ
//

#ifndef		__AMENUDATA_HH__
# define	__AMENUDATA_HH__

# include	<vector>
# include	<map>
# include	<iostream>
# include	<string>
# include	"sounddefine.h"
# include	"Element.hh"
# include	"Vector.hh"
# include	"Printer.hpp"
# include	"Scores.hh"

class		AMenuData
{
protected:
  std::vector<Element>	_elem;
  std::string		_pageName;
  std::string		_back;
  std::string		_sound;
  int			_pos;
  bool			_warning;
  bool			_soundOn;
  std::vector<std::string> _effect;
  bool			_effectOn;
  bool			_reset;
public:
  AMenuData();
  virtual ~AMenuData();
  AMenuData(const AMenuData &);
  AMenuData &operator=(const AMenuData &);
  std::vector<Element>	getElement() const;
  const std::string	&getPageName() const;
  const std::string	&getBack() const;
  const std::string	&getSound() const;
  bool			soundIsOn() const;
  std::string		getEffect();
  bool			effectIsOn() const;
  void			setEffectOn(const bool &);
  void			setEffect(const std::string &);
  int			getPos() const;
  void			setPos(const int &);
  void			setPosVector(const int &, const int &);
  void			setController(std::map<std::string, int> , const std::vector<std::string> &);
  void			setValueNb(const int &, const int &);
  void			setValueNbMin(const int &, const int &);
  void			setScores(Scores);
  void			setWarning(const bool &);
  bool			getWarning() const;
  void			addElement(const Element &, const int &);
  void			deleteElement(const int &);
  void			setSound(const bool &);
  void			setResetKlang(const bool &);
  bool			getResetKlang() const;
};

#endif
