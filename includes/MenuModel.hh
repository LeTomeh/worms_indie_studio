//
// MenuModel.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 22:33:39 2016 Maxime LECOQ
// Last update Sat Jun  4 01:20:57 2016 Maxime LECOQ
//

#ifndef		__MENUMODEL_HH__
# define	__MENUMODEL_HH__

# include	<iostream>
# include	<string>
# include	<map>
# include	<vector>
# include	"sounddefine.h"
# include	"Data.hh"
# include	"CatchIt.hpp"
# include	"AMenuData.hh"
# include	"enumEvent.hh"
# include	"MenuDataFactory.hh"
# include	"Element.hh"
# include	"String.hpp"
# include	"Scores.hh"
# include	"Loader.hh"

class		MenuModel
{
  typedef void	(MenuModel::*f)(const int &);
  typedef void	(MenuModel::*f2)(const std::string &);
private:
  Data				_data;
  std::string			_location;
  std::map<std::string, f>	_execute;
  std::map<std::string, AMenuData *> _datas;
  std::vector<std::string>	_ctrls;
  bool				_init;
  std::map<std::string, f>	_sound;
  std::vector<int>		_control;
  bool				_save;
  std::map<std::string, f2>	_game;
  std::map<std::string, std::string> _links;
public:
  MenuModel();
  ~MenuModel();
  MenuModel(const MenuModel &);
  MenuModel &operator=(const MenuModel &);
  void		setData(Data);
  void		setScores(Scores);
  void		manageEvent(const int &);
  void		mayQuit() const;
  AMenuData	*getMenuData(const std::string &);
  Data		getData() const;
  void		resetInit();
  bool		getSave() const;
  void		putSoundEnd();
  void		setEffect(const std::string &);
private:
  void		home(const int &);
  void		homeOld(const int &);
  void		option(const int &);
  void		control(const int &);
  void		sound(const int &);
  void		credit(const int &);
  void		old(const int &);
  void		game(const int &);
  void		gameParam(const int &);
  void		scores(const int &);

  void		movePos(const int &);
  void		replacePos(const int &);
  void		catchLinks(const int &);
  void		catchButton(const int &);
  void		catchSelector(const int &);
  void		catchValue(const int &);
  void		checkValues(int &, const Element &);
  void		catchBacks(const int &);
  void		catchDelete(const int &);

  void		setMusic(const int &);
  void		setAmbiance(const int &);
  void		setEffect(const int &);

  void		setGameOption(Data);

  void		partyP(const std::string &);
  void		partyN(const std::string &);
  void		turnP(const std::string &);
  void		turnN(const std::string &);
};

#endif
