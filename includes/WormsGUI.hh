//
// MenuGUI.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:14:42 2016 Maxime LECOQ
// Last update Fri Jun  3 00:12:12 2016 Maxime LECOQ
//

#ifndef		__WORMSGUI_HH__
# define	__WORMSGUI_HH__

# include       <iostream>
# include       <string>
# include       <list>
# include	<map>
# include       <vector>
# include	"sounddefine.h"
# include       "irr/irrlicht.h"
# include       "TextDisplayer.hh"
# include       "Element.hh"
# include       "Convert.hpp"
# include       "AMenuData.hh"
# include       "Rectangle.hpp"
# include       "Color.hpp"
# include	"enumEvent.hh"
# include	"AViewGUI.hh"
# include	"ImageDisplayer.hh"
# include	"View.hh"
# include	"Sound.hh"
# include	"MeshPlayer.hh"

class		WormsGUI : public AViewGUI
{
public:
  typedef void	(WormsGUI::*f)();
private:
  View		*_view;
  std::map<int, irr::EKEY_CODE>	_cmd;
  std::map<std::string, f>	_displayMode;
  std::map<int, std::string>	_wind;
public:
  WormsGUI(irr::IrrlichtDevice *, irr::video::IVideoDriver *, irr::scene::ISceneManager *, irr::gui::IGUIEnvironment *, TextDisplayer *, ImageDisplayer *, const int &, const int &, Sound *);
  ~WormsGUI();
  WormsGUI(const WormsGUI &);
  WormsGUI &operator=(const WormsGUI &);
  void			setView(View *);
  void			map();
private:
  void			pause();
  void			normal();
  void			inventory();
};

#endif
