//
// MenuDataHome.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Wed Apr 27 09:52:29 2016 Maxime LECOQ
//

#ifndef		__MENUCTRL_HH__
# define	__MENUCTRL_HH__

# include	"AMenuData.hh"

class		MenuDataControl : public AMenuData
{
private:
public:
  MenuDataControl();
  ~MenuDataControl();
};

#endif
