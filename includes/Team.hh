//
// ITeam.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:19:35 2016 Maxime LECOQ
// Last update Sun Jun  5 04:03:30 2016 Maxime LECOQ
//

#ifndef		__TEAM_HH__
# define	__TEAM_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	"ITeam.hh"
# include	"IPlayer.hh"
# include	"Player.hh"
# include	"MemberData.hh"
# include	"Convert.hpp"
# include	"TeamNameMaker.hh"
# include       "ManageInventory.hh"

class		Team : public ITeam
{
private:
  std::string	_name;
  int		_mode;
  bool		_status;
  int		_lifeMax;
  std::vector<Player *>	_player;
  int		_pos;
  Inventory	*_inventory;
public:
  Team(const TeamNameMaker *, const int &, const int &, const std::vector<MemberData *> &);
  ~Team();
  Team(const Team &);
  Team &operator=(const Team &);
  std::vector<Player *> getPlayers() const;
  const std::string &getName() const;
  std::string	getLifes() const;
  bool		getStatus() const;
  void		nextPos();
  int		getPos() const;
  bool		deletePlayer(const int &);
  void		deleteMemberIn(const std::vector<Position> &);
  int		getScores() const;
  int		getMun(const std::string &);
  Inventory	*getInventory() const;
  void		setWeapon(const int &);
  void		checkPos();
  void		checkPlayers();
  void		checkArrow(const bool &);
  void		setSelectInv(const int &);
};

#endif
