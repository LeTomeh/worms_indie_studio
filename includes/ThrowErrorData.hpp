//
// Worms.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:00:11 2016 Maxime LECOQ
// Last update Sat May 28 13:40:33 2016 Maxime LECOQ
//

#ifndef		__THROWERRORDATA_HPP__
# define	__THROWERRORDATA_HPP__

# include	<iostream>
# include	<string>
# include	<vector>
# include	"Vector.hh"

class		ThrowErrorData
{
  std::vector<std::string>	_msg;
public:
  ThrowErrorData(const std::vector<std::string> &c) : _msg(c) {}
  ~ThrowErrorData() {}
  ThrowErrorData(const ThrowErrorData &c) : _msg(c._msg) {}
  ThrowErrorData &operator=(const ThrowErrorData &c)
  {
    if (this != &c)
      _msg = c._msg;
    return (*this);
  }
  std::vector<std::string> getMsg() const
  {
    return (_msg);
  }
};

#endif
