//
// Decorator.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Mon May  2 14:43:16 2016 Maxence Gaumont
// Last update Tue May  3 15:05:32 2016 Maxence Gaumont
//

#ifndef DECORATOR_HH_
#define DECORATOR_HH_

#include "AWeapon.hh"

class			Decorator : public AWeapon
{
protected:
  AWeapon		&m_base;

  Decorator(AWeapon &weapon, int damage = 0, int munitions = -1, const std::string &name = "");
  virtual ~Decorator() = 0;

public:
  AWeapon		*getBase() const
  {
    return (&m_base);
  }
};

#endif /* DECORATOR_HH_ */
