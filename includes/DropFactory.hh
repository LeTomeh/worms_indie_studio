//
// MenuDataFactory.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Thu Jun  2 16:54:20 2016 Maxime LECOQ
//

#ifndef		__DROPFAC_HH__
# define	__DROPFAC_HH__

# include	<iostream>
# include	<string>
# include	<map>
# include	<unistd.h>
# include	"IDropable.hh"
# include	"HealPlayer.hh"
# include	"HealTeam.hh"
# include	"PistolPlus.hh"
# include	"TeleportPlus.hh"
# include	"PoisonPlayer.hh"
# include	"PoisonTeam.hh"

class		DropFactory
{
public:
  typedef	IDropable	*(DropFactory::*f)();
private:
  std::map<std::string, f>	_facto;
  std::map<int, f>		_factoRand;
public:
  DropFactory();
  ~DropFactory();
  DropFactory(const DropFactory &);
  DropFactory &operator=(const DropFactory &);
  IDropable	*get(const std::string &);
  IDropable	*get();
private:
  IDropable	*makeHealP();
  IDropable	*makeHealT();
  IDropable	*makePoisonP();
  IDropable	*makePoisonT();
  IDropable	*makePistolP();
  IDropable	*makeTeleportP();
};

#endif
