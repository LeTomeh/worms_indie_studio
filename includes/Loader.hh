//
// Loader.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 17:15:42 2016 Maxime LECOQ
// Last update Sat May 21 17:16:55 2016 Maxime LECOQ
//

#ifndef		__LOADER_HH__
# define	__LOADER_HH__

# include	<map>
# include	<vector>
# include	<iostream>
# include	<string>
# include	"Data.hh"
# include	"File.hh"
# include	"Epur.hh"
# include	"Convert.hpp"
# include	"Vector.hh"
# include	"Printer.hpp"
# include	"AError.hh"
# include	"Scores.hh"
# include	"ErrorLoader.hpp"
# include	"Png.hh"
# include	"String.hpp"

class	Loader
{
 private:
  Data				_data;
  std::map<std::string, int>	_controlsDefault;
  std::vector<std::string>	_controls;
 public:
  Loader();
  ~Loader();
  Loader(const Loader &);
  Loader &operator=(const Loader &);
  Data				load();
  std::vector<std::string>	getControls() const;
  Scores			loadScores();
 private:
  void				manageSave();
  void				loadControl();
  void				checkControls(std::map<std::string, int> &);
};

#endif
