//
// ErrorRead.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Sat Apr 23 18:31:59 2016 Maxime Lecoq
//

#ifndef		__ERRORFONT_HH__
# define	__ERRORFONT_HH__

# include	"AError.hh"

class ErrorFont : public AError
{
public:
  ErrorFont(std::string const & err) : AError(err, "ErrorFont") {};
  virtual ~ErrorFont() throw() {}
};

#endif		/*__ERROR_HH__ */
