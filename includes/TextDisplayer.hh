//
// Font.hh for Font in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <andre_o@epitech.eu>
// 
// Started on  Thu Apr 28 19:04:39 2016 Thomas André
// Last update Mon May  2 16:41:33 2016 Maxime LECOQ
//

#ifndef TEXTDISPLAYER_HH_
# define TEXTDISPLAYER_HH_

# include	<string>
# include	<iostream>
# include	<vector>
# include	"irr/irrlicht.h"

class	TextDisplayer
{
  std::vector<irr::gui::IGUIFont *>_font;
  irr::gui::IGUISkin	*_skin;
  irr::gui::IGUIEnvironment	*_env;
  unsigned int			_save;
public:
  TextDisplayer(irr::IrrlichtDevice *);
  ~TextDisplayer();
  TextDisplayer(const TextDisplayer &);
  TextDisplayer&	operator=(const TextDisplayer &);
  const std::string	&getFont() const;
  void			addFont(const char *);
  void			addFont(const std::string &);
  void			print(const wchar_t *, const irr::core::rect<irr::s32> &, const bool &, const irr::video::SColor, const unsigned int & = 0);
};

#endif /* !FONT_HH_ */
