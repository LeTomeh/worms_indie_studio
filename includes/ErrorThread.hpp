//
// ErrorThread.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Wed Apr 20 13:54:45 2016 Maxime LECOQ
//

#ifndef		__ERRORTHREAD_HH__
# define	__ERRORTHREAD_HH__

# include	"AError.hh"

class ErrorThread : public AError
{
public:
  ErrorThread(std::string const & err) : AError(err, "ErrorThread") {};
  virtual ~ErrorThread() throw() {}
};

#endif		/*__ERROR_HH__ */
