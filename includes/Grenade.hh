//
// Grenade.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:24:50 2016 Maxence Gaumont
// Last update Sat May 28 16:06:17 2016 Maxime LECOQ
//

#ifndef GRENADE_HH_
#define GRENADE_HH_

#include "AThrownWeapon.hh"

class			Grenade : public AThrownWeapon
{
public:
  Grenade(int damage = 40, int munitions = -1, const std::string &name = "Grenade");
  virtual ~Grenade();

public:
  AWeapon	*clone() const;
  void		attack() const;
  void		proMode();
};

#endif /* GRENADE_HH_ */
