//
// String.hpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Apr 29 08:49:21 2016 Maxime LECOQ
// Last update Tue May 17 12:39:02 2016 Maxime LECOQ
//

#ifndef		__STRING_HPP__
# define	__STRING_HPP__

# include	<iostream>

class		String
{
public:
  String() {}
  ~String() {}
private:
  String(const String &c) { (void)c; }
  String &operator=(const String &c) { (void)c; return (*this); }
public:
  std::string	minimise(const std::string &s)
  {
    std::string	ret;
    int		i;

    i = 0;
    while (s[i])
      {
	if (s[i] >= 'A' && s[i] <= 'Z')
	  ret += (s[i] + 32);
	else
	  ret += s[i];
	i++;
      }
    return (ret);
  }
  std::string	maximise(const std::string &s)
  {
    std::string	ret;
    int		i;

    i = 0;
    while (s[i])
      {
	if (s[i] >= 'a' && s[i] <= 'z')
	  ret += (s[i] - 32);
	else
	  ret += s[i];
	i++;
      }
    return (ret);
  }
  bool	number(const std::string &s)
  {
    int		i;

    i = 0;
    while (s[i])
      {
	if (s[i] >= '0' && s[i] <= '9')
	  i++;
	else
	  return (false);
      }
    return (true);
  }
  bool	character(const std::string &s)
  {
    int		i;

    i = 0;
    while (s[i])
      {
	if ((s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z'))
	  i++;
	else
	  return (false);
      }
    return (true);
  }
  std::string	replace(const std::string &s, const char &c, const char &c2)
  {
    std::string ret;
    int		i;

    i = 0;
    while (s[i])
      {
	if (s[i] == c)
	  ret += c2;
	else
	  ret += s[i];
	i++;
      }
    return (ret);
  }
};

#endif
