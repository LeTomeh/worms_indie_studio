//
// AThrownWeapon.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 17:40:03 2016 Maxence Gaumont
// Last update Fri Jun  3 01:09:47 2016 Maxime LECOQ
//

#ifndef ATHROWNWEAPON_HH_
#define ATHROWNWEAPON_HH_

#include "AWeapon.hh"

class			AThrownWeapon : public AWeapon
{
public:
  AThrownWeapon(int, int, const std::string &, const std::string &, const std::string &, const std::string &);
  virtual ~AThrownWeapon() {}

public:
  virtual void		attack() const;
  virtual void		proMode() = 0;
};

#endif /* ATHROWNWEAPON_HH_ */
