//
// ManageWaterUp.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:48:37 2016 Maxime LECOQ
// Last update Thu Jun  2 20:05:41 2016 Maxime LECOQ
//

#ifndef		__MANAGEPLAYER_HH__
# define	__MANAGEPLAYER_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	<ctime>
# include	"View.hh"
# include	"Player.hh"
# include	"Map.hh"
# include	"Turn.hh"
# include	"Projectile.hh"
# include	"ManageDrop.hh"

class		ManagePlayer
{
public:
  typedef void          (ManagePlayer::*f)(Player *, Map *);
  typedef void          (ManagePlayer::*f2)(Player *, Map *, const int &, std::vector<Team *> &);
private:
  View		*_view;
  std::map<int, std::string>	_converter;
  std::map<std::string, f>	_dispatch;
  std::map<std::string, f>	_dispatch2;
  std::map<std::string, f2>	_weap;
  std::clock_t			_cl;
  std::string			_last;
  int				_cpt;
  bool				_turn;
  bool				_prior;
public:
  ManagePlayer(View *);
  ~ManagePlayer();
  ManagePlayer(const ManagePlayer &);
  ManagePlayer &operator=(const ManagePlayer &);
  void		setTouch(std::map<int, std::string>);
  void		playTurn(Player *, Map *, const int &, const int &, std::vector<Team *> &);
  bool		getTurn() const;
  bool		getPrior() const;
  void		playAnimation(Player *, Map *, std::vector<Team *> &, ManageDrop *);
private:
  int		getDamage(const int &, const int &, const int &, const int &, const int &);
  void		checkColisionMap(Map *, const int &, const int &, bool &);
  void		checkColisionPlayerIn(Player *, std::vector<Team *> &, const int &, const int &, Map *, bool &);
  void		checkColisionDrop(const Position &, ManageDrop *, bool &);
  Player	*getPlayer(const Position &, std::vector<Team *> &);
  void		kill(const int &, const int &, IWeapon *, std::vector<Team *> &, Map *);
  void		execute(Player *, Map *, const int &, std::vector<Team *> &);

  void		left(Player *, Map *);
  void		right(Player *, Map *);
  void		up(Player *, Map *);
  void		down(Player *, Map *);

  void		up2(Player *, Map *);
  void		down2(Player *, Map *);

  void		teleport(Player *, Map *, const int &, std::vector<Team *> &);
  void		shotgun(Player *, Map *, const int &, std::vector<Team *> &);
  void		pistol(Player *, Map *, const int &, std::vector<Team *> &);
  void		grenade(Player *, Map *, const int &, std::vector<Team *> &);
  void		bazooka(Player *, Map *, const int &, std::vector<Team *> &);
  void		firePunch(Player *, Map *, const int &, std::vector<Team *> &);
};

#endif
