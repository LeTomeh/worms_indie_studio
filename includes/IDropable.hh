//
// IDropable.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Fri Jun  3 14:00:17 2016 Maxime LECOQ
//

#ifndef		__IDROPABLE_HH__
# define	__IDROPABLE_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	"Team.hh"

class		IDropable
{
public:
  virtual			~IDropable() {};
  virtual const std::string	&getName() const = 0;
  virtual const std::string	&getDescription() const = 0;
  virtual void			doEffect(Player *, Team *) = 0;
  virtual const std::string	&getDesc() const = 0;
};

#endif
