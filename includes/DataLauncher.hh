//
// ManageLauncher.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue May  3 23:20:48 2016 Maxime LECOQ
// Last update Sat Jun  4 02:47:09 2016 Maxime LECOQ
//

#ifndef		__DATALAUNCHER_HH__
# define	__DATALAUNCHER_HH__

# include       <iostream>
# include       <string>
# include       <map>
# include       <vector>
# include       "Data.hh"
# include       "CatchIt.hpp"
# include       "AMenuData.hh"
# include       "enumEvent.hh"
# include       "Element.hh"
# include       "String.hpp"
# include	"TeamNameMaker.hh"
# include	"MemberData.hh"

class		DataLauncher
{
private:
  bool		_music;
  bool		_ambiance;
  bool		_effect;
  bool		_save;
  std::vector<std::string>	_ctrlList;
  std::map<std::string, int>	_ctrl;
  int		_worms;
  int		_mode;
  std::vector<TeamNameMaker *>	_team;
  std::vector<std::vector<MemberData *> > _mem;
  std::string	_back;
  int		_turn;
  int		_min;
  int		_sec;
  int		_water;
  int		_turnTimeN;
  int		_turnTimeP;
  int		_partyTimeN;
  int		_partyTimeP;
  std::vector<std::string> _drops;
  int		_decal;
public:
  DataLauncher();
  ~DataLauncher();
  DataLauncher(const DataLauncher &);
  DataLauncher	&operator=(const DataLauncher &);
  void		setMusic(const bool &);
  void		setAmbiance(const bool &);
  void		setEffect(const bool &);
  void		setSave(const bool &);
  void		setCtrlList(const std::vector<std::string> &);
  void		setCtrl(std::map<std::string, int>);
  void		setBack(const std::string &);
  void		setTurn(const int &);
  int		getTurn() const;
  const std::string &getBack() const;
  bool		getMusic() const;
  bool		getAmbiance() const;
  bool		getEffect() const;
  bool		getSave() const;
  std::vector<std::string>	getCtrlList() const;
  std::map<std::string, int>	getCtrl();
  void		setWormsTeamNb(const int &);
  void		setMode(const int &);
  int		getWormsTeamNb() const;
  int		getMode() const;
  void		setTeam(const std::vector<TeamNameMaker *> &);
  std::vector<TeamNameMaker *>	getTeam() const;
  std::vector<std::vector<MemberData *>	> getMemberData() const;
  void		setMemberData(const std::vector<std::vector<MemberData *> > &);
  void		setMin(const int &);
  void		setSec(const int &);
  int		getMin() const;
  int		getSec() const;
  void		setWater(const int &);
  int		getWater() const;
  int		getTurnTimeNormal() const;
  int		getTurnTimePro() const;
  int		getPartyTimeNormal() const;
  int		getPartyTimePro() const;
  void		setTurnTimeNormal(const int &);
  void		setTurnTimePro(const int &);
  void		setPartyTimeNormal(const int &);
  void		setPartyTimePro(const int &);
  void		setDrops(const std::vector<std::string> &);
  const std::vector<std::string> &getDrops() const;
  void		setDecal(const int &);
  int		getDecal() const;
};

#endif
