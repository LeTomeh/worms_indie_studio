//
// AError.hh for  in /home/lecoq_m/cpp_plazza
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon Apr 18 13:13:17 2016 Maxime LECOQ
// Last update Sun Apr 24 18:19:25 2016 Maxime LECOQ
//

#ifndef		__AERROR_HH__
# define	__AERROR_HH__

# include       <iostream>
# include       <cstdlib>
# include	"IError.hh"

class		AError : public IError, public std::exception
{
private:
  std::string	_err;
  std::string	_class;
public:
  AError(const std::string &, const std::string &);
  AError(const std::string &);
  AError(const AError &);
  AError			&operator=(const AError &);
  const char			*what() const throw();
  void				criticalError() const;
  void				notCriticalError() const;
  virtual ~AError() throw() {};
};

#endif
