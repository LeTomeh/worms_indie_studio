//
// ErrorRead.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Mon Apr 18 13:35:59 2016 Maxime LECOQ
//

#ifndef		__ERRORREAD_HH__
# define	__ERRORREAD_HH__

# include	"AError.hh"

class ErrorRead : public AError
{
public:
  ErrorRead(std::string const & err) : AError(err, "ErrorRead") {};
  virtual ~ErrorRead() throw() {}
};

#endif		/*__ERROR_HH__ */
