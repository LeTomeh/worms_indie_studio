//
// IWeapon.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:02:42 2016 Maxence Gaumont
// Last update Sat Jun  4 09:27:07 2016 Maxime LECOQ
//

#ifndef IWEAPON_HH_
# define IWEAPON_HH_

# include	<iostream>
# include	<string>
# include	"IStuff.hh"
# include	"imagedefine.h"
# include	"otherdefine.h"
# include	"meshdefine.h"

class			IWeapon : public IStuff<IWeapon>
{
public:
  virtual ~IWeapon() {}
  virtual void			attack() const = 0;
  virtual const std::string	&getName() const = 0;
  virtual int			getDamage() const = 0;
  virtual int			getMun() const = 0;
  virtual void			setMun(const int &) = 0;
  virtual void			proMode() = 0;
  virtual const std::string     &getPictureOff() const = 0;
  virtual const std::string     &getPictureOn() const = 0;
  virtual const std::string	&getDescription() const = 0;
  virtual void                  setDescription(const std::string &) = 0;
  virtual void                  setPictureOn(const std::string &) = 0;
  virtual void                  setPictureOff(const std::string &) = 0;
  virtual const std::string	&getLeftSprite() const = 0;
  virtual const std::string	&getRightSprite() const = 0;
  virtual void			setLeftSprite(const std::string &) = 0;
  virtual void			setRightSprite(const std::string &) = 0;
  virtual const std::string	&getBulletLeft() const = 0;
  virtual const std::string	&getBulletRight() const = 0;
  virtual void			setBulletLeft(const std::string &) = 0;
  virtual void			setBulletRight(const std::string &) = 0;
  virtual void			setPow(const std::string &) = 0;
  virtual const std::string	&getPow() const = 0;
  virtual void			setMesh(const std::string &) = 0;
  virtual void			setTexture(const std::string &) = 0;
  virtual void			setSound(const std::string &) = 0;
  virtual const std::string	&getMesh() const = 0;
  virtual const std::string	&getTexture() const = 0;
  virtual const std::string	&getMeshSound() const = 0;
  virtual void			setDamage(const int &) = 0;
};

#endif /* IWEAPON_HH_ */
