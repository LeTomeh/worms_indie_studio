//
// Interpreter.hpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun May 22 13:56:39 2016 Maxime LECOQ
// Last update Sun May 22 14:06:53 2016 Maxime LECOQ
//

#ifndef		__INTERPRETER_HPP__
# define	__INTERPRETER_HPP__

# include	<iostream>
# include	<string>
# include	<map>
# include	<vector>
# include	"ErrorFind.hpp"

template <typename X, Y>
class		Interpreter
{
private:
  bool		_what;
  std::map<X, Y>	_map;
  std::vector<X>	_src;
  std::vector<Y>	_dest;
public:
  Interpreter(std::map<X, Y> map) : _what(true), _map(map) {}
  Interpreter(const std::vector<X> &x, const std::vector<Y> &y) : _what(false), _src(x), _dest(y) {}
  Interpreter(const Interpreter &c) : _what(c._what), _map(c._map), _src(c._src), _dest(c._dest) {}
  ~Interpreter() {}
  Interpreter &operator=(const Interpreter &c)
  {
    if (this != &c)
      {
	_what = c._what;
	_map = c._map;
	_src = c._src;
	_dest = c._dest;
      }
    return (*this);
  }
  Y translate(const Y &sr)
  {
    size_t	i;
    size_t	size;

    if (_what == true)
      {
	if (_map.find(sr) != _map.end())
	  return (_map[sr]);
	else
	  throw ErrorFind("interprete doesn't find your request");
      }
    else
      {
	i = 0;
	size = _src.size();
	while (i < size)
	  {
	    if (_src[i] == sr)
	      {
		if (i < _dest.size())
		  return (_dest[i]);
		else
		  throw ErrorFind("interprete doesn't find your request");
	      }
	    i++;
	  }
	throw ErrorFind("interprete doesn't find your request");
      }
  }
}

#endif
