//
// ScopedLock.hh for  in /home/lecoq_m/basics_cpp
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 20:10:25 2016 Maxime LECOQ
// Last update Sun Apr 24 20:10:32 2016 Maxime LECOQ
//

#ifndef		SCOPEDLOCK_HH__
# define	SCOPEDLOCK_HH__

#include	<pthread.h>
#include	<iostream>
#include	"Mutex.hh"

class		ScopedLock
{
private:
  Mutex		_mu;
public:
  ScopedLock(Mutex &m);
  ~ScopedLock();
private:
  ScopedLock(const ScopedLock &);
  ScopedLock &operator=(const ScopedLock &);
};

#endif		// SCOPEDLOCK_HH__
