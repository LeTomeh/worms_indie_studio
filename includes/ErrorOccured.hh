//
// Worms.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:00:11 2016 Maxime LECOQ
// Last update Sat Jun  4 03:14:52 2016 Maxime LECOQ
//

#ifndef		__ERROROCCURED_HH__
# define	__ERROROCCURED_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	"CatchIt.hpp"
# include	"enumEvent.hh"
# include	"ErrorData.hh"
# include	"File.hh"

class		ErrorOccured
{
  bool			_init;
  ErrorData		_data;
public:
  ErrorOccured();
  ~ErrorOccured();
  ErrorOccured(const ErrorOccured &);
  ErrorOccured &operator=(const ErrorOccured &);
  void	manageEvent(const int &);
  void	resetInit();
  void	setI(const int &);
  void	setSound(const bool &);
  void	setError(const std::vector<std::string> &);
};

#endif
