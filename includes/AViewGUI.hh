//
// LauncherGUI.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:14:42 2016 Maxime LECOQ
// Last update Sat Jun  4 01:19:53 2016 Maxime LECOQ
//

#ifndef		__AVIEWGUI_HH__
# define	__AVIEWGUI_HH__

# include       <iostream>
# include       <string>
# include       <list>
# include       <vector>
# include       "irr/irrlicht.h"
# include       "TextDisplayer.hh"
# include       "Element.hh"
# include       "Convert.hpp"
# include       "AMenuData.hh"
# include       "Rectangle.hpp"
# include       "Color.hpp"
# include	"enumEvent.hh"
# include	"TeamNameMaker.hh"
# include	"ImageDisplayer.hh"
# include	"Sound.hh"

class		AViewGUI
{
protected:
  irr::IrrlichtDevice           *_window;
  irr::video::IVideoDriver      *_driver;
  irr::scene::ISceneManager     *_smgr;
  irr::gui::IGUIEnvironment     *_env;
  TextDisplayer                 *_text;
  ImageDisplayer		*_img;
  int                           _width;
  int                           _height;
  Sound				*_sound;			
public:
  AViewGUI(irr::IrrlichtDevice *, irr::video::IVideoDriver *, irr::scene::ISceneManager *, irr::gui::IGUIEnvironment *, TextDisplayer *, ImageDisplayer *, const int &, const int &, Sound *);
  virtual ~AViewGUI();
  AViewGUI(const AViewGUI &);
  AViewGUI &operator=(const AViewGUI &);
  void                          textModule(const std::string &, const Rectangle &, const Color &, const unsigned int &, const bool & = false);
  void                          textModule(const std::string &, const Rectangle &, const unsigned int &, const bool & = false);
  void                          msgBox(const std::vector<std::string> &);
};

#endif
