//
// MenuData.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Wed May 18 18:34:17 2016 Maxime LECOQ
//

#ifndef		__ELEMENT_HH__
# define	__ELEMENT_HH__

# include	<vector>
# include	<iostream>
# include	<string>
# include	"Scores.hh"

class		Element
{
public:
  enum	Type
    {
      LINK,
      CONTROLLER,
      SELECTOR,
      BUTTON,
      VALUE,
      SCORES,
      DELETE
    };
private:
  std::string			_value;
  Type				_type;
  std::vector<std::string>	_selector;
  int				_posSelector;
  int				_controllerValue;
  int				_nb;
  std::string			_link;
  int				_min;
  std::vector<std::string>	_msg;
  Scores			_scr;
public:
  Element();
  ~Element();
  Element(const Element &);
  Element &operator=(const Element &);
  const std::string		&getValue() const;
  Type				getType() const;
  std::vector<std::string>	getSelector() const;
  int				getPosSelector() const;
  int				getControllerValue() const;
  int				getValueNb() const;
  int				getValueMin() const;
  const std::string		&getLink() const;
  std::vector<std::string>	getMsg() const;
  Scores			&getScores();
  void				setValue(const std::string &);
  void				setType(const Type &);
  void				setSelector(const std::vector<std::string> &);
  void				setPosSelector(const int &);
  void				setControllerValue(const int &);
  void				setValueNb(const int &);
  void				setValueMin(const int &);
  void				setLink(const std::string &);
  void				setMsg(const std::vector<std::string> &);
  void				setScores(const Scores &);
};

#endif
