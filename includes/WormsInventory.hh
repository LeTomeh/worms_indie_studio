//
// WormsInventory.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun May 29 12:13:06 2016 Maxime LECOQ
// Last update Sun May 29 13:12:24 2016 Maxime LECOQ
//

#ifndef		__WORMSINVENTORY_HH__
# define	__WORMSINVENTORY_HH__

# include	<iostream>
# include	<string>
# include	<map>
# include	<vector>
# include	"sounddefine.h"
# include	"enumEvent.hh"
# include	"Team.hh"
# include	"CatchIt.hpp"
# include	"Inventory.hpp"
# include	"IWeapon.hh"

class		WormsInventory
{
public:
  typedef void (WormsInventory::*f)(const Team *);
private:
  std::string	_loc;
  std::map<int, f>	_dispatch;
  int		_pos;
  std::string	_effect;
public:
  WormsInventory();
  ~WormsInventory();
  WormsInventory(const WormsInventory &);
  WormsInventory &operator=(const WormsInventory &);
  void		manageEvent(const int &, const Team *);
  const std::string &getLoc() const;
  void		reset();
  int		getPos() const;
  const std::string       &getEffect() const;
private:
  void		select(const Team *);
  void		right(const Team *);
  void		left(const Team *);
};

#endif
