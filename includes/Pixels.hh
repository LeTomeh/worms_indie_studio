//
// Map.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:11:29 2016 Maxime LECOQ
// Last update Tue May 17 18:48:21 2016 Maxime LECOQ
//

#ifndef		__PIXELS_HH__
# define	__PIXELS_HH__

# include	"Map.hh"
# include	"Position.hpp"
# include	"Color.hpp"

class		Pixels
{
  public:
  Pixels();
  ~Pixels();
  Pixels(const Pixels &);
  Pixels &operator=(const Pixels &);
  void		change(Map *, const Position &, const Color &, int );
 };

void		*runSave(void *);

#endif
