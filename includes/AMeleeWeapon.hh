//
// AMeleeWeapon.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 17:40:03 2016 Maxence Gaumont
// Last update Fri Jun  3 01:10:36 2016 Maxime LECOQ
//

#ifndef AMELEEWEAPON_HH_
#define AMELEEWEAPON_HH_

#include "AWeapon.hh"

class			AMeleeWeapon : public AWeapon
{
public:
  AMeleeWeapon(int, int, const std::string &, const std::string &, const std::string &, const std::string &);
  virtual ~AMeleeWeapon() {}

public:
  void			attack() const;
  virtual void		proMode() = 0;
};

#endif /* AMELEEWEAPON_HH_ */

