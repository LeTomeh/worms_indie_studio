//
// Color.hpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun May  1 13:43:08 2016 Maxime LECOQ
// Last update Sun May  1 13:46:43 2016 Maxime LECOQ
//

#ifndef		__COLOR_HPP__
# define	__COLOR_HPP__

class		Color
{
private:
  int		_r;
  int		_g;
  int		_b;
public:
  Color(const int &r = 255, const int &g = 255, const int &b = 255) : _r(r), _g(g), _b(b) {}
  ~Color() {};
  Color(const Color &c) : _r(c._r), _g(c._g), _b(c._b) {}
  Color &operator=(const Color &c)
  {
    if (this != &c)
      {
	_r = c._r;
	_g = c._g;
	_b = c._b;
      }
    return (*this);
  }
  int		getRed() const { return _r; }
  int		getGreen() const { return _g; }
  int		getBlue() const { return _b; }
};

#endif
