//
// MenuGUI.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:14:42 2016 Maxime LECOQ
// Last update Fri May 27 18:50:24 2016 Maxime LECOQ
//

#ifndef		__MENUGUI_HH__
# define	__MENUGUI_HH__

# include       <iostream>
# include       <string>
# include       <list>
# include       <vector>
# include	"sounddefine.h"
# include       "irr/irrlicht.h"
# include       "TextDisplayer.hh"
# include       "Element.hh"
# include       "Convert.hpp"
# include       "AMenuData.hh"
# include       "Rectangle.hpp"
# include       "Color.hpp"
# include	"enumEvent.hh"
# include	"AViewGUI.hh"
# include	"ImageDisplayer.hh"
# include	"Sound.hh"

class		MenuGUI : public AViewGUI
{
public:
  typedef void          (MenuGUI::*f)(Element &, int &, const bool &);
private:
  std::map<Element::Type, f>    _menuElement;
  std::map<int, std::string>    _conv;
  AMenuData                     *_data;
public:
  MenuGUI(irr::IrrlichtDevice *, irr::video::IVideoDriver *, irr::scene::ISceneManager *, irr::gui::IGUIEnvironment *, TextDisplayer *, ImageDisplayer *, const int &, const int &, Sound *);
  ~MenuGUI();
  MenuGUI(const MenuGUI &);
  MenuGUI &operator=(const MenuGUI &);
  void			setMenuData(AMenuData *);
  void			menu();
private:
  void                          linkElement(Element &, int &, const bool &);
  void                          controllerElement(Element &, int &, const bool &);
  void                          selectorElement(Element &, int &, const bool &);
  void                          buttonElement(Element &, int &, const bool &);
  void                          valueElement(Element &, int &, const bool &);
  void                          scoresElement(Element &, int &, const bool &);
  void                          deleteElement(Element &, int &, const bool &);
};

#endif
