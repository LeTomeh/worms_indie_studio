//
// MenuDataFactory.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Fri May 20 22:24:32 2016 Maxime LECOQ
//

#ifndef		__MENUDATAFAC_HH__
# define	__MENUDATAFAC_HH__

# include	<iostream>
# include	<string>
# include	<map>
# include	"AMenuData.hh"
# include	"MenuDataHome.hh"
# include	"MenuDataHomeOld.hh"
# include	"MenuDataOption.hh"
# include	"MenuDataControl.hh"
# include	"MenuDataSound.hh"
# include	"MenuDataCredit.hh"
# include	"MenuDataGame.hh"
# include	"MenuDataGameParam.hh"
# include	"MenuDataScores.hh"

class		MenuDataFactory
{
public:
  typedef	AMenuData	*(MenuDataFactory::*f)();
private:
  std::map<std::string, f>	_facto;
public:
  MenuDataFactory();
  ~MenuDataFactory();
  MenuDataFactory(const MenuDataFactory &);
  MenuDataFactory &operator=(const MenuDataFactory &);
  AMenuData	*get(const std::string &);
private:
  AMenuData	*makeHome();
  AMenuData	*makeHomeOld();
  AMenuData	*makeOption();
  AMenuData	*makeControl();
  AMenuData	*makeSound();
  AMenuData	*makeCredit();
  AMenuData	*makeGame();
  AMenuData	*makeScores();
  AMenuData	*makeGameParam();
};

#endif
