//
// IObserver.hh for  in /home/lecoq_m/basics_cpp
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 20:31:22 2016 Maxime LECOQ
// Last update Sun May  1 09:04:48 2016 Maxime LECOQ
//

#ifndef		__IOBSERVABLE_HH__
# define	__IOBSERVABLE_HH__

# include	"IObserver.hh"

class		IObservable
{
public:
  virtual ~IObservable() {}
  virtual void		runObservable() = 0;
  virtual void		setObserver(IObserver *) = 0;
  virtual void		unSetObserver() = 0;
};

#endif
