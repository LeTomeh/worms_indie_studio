//
// MenuGUI.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:14:42 2016 Maxime LECOQ
// Last update Fri May 27 17:49:32 2016 Maxime LECOQ
//

#ifndef		__GENERATORGUI_HH__
# define	__GENERATORGUI_HH__

# include       <iostream>
# include       <string>
# include       <list>
# include       <vector>
# include       "irr/irrlicht.h"
# include       "TextDisplayer.hh"
# include       "Element.hh"
# include       "Convert.hpp"
# include       "AMenuData.hh"
# include       "Rectangle.hpp"
# include       "Color.hpp"
# include	"enumEvent.hh"
# include	"AViewGUI.hh"
# include	"ImageDisplayer.hh"
# include	"GeneratorData.hh"
# include	"Sound.hh"
# include	"GeneratorPng.hh"

class		GeneratorGUI : public AViewGUI
{
private:
  GeneratorData	*_data;
public:
  GeneratorGUI(irr::IrrlichtDevice *, irr::video::IVideoDriver *, irr::scene::ISceneManager *, irr::gui::IGUIEnvironment *, TextDisplayer *, ImageDisplayer *, const int &, const int &, Sound *);
  ~GeneratorGUI();
  GeneratorGUI(const GeneratorGUI &);
  GeneratorGUI &operator=(const GeneratorGUI &);
  void		generate();
  void		setGenerate(GeneratorData *);
  void		show();
};

#endif
