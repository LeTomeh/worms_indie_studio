//
// MenuDataSound.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Fri May 20 22:24:02 2016 Maxime LECOQ
//

#ifndef		__MENUGAMEPARAM_HH__
# define	__MENUGAMEPARAM_HH__

# include	"AMenuData.hh"

class		MenuDataGameParam : public AMenuData
{
private:
public:
  MenuDataGameParam();
  ~MenuDataGameParam();
};

#endif
