//
// ErrorRead.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Sat May 21 13:02:58 2016 Maxime LECOQ
//

#ifndef		__ERRORSOUND_HH__
# define	__ERRORSOUND_HH__

# include	"AError.hh"

class ErrorSound : public AError
{
public:
  ErrorSound(std::string const & err) : AError(err, "ErrorSound") {};
  virtual ~ErrorSound() throw() {}
};

#endif		/*__ERROR_HH__ */
