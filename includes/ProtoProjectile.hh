//
// ProtoProjectile.hh for oui in /home/thoma_f/rendu/cpp/cpp_indie_studio/includes
// 
// Made by François-Malo Thomas
// Login   <thoma_f@epitech.net>
// 
// Started on  Mon May  9 11:21:28 2016 François-Malo Thomas
// Last update Wed May 18 17:42:57 2016 thoma_f
//

#ifndef		__PROTOPROJECTILE_HH__

# define	__PROTOPROJECTILE_HH__

template <class T> class Prototype
{
public:
  virtual ~Prototype() {}
  virtual T* Clone() const = 0;
};

#endif		// ! __PROTOPROJECTILE_HH__
