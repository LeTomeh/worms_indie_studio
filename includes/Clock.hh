//
// Clock.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue May  3 13:05:44 2016 Maxime LECOQ
// Last update Sun May 22 09:29:03 2016 Maxime LECOQ
//

# ifndef	__CLOCK_HH__
# define	__CLOCK_HH__

# include	<vector>
# include	<iostream>
# include	<string>
# include	<map>
# include	<ctime>
# include	"CatchIt.hpp"
# include	"enumEvent.hh"

class	Clock
{
 private:
  std::clock_t	_clock;
  int		_pause;
  std::clock_t	_clockPause;
  bool		_isPause;
  int		_min;
  int		_sec;
  std::clock_t	_clockEnd;
  int		_stepEnd;
  int		_miss;
public:
  Clock();
  ~Clock();
  Clock(const Clock &);
  Clock &operator=(const Clock &);
  void		resetClock();
  void		checkClock(const int &, const std::clock_t &);
  int		getMin() const;
  int		getSec() const;
  void		setMiss(const int &);
  void		resetPause();
  void		startPause(const std::clock_t &);
  void		endPause(const std::clock_t &);
  int		getLvlWater() const;
  void		setClock(const std::clock_t &);
};

#endif
