//
// Shotgun.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:24:50 2016 Maxence Gaumont
// Last update Sat May 28 16:06:50 2016 Maxime LECOQ
//

#ifndef SHOTGUN_HH_
#define SHOTGUN_HH_

#include "AFireWeapon.hh"

class			Shotgun : public AFireWeapon
{
public:
  Shotgun(int damage = 50, int munitions = -1, const std::string &name = "Shotgun");
  virtual ~Shotgun();

public:
  AWeapon	*clone() const;
  void		attack() const;
  void		proMode();
};

#endif /* SHOTGUN_HH_ */
