//
// ManageInventory.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Sun May  1 20:31:27 2016 Maxence Gaumont
// Last update Wed May  4 15:26:46 2016 Maxence Gaumont
//

#ifndef MANAGEINVENTORY_HH_
#define MANAGEINVENTORY_HH_

#include "StuffFactory.hpp"
#include "Inventory.hpp"
#include "DecoratorStat.hh"

class			ManageInventory
{
public:
  ManageInventory();
  ~ManageInventory();

public:
  void			addDefWeapon();
  Inventory		getStuff() const;

private:
  Inventory		stuff;
};

#endif /* MANAGEINVENTORY_HH_ */
