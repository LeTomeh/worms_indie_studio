//
// Bazooka.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:24:50 2016 Maxence Gaumont
// Last update Sun May 29 20:11:32 2016 Maxime LECOQ
//

#ifndef BAZOOKA_HH_
#define BAZOOKA_HH_

#include "ALaunchWeapon.hh"

class			Bazooka : public ALaunchWeapon
{
public:
  Bazooka(int damage = 45, int munitions = -1, const std::string &name = "Bazooka");
  virtual ~Bazooka();

public:
  AWeapon		*clone() const;
  void			attack() const;
  void			proMode();
};

#endif /* BAZOOKA_HH_ */
