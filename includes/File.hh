//
// LogFile.cpp for  in /home/lecoq_m/cpp_plazza
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sat Apr 23 09:11:38 2016 Maxime LECOQ
// Last update Sat May 28 09:26:08 2016 Maxime LECOQ
//

#ifndef		__FILE_HH__
# define	__FILE_HH__

# include	<iostream>
# include	<string>
# include       <sstream>
# include       <fstream>
# include	"IFile.hh"
# include	"ErrorOpen.hpp"

class		File : public IFile
{
private:
  std::string	_file;
public:
  File();
  File(const std::string &);
  File(const File &);
  ~File();
  File &operator=(const File &);
  void	tryOpen();
  void	setFile(const std::string &);
  void	writeAppend(const std::string &);
  void	writeTronc(const std::string &);
  std::string read();
  void	destroy();
  void	clear();
};

#endif
