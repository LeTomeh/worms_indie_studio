//
// ProjectileFactory.hh for oui in /home/thoma_f/rendu/cpp/cpp_indie_studio/includes
// 
// Made by François-Malo Thomas
// Login   <thoma_f@epitech.net>
// 
// Started on  Mon May  9 11:25:14 2016 François-Malo Thomas
// Last update Mon May 30 12:00:38 2016 thoma_f
//

#ifndef		__PROJECTILEFACTORY_HH__

# define	__PROJECTILEFACTORY_HH__

# include	"ProtoProjectile.hh"
# include	"AProjectile.hh"
# include	<map>
# include	<string>
# include	<iostream>
# include	<iterator>

class		Rocket : public AProjectile
{
public:
  Rocket(float, float, float, float, float, float, const std::string &);
  ~Rocket() {};
  IProjectile * Clone() const;
  void		Appear();
};

class		ShotgunBullet : public AProjectile
{
public:
  ShotgunBullet(float, float, float, float, float, float, const std::string &);
  ~ShotgunBullet() {};
  IProjectile * Clone() const;
  void		Appear();
};

class		Grenade : public AProjectile
{
public:
  Grenade(float, float, float, float, float, float, const std::string &);
  ~Grenade() {};
  IProjectile * Clone() const;
  void		Appear();
};

class		ProjectileFactory
{
public:
  static std::map<std::string, IProjectile *> m_map;
  static void Register(const std::string & key, IProjectile * obj);
  IProjectile * Create(const std::string & key) const;
};

#endif		// ! __PROJECTILEFACTORY_HH__
