//
// Pause.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue May  3 13:05:44 2016 Maxime LECOQ
// Last update Thu May 19 10:14:14 2016 Maxime LECOQ
//

# ifndef	__PAUSE_HH__
# define	__PAUSE_HH__

# include	<vector>
# include	<iostream>
# include	<string>
# include	<map>
# include	"CatchIt.hpp"
# include	"enumEvent.hh"

class	Pause
{
 private:
  int	_pos;
 public:
  Pause();
  ~Pause();
  Pause(const Pause &);
  Pause &operator=(const Pause &);
  std::string   manageEvent(const int &);
  void	init();
  int	getPos() const;
  void	manageSelect(const int &);
  void	manageEnter(const int &, std::string &);
};

#endif
