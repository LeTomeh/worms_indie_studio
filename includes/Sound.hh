//
// MenuDataHome.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Sun May 29 08:28:04 2016 Maxime LECOQ
//

#ifndef		__SOUND_HH__
# define	__SOUND_HH__

# include	<iostream>
# include	"sounddefine.h"
# include	"irr/irrlicht.h"
# include	"ErrorSound.hpp"

class		Sound
{
private:
  irrklang::ISoundEngine	*_engine;
public:
  Sound();
  ~Sound();
  Sound(const Sound &);
  Sound &operator=(const Sound &);
  void	play(const std::string &, const bool & = false, const bool & = false);
  void	stopSound(const std::string &);
  void	stopAllSound();
  bool	isPlaying(const std::string &);
};

#endif
