//
// Data.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 17:15:42 2016 Maxime LECOQ
// Last update Thu Jun  2 23:16:07 2016 Maxime LECOQ
//

#ifndef		__DATA_HH__
# define	__DATA_HH__

# include	<iostream>
# include	<string>
# include	<map>
# include	<vector>
# include	"File.hh"
# include	"Convert.hpp"
# include	"Vector.hh"

class		Data
{
private:
  bool		_music;
  bool		_ambiance;
  bool		_effect;
  bool		_save;
  std::map<std::string, int>	_control;
  std::map<std::string, int>	_controlDefault;
  std::vector<std::string>	_controllerList;
  int		_tP;
  int		_tN;
  int		_pP;
  int		_pN;
  bool		_perso;
  std::string	_drop;
public:
  Data();
  ~Data();
  Data(const Data &);
  Data &operator=(const Data &);
  void		setMusic(const bool &);
  void		setAmbiance(const bool &);
  void		setEffect(const bool &);
  void		setSave(const bool &);
  void		setControl(const std::map<std::string, int> &);
  void		setControlDefault(const std::map<std::string, int> &);
  void		setControllerList(const std::vector<std::string> &);
  bool		getMusic() const;
  bool		getAmbiance() const;
  bool		getEffect() const;
  bool		getSave() const;
  std::map<std::string, int> getControl();
  std::map<std::string, int> getControlDefault();
  std::vector<std::string> getControllerList();
  void		setTurnP(const int &);
  void		setTurnN(const int &);
  void		setPartyN(const int &);
  void		setPartyP(const int &);
  int		getPartyP() const;
  int		getPartyN() const;
  int		getTurnP() const;
  int		getTurnN() const;
  void		setPerso(const bool &);
  bool		getPerso() const;
  const std::string	&getDrop() const;
  void		setDrop(const std::string &);
};

#endif
