//
// Font.hh for Font in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <andre_o@epitech.eu>
// 
// Started on  Thu Apr 28 19:04:39 2016 Thomas André
// Last update Fri May 27 10:36:32 2016 Maxime LECOQ
//

#ifndef IMAGEDISPLAYER_HH_
# define IMAGEDISPLAYER_HH_

# include	<string>
# include	<iostream>
# include	<vector>
# include	"irr/irrlicht.h"

class	ImageDisplayer
{
private:
  irr::video::IVideoDriver *_driver;
  irr::scene::ISceneManager     *_smgr;
public:
  ImageDisplayer(irr::video::IVideoDriver *, irr::scene::ISceneManager *);
  ~ImageDisplayer();
  ImageDisplayer(const ImageDisplayer &);
  ImageDisplayer&	operator=(const ImageDisplayer &);
  irr::video::ITexture		*print(const std::string &, const int &, const int &, const int &, const int &);
  irr::video::ITexture		*print(const char *, const int &, const int &, const int &, const int &);
  void				moveNode(irr::scene::ISceneNode *, const int &, const int &);
  void				destroy(const std::string &);
  void				destroy(const char *);
};

#endif /* !FONT_HH_ */
