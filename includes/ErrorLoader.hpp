//
// ErrorRead.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Tue May 17 13:36:18 2016 Maxime LECOQ
//

#ifndef		__ERRORLOADER_HH__
# define	__ERRORLOADER_HH__

# include	"AError.hh"

class ErrorLoader : public AError
{
public:
  ErrorLoader(std::string const & err) : AError(err, "ErrorLoader") {};
  virtual ~ErrorLoader() throw() {}
};

#endif		/*__ERROR_HH__ */
