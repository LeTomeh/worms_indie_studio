//
// IrrlichtGUI.hh for IrrlichtGUI in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <andre_o@epitech.eu>
// 
// Started on  Wed Apr 27 17:00:35 2016 Thomas André
// Last update Wed Jun  1 13:07:23 2016 Maxime LECOQ
//

#ifndef IRRLICHTGUI_HH_
# define IRRLICHTGUI_HH_

# include	<iostream>
# include	<string>
# include	<list>
# include	<vector>
# include	"irr/irrlicht.h"
# include	"TextDisplayer.hh"
# include	"EventReceiver.hh"
# include	"IGraphic.hh"
# include	"AMenuData.hh"
# include	"ErrorWindow.hpp"
# include	"Element.hh"
# include	"Convert.hpp"
# include	"Rectangle.hpp"
# include	"Color.hpp"
# include	"TeamNameMaker.hh"
# include	"MenuGUI.hh"
# include	"LauncherGUI.hh"
# include	"ImageDisplayer.hh"
# include	"WormsGUI.hh"
# include	"ErrorGUI.hh"
# include	"GeneratorGUI.hh"
# include	"GeneratorData.hh"
# include	"Sound.hh"
# include	"ErrorData.hh"

class		IrrlichtGUI : public IGraphic
{
public:
  typedef void		(IrrlichtGUI::*f)();
private:
  irr::IrrlichtDevice		*_window;
  irr::video::IVideoDriver	*_driver;
  irr::scene::ISceneManager	*_smgr;
  irr::gui::IGUIEnvironment	*_env;
  TextDisplayer			*_text;
  int				_width;
  int				_height;
  irr::core::dimension2d<irr::u32> _deskres;
  std::string			_status;
  std::map<std::string, f>	_execute;
  MenuGUI			*_menu;
  LauncherGUI			*_launch;
  WormsGUI			*_worms;
  ErrorGUI			*_err;
  ImageDisplayer		*_img;
  bool				_reset;
  bool				_quit;
  GeneratorGUI			*_generator;
  Sound				*_sound;
  IrrlichtGUI(const IrrlichtGUI &);
  IrrlichtGUI&	operator=(const IrrlichtGUI &);
public:
  IrrlichtGUI(EventReceiver *);
  ~IrrlichtGUI();
  
  void				refresh();
  void				clear();
  void				quit();
  void				setFont(const char *);
  bool				run() const;
  bool				getReset() const;
  bool				getQuit() const;
  void				runGUI();
  void				setMenu(AMenuData *);
  void				setTeamMaker(TeamNameMaker *);
  void				setMap(View *);
  void				setError(ErrorData *);
  void				putOff();
  void				setGenerate(GeneratorData *);
  void				sayByeBye();
  void				sayHello();
  void				stopSounds();
private:
  void				introduction();
  void				menu();
  void				launcher();
  void				map();
  void				error();
  void				drawMenu(const int &);
  void				drawContinue(const int &);
  void				generate();
};

#endif /* !IRRLICHTGUI_HH_ */
