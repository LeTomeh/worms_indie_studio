//
// ITeam.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:19:35 2016 Maxime LECOQ
// Last update Wed May  4 17:21:14 2016 Maxime LECOQ
//

#ifndef		__ITEAM_HH__
# define	__ITEAM_HH__

class		ITeam
{
public:
  virtual ~ITeam() {};
};

#endif
