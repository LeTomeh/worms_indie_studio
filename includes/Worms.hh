//
// Worms.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:00:11 2016 Maxime LECOQ
// Last update Fri Jun  3 00:12:01 2016 Maxime LECOQ
//

#ifndef		__WORMS_HH__
# define	__WORMS_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	<list>
# include	"sounddefine.h"
# include	"enumEvent.hh"
# include	"DataLauncher.hh"
# include	"CatchIt.hpp"
# include	"ITeam.hh"
# include	"Team.hh"
# include	"View.hh"
# include	"Map.hh"
# include	"Pixels.hh"
# include	"IObservable.hh"
# include	"IObserver.hh"
# include	"Pause.hh"
# include	"Turn.hh"
# include	"Clock.hh"
# include	"AThread.hh"
# include	"WormsInventory.hh"
# include	"ManageWaterUp.hh"
# include	"ManagePlayer.hh"
# include	"IA.hh"
# include	"Printer.hpp"
# include	"ManageDrop.hh"

class		Worms : public IObservable
{
public:
  typedef void	(Worms::*f)();
private:
  std::vector<Team *>	_team;
  std::vector<Team *>	_teamSave;
  View			*_view;
  Map			*_map;
  bool			_init;
  std::map<std::string, int>    _ctrl;
  std::vector<std::string>	_ctrlList;
  IObserver		*_observer;
  bool			_observe;
  bool			_run;
  bool			_game;
  std::string		_location;
  int			_event;
  std::map<int, std::string>    _converter;
  bool				_end;
  std::string			_loc;
  std::map<std::string, f>	_position;
  Pause			_pause;
  Turn			*_turn;
  int			_wormsPerTeam;
  int			_mode;
  std::string		_back;
  int			_time;
  int			_sel;
  Clock			_clock;
  int			_gameTime;
  AThread		_thMap;
  bool			_gameMap;
  AThread		_thCnf;
  bool			_gameCnf;
  AThread		_thMapDestroy;
  std::vector<std::string>	_begin;
  WormsInventory	_inventory;
  ManageWaterUp		*_water;
  ManagePlayer		*_managePlayer;
  IA			_ia;
  bool			_prior;
  std::map<std::string, int> _revConverter;
  int			_wind;
  ManageDrop		*_drop;
  AThread		_thDrop;
public:
  Worms();
  virtual ~Worms();
  Worms(const Worms &);
  Worms &operator=(const Worms &);
  void	setDataLauncher(DataLauncher);
  void	setEvent(const int &);
  void	openMap();
  void	closeMap();
  void	destroyMap();
  void	resetInit();
  void  runObservable();
  void  setObserver(IObserver *);
  void	unSetObserver();
  void	setRun(const bool &);
  void	setGame(const bool &);
  const std::string &getLocation() const;
  void	manageEvent();
  bool	getEnd() const;
  bool	getGameCnf() const;
  bool	getGameMap() const;
  void	setGameCnf(const bool &);
  void	setGameMap(const bool &);
  void	writeCnf();
  void	saveMap();
  void	destroyerMap();
  void	checkGravityPlayer(bool &);
  void	editScores();
  bool	wormsIn(const Position &);
  bool	wormsIn(const int &, const int &);
  void	writeDrop();
private:
  bool	checkEnd();

  void	executeNormal(bool &);

  void	passTurn();
  void	epurTeam();
  int	getTeam(const std::string &);
  bool	checkIntegrity(const std::string &);
  void	clockChecks(bool &);

  void	normal();
  void	pause();
  void	inventory();

  void	manageMenu();
  void	manageInventory();
  void	manageSwitch();
  void	managePause();
  void	manageNextTurn();

  void	deathPlayer(const int &, const int &);
};

void	*runMap(void *);
void	*runCnf(void *);
void	*runDestroy(void *);
void	*runDrop(void *);

#endif
