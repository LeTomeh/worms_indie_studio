//
// FirePunch.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:24:50 2016 Maxence Gaumont
// Last update Sat May 28 16:01:02 2016 Maxime LECOQ
//

#ifndef FIREPUNCH_HH_
#define FIREPUNCH_HH_

#include "AMeleeWeapon.hh"

class			FirePunch : public AMeleeWeapon
{
public:
  FirePunch(int damage = 30, int munitions = -1, const std::string &name = "FirePunch");
  virtual ~FirePunch();

public:
  AWeapon		*clone() const;
  void			attack() const;
  void			proMode();
};

#endif /* FIREPUNCH_HH_ */
