//
// ITeam.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:19:35 2016 Maxime LECOQ
// Last update Wed May  4 17:49:10 2016 Maxime LECOQ
//

#ifndef		__IPLAYER_HH__
# define	__IPLAYER_HH__

# include	<iostream>

class		IPlayer
{
public:
  virtual ~IPlayer() {};
  virtual void setLife(const int &) = 0;
  virtual int getLife() const = 0;
  virtual const std::string &getTeamName() const = 0;
};

#endif
