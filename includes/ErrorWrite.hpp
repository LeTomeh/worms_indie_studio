//
// ErrorWrite.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Sun Apr 24 19:04:00 2016 Maxime LECOQ
//

#ifndef		__ERRORWRITE_HH__
# define	__ERRORWRITE_HH__

# include	"AError.hh"

class ErrorWrite : public AError
{
public:
  ErrorWrite(std::string const & err) : AError(err, "ErrorWrite") {};
  virtual ~ErrorWrite() throw() {}
};

#endif		/*__ERROR_HH__ */
