//
// Model.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 10:30:02 2016 Maxime LECOQ
// Last update Sun May 29 08:31:16 2016 Maxime LECOQ
//

#ifndef		__MODEL_HH__
# define	__MODEL_HH__

# include       <iostream>
# include       <string>
# include       "Loader.hh"
# include       "MenuModel.hh"
# include       "AMenuData.hh"
# include       "CatchIt.hpp"
# include       "ManageLauncher.hh"
# include	"Worms.hh"
# include	"ErrorOccured.hh"
# include	"IObserver.hh"
# include	"IThread.hh"
# include	"AThread.hh"
# include	"GeneratorMap.hh"
# include	"ThrowErrorData.hpp"
# include	"Vector.hh"

class		Model
{
public:
  typedef void		(Model::*f)(const int &);
private:
  Loader			_load;
  std::string			_location;
  std::map<std::string, f>	_execute;
  MenuModel			_menuM;
  ManageLauncher		_launcherM;
  Worms				_gameM;
  ErrorOccured			_err;
  IThread			*_th;
  GeneratorMap			_gene;
public:
  Model();
  ~Model();
  Model(const Model &);
  Model &operator=(const Model &);
  void	execute(const int &);
  void	setObserver(IObserver *);
  bool	getEffect() const;
private:
  void          game(const int &);
  void          menu(const int &);
  void          gameLoader(const int &);
  void		error(const int &);
  void		generate(const int &);
};

void		*runGame(void *arg);

#endif
