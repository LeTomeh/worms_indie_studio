//
// View.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May  5 15:32:39 2016 Maxime LECOQ
// Last update Sat Jun  4 08:32:22 2016 Maxime LECOQ
//

#ifndef		__VIEW_HH__
# define	__VIEW_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	<ctime>
# include	"Team.hh"
# include	"Inventory.hpp"
# include	"Projectile.hh"
# include	"Drop.hh"

class		View
{
  std::string	_back;
  std::string	_map;
  std::vector<Team *>	_team;
  std::map<std::string, int>    _ctrl;
  std::vector<std::string>      _ctrlList;
  int				_posPause;
  std::vector<std::string>	_pause;
  std::string			_location;
  std::string			_data;
  std::string			_teamTurn;
  int				_time;
  int				_min;
  int				_sec;
  int				_waterLvl;
  int				_waterMin;
  std::string			_clock;
  std::string			_clockAlert;
  std::string			_waves;
  std::string			_pauseB;
  bool				_edited;
  std::string			_sound;
  bool				_soundOn;
  std::vector<std::string>	_effect;
  bool				_effectOn;
  Inventory			*_inv;
  int				_invPos;
  Projectile			*_pro;
  bool				_editIt;
  int				_wind;
  std::vector<Drop>		_drops;
  bool				_meshStat;
  std::string			_meshName;
  std::string			_meshTexture;
  std::string			_meshSound;
  std::string			_desc;
  std::clock_t			_clockDesc;
public:
  View();
  ~View();
  View(const View &);
  View &operator=(const View &);
  void	setBack(const std::string &);
  void	setMap(const std::string &);
  void	setTeam(const std::vector<Team *> &);
  std::vector<Team *>	getTeam() const;
  const std::string &getBack() const;
  const std::string &getMap() const;
  void		setCtrl(std::map<std::string, int>);
  void		setCtrlList(const std::vector<std::string> &);
  std::map<std::string, int>	getCtrl();
  std::vector<std::string>	getCtrlList() const;
  void	setPosPause(const int &);
  int	getPosPause() const;
  std::vector<std::string>	getPause() const;
  void				setLocation(const std::string &);
  const std::string		&getLocation() const;
  void				setData(const std::string &);
  const std::string		&getData() const;
  const std::string		&getTeamTurn() const;
  void				setTeamTurn(const std::string &);
  void				setTime(const int &);
  int				getTime() const;
  int				getMin() const;
  int				getSec() const;
  void				setMin(const int &);
  void				setSec(const int &);
  int				getWaterLvl() const;
  void				setWaterLvl(const int &);
  int				getWaterMin() const;
  void				setWaterMin(const int &);
  const std::string		&getClock() const;
  const std::string		&getClockAlert() const;
  const std::string		&getPauseB() const;
  const std::string		&getWaves() const;
  bool				edited() const;
  void				setEdited(const bool &);
  const std::string		&getSound() const;
  bool				soundIsOn() const;
  std::string			getEffect();
  bool				effectIsOn() const;
  void				setEffectOn(const bool &);
  void				setEffect(const std::string &);
  void				setSound(const bool &);
  void				setInventory(Inventory *);
  Inventory			*getInventory() const;
  int				getInvPos() const;
  void				setInvPos(const int &);
  void				setProjectile(Projectile *);
  Projectile			*getProjectile() const;
  bool				getEditIt() const;
  void				setEditIt(const bool &);
  void				setWind(const int &);
  int				getWind() const;
  void				setDrop(const std::vector<Drop> &);
  const std::vector<Drop>	&getDrop() const;
  void				setMeshStatus(const bool &);
  void				setMeshName(const std::string &);
  void				setMeshTexture(const std::string &);
  void				setMeshSound(const std::string &);
  bool				getMeshStatus() const;
  const std::string		&getMeshName() const;
  const std::string		&getMeshTexture() const;
  const std::string		&getMeshSound() const;
  const std::string		&getDesc();
  void				setDesc(const std::string &);
};

#endif
