//
// ManageWaterUp.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:48:37 2016 Maxime LECOQ
// Last update Mon May 30 10:02:40 2016 Maxime LECOQ
//

#ifndef		__MANAGEWATERUP_HH__
# define	__MANAGEWATERUP_HH__

# include	<iostream>
# include	<string>
# include	<vector>
# include	<map>
# include	"View.hh"
# include	"Team.hh"
# include	"Player.hh"
# include	"Map.hh"
# include	"Turn.hh"

class		ManageWaterUp
{
private:
  View		*_view;
  Map		*_map;
  Turn		*_turn;
  int		_time;
public:
  ManageWaterUp(View *);
  ~ManageWaterUp();
  ManageWaterUp(const ManageWaterUp &);
  ManageWaterUp &operator=(const ManageWaterUp &);
  void		setter(Map *, Turn *, const int &);
  void		checkWaterDeath(std::vector<Team *> &);
private:
  void		deathPlayer(const int &, const int &, std::vector<Team *> &);
};

#endif
