//
// Drop.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:09:58 2016 Maxime LECOQ
// Last update Thu Jun  2 23:31:54 2016 Maxime LECOQ
//

#ifndef		__MANAGEDROP_HH__
# define	__MANAGEDROP_HH__

# include	<string>
# include	<iostream>
# include	<vector>
# include	<ctime>
# include	"Team.hh"
# include	"Drop.hh"
# include	"DropFactory.hh"
# include	"Map.hh"
# include	"View.hh"
# include	"Convert.hpp"

class		ManageDrop
{
private:
  std::vector<Drop>	_drops;
  DropFactory		_fac;
  std::clock_t		_cl;
  std::clock_t		_cl2;
  View			*_view;
  size_t		_cpt;
public:
  ManageDrop(View *);
  ManageDrop(const ManageDrop &);
  ManageDrop &operator=(const ManageDrop &);
  ~ManageDrop();
  void		newDrop(const int &);
  void		addDrop(const std::string &, const Position &);
  void		askDrop(const int &, const size_t &);
  const std::vector<Drop>	&getDrops() const;
  void		reset();
  void		gravity(Map *, bool &);
  void		take(std::vector<Team *> &);
  bool		checkIn(const Position &);
  void		write();
  void		checkPos(Map *);
};

#endif
