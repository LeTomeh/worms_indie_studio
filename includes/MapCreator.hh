//
// Map.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:11:29 2016 Maxime LECOQ
// Last update Sat May 28 13:40:23 2016 Maxime LECOQ
//

#ifndef		__MAPCREATOR_HH__
# define	__MAPCREATOR_HH__

# include	<iostream>
# include	<vector>
# include	<stdlib.h>
# include	<time.h>
# include	<sys/time.h>
# include	"Png.hh"
# include	"File.hh"
# include	"CatchIt.hpp"
# include	"AError.hh"
# include	"png.h"
# include	"ThrowErrorData.hpp"
# include	"Vector.hh"

class		MapCreator
{
private:
  png_bytep	*_rowPointers;
  Png		*png;
  int		 _width;
  int		_height;
  std::vector<Png> _elem;
  std::string	_link;
public:
  MapCreator();
  ~MapCreator();
  MapCreator(const MapCreator &);
  MapCreator &operator=(const MapCreator &);
  void		setRowPointers();
  void		copyBlock(const int &, const int &, Png *);
  void		copyBlock(const int &, const int &, Png);
private:
  void		makeGround();
  void		makeVolatile();
  void		loadPng(const std::string &);
  void		deletePoint(const int &, const int &, png_bytep *);
  void		firstDelete(const int &, const int &, png_bytep *);
};

#endif
