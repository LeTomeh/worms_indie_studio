//
// ManageLauncher.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue May  3 23:20:48 2016 Maxime LECOQ
// Last update Sat May 28 10:02:25 2016 Maxime LECOQ
//

#ifndef		__MANAGELAUNCHER_HH__
# define	__MANAGELAUNCHER_HH__

# include       <iostream>
# include       <string>
# include       <map>
# include       <vector>
# include	"sounddefine.h"
# include       "Data.hh"
# include       "CatchIt.hpp"
# include       "AMenuData.hh"
# include       "enumEvent.hh"
# include       "Element.hh"
# include       "String.hpp"
# include	"DataLauncher.hh"
# include	"TeamNameMaker.hh"
# include	"MemberData.hh"
# include	"Position.hpp"
# include	"MapCreator.hh"
# include	"Map.hh"
# include	"Vector.hh"
# include	"ErrorRead.hpp"
# include	"ThrowErrorData.hpp"

class		ManageLauncher
{
private:
  DataLauncher			_launch;
  std::vector<TeamNameMaker *>	_team;
  std::vector<TeamNameMaker *>	_teamIA;
  bool				_save;
  int				_posX;
  bool				_init;
  int				_posY;
  std::map<int, std::vector<std::string> > _msg;
  std::map<int, char>		_touch;
  std::vector<TeamNameMaker *>	_teamList;
  MapCreator			*_creat;
  bool				_map;
public:
  ManageLauncher();
  ~ManageLauncher();
  ManageLauncher(const ManageLauncher &);
  ManageLauncher	&operator=(const ManageLauncher &);
  void		setData(const Data &);
  void		setControl(AMenuData *);
  void		setGameParameter(AMenuData *);
  bool		getSave() const;
  void		manageEvent(const int &);
  void		reset();
  void		readData();
  void		validationTeam();
  void		loadMap();
  DataLauncher	getDataLauncher() const;
  void		setSave(const bool &);
  size_t	getTeamSize() const;
private:
  void		listPosition(std::vector<Position> &);
  void		catchSelectX(const int &);
  void		catchSelectY(const int &);
  void		catchReturn(const int &);
  void		catchRaccourci(const int &);
  void		catchCharacter(const int &);
  void		checkIntegrity();
  bool		epurVec(std::vector<Position> &, const int &);
  void		setWormsTeamNb(const std::string &);
  void		setMode(const std::string &);
  void		setBack(const std::string &);
  void		setTurn(const std::string &);
  void		setTime(const std::string &);
  void		setWater(const std::string &);

  void		copyPersonalMap();
};

#endif
