//
// MenuDataHome.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:20:12 2016 Maxime LECOQ
// Last update Wed Apr 27 09:51:38 2016 Maxime LECOQ
//

#ifndef		__MENUHOMEO_HH__
# define	__MENUHOMEO_HH__

# include	"AMenuData.hh"

class		MenuDataHomeOld : public AMenuData
{
private:
public:
  MenuDataHomeOld();
  ~MenuDataHomeOld();
};

#endif
