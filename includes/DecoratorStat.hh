//
// DecoratorStat.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Mon May  2 14:50:46 2016 Maxence Gaumont
// Last update Sat May 28 18:51:52 2016 Maxime LECOQ
//

#ifndef DECORATORSTAT_HH_
#define DECORATORSTAT_HH_

#include "Decorator.hh"
#include "Bazooka.hh"

class			DecoratorStat : public Decorator
{
public:
  int			getDamage() const;
  int			getMun() const;
  const std::string	&getName() const;
  virtual void		attack() const;
  virtual void		proMode();
  virtual IWeapon	*clone() const;
  DecoratorStat(AWeapon &weapon, int damage = 0, int munitions = 0, const std::string &name = "");
  ~DecoratorStat() {}
};

#endif /* DECORATORSTAT_HH_ */
