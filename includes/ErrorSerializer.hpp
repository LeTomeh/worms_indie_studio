//
// ErrorSerializer.hh for  in /home/lecoq_m/cpp_arcade
// 
// Made by Maxime Lecoq
// Login   <lecoq_m@epitech.net>
// 
// Started on  Fri Mar 18 09:35:48 2016 Maxime Lecoq
// Last update Sun Apr 24 20:43:58 2016 Maxime LECOQ
//

#ifndef		__ERRORSERIALIZER_HH__
# define	__ERRORSERIALIZER_HH__

# include	"AError.hh"

class ErrorSerializer : public AError
{
public:
  ErrorSerializer(std::string const & err) : AError(err, "ErrorSerializer") {};
  virtual ~ErrorSerializer() throw() {}
};

#endif		/*__ERROR_HH__ */
