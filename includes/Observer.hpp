//
// Observer.hpp for  in /home/lecoq_m/basics_cpp
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun Apr 24 20:12:53 2016 Maxime LECOQ
// Last update Sun Apr 24 21:19:41 2016 Maxime LECOQ
//

#ifndef		__OBSERVER_HH__
# define	__OBSERVER_HH__

# include	"IObserver.hh"

template<class T>
class		Observer : public IObserver
{
private:
  T		_class;
public:
  Observer(const T &cl) : _class(cl) {}
  ~Observer() {}
private:
  Observer(const Observer &c) : _class(c._class) {}
  Observer	&operator=(const Observer &c)
  {
    if (this != &c)
      _class = c._class;
    return (*this);
  }
public:
  bool		isNotifiable() const
  {
    return (_class.isNotifiable());
  }
};

#endif
