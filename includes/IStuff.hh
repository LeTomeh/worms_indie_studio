//
// IStuff.hh for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:15:12 2016 Maxence Gaumont
// Last update Wed Apr 27 12:12:56 2016 Maxence Gaumont
//

#ifndef ISTUFF_HH_
#define ISTUFF_HH_

template <class T>
class			IStuff
{
public:
  virtual ~IStuff() {}
  virtual T*		clone() const = 0;
};

#endif /* ISTUFF_HH_ */
