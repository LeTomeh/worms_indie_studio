//
// Model.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 10:33:27 2016 Maxime LECOQ
// Last update Sat Jun  4 05:38:47 2016 Maxime LECOQ
//

#include	"Model.hh"
 
Model::Model()
{
  _location = "menu";
  _execute["menu"] = &Model::menu;
  _execute["gameLoader"] = &Model::gameLoader;
  _execute["game"] = &Model::game;
  _execute["error"] = &Model::error;
  _execute["generate"] = &Model::generate;
  _menuM.setData(_load.load());
  _menuM.setScores(_load.loadScores());
  _th = new AThread;
  try
    {
      _th->run(runGame, &_gameM);
    }
  catch (AError const &e)
    {
      e.criticalError();
    }
}

Model::~Model()
{
  _gameM.setGame(false);
  _gameM.setRun(false);
  try
    {
      _th->waitDeath();
    }
  catch (AError const &e) {}
  _gameM.unSetObserver();
  delete (_th);
}

Model::Model(const Model &cop) : _load(cop._load), _location(cop._location), _execute(cop._execute), _menuM(cop._menuM), _launcherM(cop._launcherM), _err(cop._err), _th(cop._th), _gene(cop._gene) {}

Model	&Model::operator=(const Model &cop)
{
  if (this != &cop)
    {
      _load = cop._load;
      _location = cop._location;
      _execute = cop._execute;
      _menuM = cop._menuM;
      _launcherM = cop._launcherM;
      _gene = cop._gene;
      _err = cop._err;
      _th = cop._th;
    }
  return (*this);
}

void		Model::execute(const int &ev)
{
  if (_execute.find(_location) != _execute.end())
    (this->*_execute[_location])(ev);
}

void		Model::game(const int &ev)
{
  std::string	tmp;

  _gameM.setEvent(ev);
  _gameM.manageEvent();
  tmp = _gameM.getLocation();
  if (tmp == "menu")
    {
      _gameM.setGame(false);
      _gameM.setEvent(0);
      _gameM.closeMap();
      if (_gameM.getEnd() == true)
	{
	  _gameM.editScores();
	  _gameM.destroyMap();
	}
      _menuM.setScores(_load.loadScores());
      _menuM.setData(_load.load());
      _location = "menu";
      _menuM.resetInit();
      _gameM.resetInit();
      if (_gameM.getEnd() == true)
	_menuM.putSoundEnd();
      else
	_menuM.setEffect(SEL);
    }
  else
    if (tmp != "game")
      throw CatchIt<bool> (false);
}

void		Model::gameLoader(const int &ev)
{
  try
    {
      _launcherM.manageEvent(ev);
    }
  catch (CatchIt<std::string> const &b)
    {
      if (b.getIt() != "quit" && b.getIt() != "error")
	{
	  try
	    {
	      _location = b.getIt();
	      if (_location == "game")
		{
		  _launcherM.validationTeam();
		  _launcherM.loadMap();
		  _launcherM.readData();
		  _gameM.setDataLauncher(_launcherM.getDataLauncher());
		  _gameM.openMap();
		  _gameM.setGame(true);
		}
	      else
		{
		  _menuM.resetInit();
		  _menuM.manageEvent(0);
		}
	    }
	  catch (ThrowErrorData const &s)
	    {
	      _location = "error";
	      _err.resetInit();
	      _err.setSound(_menuM.getData().getEffect());
	      _err.setError(s.getMsg());
	    }
	}
      else
	if (b.getIt() == "quit")
	  throw CatchIt<bool> (false);
    }
  catch (ThrowErrorData const &s)
    {
      _location = "error";
      _err.resetInit();
      _err.setSound(_menuM.getData().getEffect());
      _err.setError(s.getMsg());
    }
}

void		Model::menu(const int &ev)
{
  try
    {
      _menuM.manageEvent(ev);
    }
  catch (CatchIt<bool> const &b)
    {
      if (b.getIt() == true)
        {
	  try
	    {
	      _launcherM.reset();
	      _launcherM.setData(_load.load());
	      _launcherM.setSave(_menuM.getSave());
	      _launcherM.setControl(_menuM.getMenuData("control"));
	      _launcherM.setGameParameter(_menuM.getMenuData("game"));
	      if (_launcherM.getSave() == true)
		{
		  _location = "game";
		  _launcherM.loadMap();
		  _launcherM.readData();
		  _gameM.setDataLauncher(_launcherM.getDataLauncher());
		  _gameM.openMap();
		  _gameM.setGame(true);
		}
	      else
		{
		  _location = "gameLoader";
		  gameLoader(0);
		}
	      _menuM.resetInit();
	    }
	  catch (ThrowErrorData const &s)
	    {
	      _location = "error";
	      _err.resetInit();
	      _err.setSound(_menuM.getData().getEffect());
	      _err.setError(s.getMsg());
	    }
        }
      else
        throw CatchIt<bool> (false);
    }
  catch (CatchIt<std::string> const &s)
    {
      if (s.getIt() == "generator")
	{
	  _gene.resetInit();
	  try
	    {
	      _gene.enterIn();
	      _gene.setSound(_menuM.getData().getMusic());
	      _gene.setEffect(_menuM.getData().getEffect());
	      _location = "generate";
	    }
	  catch (ThrowErrorData const  &e)
	    {
	      _location = "error";
	      _err.resetInit();
	      _err.setSound(_menuM.getData().getEffect());
	      _err.setError(e.getMsg());
	    }
	}
    }
}

void			Model::error(const int &ev)
{
  try
    {
      _err.manageEvent(ev);
    }
  catch (CatchIt<bool> const &b)
    {
      if (b.getIt() == false)
	throw CatchIt<bool> (false);
      else
	{
	  _menuM.setData(_load.load());
          _menuM.setScores(_load.loadScores());
	  _menuM.resetInit();
	  _location = "menu";
	}
    }
}

void		Model::setObserver(IObserver *ob)
{
  _gameM.setObserver(ob);
}

void		Model::generate(const int &ev)
{
  try
    {
      _gene.manageEvent(ev);
    }
  catch (CatchIt<bool> const &b)
    {
      if (b.getIt() == false)
	throw CatchIt<bool> (false);
      else
	{
	  _gene.resetInit();
	  _menuM.setData(_load.load());
	  _location = "menu";
	  _menuM.resetInit();
	  if (_menuM.getData().getPerso() == true)
	    _menuM.setEffect(SAVE);
	}
    }
  catch (ThrowErrorData const &s)
    {
      _location = "error";
      _err.resetInit();
      _err.setSound(_menuM.getData().getEffect());
      _err.setError(s.getMsg());
    }
}

bool		Model::getEffect() const
{
  return (_menuM.getData().getEffect());
}
