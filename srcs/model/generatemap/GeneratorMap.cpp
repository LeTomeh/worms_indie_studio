//
// GeneratorMap.cpp for GeneratorMap.cpp in /home/dufren_b/teck2/rendu/CPP/cpp_indie_studio/srcs/GenerateMap
// 
// Made by julien dufrene
// Login   <dufren_b@epitech.net>
// 
// Started on  Fri May 20 14:12:33 2016 julien dufrene
// Last update Sat Jun  4 04:53:10 2016 Maxime LECOQ
//

#include "GeneratorMap.hh"

GeneratorMap::GeneratorMap()
{
  _init = true;
  getSources();
  num_png = 0;
  posX = 0;
  posY = 0;
  _exec[RETURN_K] = &GeneratorMap::save;
  _exec[RIGHT_K] = &GeneratorMap::moveRight;
  _exec[LEFT_K] = &GeneratorMap::moveLeft;
  _exec[UP_K] = &GeneratorMap::moveUp;
  _exec[DOWN_K] = &GeneratorMap::moveDown;
  _exec[Z_K] = &GeneratorMap::nextObj;
  _exec[A_K] = &GeneratorMap::prevObj;
  _exec[Q_K] = &GeneratorMap::quit;
  _exec[MMAJ_K] = &GeneratorMap::quit;
  _cpt = 1;
  _last = 0;
}

GeneratorMap::~GeneratorMap() {}

GeneratorMap::GeneratorMap(const GeneratorMap &c) : _init(c._init)
{
  srcs = c.srcs;
  num_png = c.num_png;
  png = c.png;
  posX = c.posX;
  posY = c.posY;
  _cpt = c._cpt;
  _last = c._last;
}

GeneratorMap &GeneratorMap::operator=(const GeneratorMap &c)
{
  if (this != &c)
    {
      _init = c._init;
      srcs = c.srcs;
      num_png = c.num_png;
      png = c.png;
      posX = c.posX;
      posY = c.posY;
      _cpt = c._cpt;
      _last = c._last;
    }
  return (*this);
}

void		GeneratorMap::getSources()
{
  Convert<int> conv;
  DIR *dir;
  struct dirent *entry;
  int i;
  std::string direc;

  i = 1;
  while (i < 4)
    {
      std::string directory(".cfg/map/world");
      directory += conv.toString(i) + "/";
      if ((dir = opendir(directory.c_str())) != NULL)
	{
	  while ((entry = readdir(dir)) != NULL)
	    {
	      std::string tmp(entry->d_name);
	      if (tmp != "back.png" && tmp[0] != '.')
		srcs.push_back(directory + tmp);
	    }
	  closedir(dir);
	}
      i++;
    }
  direc += ".cfg/map/form/";
  if ((dir = opendir(direc.c_str())) != NULL)
    {
      while ((entry = readdir(dir)) != NULL)
	{
	  std::string tmp(entry->d_name);
	  if (tmp[0] != '.')
	    srcs.push_back(direc + tmp);
	}
      closedir(dir);
    }
}

void	GeneratorMap::save()
{
  Vector	vec;

  try
    {
      Png	i(srcs[num_png]);

      png->copyBlock(posX, posY, i);
      i.clean();
      _data.setEffect(SEL);
      png->save();
      _data.reload(true);
    }
  catch (AError const &e)
    {
      throw ThrowErrorData(vec.getVector("GeneratorPng\ncan't save the map", '\n'));
    }
  posX = 0;
  posY = 0;
}

void	GeneratorMap::moveRight()
{
  if (_last == RIGHT_K)
    _cpt++;
  else
    _cpt = 1;
  posX += _cpt;
  _last = RIGHT_K;
  mclock = clock();
}

void	GeneratorMap::moveLeft()
{
  if (_last == LEFT_K)
    _cpt++;
  else
    _cpt = 1;
  posX -= _cpt;
  _last = LEFT_K;
  mclock = clock();
}

void	GeneratorMap::moveUp()
{
  if (_last == UP_K)
    _cpt++;
  else
    _cpt = 1;
  posY -= _cpt;
  _last = UP_K;
  mclock = clock();
}

void	GeneratorMap::moveDown()
{
  if (_last == DOWN_K)
    _cpt++;
  else
    _cpt = 1;
  posY += _cpt;
  _last = DOWN_K;
  mclock = clock();
}

void	GeneratorMap::nextObj()
{
  num_png++;
  if (num_png > (int)srcs.size() - 1)
    num_png = 0;
  _cpt = 1;
  _last = 0;
}

void	GeneratorMap::prevObj()
{
  num_png--;
  if (num_png < 0)
    num_png = srcs.size() - 1;
  _cpt = 1;
  _last = 0;
}

void	GeneratorMap::quit()
{
  delete (png);
  throw CatchIt<bool> (true);
}

void	GeneratorMap::checkExistence()
{
  File	tmp(srcs[num_png]);

  try
    {
      tmp.tryOpen();
    }
  catch (AError const &e)
    {
      srcs.erase(srcs.begin() + num_png);
      if (num_png >= (int)srcs.size())
	num_png = 0;
      if (srcs.size() != 0)
	checkExistence();
    }
}

void	GeneratorMap::checkSize()
{
  Vector	vec;

  if (srcs.size() == 0)
    throw ThrowErrorData(vec.getVector("GeneratorMap\nno png sources found", '\n'));
}

void	GeneratorMap::manageEvent(const int &ev)
{
  checkSize();
  if (ev != 0 && _exec.find(ev) != _exec.end())
    (this->*_exec[ev])();
  if (((double)clock() - (double)mclock) / CLOCKS_PER_SEC > 0.20)
    {
      _cpt = 1;
      _last = 0;
    }
  if (ev != 0 || _init == true)
    {
      if (_init == true)
	_data.reload(true);
      _init = false;
      checkExistence();
      checkSize();
      _data.setNamePng(srcs[num_png]);
      _data.setX(posX);
      _data.setY(posY);
      throw CatchIt<GeneratorData *> (&_data);
    }
}

void GeneratorMap::resetInit()
{
  _emptyEv = 0;
  _init = true;
  _last = 0;
  _cpt = 1;
}


void	GeneratorMap::enterIn()
{
  png = new GeneratorPng;
}

void	GeneratorMap::setSound(const bool &c)
{
  _data.setSound(c);
}

void	GeneratorMap::setEffect(const bool &c)
{
  _data.setEffectOn(c);
}
