//
// GeneratorPng.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas ANDRE
// Login   <andre_o@epitech.net>
// 
// Started on  Thu May 12 13:12:38 2016 Thomas ANDRE
// Last update Sat May 28 14:00:19 2016 Maxime LECOQ
//

#include	"GeneratorPng.hh"

GeneratorPng::GeneratorPng()
{
  File				dest(".cfg/perso.png");
  File				src(".cfg/map/template.png");
  Vector			vec;

  _save = false;
  try
    {
      dest.writeTronc(src.read());
      png = new Png(".cfg/perso.png");
      _rowPointers = png->getRowPointers();
      _width = png->getWidth();
      _height = png->getHeight();
    }
  catch (AError const &e)
    {
      e.notCriticalError();
      throw ThrowErrorData(vec.getVector("GeneratorPng\ntemplate.png not found", '\n'));
    }
}

GeneratorPng::~GeneratorPng()
{
  File		destroy(".cfg/perso.png");

  if (_save == false)
    try
      {
	destroy.destroy();
      }
    catch (AError const &e) {}
  delete (png);
}

GeneratorPng::GeneratorPng(const GeneratorPng &c) : _rowPointers(c._rowPointers), _width(c._width), _height(c._height), png(c.png), _save(c._save) {}

GeneratorPng	&GeneratorPng::operator=(const GeneratorPng &c)
{
  if (this != &c)
    {
      _rowPointers = c._rowPointers;
      _width = c._width;
      _height = c._height;
      png = c.png;
      _save = c._save;
    }
  return (*this);
}

void		GeneratorPng::save()
{
  Vector	vec;

  try
    {
      png->setRowPointers(_rowPointers);
      png->save(".cfg/perso.png");
      delete (png);
      png = new Png(".cfg/perso.png");
      _width = png->getWidth();
      _height = png->getHeight();
      _rowPointers = png->getRowPointers();
      _save = true;
    }
  catch (AError const &e)
    {
      e.notCriticalError();
      throw ThrowErrorData(vec.getVector("GeneratorPng\ncan't save the map", '\n'));
    }
}

void		GeneratorPng::setRowPointers()
{
  png->setRowPointers(_rowPointers);
}

void		GeneratorPng::copyBlock(const int &x, const int &y, Png form)
{
  int		i;
  int		i2;
  int		wi;
  int		he;
  png_bytep	*img;
  png_byte	*rowi;
  png_byte	*ptri;
  png_byte	*row;
  png_byte	*ptr;
  
  wi = form.getWidth();
  he = form.getHeight();
  img = form.getRowPointers();
  i = 0;
  i2 = 0;
  while (i < he && (i + y) < _height)
    {
      i2 = 0;
      if (i + y > 0)
	{
	  rowi = img[i];
	  row = _rowPointers[y + i];
	  while (i2 < wi && (i2 + x) < _width)
	    {
	      if (i2 + x > 0)
		{
		  ptri = &(rowi[i2 * 4]);
		  ptr = &(row[(i2 + x) * 4]);
		  if (ptr[3] == 0)
		    {
		      ptr[0] = ptri[0];
		      ptr[1] = ptri[1];
		      ptr[2] = ptri[2];
		      ptr[3] = ptri[3];
		    }
		}
	      i2++;
	    }
	}
      i++;
    }
}

void		GeneratorPng::deletePoint(const int &x, const int &y, png_bytep *rowPointers)
{
  png_byte			*row;
  png_byte			*ptr;

  row = rowPointers[y];
  ptr = &(row[x*4]);
  ptr[0] = 0;
  ptr[1] = 0;
  ptr[2] = 0;
  ptr[3] = 0;
}

void		GeneratorPng::firstDelete(const int &width, const int &height, png_bytep *rowPointers)
{
  int		y;
  int		x;

  y = 0;
  while (y < 40)
    {
      x = 0;
      while (x < width)
	deletePoint(x++, y, rowPointers);
      y++; 
    }
  y = height - 20;
  while (y < height)
    {
      x = 0;
      while (x < width)
	deletePoint(x++, y, rowPointers);
      y++; 
    }
  x = 0;
  while (x < 20)
    {
      y = 0;
      while (y < height)
	deletePoint(x, y++, rowPointers);
      x++;
	}
  x = width - 20;
  while (x < width)
    {
      y = 0;
      while (y < height)
	deletePoint(x, y++, rowPointers);
      x++;
    }
}
