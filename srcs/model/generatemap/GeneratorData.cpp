//
// ErrorGUI.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas ANDRE
// Login   <andre_o@epitech.net>
// 
// Started on  Wed May  4 19:21:20 2016 Thomas ANDRE
// Last update Sat May 28 14:07:27 2016 Maxime LECOQ
//

#include	"GeneratorData.hh"

GeneratorData::GeneratorData()
{
  x = 0;
  y = 0;
  _sound = MENU;
  _soundOn = false;
  _effectOn = false;
  _reload = false;
}

GeneratorData::~GeneratorData() {}

GeneratorData::GeneratorData(const GeneratorData &o)
{
  _pngName = o._pngName;
  x = o.x;
  y = o.y;
  _sound = o._sound;
  _soundOn = o._soundOn;
  _effect = o._effect;
  _effectOn = o._effectOn;
  _reload = o._reload;
}

GeneratorData	&GeneratorData::operator=(const GeneratorData &o)
{
  if (this != &o)
    {
      _pngName = o._pngName;
      x = o.x;
      y = o.y;
      _sound = o._sound;
      _soundOn = o._soundOn;
      _effect = o._effect;
      _effectOn = o._effectOn;
      _reload = o._reload;
    }
  return (*this);
}

void		GeneratorData::setX(const int &_x)
{
  x = _x;
}

void		GeneratorData::setY(const int &_y)
{
  y = _y;
}

int		GeneratorData::getX() const
{
  return (x);
}

int		GeneratorData::getY() const
{
  return (y);
}

const std::string	&GeneratorData::getNamePng() const
{
  return (_pngName);
}

void		GeneratorData::setNamePng(const std::string &name)
{
  _pngName = name;
}

const std::string	&GeneratorData::getSound() const
{
  return (_sound);
}

bool			GeneratorData::soundIsOn() const
{
  return (_soundOn);
}

void			GeneratorData::setSound(const bool &c)
{
  _soundOn = c;
}

const std::string     &GeneratorData::getEffect() const
{
  return (_effect);
}

bool                  GeneratorData::effectIsOn() const
{
  return (_effectOn);
}

void                  GeneratorData::setEffectOn(const bool &c)
{
  _effectOn = c;
}

void                  GeneratorData::setEffect(const std::string &c)
{
  _effect = c;
}

void			GeneratorData::reload(const bool &c)
{
  _reload = c;
}

bool			GeneratorData::getReload() const
{
  return (_reload);
}
