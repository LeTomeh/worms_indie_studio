//
// ErrorData.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:01:55 2016 Maxime LECOQ
// Last update Sat May 28 13:53:46 2016 Maxime LECOQ
//

#include	"ErrorData.hh"

ErrorData::ErrorData()
{
  _i = 1;
  _soundOn = false;
}

ErrorData::~ErrorData()
{
}

ErrorData::ErrorData(const ErrorData &c) : _i(c._i), _sound(c._sound), _soundOn(c._soundOn), _error(c._error) {}

ErrorData	&ErrorData::operator=(const ErrorData &c)
{
  if (this != &c)
    {
      _i = c._i;
      _sound = c._sound;
      _soundOn = c._soundOn;
      _error = c._error;
    }
  return (*this);
}

void		ErrorData::setI(const int &i)
{
  _i = i;
}

int		ErrorData::getI() const
{
  return (_i);
}

const std::string &ErrorData::getSound() const
{
  return (_sound);
}

bool			ErrorData::getSoundOn() const
{
  return (_soundOn);
}

void			ErrorData::setSoundOn(const bool &c)
{
  _soundOn = c;
}

void			ErrorData::setSound(const std::string &c)
{
  _sound = c;
}

void			ErrorData::setError(const std::vector<std::string> &c)
{
  _error = c;
}

std::vector<std::string>	ErrorData::getError() const
{
  return (_error);
}
