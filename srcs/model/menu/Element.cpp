//
// Element.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Tue May  3 16:34:27 2016 Maxime LECOQ
//

#include	"Element.hh"

Element::Element() : _value(""), _type(Element::LINK), _posSelector(0), _controllerValue(-1), _nb(0), _min(0) {}

Element::~Element() {}

Element::Element(const Element &c) : _value(c._value), _type(c._type), _selector(c._selector), _posSelector(c._posSelector), _controllerValue(c._controllerValue), _nb(c._nb), _link(c._link), _min(c._min), _msg(c._msg), _scr(c._scr) {}

Element	&Element::operator=(const Element &c)
{
  if (this != &c)
    {
      _value = c._value;
      _type = c._type;
      _selector = c._selector;
      _posSelector = c._posSelector;
      _controllerValue = c._controllerValue;
      _nb = c._nb;
      _link = c._link;
      _min = c._min;
      _msg = c._msg;
      _scr = c._scr;
    }
  return (*this);
}

const std::string             &Element::getValue() const
{
  return (_value);
}

Element::Type                          Element::getType() const
{
  return (_type);
}

std::vector<std::string>      Element::getSelector() const
{
  return (_selector);
}

int				Element::getPosSelector() const
{
  return (_posSelector);
}

int				Element::getControllerValue() const
{
  return (_controllerValue);
}

int				Element::getValueNb() const
{
  return (_nb);
}

int				Element::getValueMin() const
{
  return (_min);
}

const std::string             &Element::getLink() const
{
  return (_link);
}

std::vector<std::string>	Element::getMsg() const
{
  return (_msg);
}

Scores				&Element::getScores()
{
  return (_scr);
}
void                          Element::setValue(const std::string &d)
{
  _value = d;
}

void                          Element::setType(const Type &d)
{
  _type = d;
}

void                          Element::setSelector(const std::vector<std::string> &d)
{
  _selector = d;
}

void                          Element::setPosSelector(const int &d)
{
  _posSelector = d;
}

void                          Element::setControllerValue(const int &d)
{
  _controllerValue = d;
}

void				Element::setValueNb(const int &d)
{
  _nb = d;
}

void				Element::setValueMin(const int &d)
{
  _min = d;
}

void				Element::setLink(const std::string &c)
{
  _link = c;
}

void				Element::setMsg(const std::vector<std::string> &c)
{
  _msg = c;
}

void				Element::setScores(const Scores &c)
{
  _scr = c;
}
