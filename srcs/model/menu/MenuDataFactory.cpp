//
// MenuDataFactory.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Fri May 20 22:25:16 2016 Maxime LECOQ
//

#include	"MenuDataFactory.hh"

MenuDataFactory::MenuDataFactory()
{
  _facto["home"] = &MenuDataFactory::makeHome;
  _facto["homeOld"] = &MenuDataFactory::makeHomeOld;
  _facto["option"] = &MenuDataFactory::makeOption;
  _facto["control"] = &MenuDataFactory::makeControl;
  _facto["sound"] = &MenuDataFactory::makeSound;
  _facto["credit"] = &MenuDataFactory::makeCredit;
  _facto["game"] = &MenuDataFactory::makeGame;
  _facto["scores"] = &MenuDataFactory::makeScores;
  _facto["gameparameter"] = &MenuDataFactory::makeGameParam;
}

MenuDataFactory::~MenuDataFactory() {}

MenuDataFactory::MenuDataFactory(const MenuDataFactory &c) : _facto(c._facto) {}

MenuDataFactory	&MenuDataFactory::operator=(const MenuDataFactory &c)
{
  if (this != &c)
    {
      _facto = c._facto;
    }
  return (*this);
}

AMenuData	*MenuDataFactory::get(const std::string &what)
{
  if (_facto.find(what) != _facto.end())
    return ((this->*_facto[what])());
  return (NULL);
}

AMenuData	*MenuDataFactory::makeHome()
{
  return (new MenuDataHome);
}

AMenuData	*MenuDataFactory::makeHomeOld()
{
  return (new MenuDataHomeOld);
}

AMenuData	*MenuDataFactory::makeOption()
{
  return (new MenuDataOption);
}

AMenuData	*MenuDataFactory::makeControl()
{
  return (new MenuDataControl);
}

AMenuData	*MenuDataFactory::makeSound()
{
  return (new MenuDataSound);
}

AMenuData	*MenuDataFactory::makeCredit()
{
  return (new MenuDataCredit);
}

AMenuData	*MenuDataFactory::makeGame()
{
  return (new MenuDataGame);
}

AMenuData	*MenuDataFactory::makeScores()
{
  return (new MenuDataScores);
}

AMenuData	*MenuDataFactory::makeGameParam()
{
  return (new MenuDataGameParam);
}
