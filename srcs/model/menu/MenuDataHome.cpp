//
// MenuDataHome.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Sun Jun  5 19:25:40 2016 thoma_f
//

#include	"MenuDataHome.hh"

MenuDataHome::MenuDataHome()
{
  Element	elem;
  Vector	vec;

  _pageName = "Home";
  _back = "assets/fond.jpg";
  _pos = 0;
  elem.setValue("Start Game");
  elem.setMsg(vec.getVector("Join us\nand take\nthis banana !", '\n'));
  elem.setType(Element::LINK);
  elem.setLink("game");
  _elem.push_back(elem);
  elem.setValue("Scores");
  elem.setMsg(vec.getVector("Do you want\nto see the\nlast scores ?", '\n'));
  elem.setLink("scores");
  _elem.push_back(elem);
  elem.setValue("Options");
  elem.setMsg(vec.getVector("Do you want\nto edit your\ngame options ?", '\n'));
  elem.setLink("option");
  _elem.push_back(elem);
  elem.setValue("Credit");
  elem.setMsg(vec.getVector("Did you\nsee our\ncredits ?\nGo !", '\n'));
  elem.setLink("credit");
  _elem.push_back(elem);
  elem.setValue("Quit");
  elem.setMsg(vec.getVector("Oh No !\nDon't\nleave us !", '\n'));
  elem.setLink("quit");
  _elem.push_back(elem);
}

MenuDataHome::~MenuDataHome() {}
