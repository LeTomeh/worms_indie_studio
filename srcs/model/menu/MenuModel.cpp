//
// MenuModel.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
 // Started on  Tue Apr 26 22:35:27 2016 Maxime LECOQ
// Last update Sun Jun  5 19:35:47 2016 thoma_f
//

#include	"MenuModel.hh"

MenuModel::MenuModel()
{
  MenuDataFactory	factory;

  _execute["home"] = &MenuModel::home;
  _execute["homeOld"] = &MenuModel::homeOld;
  _execute["option"] = &MenuModel::option;
  _execute["control"] = &MenuModel::control;
  _execute["sound"] = &MenuModel::sound;
  _execute["credit"] = &MenuModel::credit;
  _execute["game"] = &MenuModel::game;
  _execute["scores"] = &MenuModel::scores;
  _execute["gameparameter"] = &MenuModel::gameParam;
  _datas["home"] = factory.get("home");
  _datas["homeOld"] = factory.get("homeOld");
  _datas["option"] = factory.get("option");
  _datas["control"] = factory.get("control");
  _datas["sound"] = factory.get("sound");
  _datas["credit"] = factory.get("credit");
  _datas["game"] = factory.get("game");
  _datas["scores"] = factory.get("scores");
  _datas["gameparameter"] = factory.get("gameparameter");
  _init = true;
  _sound["Music"] = &MenuModel::setMusic;
  _sound["Ambiance"] = &MenuModel::setAmbiance;
  _sound["Effect"] = &MenuModel::setEffect;
  _game["Turn time (P) in seconds"] = &MenuModel::turnP;
  _game["Turn time (N) in seconds"] = &MenuModel::turnN;
  _game["Party time (P) in minutes"] = &MenuModel::partyP;
  _game["Party time (N) in minutes"] = &MenuModel::partyN;
  _control.push_back(SPACE_K);
  _control.push_back(A_K);
  _control.push_back(B_K);
  _control.push_back(C_K);
  _control.push_back(D_K);
  _control.push_back(E_K);
  _control.push_back(F_K);
  _control.push_back(G_K);
  _control.push_back(H_K);
  _control.push_back(I_K);
  _control.push_back(J_K);
  _control.push_back(K_K);
  _control.push_back(L_K);
  _control.push_back(M_K);
  _control.push_back(N_K);
  _control.push_back(O_K);
  _control.push_back(P_K);
  _control.push_back(Q_K);
  _control.push_back(R_K);
  _control.push_back(S_K);
  _control.push_back(T_K);
  _control.push_back(U_K);
  _control.push_back(V_K);
  _control.push_back(W_K);
  _control.push_back(X_K);
  _control.push_back(Y_K);
  _control.push_back(Z_K);
  _control.push_back(DOWN_PK);
  _control.push_back(UP_PK);
  _control.push_back(RIGHT_PK);
  _control.push_back(LEFT_PK);
  _save = true;
  _links["play"] = "Play";
  _links["playOld"] = "PlayOld";
  _links["generatormap"] = "generator";
}

MenuModel::~MenuModel() {}

MenuModel::MenuModel(const MenuModel &c) : _data(c._data), _location(c._location), _execute(c._execute), _datas(c._datas), _init(c._init), _sound(c._sound), _control(c._control), _save(c._save), _game(c._game), _links(c._links) {}

MenuModel	&MenuModel::operator=(const MenuModel &c)
{
  if (this != &c)
    {
      _data = c._data;
      _location = c._location;
      _execute = c._execute;
      _datas = c._datas;
      _init = c._init;
      _sound = c._sound;
      _control = c._control;
      _save = c._save;
      _game = c._game;
      _links = c._links;
    }
  return (*this);
}

void			MenuModel::turnP(const std::string &d)
{
  Convert<int>		c;

  _data.setTurnP(c.toNumber(d));
}

void			MenuModel::turnN(const std::string &d)
{
  Convert<int>		c;

  _data.setTurnN(c.toNumber(d));
}

void			MenuModel::partyN(const std::string &d)
{
  Convert<int>		c;

  _data.setPartyN(c.toNumber(d));
}

void			MenuModel::partyP(const std::string &d)
{
  Convert<int>		c;

  _data.setPartyP(c.toNumber(d));
}

void			MenuModel::setMusic(const int &d)
{
  Convert<int>		conv;
  Convert<bool>		conv2;

  _data.setMusic(conv2.toNumber(conv.toString(d)));
  _datas["home"]->setSound(_data.getMusic());
  _datas["homeOld"]->setSound(_data.getMusic());
  _datas["option"]->setSound(_data.getMusic());
  _datas["control"]->setSound(_data.getMusic());
  _datas["sound"]->setSound(_data.getMusic());
  _datas["credit"]->setSound(_data.getMusic());
  _datas["game"]->setSound(_data.getMusic());
  _datas["gameparameter"]->setSound(_data.getMusic());
  _datas["scores"]->setSound(_data.getMusic());
}

void			MenuModel::setAmbiance(const int &d)
{
  Convert<int>		conv;
  Convert<bool>		conv2;

  _data.setAmbiance(conv2.toNumber(conv.toString(d)));
}

void			MenuModel::setEffect(const int &d)
{
  Convert<int>		conv;
  Convert<bool>		conv2;

  _data.setEffect(conv2.toNumber(conv.toString(d)));
  _datas["home"]->setEffectOn(_data.getEffect());
  _datas["homeOld"]->setEffectOn(_data.getEffect());
  _datas["option"]->setEffectOn(_data.getEffect());
  _datas["control"]->setEffectOn(_data.getEffect());
  _datas["sound"]->setEffectOn(_data.getEffect());
  _datas["credit"]->setEffectOn(_data.getEffect());
  _datas["game"]->setEffectOn(_data.getEffect());
  _datas["gameparameter"]->setEffectOn(_data.getEffect());
  _datas["scores"]->setEffectOn(_data.getEffect());
}

void		MenuModel::setScores(Scores sc)
{
  _datas["scores"]->setScores(sc);
}

void				MenuModel::setGameOption(Data d)
{
  std::vector<std::string>	tmp;
  size_t			i;
  size_t			size;
  Convert<int>			conv;
  std::string			val;

  tmp = _datas["gameparameter"]->getElement()[0].getSelector();
  i = 0;
  size = tmp.size();
  val = conv.toString(d.getTurnN());
  while (i < size)
    {
      if (tmp[i] == val)
	{
	  _datas["gameparameter"]->setPosVector(0, i);
	  break;
	}
      i++;
    }
  tmp = _datas["gameparameter"]->getElement()[1].getSelector();
  i = 0;
  size = tmp.size();
  val = conv.toString(d.getTurnP());
  while (i < size)
    {
      if (tmp[i] == val)
	{
	  _datas["gameparameter"]->setPosVector(1, i);
	  break;
	}
      i++;
    }
  tmp = _datas["gameparameter"]->getElement()[2].getSelector();
  i = 0;
  size = tmp.size();
  val = conv.toString(d.getPartyN());
  while (i < size)
    {
      if (tmp[i] == val)
	{
	  _datas["gameparameter"]->setPosVector(2, i);
	  break;
	}
      i++;
    }
  tmp = _datas["gameparameter"]->getElement()[3].getSelector();
  i = 0;
  size = tmp.size();
  val = conv.toString(d.getPartyP());
  while (i < size)
    {
      if (tmp[i] == val)
	{
	  _datas["gameparameter"]->setPosVector(3, i);
	  break;
	}
      i++;
    }

}

void		MenuModel::setData(Data d)
{
  Element	elem;
  Vector	vec;

  _data = d;
  if (_data.getMusic() == true)
    _datas["sound"]->setPosVector(0, 1);
  else
    _datas["sound"]->setPosVector(0, 0);
  if (_data.getEffect() == true)
    _datas["sound"]->setPosVector(1, 1);
  else
    _datas["sound"]->setPosVector(1, 0);
  _datas["control"]->setController(d.getControl(), d.getControllerList());
  setGameOption(d);
  _save = _data.getSave();
  if (_data.getPerso() == true)
    {
      elem.setValue("Map");
      elem.setMsg(vec.getVector("Do you prefer\na random map\nor your map ?", '\n'));
      elem.setType(Element::SELECTOR);
      elem.setSelector(vec.getVector("Random-My map", '-'));
      if (_datas["game"]->getElement()[4].getType() != Element::SELECTOR)
	_datas["game"]->addElement(elem, 4);
    }
  else
    if (_datas["game"]->getElement()[4].getType() == Element::SELECTOR)
      _datas["game"]->deleteElement(4);
  if (_data.getSave())
    {
      _location = "homeOld";
      elem.setValue("Delete last game");
      elem.setMsg(vec.getVector("Do you want\nto delete\nyour last saved game", '\n'));
      elem.setType(Element::DELETE);
      if (_datas["option"]->getElement()[4].getType() != Element::DELETE)
	_datas["option"]->addElement(elem, 4);
    }
  else
    _location = "home";
  _datas["home"]->setSound(_data.getMusic());
  _datas["homeOld"]->setSound(_data.getMusic());
  _datas["option"]->setSound(_data.getMusic());
  _datas["control"]->setSound(_data.getMusic());
  _datas["sound"]->setSound(_data.getMusic());
  _datas["credit"]->setSound(_data.getMusic());
  _datas["game"]->setSound(_data.getMusic());
  _datas["gameparameter"]->setSound(_data.getMusic());
  _datas["scores"]->setSound(_data.getMusic());
  _datas["home"]->setEffectOn(_data.getEffect());
  _datas["homeOld"]->setEffectOn(_data.getEffect());
  _datas["option"]->setEffectOn(_data.getEffect());
  _datas["control"]->setEffectOn(_data.getEffect());
  _datas["sound"]->setEffectOn(_data.getEffect());
  _datas["credit"]->setEffectOn(_data.getEffect());
  _datas["game"]->setEffectOn(_data.getEffect());
  _datas["scores"]->setEffectOn(_data.getEffect());
  _datas["gameparameter"]->setEffectOn(_data.getEffect());
}

void		MenuModel::manageEvent(const int &ev)
{
  std::string	tmp;

  if (ev != 0 || _init == true)
    {
      _init = false;
      tmp = _location;
      try
	{
	  movePos(ev);
	  (this->*_execute[_location])(ev); 
	}
      catch (CatchIt<bool> const &b)
	{
	  throw CatchIt<bool> (b.getIt());
	}
      if (tmp != _location)
	_init = true;
      throw CatchIt<AMenuData *> (_datas[_location]);
    }
}

void		MenuModel::homeOld(const int &ev)
{
  try
    {
      replacePos(_datas["homeOld"]->getElement().size() - 1);
      catchLinks(ev);
    }
  catch (CatchIt<bool> const &b)
    {
      throw CatchIt<bool> (b.getIt());
    }
  catch (CatchIt<std::string> const &s)
    {
      if (s.getIt() == "PlayOld")
	{
	  _save = true;
	  _datas[_location]->setPos(0);
	  _location = "home";
	  _datas[_location]->setEffect(SEL);
	  throw CatchIt<bool> (true);
	}
    }
}

void		MenuModel::home(const int &ev)
{
  try
    {
      replacePos(_datas["home"]->getElement().size() - 1);
      catchLinks(ev);
    }
  catch (CatchIt<bool> const &b)
    {
      throw CatchIt<bool> (b.getIt());
    }
  catch (CatchIt<std::string> const &s) {}
}

void		MenuModel::option(const int &ev)
{
  try
    {
      replacePos(_datas["option"]->getElement().size() - 1);
      catchBacks(ev);
      catchLinks(ev);
      catchButton(ev);
      catchDelete(ev);
    }
  catch (CatchIt<bool> const  &b) {}
  catch (CatchIt<std::string> const &s)
    {
      if (s.getIt() == "back")
	{
	  if (_data.getSave() == true)
	    _location = "homeOld";
	  else
	    _location = "home";
	  _datas[_location]->setPos(0);
	  _datas[_location]->setEffect(SEL);
	}
      else
	if (s.getIt() == "generator")
	  throw CatchIt<std::string> (s.getIt());
    }
}

void		MenuModel::control(const int &ev)
{
  size_t			i;
  size_t			size;
  std::map<std::string, int>	ctrl;
  std::vector<std::string>	list;
  bool				war;
  Element			tmp;
  int				event;

  event = ev;
  try
    {
      replacePos(_datas["control"]->getElement().size() - 1);
      catchBacks(event);
      if (_datas["control"]->getWarning() == false || _datas["control"]->getElement()[_datas["control"]->getPos()].getValue() == "Reset")
	catchButton(event);
    }
  catch (CatchIt<bool> const  &b)
    {
      if (b.getIt() == true && _datas["control"]->getWarning() == true)
	{
	  _data.setControl(_data.getControlDefault());
	  _datas["control"]->setController(_data.getControl(), _data.getControllerList());
	  _datas["control"]->setWarning(false);
	}
    }
  catch (CatchIt<std::string> const &s)
    {
      if (s.getIt() == "back")
	{
	  _location = "option";
	  _datas[_location]->setPos(0);
	  _datas[_location]->setEffect(SEL);
	}
      else
	{
	  _data.setControl(_data.getControlDefault());
	  _datas["control"]->setController(_data.getControl(), _data.getControllerList());
	  _datas["control"]->setWarning(false);
	}
    }
  try
    {
      war = false;
      i = 0;
      size = _control.size();
      while (i < size && _control[i] != event)
	i++;
      tmp = _datas["control"]->getElement()[_datas["control"]->getPos()];
      if (tmp.getType() == Element::CONTROLLER && i < size)
	{
	  list = _data.getControllerList();
	  ctrl = _data.getControl();
	  i = 0;
	  size = list.size();
	  if (event >= 84)
	    event -= 81;
	  while (i < size)
	    {
	      if (ctrl[list[i]] == event)
		ctrl[list[i]] = 0;
	      if (ctrl[list[i]] == 0 && list[i] != tmp.getValue())
		war = true;
	      i++;
	    }
	  if (war == true && _datas["control"]->getWarning() == false)
	    _datas["control"]->setEffect(ERROR);
	  _datas["control"]->setWarning(war);
	  ctrl[tmp.getValue()] = event;
	  _data.setControl(ctrl);
	  _datas["control"]->setController(ctrl, _data.getControllerList());
	}
    }
  catch (CatchIt<bool> const &b) {}
}

void		MenuModel::sound(const int &ev)
{
  try
    {
      replacePos(_datas["sound"]->getElement().size() - 1);
      catchBacks(ev);
      catchButton(ev);
      catchSelector(ev);
    }
  catch (CatchIt<bool> const  &b) {}
  catch (CatchIt<std::string> const &s)
    {
      _location = "option";
      _datas[_location]->setPos(0);
      _datas[_location]->setEffect(SEL);
    }
}

void		MenuModel::gameParam(const int &ev)
{
  try
    {
      replacePos(_datas["gameparameter"]->getElement().size() - 1);
      catchBacks(ev);
      catchButton(ev);
      catchSelector(ev);
    }
  catch (CatchIt<bool> const  &b) {}
  catch (CatchIt<std::string> const &s)
    {
      _location = "option";
      _datas[_location]->setPos(0);
      _datas[_location]->setEffect(SEL);
    }
}

void		MenuModel::credit(const int &ev)
{
  try
    {
      replacePos(_datas["credit"]->getElement().size() - 1);
      catchBacks(ev);
      catchButton(ev);
    }
  catch (CatchIt<bool> const &b) {}
  catch (CatchIt<std::string> const &s)
    {
      if (_data.getSave() == true)
	_location = "homeOld";
      else
	_location = "home";
      _datas[_location]->setPos(0);
      _datas[_location]->setEffect(SEL);
    }
}

void		MenuModel::game(const int &ev)
{
  try
    {
      replacePos(_datas["game"]->getElement().size() - 1);
      catchBacks(ev);
      catchButton(ev);
      catchSelector(ev);
      catchValue(ev);
      catchLinks(ev);
    }
  catch (CatchIt<bool> const &b) {}
  catch (CatchIt<std::string> const &s)
    {
      if (s.getIt() == "Play")
	{
	  _save = false;
	  _datas[_location]->setPos(0);
	  _location = "home";
	  _datas[_location]->setEffect(SEL);
	  throw CatchIt<bool> (true);
	}
      else
	{
	  if (_data.getSave() == true)
	    _location = "homeOld";
	  else
	    _location = "home";
	  _datas[_location]->setPos(0);
	  _datas[_location]->setEffect(SEL);
	}
    }
}

void		MenuModel::scores(const int &ev)
{
  try
    {
      replacePos(_datas["scores"]->getElement().size() - 1);
      catchBacks(ev);
      catchButton(ev);
      catchSelector(ev);
    }
  catch (CatchIt<bool> const  &b) {}
  catch (CatchIt<std::string> const &s)
    {
      if (s.getIt() == "back")
	{
	  if (_data.getSave() == true)
	    _location = "homeOld";
	  else
	    _location = "home";
	  _datas[_location]->setPos(0);
	  _datas[_location]->setEffect(SEL);
	}
      else
	{
	  try
	    {
	      File	file(".cfg/scores");
	      file.clear();
	      Loader	load;
	      _datas["scores"]->setScores(load.loadScores());
	    }
	  catch (AError const &e) { e.notCriticalError(); }
	}
    }
}

void		MenuModel::movePos(const int &ev)
{
  if (ev == DOWN_K)
    {
      _datas[_location]->setPos(_datas[_location]->getPos() + 1);
      _datas[_location]->setEffect(SWI);
    }
  else
    if (ev == UP_K)
      {
	_datas[_location]->setPos(_datas[_location]->getPos() - 1);
	_datas[_location]->setEffect(SWI);
      }
}

void		MenuModel::replacePos(const int &max)
{
  if (_datas[_location]->getPos() > max)
    _datas[_location]->setPos(0);
  else
    if (_datas[_location]->getPos() < 0)
      _datas[_location]->setPos(max);
}

void		MenuModel::catchLinks(const int &ev)
{
  Element	tmp;

  tmp = _datas[_location]->getElement()[_datas[_location]->getPos()];
  if (ev == RETURN_K && tmp.getType() == Element::LINK)
    {
      _datas[_location]->setPos(0);
      if (tmp.getLink() == "quit")
	throw CatchIt<bool> (false);
      else
	if (_links.find(tmp.getLink()) != _links.end())
	  throw CatchIt<std::string> (_links[tmp.getLink()]);
      _datas[tmp.getLink()]->setPos(0);
      _location = tmp.getLink();
      _datas[_location]->setEffect(SEL);
      throw CatchIt<std::string> ("links");
    }
}

void		MenuModel::catchButton(const int &ev)
{
  Element	tmp;

  tmp = _datas[_location]->getElement()[_datas[_location]->getPos()];
  if (ev == RETURN_K && tmp.getType() == Element::BUTTON)
    {
      _datas[_location]->setPos(0);
      if (tmp.getValue() == "Back")
	throw CatchIt<std::string> ("back");
      else
	throw CatchIt<std::string> ("reset");
    }
}

void		MenuModel::catchSelector(const int &ev)
{
  Element	tmp;
  int		i;
  String	string;

  tmp = _datas[_location]->getElement()[_datas[_location]->getPos()];
  if ((ev == RIGHT_K || ev == LEFT_K) && (tmp.getType() == Element::SELECTOR || tmp.getType() == Element::SCORES))
    {
      i = tmp.getPosSelector();
      if (ev == RIGHT_K)
	i++;
      else
	i--;
      if (i < 0)
	i = tmp.getSelector().size() - 1;
      else
	if ((unsigned int)i > tmp.getSelector().size() - 1)
	  i = 0;
      _datas[_location]->setPosVector(_datas[_location]->getPos(), i);
      _datas[_location]->setEffect(SWI);
      if (_sound.find(tmp.getValue()) != _sound.end())
	(this->*_sound[tmp.getValue()])(i);
      if (_game.find(tmp.getValue()) != _game.end())
	(this->*_game[tmp.getValue()])(tmp.getSelector()[i]);
      throw CatchIt<bool> (true);
    }
}

void		MenuModel::catchValue(const int &ev)
{
  Element	tmp;
  int		i;
  String	string;

  tmp = _datas[_location]->getElement()[_datas[_location]->getPos()];
  if ((ev == RIGHT_K || ev == LEFT_K) && tmp.getType() == Element::VALUE)
    {
      i = tmp.getValueNb();
      if (ev == RIGHT_K)
	i++;
      else
	i--;
      if (tmp.getValue() == "Number of IA Teams" || tmp.getValue() == "Number of Player Teams")
	checkValues(i, tmp);
      else
	if (i < tmp.getValueMin())
	  i = tmp.getValueMin();
      _datas[_location]->setValueNb(_datas[_location]->getPos(), i);
      _datas[_location]->setEffect(SWI);
      throw CatchIt<bool> (true);
    }
}

void		MenuModel::checkValues(int &i, const Element &elem)
{
  size_t	x;
  size_t	size;
  std::vector<Element>	tmp;

  tmp = _datas[_location]->getElement();
  x = 0;
  size = tmp.size();
  while (x < size)
    {
      if ((elem.getValue() == "Number of IA Teams" && tmp[x].getValue() == "Number of Player Teams")
	  || (tmp[x].getValue() == "Number of IA Teams" && elem.getValue() == "Number of Player Teams"))
	break;
      x++;
    }
  if (i <= 0)
    {
      _datas[_location]->setValueNbMin(x, 2);
      if (_datas[_location]->getElement()[x].getValueNb() < _datas[_location]->getElement()[x].getValueMin())
	_datas[_location]->setValueNb(x, 2);
      _datas[_location]->setValueNbMin(_datas[_location]->getPos(), 0);
      i = 0;
    }
  if (i == 1)
    {
      _datas[_location]->setValueNbMin(x, 1);
      if (_datas[_location]->getElement()[x].getValueNb() < _datas[_location]->getElement()[x].getValueMin())
	_datas[_location]->setValueNb(x, 1);
      _datas[_location]->setValueNbMin(_datas[_location]->getPos(), 0);
    }
}

void			MenuModel::catchBacks(const int &ev)
{
  std::vector<Element>	tmp;
  size_t		i;
  size_t		size;

  i = 0;
  tmp = _datas[_location]->getElement();
  if (ev == F1_K || ev == F2_K || ev == BMAJ_K || ev == RMAJ_K)
    {
      i = 0;
      size = tmp.size();
      while (i < size)
	{
	  if ((((ev == F1_K || ev == BMAJ_K) && tmp[i].getValue() == "Back")
	       || ((ev == F2_K || ev == RMAJ_K)&& tmp[i].getValue() == "Reset"))
	      && tmp[i].getType() == Element::BUTTON)
	    break;
	  i++;
	}
      if (i < size)
	_datas[_location]->setPos((int)i);
      throw CatchIt<bool> (false);
    }
  else
    if (ev == F12_K || ev == MMAJ_K)
      {
	_datas[_location]->setPos(0);
	if (_data.getSave() == true)
	  _location = "homeOld";
	else
	  _location = "home";
	_datas[_location]->setPos(0);
	_datas[_location]->setEffect(SEL);
	throw CatchIt<bool> (true);
      }
}

void		MenuModel::catchDelete(const int &ev)
{
  Element	tmp;

  tmp = _datas[_location]->getElement()[_datas[_location]->getPos()];
  if ((ev == SPACE_K || ev == RETURN_K) && tmp.getType() == Element::DELETE)
    {
      File	map(".cfg/map.png");
      File	cnf(".cfg/.cnf");

      try
	{
	  map.destroy();
	}
      catch (AError const &e) {}
      try
	{
	  cnf.destroy();
	}
      catch (AError const &e) {}
      _data.setSave(false);
      _datas[_location]->deleteElement(4);
      setData(_data);
      _location = "home";
      _datas[_location]->setEffect(DEL);
    }
}

AMenuData		*MenuModel::getMenuData(const std::string &s)
{
  if (_datas.find(s) != _datas.end())
    return (_datas[s]);
  else
    return (NULL);
}

Data			MenuModel::getData() const
{
  return (_data);
}

void			MenuModel::resetInit()
{
  _init = true;
  if (_save == true)
    _location = "homeOld";
  else
    _location = "home";
}

bool			MenuModel::getSave() const
{
  return (_save);
}

void			MenuModel::putSoundEnd()
{
  _datas[_location]->setEffect(ENDGAME);
  _datas[_location]->setEffect(EXCELLENT);
}

void			MenuModel::setEffect(const std::string &s)
{
  _datas[_location]->setEffect(s);
}
