//
// MenuDataCredit.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Sun Jun  5 19:19:23 2016 thoma_f
//

#include	"MenuDataCredit.hh"

MenuDataCredit::MenuDataCredit()
{
  Element       elem;
  Vector	vec;

  _pageName = "Credit";
  _pos = 0;
  elem.setValue("Back");
  elem.setMsg(vec.getVector("Thanks\nfor\nplaying", '\n'));
  elem.setType(Element::BUTTON);
  _elem.push_back(elem);
  _back = "assets/credit.jpg";
}

MenuDataCredit::~MenuDataCredit() {}
