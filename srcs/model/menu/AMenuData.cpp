//
// AMenuData.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Sun Jun  5 19:57:55 2016 thoma_f
//

#include	"AMenuData.hh"

AMenuData::AMenuData() : _pageName(""), _back(""), _sound(MENU), _warning(false), _soundOn(false), _effectOn(false), _reset(false) {}

AMenuData::~AMenuData() {}

AMenuData::AMenuData(const AMenuData &c) : _elem(c._elem), _pageName(c._pageName), _back(c._back), _sound(c._sound), _warning(c._warning), _soundOn(c._soundOn), _effect(c._effect), _effectOn(c._effectOn), _reset(c._reset) {}

AMenuData	&AMenuData::operator=(const AMenuData &c)
{
  if (this != &c)
    {
      _elem = c._elem;
      _pageName = c._pageName;
      _back = c._back;
      _sound = c._sound;
      _warning = c._warning;
      _soundOn = c._soundOn;
      _effect = c._effect;
      _effectOn = c._effectOn;
      _reset = c._reset;
    }
  return (*this);
}

std::vector<Element>		AMenuData::getElement() const
{
  return (_elem);
}

const std::string		&AMenuData::getPageName() const
{
  return (_pageName);
}

const std::string		&AMenuData::getBack() const
{
  return (_back);
}

const std::string		&AMenuData::getSound() const
{
  return (_sound);
}

bool				AMenuData::soundIsOn() const
{
  return (_soundOn);
}

std::string			AMenuData::getEffect()
{
  std::string                   tmp;

  if (_effect.size() != 0)
    {
      tmp = _effect[0];
      _effect.erase(_effect.begin());
      return (tmp);
    }
  else
    return ("");
}

bool				AMenuData::effectIsOn() const
{
  return (_effectOn);
}

int				AMenuData::getPos() const
{
  return (_pos);
}

void				AMenuData::setSound(const bool &c)
{
  _soundOn = c;
}

void				AMenuData::setEffect(const std::string &c)
{
  _effect.push_back(c);
}

void				AMenuData::setEffectOn(const bool &c)
{
  _effectOn = c;
}

void				AMenuData::setPos(const int &pos)
{
  _pos = pos;
}

void				AMenuData::setPosVector(const int &pos, const int &value)
{
  _elem[pos].setPosSelector(value);
}

void				AMenuData::setValueNb(const int &pos, const int &value)
{
  _elem[pos].setValueNb(value);
}

void				AMenuData::setValueNbMin(const int &pos, const int &value)
{
  _elem[pos].setValueMin(value);
}

void				AMenuData::setController(std::map<std::string, int> controls, const std::vector<std::string> &ctrl)
{
  std::vector<Element>		vElem;
  Element			elem;
  size_t			i;
  size_t			size;
  Vector			vec;

  i = 0;
  size = ctrl.size();
  elem.setType(Element::CONTROLLER);
  while (i < size)
    {
      elem.setValue(ctrl[i]);
      elem.setMsg(vec.getVector("edit your\n\"" + ctrl[i] + "\" key", '\n'));
      elem.setControllerValue(controls[ctrl[i]]);
      vElem.push_back(elem);
      i++;
    }
  size = _elem.size();
  i = size - 2;
  while (i < size)
    {
      vElem.push_back(_elem[i]);
      i++;
    }
  _elem = vElem;
}

void			AMenuData::setScores(Scores sc)
{
  _elem[0].setScores(sc);
}

bool			AMenuData::getWarning() const
{
  return (_warning);
}

void			AMenuData::setWarning(const bool &c)
{
  _warning = c;
}

void			AMenuData::addElement(const Element &el, const int &c)
{
  _elem.insert(_elem.begin() + c, el);
}

void			AMenuData::deleteElement(const int &c)
{
  _elem.erase(_elem.begin() + c);
}

void			AMenuData::setResetKlang(const bool &c)
{
  _reset = c;
}

bool			AMenuData::getResetKlang() const
{
  return (_reset);
}
