//
// MenuDataSound.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Sun Jun  5 19:30:56 2016 thoma_f
//

#include	"MenuDataSound.hh"

MenuDataSound::MenuDataSound()
{
  Element       elem;
  Vector	vec;
  std::string	tmp;

  _pageName = "Sound Option";
  _back = "assets/fond.jpg";
  _pos = 0;
  elem.setValue("Music");
  tmp = "No-Yes";
  elem.setSelector(vec.getVector(tmp, '-'));
  elem.setMsg(vec.getVector("Do you want\nto put\nthe music\non or off ?", '\n'));
  elem.setType(Element::SELECTOR);
  _elem.push_back(elem);
  elem.setValue("Effect");
  elem.setMsg(vec.getVector("Do you want\nto put\nthe effect\non or off ?", '\n'));
  _elem.push_back(elem);
  elem.setValue("Back");
  elem.setMsg(vec.getVector("Do you want\nto go back ?", '\n'));
  elem.setSelector(vec.getVector("", '-'));
  elem.setType(Element::BUTTON);
  _elem.push_back(elem);
}

MenuDataSound::~MenuDataSound() {}
