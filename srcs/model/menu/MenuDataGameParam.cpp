//
// MenuDataGameParam.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Sun Jun  5 19:30:07 2016 thoma_f
//

#include	"MenuDataGameParam.hh"

MenuDataGameParam::MenuDataGameParam()
{
  Element       elem;
  Vector	vec;
  std::string	tmp;

  _pageName = "Game Option";
  _back = "assets/fond.jpg";
  _pos = 0;
  elem.setValue("Turn time (N) in seconds");
  tmp = "10-15-20-25-30-35-40-45-50-55-60";
  elem.setSelector(vec.getVector(tmp, '-'));
  elem.setMsg(vec.getVector("How much time\nfor one turn\nin normal mode ?", '\n'));
  elem.setType(Element::SELECTOR);
  _elem.push_back(elem);
  elem.setValue("Turn time (P) in seconds");
  tmp = "10-15-20-25-30-35-40-45-50-55-60";
  elem.setSelector(vec.getVector(tmp, '-'));
  elem.setMsg(vec.getVector("How much time\nfor one turn\nin pro mode ?", '\n'));
  _elem.push_back(elem);
  elem.setValue("Party time (N) in minutes");
  tmp = "1-5-10-15-20-25-30-35-40-45-50-55-60";
  elem.setSelector(vec.getVector(tmp, '-'));
  elem.setMsg(vec.getVector("How much time\nfor one party\nin normal mode ?", '\n'));
  _elem.push_back(elem);
  elem.setValue("Party time (P) in minutes");
  tmp = "1-5-10-15-20-25-30-35-40-45-50-55-60";
  elem.setSelector(vec.getVector(tmp, '-'));
  elem.setMsg(vec.getVector("How much time\nfor one party\nin pro mode ?", '\n'));
  _elem.push_back(elem);
  elem.setValue("Back");
  elem.setMsg(vec.getVector("Do you want\nto go back ?", '\n'));
  elem.setSelector(vec.getVector("", '-'));
  elem.setType(Element::BUTTON);
  _elem.push_back(elem);
}

MenuDataGameParam::~MenuDataGameParam() {}
