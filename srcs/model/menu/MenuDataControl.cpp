//
// MenuDataControl.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Tue May  3 09:13:08 2016 Maxime LECOQ
//

#include	"MenuDataControl.hh"

MenuDataControl::MenuDataControl()
{
  Element       elem;
  Vector        vec;

  _pageName = "Control Option";
  _back = "assets/fond.jpg";
  _pos = 0;
  elem.setValue("Back");
  elem.setMsg(vec.getVector("Do you want\nto go back ?", '\n'));
  elem.setType(Element::BUTTON);
  _elem.push_back(elem);
  elem.setValue("Reset");
  elem.setMsg(vec.getVector("Do you want\nto reset\nyour controller ?", '\n'));
  _elem.push_back(elem);
}

MenuDataControl::~MenuDataControl() {}
