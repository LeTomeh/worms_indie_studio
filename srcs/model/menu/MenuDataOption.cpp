//
// MenuDataOption.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Sun Jun  5 19:27:23 2016 thoma_f
//

#include	"MenuDataOption.hh"

MenuDataOption::MenuDataOption()
{
  Element       elem;
  Vector	vec;

  _pageName = "Options";
  _back = "assets/fond.jpg";
  _pos = 0;
  elem.setValue("Control");
  elem.setMsg(vec.getVector("Do you want\nto edit your\ncontroller ?", '\n'));
  elem.setType(Element::LINK);
  elem.setLink("control");
  _elem.push_back(elem);
  elem.setValue("Sound");
  elem.setMsg(vec.getVector("Do you want\nto edit\nthe sound ?", '\n'));
  elem.setLink("sound");
  _elem.push_back(elem);
  elem.setValue("Game");
  elem.setMsg(vec.getVector("Do you want\nto edit\ngame parameters ?", '\n'));
  elem.setLink("gameparameter");
  _elem.push_back(elem);
  elem.setValue("Map Generator");
  elem.setMsg(vec.getVector("Do you want\nto create\nyour own map ?", '\n'));
  elem.setLink("generatormap");
  _elem.push_back(elem);
  elem.setLink("");
  elem.setValue("Back");
  elem.setMsg(vec.getVector("Do you want\nto go back ?", '\n'));
  elem.setType(Element::BUTTON);
  _elem.push_back(elem);
}

MenuDataOption::~MenuDataOption() {}
