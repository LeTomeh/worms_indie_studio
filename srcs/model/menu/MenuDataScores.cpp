 //
// MenuDataScores.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Tue May  3 22:53:52 2016 Maxime LECOQ
//

#include	"MenuDataScores.hh"

MenuDataScores::MenuDataScores()
{
  Element       elem;
  Vector	vec;

  _pageName = "Scores";
  _back = "assets/fond.jpg";
  _pos = 0;
  elem.setType(Element::SCORES);
  elem.setSelector(vec.getVector("Top Winners\nTop Loosers\nBest Ratio (W/D)\nTop Scores", '\n'));
  elem.setMsg(vec.getVector("In which order\ndo you want\nto display ranks ?", '\n'));
  elem.setPosSelector(0);
  _elem.push_back(elem);
  elem.setSelector(vec.getVector("", '\n'));
  elem.setValue("Back");
  elem.setMsg(vec.getVector("Do you want\nto go back ?", '\n'));
  elem.setType(Element::BUTTON);
  _elem.push_back(elem);
  elem.setValue("Reset");
  elem.setMsg(vec.getVector("Do you want\nto reset scores ?", '\n'));
  _elem.push_back(elem);
}

MenuDataScores::~MenuDataScores() {}
