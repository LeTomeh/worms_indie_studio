//
// MenuDataGame.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Sun Jun  5 19:22:18 2016 thoma_f
//

#include	"MenuDataGame.hh"

MenuDataGame::MenuDataGame()
{
  Element       elem;
  Vector        vec;
  std::string   tmp;

  _pageName = "Manage Party";
  _back = "assets/fond.jpg";
  _pos = 0;
  elem.setValue("Number of IA Teams");
  elem.setMsg(vec.getVector("How many\nIA teams do\nyou want ?", '\n'));
  elem.setType(Element::VALUE);
  elem.setValueNb(1);
  elem.setValueMin(0);
  _elem.push_back(elem);
  elem.setValue("Number of Player Teams");
  elem.setMsg(vec.getVector("How many\nPlayers teams do\nyou want ?", '\n'));
  elem.setValueNb(1);
  elem.setValueMin(0);
  _elem.push_back(elem);
  elem.setValue("Number of Worms per Team");
  elem.setMsg(vec.getVector("How many\nworms per\nteam do\nyou want ?", '\n'));
  elem.setValueNb(4);
  elem.setValueMin(4);
  _elem.push_back(elem);
  elem.setValueMin(0);
  elem.setValue("Mode");
  elem.setMsg(vec.getVector("Which mode\ndo you\nprefer ?\nNormal / Pro", '\n'));
  elem.setType(Element::SELECTOR);
  tmp = "Normal-Pro";
  elem.setSelector(vec.getVector(tmp, '-'));
  elem.setPosSelector(0);
  _elem.push_back(elem);
  elem.setType(Element::BUTTON);
  elem.setValue("Back");
  elem.setMsg(vec.getVector("Do you want\nto go back ?", '\n'));
  elem.setSelector(vec.getVector("", '-'));
  _elem.push_back(elem);
  elem.setType(Element::LINK);
  elem.setValue("Continue");
  elem.setLink("play");
  elem.setMsg(vec.getVector("Do you want\n to continue ?", '\n'));
  _elem.push_back(elem);
}

MenuDataGame::~MenuDataGame() {}
