//
// ErrorOccured.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:01:55 2016 Maxime LECOQ
// Last update Sat Jun  4 03:16:03 2016 Maxime LECOQ
//

#include	"ErrorOccured.hh"

ErrorOccured::ErrorOccured()
{
  _init = true;
}

ErrorOccured::~ErrorOccured()
{
}

ErrorOccured::ErrorOccured(const ErrorOccured &c) : _init(c._init), _data(c._data) {}

ErrorOccured	&ErrorOccured::operator=(const ErrorOccured &c)
{
  if (this != &c)
    {
      _init = c._init;
      _data = c._data;
    }
  return (*this);
}

void		ErrorOccured::manageEvent(const int &ev)
{
  if (ev == SPACE_K || ev == RETURN_K)
    throw CatchIt<bool> (true);
  else
    if (ev != 0 || _init == true)
      {
	if (_init == true)
	  _data.setSound(ERROR);
	_init = false;
	throw CatchIt<ErrorData *> (&_data);
      }
}

void		ErrorOccured::resetInit()
{
  _init = true;
}

void		ErrorOccured::setI(const int &i)
{
  _data.setI(i);
}

void		ErrorOccured::setSound(const bool &c)
{
  File		cnf(".cfg/.cnf");
  File		map(".cfg/map.png");
  File		drop(".cfg/.drop");

  try
    {
      cnf.destroy();
    }
  catch (AError const &e) {}
  try
    {
      map.destroy();
    }
  catch (AError const &e) {}
  try
    {
      drop.destroy();
    }
  catch (AError const &e) {}
  _data.setSoundOn(c);
}

void		ErrorOccured::setError(const std::vector<std::string> &c)
{
  _data.setError(c);
}
