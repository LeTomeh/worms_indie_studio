//
// ManageLauncher.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas ANDRE
// Login   <andre_o@epitech.net>
// 
// Started on  Tue May  3 23:25:26 2016 Thomas ANDRE
// Last update Sun Jun  5 19:42:16 2016 thoma_f
//

#include	"ManageLauncher.hh"

ManageLauncher::ManageLauncher() : _posX(0), _posY(0)
{
  Vector	vec;

  _msg[1] = vec.getVector("Do you want\nto come back\nto the menu ?", '\n');
  _msg[2] = vec.getVector("Do you want\nto start\nthe game ?", '\n');
  _touch[SPACE_K] = '-';
  _touch[A_K] = 'a';
  _touch[B_K] = 'b';
  _touch[C_K] = 'c';
  _touch[D_K] = 'd';
  _touch[E_K] = 'e';
  _touch[F_K] = 'f';
  _touch[G_K] = 'g';
  _touch[H_K] = 'h';
  _touch[I_K] = 'i';
  _touch[J_K] = 'j';
  _touch[K_K] = 'k';
  _touch[L_K] = 'l';
  _touch[M_K] = 'm';
  _touch[N_K] = 'n';
  _touch[O_K] = 'o';
  _touch[P_K] = 'p';
  _touch[Q_K] = 'q';
  _touch[R_K] = 'r';
  _touch[S_K] = 's';
  _touch[T_K] = 't';
  _touch[U_K] = 'u';
  _touch[V_K] = 'v';
  _touch[W_K] = 'w';
  _touch[X_K] = 'x';
  _touch[Y_K] = 'y';
  _touch[Z_K] = 'z';
  _map = false;
}

ManageLauncher::~ManageLauncher() {}

ManageLauncher::ManageLauncher(const ManageLauncher &c) : _launch(c._launch), _team(c._team), _teamIA(c._teamIA), _save(c._save), _posX(c._posX), _init(c._init), _posY(c._posY), _msg(c._msg), _touch(c._touch), _creat(c._creat), _map(c._map) {}

ManageLauncher  &ManageLauncher::operator=(const ManageLauncher &c)
{
  if (this != &c)
    {
      _launch = c._launch;
      _team = c._team;
      _teamIA = c._teamIA;
      _save = c._save;
      _posX = c._posX;
      _init = c._init;
      _posY = c._posY;
      _msg = c._msg;
      _touch = c._touch;
      _creat = c._creat;
      _map = c._map;
    }
  return (*this);
}

void          ManageLauncher::setData(const Data &d)
{
  Vector	vec;

  _posX = 0;
  _posY = 0;
  _init = true;
  _launch.setMusic(d.getMusic());
  _launch.setAmbiance(d.getAmbiance());
  _launch.setEffect(d.getEffect());
  _launch.setSave(d.getSave());
  _launch.setTurnTimeNormal(d.getTurnN());
  _launch.setTurnTimePro(d.getTurnP());
  _launch.setPartyTimeNormal(d.getPartyN());
  _launch.setPartyTimePro(d.getPartyP());
  _launch.setDrops(vec.getVector(d.getDrop(), '\n'));
}

void		ManageLauncher::setSave(const bool &b)
{
  _save = b;
}

void				ManageLauncher::setControl(AMenuData *d)
{
  std::vector<Element>		elem;
  size_t			i;
  size_t			size;
  std::vector<std::string>	ctrlList;
  std::map<std::string, int>	ctrl;

  i = 0;
  elem = d->getElement();
  size = elem.size();
  while (i < size)
    {
      if (elem[i].getType() == Element::CONTROLLER)
	{
	  ctrlList.push_back(elem[i].getValue());
	  ctrl[elem[i].getValue()] = elem[i].getControllerValue();
	}
      i++;
    }
  _launch.setCtrlList(ctrlList);
  _launch.setCtrl(ctrl);
}

bool			ManageLauncher::getSave() const
{
  return (_save);
}

void			ManageLauncher::manageEvent(const int &ev)
{
  Vector		vec;

  if (_team.size() == 0)
    throw CatchIt<std::string> ("game");
  if (_init == true || ev != 0)
    {
      _init = false;
      try
	{
	  catchSelectY(ev);
	  catchReturn(ev);
	  catchRaccourci(ev);
	  if (_posY == 0)
	    {
	      catchSelectX(ev);
	      catchCharacter(ev);
	    }
	}
      catch (CatchIt<bool> const &b) {}
      _team[_posX]->setPos(_posY);
     _team[_posX]->setMax(_team.size());
      _msg[0] = vec.getVector("What name\ndo you want\n" + _team[_posX]->getBasic() + " ?", '\n');
      _team[_posX]->setMap(_msg);
      throw CatchIt<TeamNameMaker *> (_team[_posX]);
    }
}

void			ManageLauncher::catchSelectX(const int &ev)
{
  if (ev == RIGHT_K || ev == LEFT_K)
    {
      if (ev == RIGHT_K)
	_posX++;
      else
	_posX--;
      if (_posX < 0)
	_posX = _team.size() - 1;
      if (_posX >= (int)_team.size())
	_posX = 0;
      if (_posY == 0 && _team.size() != 0)
	_team[_posX]->setEffect(SWI);
      throw CatchIt<bool> (false);
    }
}

void			ManageLauncher::catchReturn(const int &ev)
{
  if (ev == RETURN_K)
    {
      if (_posY == 1)
	throw CatchIt<std::string> ("menu");
      else
	if (_posY == 2)
	  throw CatchIt<std::string> ("game");
      throw CatchIt<bool> (false);
    }
}

void			ManageLauncher::catchSelectY(const int &ev)
{
  if (ev == UP_K || ev == DOWN_K)
    {
      if (ev == UP_K)
	_posY--;
      else
	_posY++;
      if (_posY < 0)
	_posY = 2;
      if (_posY >= 3)
	_posY = 0;
      _team[_posX]->setEffect(SWI);
      throw CatchIt<bool> (false);
    }
}

void			ManageLauncher::catchRaccourci(const int &ev)
{
  if (ev == F12_K || ev == MMAJ_K)
    _posY = 1;
  if (ev == F1_K || ev == BMAJ_K || ev == F2_K || ev == CMAJ_K)
    {
      if (ev == F1_K || ev == BMAJ_K)
	_posY = 1;
      else
	_posY = 2;
      throw CatchIt<bool> (false);
    }
}

void			ManageLauncher::catchCharacter(const int &ev)
{
  std::string		tmp;

  tmp = _team[_posX]->getName();
  if (ev == DEL_K || _touch.find(ev) != _touch.end())
    {
      if (ev == DEL_K)
	tmp = tmp.substr(0, tmp.size() - 1);
      else
	tmp += _touch[ev];
      if (tmp.size() > 20)
	tmp = tmp.substr(0, 20);
      _team[_posX]->setName(tmp);
    }
}

DataLauncher		ManageLauncher::getDataLauncher() const
{
  return (_launch);
}

void			ManageLauncher::reset()
{
  std::vector<TeamNameMaker *>	t;
  DataLauncher			tmp;

  _team = t;
  _teamIA = t;
  _teamList = t;
  _init = true;
  _launch = tmp;
}

void			ManageLauncher::validationTeam()
{
  size_t		x;
  size_t		size;
  size_t		size2;

  x = 0;
  size = _teamIA.size();
  size2 = _team.size();
  while (x < size || x < size2)
    {
      if (x < size)
	_teamList.push_back(_teamIA[x]);
      if (x < size2)
	_teamList.push_back(_team[x]);
      x++;
    }
  checkIntegrity();
  _launch.setTeam(_teamList);
}

void			ManageLauncher::checkIntegrity()
{
  bool			ok;
  size_t		i;
  size_t		x;
  size_t		size;

  ok = true;
  i = 0;
  size = _teamList.size();
  while (i < size)
    {
      x = i + 1;
      while (x < size)
	{
	  if (_teamList[i]->getName() == _teamList[x]->getName())
	    {
	      ok = false;
	      _teamList[x]->setName(_teamList[x]->getName() + "bis");
	      i = size;
	      break;
	    }
	  x++;
	}
      i++;
    }
  if (ok == false)
    checkIntegrity();
}

size_t				ManageLauncher::getTeamSize() const
{
  return (_team.size());
}

void				ManageLauncher::setGameParameter(AMenuData *d)
{
  size_t			i;
  size_t			x;
  size_t			size;
  std::vector<Element>		elem;
  std::vector<TeamNameMaker *>	maker;
  std::vector<TeamNameMaker *>	maker2;
  std::vector<std::string>	list;
  File				file(".cfg/names");
  Vector			vec;
  File				cnf(".cfg/.cnf");
  File				drop(".cfg/.drop");
  std::string			tmp;
  Convert<int>			conv;
  Convert<bool>			convB;

  if (_save == false)
    {
      try
	{
	  drop.destroy();
	}
      catch (AError const &e) {}
      _launch.setDrops(vec.getVector("", '\n'));
      elem = d->getElement();
      i = 0;
      size = elem.size();
      while (i < size)
	{
	  if (elem[i].getValue() == "Number of Player Teams")
	    break;
	  i++;
	}
      if (i < size)
	size = elem[i].getValueNb();
      else
	size = 0;
      i = 0;
      while (i < size)
	{
	  maker.push_back(new TeamNameMaker(++i, list, true));
	  maker[maker.size() - 1]->setSound(_launch.getMusic());
	  maker[maker.size() - 1]->setEffectOn(_launch.getEffect());
	}
      try
	{
	  list = vec.getVector(file.read(), '\n');
	}
      catch (AError const &e)
	{
	  e.notCriticalError();
	  list.push_back("IA_Player");
	}
      x = 0;
      size = elem.size();
      while (x < size)
	{
	  if (elem[x].getValue() == "Number of IA Teams")
	    break;
	  x++;
	}
      if (x < size)
	size = elem[x].getValueNb();
      else
	size = 0;
      _team = maker;
      x = 0;
      while (x++ < size)
	{
	  maker2.push_back(new TeamNameMaker(++i, list, false));
      	  maker2[maker2.size() - 1]->setSound(_launch.getMusic());
	  maker2[maker2.size() - 1]->setEffectOn(_launch.getEffect());
	  }
      _teamIA = maker2;
      i = 0;
      size = elem.size();
      while (i < size)
	{
	  if (elem[i].getValue() == "Number of Worms per Team")
	    break;
	  i++;
	}
      if (i < size)
	_launch.setWormsTeamNb(elem[i].getValueNb());
      else
	_launch.setWormsTeamNb(4);
      i = 0;
      while (i < size)
	{
	  if (elem[i].getType() == Element::SELECTOR)
	    break;
	  i++;
	}
      if (i < size)
	_launch.setMode(elem[i].getPosSelector());
      else
	_launch.setMode(0);
      tmp += "WormsPerTeam " + conv.toString(_launch.getWormsTeamNb()) + "\nMode " + conv.toString(_launch.getMode()) + "\nTurn 0\nTime ";
      if (_launch.getMode() == 0)
	tmp += conv.toString(_launch.getPartyTimeNormal()) + ":00 0\nWaterLvl 0\n";
      else
	tmp += conv.toString(_launch.getPartyTimePro()) + ":00 0\nWaterLvl 0\n";
      try
	{
	  cnf.writeTronc(tmp);
	}
      catch (AError const &e)
	{
	  e.notCriticalError();
	  throw ThrowErrorData(vec.getVector("Launcher\ncan't save configuration game", '\n'));
	}
      i = 0;
      size = elem.size();
      while (i < size)
	{
	  if (elem[i].getValue() == "Map")
	    break;
	  i++;
	}
      if (i < size)
	_map = convB.toNumber(conv.toString(elem[i].getPosSelector()));
      else
	_map = false;
    }
}


void			ManageLauncher::listPosition(std::vector<Position> &pos)
{
  Map			map(".cfg/map.png");
  int			width;
  int			height;
  int			y;
  int			x;

  width = map.getWidth();
  height = map.getHeight();
  y = 20;
  while (y < height)
    {
      x = 0;
      while (x < width)
	{
	  if (y + 1 < height && map.ground(x, y + 1) == true && map.luminosity(x, y + 1) > 50 && map.ground(x, y) == false)
	    pos.push_back(Position(x, y));
	  x++;
	}
      y++;
    }
}

bool			ManageLauncher::epurVec(std::vector<Position> &pos, const int &cpt)
{
  size_t		i;
  size_t		size;
  bool			ck;

  size = pos.size();
  i = 0;
  ck = false;
  while (i < size)
    {
      if (i + 1 < size && pos[i].getY() == pos[i + 1].getY() && pos[i].getX() - pos[i + 1].getX() <= cpt)
	{
	  pos.erase(pos.begin() + i);
	  ck = true;
	  size--;
	}
      else
	i++;
    }
  return (ck);
}
		
void						ManageLauncher::copyPersonalMap()
{
  File						src(".cfg/perso.png");
  File						dest(".cfg/map.png");
  File						cnf(".cfg/.cnf");
  std::string					tmp;
  Vector					vec;

  try
    {
      tmp = src.read();
      if (tmp.empty() == true)
	throw ErrorRead("Personnal Map Empty");
      dest.writeTronc(src.read());
      cnf.writeAppend("assets/back.png");
    }
  catch (const AError &e)
    {
      try
	{
	  src.destroy();
	}
      catch (const AError &e) {}
      try
	{
	  dest.destroy();
	}
      catch (const AError &e) {}
      try
	{
	  cnf.destroy();
	}
      catch (const AError &e) {}
      throw ThrowErrorData(vec.getVector("Launcher\ncan't find your personnal map", '\n'));
    }
}

void						ManageLauncher::loadMap()
{
  size_t					i;
  size_t					x;
  size_t					size;
  size_t					size2;
  std::string					tmp;
  int						sel;
  Convert<int>					conv;
  std::vector<std::vector<MemberData *> >	team;
  std::vector<MemberData *>			players;
  std::vector<Position>				pos;
  std::vector<Position>				pos2;
  File						file(".cfg/.cnf");
  Vector					vec;

  if (_save == false)
    {
      if (_map == false)
	{
	  _creat = new MapCreator;
	  delete (_creat);
	}
      else
	copyPersonalMap();
      i = 0;
      listPosition(pos);
      if (pos.size() < _launch.getWormsTeamNb() * _launch.getTeam().size())
	throw ThrowErrorData(vec.getVector("Launcher\nnot enough position for worms", '\n'));
      pos2 = pos;
      i = 3;
      while (pos2.size() > _launch.getWormsTeamNb() * _launch.getTeam().size())
	{
	  pos = pos2;
	  if (epurVec(pos2, i) == false)
	    break;
	  i += 3;
	}
      i = 0;
      size = _teamList.size();
      size2 = _launch.getWormsTeamNb();
      while (i < size)
	{
	  x = 0;
	  tmp += _teamList[i]->getName() + " 0 ";
	  if (_launch.getMode() == 0)
	    tmp += "30 3;";
	  else
	    tmp += "10 1;";
	  while (x < size2)
	    {
	      sel = rand() % pos.size();
	      if (_launch.getMode() == 0)
		tmp += "100 " + conv.toString(pos[sel].getX()) + " " + conv.toString(pos[sel].getY());
	      else
		tmp += "50 " + conv.toString(pos[sel].getX()) + " " + conv.toString(pos[sel].getY());
		pos.erase(pos.begin() + sel);
	      x++;
	      if (x < size2)
		tmp += ";";
	    }
	  i++;
	  tmp += "\n";
	}
      try
	{
	  file.writeAppend(tmp);
	}
      catch (AError const &e)
	{
	  e.notCriticalError();
	  throw ThrowErrorData(vec.getVector("Launcher\ncan't save configuration game", '\n'));
	}
    }
}

void			ManageLauncher::setWormsTeamNb(const std::string &s)
{
  std::string		tmp;
  String		st;
  Convert<int>		conv;
  Vector		vec;

  if (s.substr(0, 13) != "WormsPerTeam ")
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(worms per team)", '\n'));
  tmp = s.substr(13, s.size());
  if (st.number(tmp) == false || conv.toNumber(tmp) < 4)
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(worms per team)", '\n'));
  _launch.setWormsTeamNb(conv.toNumber(tmp));
}

void			ManageLauncher::setBack(const std::string &s)
{
  Vector		vec;

  if (s != "assets/back.png" && s.substr(0, 14) != ".cfg/map/world")
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(background)", '\n'));
  _launch.setBack(s);
}

void			ManageLauncher::setMode(const std::string &s)
{
  std::string		tmp;
  String		st;
  Convert<int>		conv;
  Vector		vec;

  if (s.size() != 6 || s.substr(0, 5) != "Mode ")
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(mode)", '\n'));
  tmp = s.substr(5, 6);
  if (st.number(tmp) == false)
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(mode)", '\n'));
  _launch.setMode(conv.toNumber(tmp));
}

void			ManageLauncher::setTurn(const std::string &s)
{
  std::string		tmp;
  String		st;
  Convert<int>		conv;
  Vector		vec;

  if (s.substr(0, 5) != "Turn ")
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(turn)", '\n'));
  tmp = s.substr(5, s.size());
  if (st.number(tmp) == false || tmp.size() == 0)
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(turn)", '\n'));
  _launch.setTurn(conv.toNumber(tmp));
}

void			ManageLauncher::setWater(const std::string &s)
{
  std::string		tmp;
  String		st;
  Convert<int>		conv;
  Vector		vec;

  if (s.substr(0, 9) != "WaterLvl ")
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(water)", '\n'));
  tmp = s.substr(9, s.size());
  if (st.number(tmp) == false)
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(water)", '\n'));
  _launch.setWater(conv.toNumber(tmp));
}

void			ManageLauncher::setTime(const std::string &s)
{
  std::string		tmp;
  size_t		find;
  std::string		min;
  std::string		sec;
  String		st;
  Convert<int>		conv;
  Vector		vec;
  std::vector<std::string> t;

  t = vec.getVector(s, ' ');
  if (t.size() != 3 || t[0] != "Time" || st.number(t[2]) == false)
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(time)", '\n'));
  tmp = t[1];
  if ((find = t[1].find(":")) > t[1].size())
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(time)", '\n'));
  min = t[1].substr(0, find);
  sec = t[1].substr(find + 1, t[1].size());
  if (min.size() == 0 || sec.size() == 0
      || st.number(min) == false || st.number(sec) == false)
    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(time)", '\n'));
  _launch.setMin(conv.toNumber(min));
  _launch.setSec(conv.toNumber(sec));
  _launch.setDecal(conv.toNumber(t[2]));
}

void			ManageLauncher::readData()
{
  size_t					i;
  File						f(".cfg/.cnf");
  std::string					file;
  Vector					vec;
  std::vector<std::string>			line;
  std::vector<TeamNameMaker *>			list;
  int						id;
  size_t					size;
  std::vector<std::vector<MemberData *> >	team;
  String					s;
  std::vector<std::string>			null;
  bool						ck;
  int						max;
  size_t					col;
  size_t					colMax;
  Convert<int>					conv;

  try
    {
      file = f.read();
      line = vec.getVector(file, '\n');
      if (line.size() < 8)
	throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(trunc)", '\n'));
    }
  catch (AError const &e)
    {
      e.notCriticalError();
      throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(doesn't exist)", '\n'));
    }
  setWormsTeamNb(line[0]);
  setMode(line[1]);
  setTurn(line[2]);
  setTime(line[3]);
  setWater(line[4]);
  setBack(line[5]);
  line.erase(line.begin());
  line.erase(line.begin());
  line.erase(line.begin());
  line.erase(line.begin());
  line.erase(line.begin());
  line.erase(line.begin());
  if (_launch.getMode() == 0)
    max = 100;
  else
    max = 50;
  id = 1;
  i = 0;
  size = line.size();
  while (i < size)
    {
      std::vector<std::string>	teamLine;
      std::vector<MemberData *>	players;
      TeamNameMaker		*teamName;
      std::vector<std::string>	dataTmp;

      teamLine = vec.getVector(line[i], ';');
      if ((int)teamLine.size() != 1 + _launch.getWormsTeamNb())
	throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(not enought worms in team)", '\n'));
      if (teamLine[0].substr(0, 3) == "IA_")
	ck = false;
      else
	ck = true;
      teamName = new TeamNameMaker(id, null, ck);
      dataTmp = vec.getVector(teamLine[0], ' ');
      if (dataTmp.size() != 4 || s.number(dataTmp[1]) == false
	  || s.number(dataTmp[2]) == false || s.number(dataTmp[3]) == false)
	throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(no team turn set)", '\n'));
      teamName->setName(dataTmp[0]);
      teamName->setTurn(conv.toNumber(dataTmp[1]));
      teamName->setPistol(conv.toNumber(dataTmp[2]));
      teamName->setMissile(conv.toNumber(dataTmp[3]));
      teamName->setMax(max);
      teamLine.erase(teamLine.begin());
      colMax = teamLine.size();
      col = 0;
      while (col < colMax)
	{
	  std::vector<std::string> playerData;

	  playerData = vec.getVector(teamLine[col], ' ');
	  if (playerData.size() != 3 || s.number(playerData[0]) == false
	      || s.number(playerData[1]) == false || s.number(playerData[2]) == false)
	    throw ThrowErrorData(vec.getVector("Launcher\nconfiguration file not ok\n(player not ok)", '\n'));
	  players.push_back(new MemberData(conv.toNumber(playerData[0]),
					   Position(conv.toNumber(playerData[1]),conv.toNumber(playerData[2]))));
	  col++;
	}
      team.push_back(players);
      list.push_back(teamName);
      i++;
      id++;
    }
  _launch.setMemberData(team);
  _launch.setTeam(list);
}
