//
// TeamNameMaker.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas ANDRE
// Login   <andre_o@epitech.net>
// 
// Started on  Wed May  4 09:35:40 2016 Thomas ANDRE
// Last update Sat May 28 19:14:55 2016 Maxime LECOQ
//

#include	"TeamNameMaker.hh"

TeamNameMaker::TeamNameMaker(const int &id, const std::vector<std::string> &v, const bool &st) : _id(id), _status(st)
{
  Convert<int>	conv;

  if (v.size() == 0)
    {
      _name = "Player" + conv.toString(id);
      _basic = _name;
    }
  else
    _name = v[(id + (int)time(NULL)) % v.size()];
  _pos = 0;
  _turn = 0;
  _sound = MENU;
  _soundOn = false;
  _effectOn = false;
  _missile = 5;
  _pistol = 20;
}

TeamNameMaker::~TeamNameMaker() {}

TeamNameMaker::TeamNameMaker(const TeamNameMaker &c) : _id(c._id), _name(c._name), _basic(c._basic), _pos(c._pos), _max(c._max), _map(c._map), _status(c._status), _turn(c._turn), _sound(c._sound), _soundOn(c._soundOn), _effect(c._effect), _effectOn(c._effectOn), _pistol(c._pistol), _missile(c._missile) {}

TeamNameMaker	&TeamNameMaker::operator=(const TeamNameMaker &c)
{
  if (this != &c)
    {
      _id = c._id;
      _name = c._name;
      _basic = c._basic;
      _pos = c._pos;
      _max = c._max;
      _map = c._map;
      _status = c._status;
      _turn = c._turn;
      _sound = c._sound;
      _soundOn = c._soundOn;
      _effect = c._effect;
      _effectOn = c._effectOn;
      _pistol = c._pistol;
      _missile = c._missile;
    }
  return (*this);
}

const std::string	&TeamNameMaker::getName() const
{
  return (_name);
}

const std::string	&TeamNameMaker::getBasic() const
{
  return (_basic);
}

void			TeamNameMaker::setName(const std::string &n)
{
  _name = n;
}

void			TeamNameMaker::setPos(const int &i)
{
  _pos = i;
}

int			TeamNameMaker::getPos() const
{
  return (_pos);
}

unsigned int		TeamNameMaker::getMax() const
{
  return (_max);
}

void			TeamNameMaker::setMax(const size_t &i)
{
  _max = i;
}

void			TeamNameMaker::setMap(std::map<int, std::vector<std::string> > d)
{
  _map = d;
}

std::map<int, std::vector<std::string> > TeamNameMaker::getMap()
{
  return (_map);
}

bool			TeamNameMaker::getStatus() const
{
  return (_status);
}

void			TeamNameMaker::setTurn(const int &i)
{
  _turn = i;
}

int			TeamNameMaker::getTurn() const
{
  return (_turn);
}

const std::string	&TeamNameMaker::getSound() const
{
  return (_sound);
}

bool			TeamNameMaker::soundIsOn() const
{
  return (_soundOn);
}

void			TeamNameMaker::setSound(const bool &c)
{
  _soundOn = c;
}

const std::string	&TeamNameMaker::getEffect() const
{
  return (_effect);
}

bool			TeamNameMaker::effectIsOn() const
{
  return (_effectOn);
}

void			TeamNameMaker::setEffect(const std::string &c)
{
  _effect = c;
}

void			TeamNameMaker::setEffectOn(const bool &c)
{
  _effectOn = c;
}

int			TeamNameMaker::getMissile() const
{
  return (_missile);
}

int			TeamNameMaker::getPistol() const
{
  return (_pistol);
}

void			TeamNameMaker::setPistol(const int &c)
{
  _pistol = c;
}

void			TeamNameMaker::setMissile(const int &c)
{
  _missile = c;
}
