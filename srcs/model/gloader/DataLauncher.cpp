//
// DataLauncher.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas ANDRE
// Login   <andre_o@epitech.net>
// 
// Started on  Tue May  3 23:25:26 2016 Thomas ANDRE
// Last update Sat Jun  4 02:50:28 2016 Maxime LECOQ
//

#include	"DataLauncher.hh"

DataLauncher::DataLauncher()
{
  _partyTimeN = 20;
  _partyTimeP = 10;
  _turnTimeN = 45;
  _turnTimeP = 30;
  _decal = 0;
}

DataLauncher::~DataLauncher() {}

DataLauncher::DataLauncher(const DataLauncher &c) : _music(c._music), _ambiance(c._ambiance), _effect(c._effect), _save(c._save), _ctrlList(c._ctrlList), _ctrl(c._ctrl), _worms(c._worms), _mode(c._mode), _team(c._team), _mem(c._mem), _back(c._back), _turn(c._turn), _min(c._min), _sec(c._sec), _water(c._water), _turnTimeN(c._turnTimeN), _turnTimeP(c._turnTimeP), _partyTimeN(c._partyTimeN), _partyTimeP(c._partyTimeP), _drops(c._drops), _decal(c._decal) {}

DataLauncher  &DataLauncher::operator=(const DataLauncher &c)
{
  if (this != &c)
    {
      _music = c._music;
      _ambiance = c._ambiance;
      _effect = c._effect;
      _save = c._save;
      _ctrlList = c._ctrlList;
      _ctrl = c._ctrl;
      _worms = c._worms;
      _mode = c._mode;
      _team = c._team;
      _mem = c._mem;
      _back = c._back;
      _turn = c._turn;
      _min = c._min;
      _sec = c._sec;
      _water = c._water;
      _turnTimeN = c._turnTimeN;
      _turnTimeP = c._turnTimeP;
      _partyTimeN = c._partyTimeN;
      _partyTimeP = c._partyTimeP;
      _drops = c._drops;
      _decal = c._decal;
    }
  return (*this);
}

void		DataLauncher::setMusic(const bool &c)
{
  _music = c;
}

bool		DataLauncher::getMusic() const
{
  return (_music);
}

void		DataLauncher::setAmbiance(const bool &c)
{
  _ambiance = c;
}

bool		DataLauncher::getAmbiance() const
{
  return (_ambiance);
}

void		DataLauncher::setEffect(const bool &c)
{
  _effect = c;
}

bool		DataLauncher::getEffect() const
{
  return (_effect);
}

void		DataLauncher::setSave(const bool &c)
{
  _save = c;
}

bool		DataLauncher::getSave() const
{
  return (_save);
}

void		DataLauncher::setCtrlList(const std::vector<std::string> &v)
{
  _ctrlList = v;
}

std::vector<std::string>	DataLauncher::getCtrlList() const
{
  return (_ctrlList);
}

void		DataLauncher::setCtrl(std::map<std::string, int> v)
{
  _ctrl = v;
}

std::map<std::string, int>	DataLauncher::getCtrl()
{
  return (_ctrl);
}

void				DataLauncher::setWormsTeamNb(const int &i)
{
  _worms = i;
}

void				DataLauncher::setMode(const int &i)
{
  _mode = i;
}

int				DataLauncher::getWormsTeamNb() const
{
  return (_worms);
}

int				DataLauncher::getMode() const
{
  return (_mode);
}

void				DataLauncher::setTeam(const std::vector<TeamNameMaker *> &team)
{
  _team = team;
}

std::vector<TeamNameMaker *>  DataLauncher::getTeam() const
{
  return (_team);
}

std::vector<std::vector<MemberData *> >	DataLauncher::getMemberData() const
{
  return (_mem);
}

void				DataLauncher::setMemberData(const std::vector<std::vector<MemberData *> >&d)
{
  _mem = d;
}

void			DataLauncher::setBack(const std::string &s)
{
  _back = s;
}

const std::string	&DataLauncher::getBack() const
{
  return (_back);
}

void			DataLauncher::setTurn(const int &i)
{
  _turn = i;
}

int			DataLauncher::getTurn() const
{
  return (_turn);
}

void				DataLauncher::setMin(const int &i)
{
  _min = i;
}

int				DataLauncher::getMin() const
{
  return (_min);
}

void				DataLauncher::setSec(const int &i)
{
  _sec = i;
}

int				DataLauncher::getSec() const
{
  return (_sec);
}

void				DataLauncher::setWater(const int &i)
{
  _water = i;
}

int				DataLauncher::getWater() const
{
  return (_water);
}

void				DataLauncher::setPartyTimePro(const int &i)
{
  _partyTimeP = i;
}

int				DataLauncher::getPartyTimePro() const
{
  return (_partyTimeP);
}

void				DataLauncher::setPartyTimeNormal(const int &i)
{
  _partyTimeN = i;
}

int				DataLauncher::getPartyTimeNormal() const
{
  return (_partyTimeN);
}

void				DataLauncher::setTurnTimePro(const int &i)
{
  _turnTimeP = i;
}

int				DataLauncher::getTurnTimePro() const
{
  return (_turnTimeP);
}

void				DataLauncher::setTurnTimeNormal(const int &i)
{
  _turnTimeN = i;
}

int				DataLauncher::getTurnTimeNormal() const
{
  return (_turnTimeN);
}

void			DataLauncher::setDrops(const std::vector<std::string> &d)
{
  _drops = d;
}

const std::vector<std::string> &DataLauncher::getDrops() const
{
  return (_drops);
}

void			DataLauncher::setDecal(const int &c)
{
  _decal = c;
}

int			DataLauncher::getDecal() const
{
  return (_decal);
}
