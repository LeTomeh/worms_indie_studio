//
// WormsInventory.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sun May 29 12:15:28 2016 Maxime LECOQ
// Last update Sun May 29 13:30:25 2016 Maxime LECOQ
//

#include	"WormsInventory.hh"

WormsInventory::WormsInventory()
{
  _dispatch[RETURN_K] = &WormsInventory::select;
  _dispatch[SPACE_K] = &WormsInventory::select;
  _dispatch[RIGHT_K] = &WormsInventory::right;
  _dispatch[LEFT_K] = &WormsInventory::left;
  _pos = 0;
}

WormsInventory::~WormsInventory() {}

WormsInventory::WormsInventory(const WormsInventory &c) : _loc(c._loc), _dispatch(c._dispatch), _pos(c._pos), _effect(c._effect) {}

WormsInventory &WormsInventory::operator=(const WormsInventory &c)
{
  if (this != &c)
    {
      _loc = c._loc;
      _dispatch = c._dispatch;
      _pos = c._pos;
      _effect = c._effect;
    }
  return (*this);
}

void		WormsInventory::manageEvent(const int &ev, const Team *t)
{
  _loc = "inventory";
  _effect = "";
  if (_dispatch.find(ev) != _dispatch.end())
    {
      (this->*_dispatch[ev])(t);
      throw CatchIt<bool> (true);
    }
}

const std::string	&WormsInventory::getLoc() const
{
  return (_loc);
}

void			WormsInventory::reset()
{
  _loc = "inventory";
  _pos = 0;
}

void			WormsInventory::select(const Team *t)
{
  Inventory		*in;
  std::vector<std::string>	weap;

  in = t->getInventory();
  weap = in->getWeapons();
  if (in->getStuff<IWeapon *>(weap[_pos])->getMun() == 0)
    _effect = FAIL;
  else
    {
      _effect = SELWE;
      _loc = "normal";
    }
}

void			WormsInventory::right(const Team *t)
{
  Inventory		*in;
  std::vector<std::string>	weap;

  in = t->getInventory();
  weap = in->getWeapons();
  _pos++;
  if (_pos >= (int)weap.size())
    _pos = 0;
}

void			WormsInventory::left(const Team *t)
{
  Inventory		*in;
  std::vector<std::string>	weap;

  in = t->getInventory();
  weap = in->getWeapons();
  _pos--;
  if (_pos < 0)
    _pos = weap.size() - 1;
}

int			WormsInventory::getPos() const
{
  return (_pos);
}

const std::string	&WormsInventory::getEffect() const
{
  return (_effect);
}
