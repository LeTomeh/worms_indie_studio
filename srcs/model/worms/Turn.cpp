//
// Turn.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May 18 16:11:05 2016 Maxime LECOQ
// Last update Sun Jun  5 22:35:07 2016 Maxime LECOQ
//

#include	"Worms.hh"

Turn::Turn(std::map<int, std::string> conv)
{
  _pTeam = 0;
  _converter = conv;
  _clock = clock();
  _dispatch["RIGHT"] = &Turn::right;
  _dispatch["LEFT"] = &Turn::left;
  _dispatch["JUMPLEFT"] = &Turn::jumpLeft;
  _dispatch["JUMPRIGHT"] = &Turn::jumpRight;
  _pause = 0;
  _isPause = false;
  _move = false;
  _jump = false;
  _decal = 0;
}

Turn::~Turn() {}

Turn::Turn(const Turn &c) : _pixels(c._pixels), _pTeam(c._pTeam), _converter(c._converter), _clock(c._clock), _dispatch(c._dispatch), _pause(c._pause), _clockPause(c._clockPause), _isPause(c._isPause), _team(c._team), _move(c._move), _jump(c._jump), _decal(c._decal) {}

Turn	&Turn::operator=(const Turn &c)
{
  if (this != &c)
    {
      _pixels = c._pixels;
      _pTeam = c._pTeam;
      _converter = c._converter;
      _clock = c._clock;
      _dispatch = c._dispatch;
      _pause = c._pause;
      _clockPause = c._clockPause;
      _isPause = c._isPause;
      _team = c._team;
      _move = c._move;
      _jump = c._jump;
      _decal = c._decal;
    }
  return (*this);
}

void		Turn::setDecal(const int &dec)
{
  _decal = dec * 2;
}

bool		Turn::getMove() const
{
  return (_move);
}

bool		Turn::getJump() const
{
  return (_jump);
}

void		Turn::setTurn(const int &t)
{
  _pTeam = t;
}

int		Turn::getTeamTurn() const
{
  return (_pTeam);
}

void		Turn::startPause(const std::clock_t &c)
{
  _clockPause = c;
  _isPause = true;
}

void		Turn::endPause(const std::clock_t &c)
{
  _pause += (int)((((double)c - (double)_clockPause) / CLOCKS_PER_SEC) / 2);
  _isPause = false;
}

void		Turn::resetPause()
{
  _pause = 0;
}

bool            Turn::wormsIn(const Position &p)
{
  size_t        i;
  size_t        size;
  size_t        x;
  size_t        size2;
  std::vector<Player *> players;

  i = 0;
  size = _team.size();
  while (i < size)
    {
      x = 0;
      players = _team[i]->getPlayers();
      size2 = players.size();
      while (x < size2)
        {
          if (p.getX() == players[x]->getPosition().getX() && p.getY() == players[x]->getPosition().getY())
            return (true);
          x++;
	}
      i++;
    }
  return (false);
}

void		Turn::checkJump(std::vector<Team *> &team, Map *map)
{
  Player	*player;

  _team = team;
  team[_pTeam]->checkPos();
  if (team[_pTeam]->getPlayers().size() == 0)
    return;
  player = team[_pTeam]->getPlayers()[team[_pTeam]->getPos()];
  if (player != NULL && player->getJump().size() != 0)
    continueJump(player, map);
}

void		Turn::playTurn(std::vector<Team *> &team, Map *map, const int &ev)
{
  Player	*player;

  _team = team;
  _move = false;
  _jump = false;
  player = team[_pTeam]->getPlayers()[team[_pTeam]->getPos()];
  if (_converter.find(ev) != _converter.end() && _dispatch.find(_converter[ev]) != _dispatch.end())
    (this->*_dispatch[_converter[ev]])(player, map);
}

void		Turn::checkTime(int time, const std::clock_t &c)
{
  double	tmp;
  int		ret;

  if (_isPause == true)
    tmp = ((((double)c - (double)_clockPause) / CLOCKS_PER_SEC) / 2);
  else
    tmp = 0.0;
  if (((time * 2) - (int)((((double)c - (double)_clock) / CLOCKS_PER_SEC) / 2) + _pause + (int)tmp - _decal) / 2 == 0)
    {
      _decal = 0;
      throw CatchIt<bool> (true);
    }
  else
    {
      ret = ((time * 2) - (int)((((double)c - (double)_clock) / CLOCKS_PER_SEC) / 2) + _pause + (int)tmp - _decal) / 2;
      throw CatchIt<int> (ret);
    }
}

void		Turn::moveTo(Position &prev, const Position &pos, Map *m)
{
  if (pos.getY() > 0 && m->ground(pos.getX(), pos.getY() - 1) == false && pos.getX() >= 0 && pos.getX() <= m->getWidth())
    {
      prev.setX(pos.getX());
      prev.setY(pos.getY());
      throw CatchIt<bool> (true);
    }
}

void		Turn::right(Player *p, Map *m)
{
  Position	prev;

  if (p->getFall() != 0)
    {
      p->changeDir(true);
      throw CatchIt<bool> (true);
    }
  prev = p->getPosition();
  try
    {
      moveTo(prev, Position(prev.getX() + 2, prev.getY()), m);
      moveTo(prev, Position(prev.getX() + 3, prev.getY()), m);
      moveTo(prev, Position(prev.getX() + 1, prev.getY()), m);
      moveTo(prev, Position(prev.getX() + 2, prev.getY() - 1), m);
      moveTo(prev, Position(prev.getX() + 3, prev.getY() - 1), m);
      moveTo(prev, Position(prev.getX() + 1, prev.getY() - 1), m);
      moveTo(prev, Position(prev.getX() + 2, prev.getY() - 2), m);
      moveTo(prev, Position(prev.getX() + 3, prev.getY() - 2), m);
      moveTo(prev, Position(prev.getX() + 1, prev.getY() - 2), m);
      moveTo(prev, Position(prev.getX() + 2, prev.getY() + 1), m);
      moveTo(prev, Position(prev.getX() + 3, prev.getY() + 1), m);
      moveTo(prev, Position(prev.getX() + 1, prev.getY() + 1), m);
      moveTo(prev, Position(prev.getX() + 2, prev.getY() + 2), m);
      moveTo(prev, Position(prev.getX() + 3, prev.getY() + 2), m);
      moveTo(prev, Position(prev.getX() + 1, prev.getY() + 2), m);
      moveTo(prev, Position(prev.getX() + 2, prev.getY() + 3), m);
      moveTo(prev, Position(prev.getX() + 3, prev.getY() + 3), m);
      moveTo(prev, Position(prev.getX() + 1, prev.getY() + 3), m);
      moveTo(prev, Position(prev.getX() + 2, prev.getY() - 3), m);
      moveTo(prev, Position(prev.getX() + 3, prev.getY() - 3), m);
      moveTo(prev, Position(prev.getX() + 1, prev.getY() - 3), m);
    }
  catch (CatchIt<bool> const &b)
    {
      _move = true;
      p->setPosition(prev);
      p->changeDir(true);
      if (b.getIt() == false)
	throw CatchIt<bool> (b.getIt());
    }
  throw CatchIt<bool> (true);
}

void		Turn::left(Player *p, Map *m)
{
  Position	prev;

  if (p->getFall() != 0)
    {
      p->changeDir(false);
      throw CatchIt<bool> (true);
    }
  prev = p->getPosition();
  try
    {
      moveTo(prev, Position(prev.getX() - 2, prev.getY()), m);
      moveTo(prev, Position(prev.getX() - 3, prev.getY()), m);
      moveTo(prev, Position(prev.getX() - 1, prev.getY()), m);
      moveTo(prev, Position(prev.getX() - 2, prev.getY() - 1), m);
      moveTo(prev, Position(prev.getX() - 3, prev.getY() - 1), m);
      moveTo(prev, Position(prev.getX() - 1, prev.getY() - 1), m);
      moveTo(prev, Position(prev.getX() - 2, prev.getY() - 2), m);
      moveTo(prev, Position(prev.getX() - 3, prev.getY() - 2), m);
      moveTo(prev, Position(prev.getX() - 1, prev.getY() - 2), m);
      moveTo(prev, Position(prev.getX() - 2, prev.getY() + 1), m);
      moveTo(prev, Position(prev.getX() - 3, prev.getY() + 1), m);
      moveTo(prev, Position(prev.getX() - 1, prev.getY() + 1), m);
      moveTo(prev, Position(prev.getX() - 2, prev.getY() + 2), m);
      moveTo(prev, Position(prev.getX() - 3, prev.getY() + 2), m);
      moveTo(prev, Position(prev.getX() - 1, prev.getY() + 2), m);
      moveTo(prev, Position(prev.getX() - 2, prev.getY() + 3), m);
      moveTo(prev, Position(prev.getX() - 3, prev.getY() + 3), m);
      moveTo(prev, Position(prev.getX() - 1, prev.getY() + 3), m);
      moveTo(prev, Position(prev.getX() - 2, prev.getY() - 3), m);
      moveTo(prev, Position(prev.getX() - 3, prev.getY() - 3), m);
      moveTo(prev, Position(prev.getX() - 1, prev.getY() - 3), m);
    }
  catch (CatchIt<bool> const &b)
    {
      _move = true;
      p->setPosition(prev);
      p->changeDir(false);
      if (b.getIt() == false)
	throw CatchIt<bool> (b.getIt());
    }
  m->resetPosition(prev, p->getPosition());
  throw CatchIt<bool> (true);
}

void		Turn::nextTurn(const std::vector<Team *> &team)
{
  std::clock_t	c;

  c = clock();
  _clock = c;
  _clockPause = c;
  _pause = 0;
  _pTeam++;
  if (_pTeam >= team.size())
    _pTeam = 0;
  team[_pTeam]->checkPos();
  _decal = 0;
}

std::clock_t	Turn::getClock() const
{
  return (_clock);
}

void		Turn::jumpLeft(Player *p, Map *m)
{
  if (p->getFall() == 0 && p->getJump().size() == 0 && p->getFall() == 0)
    {
      p->setJump("LEFT");
      p->changeDir(false);
      (void)m;
      _jump = true;
      throw CatchIt<bool> (true);
    }
}

void		Turn::jumpRight(Player *p, Map *m)
{
  if (p->getFall() == 0 && p->getJump().size() == 0 && p->getFall() == 0)
    {
      p->setJump("RIGHT");
      p->changeDir(true);
      (void)m;
      _jump = true;
      throw CatchIt<bool> (true);
    }
}

void		Turn::continueJump(Player *p, Map *m)
{
  Position	prev;
  int		i;
  int		i2;
  bool		ck;

  if (p->getJump() == "LEFT")
    i = -1;
  else
    i = 1;
  i2 = -1;
  prev = p->getPosition();
  ck = false;
  try
    {
      moveTo(prev, Position(prev.getX() + i * 5, prev.getY() + i2 * 5), m);
      moveTo(prev, Position(prev.getX() + i * 4, prev.getY() + i2 * 5), m);
      moveTo(prev, Position(prev.getX() + i * 3, prev.getY() + i2 * 5), m);
      moveTo(prev, Position(prev.getX() + i * 2, prev.getY() + i2 * 5), m);
      moveTo(prev, Position(prev.getX() + i * 1, prev.getY() + i2 * 5), m);
      moveTo(prev, Position(prev.getX() + i * 5, prev.getY() + i2 * 4), m);
      moveTo(prev, Position(prev.getX() + i * 4, prev.getY() + i2 * 4), m);
      moveTo(prev, Position(prev.getX() + i * 3, prev.getY() + i2 * 4), m);
      moveTo(prev, Position(prev.getX() + i * 2, prev.getY() + i2 * 4), m);
      moveTo(prev, Position(prev.getX() + i * 1, prev.getY() + i2 * 4), m);
      moveTo(prev, Position(prev.getX() + i * 5, prev.getY() + i2 * 3), m);
      moveTo(prev, Position(prev.getX() + i * 4, prev.getY() + i2 * 3), m);
      moveTo(prev, Position(prev.getX() + i * 3, prev.getY() + i2 * 3), m);
      moveTo(prev, Position(prev.getX() + i * 2, prev.getY() + i2 * 3), m);
      moveTo(prev, Position(prev.getX() + i * 1, prev.getY() + i2 * 3), m);
      moveTo(prev, Position(prev.getX() + i * 4, prev.getY() + i2 * 2), m);
      moveTo(prev, Position(prev.getX() + i * 3, prev.getY() + i2 * 2), m);
      moveTo(prev, Position(prev.getX() + i * 2, prev.getY() + i2 * 2), m);
      moveTo(prev, Position(prev.getX() + i * 1, prev.getY() + i2 * 2), m);
      moveTo(prev, Position(prev.getX() + i * 3, prev.getY() + i2 * 1), m);
      moveTo(prev, Position(prev.getX() + i * 2, prev.getY() + i2 * 1), m);
      moveTo(prev, Position(prev.getX() + i * 1, prev.getY() + i2 * 1), m);
    }
  catch (CatchIt<bool> const &b)
    {
      ck = true;
      p->setPosition(prev);
      p->jump();
      if (b.getIt() == false)
	throw CatchIt<bool> (b.getIt());
    }
  if (ck == false || p->getJumpCpt() > 7)
    p->endJump();
}
