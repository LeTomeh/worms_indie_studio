//
// Clock.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May 18 16:11:05 2016 Maxime LECOQ
// Last update Fri Jun  3 10:48:12 2016 Maxime LECOQ
//

#include	"Worms.hh"

Clock::Clock()
{
  _pause = 0;
  _isPause = false;
  _min = 0;
  _sec = 0;
}

Clock::~Clock() {}

Clock::Clock(const Clock &c) : _clock(c._clock), _pause(c._pause), _clockPause(c._clockPause), _isPause(c._isPause), _min(c._min), _sec(c._sec), _clockEnd(c._clockEnd), _stepEnd(c._stepEnd), _miss(c._miss) {}

Clock	&Clock::operator=(const Clock &c)
{
  if (this != &c)
    {
      _clock = c._clock;
      _pause = c._pause;
      _clockPause = c._clockPause;
      _isPause = c._isPause;
      _min = c._min;
      _sec = c._sec;
      _clockEnd = c._clockEnd;
      _stepEnd = c._stepEnd;
      _miss = c._miss;
    }
  return (*this);
}

void		Clock::resetClock()
{
  _stepEnd = 0;
}

int		Clock::getMin() const
{
  return (_min);
}

int		Clock::getSec() const
{
  return (_sec);
}

void		Clock::startPause(const std::clock_t &c)
{
  _clockPause = c;
  _isPause = true;
}

void		Clock::endPause(const std::clock_t &c)
{
  _pause += (int)((((double)c - (double)_clockPause) / CLOCKS_PER_SEC) / 2);
  _isPause = false;
}

void		Clock::resetPause()
{
  _pause = 0;
  _stepEnd = -1;
}

void		Clock::checkClock(const int &time, const std::clock_t &c)
{
  double	tmp;
  int		ret;

  if (_isPause == true)
    tmp = (((double)c - (double)_clockPause) / CLOCKS_PER_SEC) / 2;
  else
    tmp = 0.0;
  ret = (((time - _miss) * 2) - (int)((((double)c - (double)_clock) / CLOCKS_PER_SEC) / 2) + _pause + (int)tmp) / 2;
  if (ret >= 0)
    {
      _min = ret / 60;
      _sec = ret - (_min * 60);
      _stepEnd = -1;
    }
  else
    {
      if (_stepEnd == -1)
	_clockEnd = clock();
      _min = 0;
      _sec = 0;
      _stepEnd = (int)((((double)c - (double)_clockEnd) / CLOCKS_PER_SEC) / 2) / 2;
    }
}

int		Clock::getLvlWater() const
{
  return (_stepEnd);
}

void		Clock::setClock(const std::clock_t &c)
{
  _clock = c;
}

void		Clock::setMiss(const int &c)
{
  _miss = c;
}
