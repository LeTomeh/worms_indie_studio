//
// Worms.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:01:55 2016 Maxime LECOQ
// Last update Sun Jun  5 16:32:16 2016 Maxime LECOQ
//

#include	"Worms.hh"

Worms::Worms()
{
  _gameCnf = false;
  _gameMap = false;
  _thMapDestroy.run(runDestroy, this);
  _view = new View;
  _view->setMap(".cfg/map.png");
  _init = true;
  _observe = false;
  _run = true;
  _game = false;
  _event = 0;
  _end = false;
  _loc = "normal";
  _position["normal"] = &Worms::normal;
  _position["pause"] = &Worms::pause;
  _position["inventory"] = &Worms::inventory;
  _sel = 0;
  _turn = NULL;
  _begin.push_back(S1);
  _begin.push_back(S2);
  _begin.push_back(S3);
  _begin.push_back(S4);
  _begin.push_back(S5);
  _begin.push_back(S6);
  _begin.push_back(S7);
  _begin.push_back(S8);
  _begin.push_back(S9);
  _begin.push_back(S10);
  _water = new ManageWaterUp(_view);
  _managePlayer = new ManagePlayer(_view);
  _drop = new ManageDrop(_view);
  _prior = false;
  _wind = 0;
}

Worms::~Worms()
{
  _gameCnf = false;
  _gameMap = false;
  try
    {
      if (_thMap.getStatus() == IThread::RUN)
	_thMap.waitDeath();
    }
  catch (AError const &e) {}
  try
    {
      if (_thCnf.getStatus() == IThread::RUN)
      _thCnf.waitDeath();
    }
  catch (AError const &e) {}
  try
    {
      if (_thMapDestroy.getStatus() == IThread::RUN)
      _thMapDestroy.waitDeath();
    }
  catch (AError const &e) {}
  try
    {
      if (_thDrop.getStatus() == IThread::RUN)
	_thDrop.waitDeath();
    }
  catch (AError const &e) {}
  delete (_view);
  if (_turn != NULL)
    delete (_turn);
  delete (_drop);
  delete (_water);
  delete (_managePlayer);
}

Worms::Worms(const Worms &c) : _team(c._team), _teamSave(c._teamSave), _view(c._view), _map(c._map), _init(c._init), _observer(c._observer), _observe(c._observe), _run(c._run), _game(c._game), _location(c._location), _event(c._event), _converter(c._converter), _end(c._end), _loc(c._loc), _position(c._position), _sel(c._sel), _clock(c._clock), _gameTime(c._gameTime), _thMap(c._thMap), _gameMap(c._gameMap), _thCnf(c._thCnf), _gameCnf(c._gameCnf), _thMapDestroy(c._thMapDestroy), _inventory(c._inventory), _water(c._water), _ia(c._ia), _prior(c._prior), _revConverter(c._revConverter), _wind(c._wind), _drop(c._drop), _thDrop(c._thDrop) {}

Worms	&Worms::operator=(const Worms &c)
{
  if (this != &c)
    {
      _team = c._team;
      _teamSave = c._teamSave;
      _view = c._view;
      _map = c._map;
      _init = c._init;
      _observer = c._observer;
      _observe = c._observe;
      _run = c._run;
      _game = c._game;
      _location = c._location;
      _event = c._event;
      _converter = c._converter;
      _revConverter = c._revConverter;
      _end = c._end;
      _loc = c._loc;
      _position = c._position;
      _sel = c._sel;
      _clock = c._clock;
      _gameTime = c._gameTime; 
      _thMap = c._thMap;
      _gameMap = c._gameMap;
      _thCnf = c._thCnf;
      _gameCnf = c._gameCnf;
      _thMapDestroy = c._thMapDestroy;
      _inventory = c._inventory;
      _water = c._water;
      _ia = c._ia;
      _prior = c._prior;
      _wind = c._wind;
      _drop = c._drop;
      _thDrop = c._thDrop;
    }
  return (*this);
}

void				Worms::setDataLauncher(DataLauncher d)
{
  std::vector<Team *>		team;
  std::vector<TeamNameMaker *>  t;
  std::vector<std::vector<MemberData *>	> mem;
  size_t			i;
  size_t			size;
  std::vector<std::string>	ctrlList;
  std::map<std::string, int>	ctrl;
  Convert<size_t>		conv;
  std::vector<std::string>	drop;
  Vector			vec;
  std::vector<std::string>	tmp;
  String			s;

  ctrl = d.getCtrl();
  ctrlList = d.getCtrlList();
  i = 0;
  size = ctrlList.size();
  while (i < size)
    {
      _converter[ctrl[ctrlList[i]]] = ctrlList[i];
      _revConverter[ctrlList[i]] = ctrl[ctrlList[i]];
      i++;
    }
  _view->setCtrl(d.getCtrl());
  _view->setCtrlList(d.getCtrlList());
  t = d.getTeam();
  mem = d.getMemberData();
  i = 0;
  size = t.size();
  while (i < size)
    {
      team.push_back(new Team(t[i], d.getMode(), d.getWormsTeamNb(), mem[i]));
      if (team[team.size() - 1]->getPlayers().size() == 0)
	team.erase(team.begin() + team.size() - 1);
      i++;
    }
  _team = team;
  _teamSave = team;
  i = 0;
  size = _teamSave.size();
  while (i < size)
    {
      if (_teamSave[i]->getStatus() == false)
	{
	  _teamSave.erase(_teamSave.begin() + i);
	  size--;
	}
      else
	i++;
    }
  _view->setBack(d.getBack());
  _sel = 0;
  _view->setData("< " + _team[_sel]->getName() + " -> life : " + _team[_sel]->getLifes() + "%, " + conv.toString(_team[_sel]->getPlayers().size()) + " worms >");
  if (_turn != NULL)
    delete (_turn);
  _turn = new Turn(_converter);
  if (d.getTurn() < (int)_team.size())
    _turn->setTurn(d.getTurn());
  _clock.resetClock();
  _clock.setClock(_turn->getClock());
  _view->setTeamTurn(_team[_turn->getTeamTurn()]->getName() + " turn");
  _wormsPerTeam = d.getWormsTeamNb();
  _mode = d.getMode();
  _back = d.getBack();
  if (_mode == 0)
    {
      _time = d.getTurnTimeNormal();
      _gameTime = d.getPartyTimeNormal() * 60;
    }
  else
    {
      _time = d.getTurnTimePro();
      _gameTime = d.getPartyTimePro() * 60;
    }
  _view->setTime(_time);
  _view->setWaterMin(d.getWater());
  _clock.resetPause();
  _clock.setMiss(_gameTime - ((d.getMin() * 60) + d.getSec()));
  _view->setEffectOn(d.getEffect());
  _view->setSound(d.getMusic());
  _view->setEdited(true);
  _drop->reset();
  drop = d.getDrops();
  i = 0;
  size = drop.size();
  while (i < size)
    {
      tmp = vec.getVector(drop[i], ' ');
      if (tmp.size() == 3 && s.number(tmp[1]) == true && s.number(tmp[2]) == true &&
	  (tmp[0] == "HealPlayer" || tmp[0] == "HealTeam" || tmp[0] == "PoisonTeam"
	   || tmp[0] == "PoisonPlayer" || tmp[0] == "TeleportPlus" || tmp[0] == "PistolPlus"))
	_drop->addDrop(tmp[0], Position(conv.toNumber(tmp[1]), conv.toNumber(tmp[2])));
      i++;
    }
  if (d.getDecal() < _time)
    _turn->setDecal(d.getDecal());
}

void		Worms::openMap()
{
  Projectile	*bul;

  _loc = "normal";
  _map = new Map(".cfg/map.png");
  _end = false;
  _pause.init();
  _view->setPosPause(0);
  _gameCnf = true;
  _gameMap = true;
  _thMap.run(runMap, this);
  _thCnf.run(runCnf, this);
  _thDrop.run(runDrop, this);
  _thMapDestroy.run(runDestroy, this);
  _view->setEffect(_begin[rand() % _begin.size()]);
  _view->setEffect(START);
  _inventory.reset();
  _water->setter(_map, _turn, _time);
  _managePlayer->setTouch(_converter);
  _prior = false;
  _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->take(true);
  _drop->checkPos(_map);
  bul = _view->getProjectile();
  bul->freeProjectile();
  bul->active(false);
  _view->setProjectile(bul);
  _init = true;
  _view->setEdited(true);
}

void		Worms::destroyMap()
{
  File		map(".cfg/map");
  File		cnf(".cfg/.cnf");
  File		drop(".cfg/.drop");

  try
    {
      map.destroy();
    }
  catch (const AError &e) {}
  try
    {
      cnf.destroy();
    }
  catch (const AError &e) {}
  try
    {
      drop.destroy();
    }
  catch (const AError &e) {}
}

void		Worms::closeMap()
{
  _gameCnf = false;
  _gameMap = false;
  try
    {
      _thMap.waitDeath();
    }
  catch (AError const &e) {}
  try
    {
      _thCnf.waitDeath();
    }
  catch (AError const &e) {}
  try
    {
      _thMapDestroy.waitDeath();
    }
  catch (AError const &e) {}
  try
    {
      _thDrop.waitDeath();
    }
  catch (AError const &e) {}
  delete (_map);
  _view->setEdited(true);
}

void		Worms::setEvent(const int &ev)
{
  if (ev != 0 || _game == false)
    _event = ev;
}

void		Worms::resetInit()
{
  _init = true;
}

void		Worms::runObservable()
{
}

void          Worms::setObserver(IObserver *ob)
{
  _observe = true;
  _observer = ob;
}

void          Worms::unSetObserver()
{
  _observe = false;
  _observer = NULL;
}

void		Worms::setRun(const bool &c)
{
  _run = c;
}

void		Worms::setGame(const bool &c)
{
  _game = c;
  if (c == true)
    _location = "game";
}

const std::string	&Worms::getLocation() const
{
  return (_location);
}

bool			Worms::getEnd() const
{
  return (_end);
}

void			Worms::deathPlayer(const int &x, const int &i)
{
  if (_team[i]->deletePlayer(x) == true)
    {
      _view->setEffect(DEAD);
      _turn->nextTurn(_team);
      _view->setTeamTurn(_team[_turn->getTeamTurn()]->getName() + " turn");
      _view->setTime(_time);
      _turn->resetPause();
    }
}

int		Worms::getTeam(const std::string &name)
{
  size_t	i;
  size_t	size;

  i = 0;
  size = _team.size();
  while (i < size)
    {
      if (_team[i]->getName() == name)
	return (i);
      i++;
    }
  return (0);
}

void		Worms::checkGravityPlayer(bool &ck)
{
  size_t		i;
  size_t		x;
  std::vector<Player *>	players;
  Position		tmp;
  int			z;
  int			lifeTmp;

  i = 0;
  while (i < _team.size())
    {
      x = 0;
      players = _team[i]->getPlayers();
      while (x < players.size())
	{
	  z = 0;
	  if (players[x]->getLife() <= 0)
	    {
	      deathPlayer(x, i);
	      ck = true;
	      break;
	    }
	  if (players[x]->getJump().size() == 0)
	    {
	      tmp = players[x]->getPosition();
	      while (z < 5)
		{
		  if (tmp.getY() + 1 < _map->getHeight() && _map->ground(tmp.getX(), tmp.getY() + 1) == false
		      && wormsIn(tmp.getX(), tmp.getY() + 1) == false)
		    {
		      players[x]->fall();
		      tmp.setY(tmp.getY() + 1);
		      players[x]->setPosition(tmp);
		      if (tmp.getY() == _map->getHeight())
			{
			  deathPlayer(x, i);
			  x = 0;
			  ck = true;
			  break;
			}
		      if (tmp.getY() == _map->getHeight() || _map->getWaterAt(tmp, _view->getWaterMin() + _view->getWaterLvl()) == true)
			{
			  deathPlayer(x, i);
			  x = 0;
			  ck = true;
			  break;
			}
		      ck = true;
		    }
		  else
		    {
		      lifeTmp = players[x]->getLife();
		      players[x]->endFall();
		      if ((lifeTmp != players[x]->getLife() && (players[x]->getLife() <= 0 || tmp.getY() >= _map->getHeight() - 1)) || tmp.getY() + 1 >= _map->getHeight())
			{
			  deathPlayer(x, i);
			  x = 0;
			  ck = true;
			  break;
			}
		      else
			if (lifeTmp - players[x]->getLife() > 5)
			  {
			    if (lifeTmp - players[x]->getLife() > 10)
			      _view->setEffect(FALL);
			    if ((int)x == _team[_turn->getTeamTurn()]->getPos() && (int)i == _turn->getTeamTurn())
			      passTurn();
			  }
		    }
		  z++;
		}
	      if (z < 5)
		break;
	    }
	  x++;
	}
      i++;
    }
}

void			Worms::passTurn()
{
  _loc = "normal";
  if (_turn->getTeamTurn() < (int)_team.size())
    {
      _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->setSprite(FRONT);
      _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->endJump();
      _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->getCursor()->setActive(false);
      _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->setJump("");
      _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->take(false);
      _team[_turn->getTeamTurn()]->nextPos();
    }
  _turn->nextTurn(_team);
  _view->setEffect(ENDTURN);
  _view->setTeamTurn(_team[_turn->getTeamTurn()]->getName() + " turn");
  _view->setTime(_time);
  _turn->resetPause();
  _wind = (rand() % 6) - 2;
  if (_wind == 3)
    _wind = 0;
  _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->changeDir(false);
  _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->take(true);
  _drop->askDrop(_map->getWidth(), _team.size());
}

void		Worms::clockChecks(bool &ck)
{
  std::clock_t	clo;

  clo = clock();
  _clock.checkClock(_gameTime, clo);
  if (_view->getMin() != _clock.getMin() ||
      _view->getSec() != _clock.getSec() || _view->getWaterLvl() != _clock.getLvlWater())
    {
      _view->setMin(_clock.getMin());
      _view->setSec(_clock.getSec());
      _view->setWaterLvl(_clock.getLvlWater());
      if (_view->getWaterLvl() + _view->getWaterMin())
	_water->checkWaterDeath(_team);
      ck = true;
    }
  try
    {
      _turn->checkTime(_time, clo);
    }
  catch (CatchIt<bool> &b)
    {
      if (b.getIt() == true)
	{
	  passTurn();
	  ck = true;
	}
      else
	throw (CatchIt<bool> (b.getIt()));
    }
  catch (CatchIt<int> &i)
    {
      if (_view->getTime() != i.getIt() && _loc != "pause")
	ck = true;
      _view->setTime(i.getIt());
    }
}

void		Worms::epurTeam()
{
  size_t	i;
  size_t	size;

  i = 0;
  size = _team.size();
  while (i < size)
    {
      if (_team[i]->getPlayers().size() == 0)
	{
	  _team.erase(_team.begin() + i);
	  if (_turn->getTeamTurn() == (int)i)
	    {
	      _view->setEffect(ENDTURN);
	      _turn->nextTurn(_team);
	      _view->setTeamTurn(_team[_turn->getTeamTurn()]->getName() + " turn");
	      _view->setTime(_time);
	      _turn->resetPause();
	    }
	  size--;
	}
      else
	{
	  if ((int)i == _turn->getTeamTurn())
	    _team[i]->checkArrow(true);
	  else
	    _team[i]->checkArrow(false);
	  i++;
	}
    }
}

void		Worms::executeNormal(bool &ck)
{
  std::string	tmp;

  if (_team[_turn->getTeamTurn()]->getStatus() == false && _loc != "pause")
    {
      try
	{
	  tmp = _ia.playTurn(_team, _turn->getTeamTurn() , _map);
	  if (tmp.size() && _revConverter.find(tmp) != _revConverter.end())
	    _event = _revConverter[tmp];
	  else
	    _event = 0;
	}
      catch (CatchIt<int> &ev) { _event = ev.getIt(); }
    }
  if (_event != 0)
    {
      try
	{
	  if (_position.find(_loc) != _position.end())
	    (this->*_position[_loc])();
	  else
	    throw CatchIt<bool> (false);
	}
      catch (CatchIt<bool> &b)
	{
	  if (b.getIt() == false)
	    throw CatchIt<bool> (false);
	  ck = true;
	}
      _event = 0;
    }
  clockChecks(ck);
}

bool		Worms::checkEnd()
{
  if (_team.size() <= 1)
    {
      if (_team.size() == 1)
	_end = true;
      _location = "menu";
      return (true);
    }
  return (false);
}

void		Worms::manageEvent()
{
  bool		ck;
  Convert<int>	conv;

  ck = false;
  if (checkEnd() == true)
    return;
  try
    {
      manageMenu();
    }
  catch (CatchIt<bool> &b) { return; }
  try
    {
      if (_loc == "normal" || _loc == "inventory")
	manageSwitch();
      managePause();
      if (_prior == false)
	executeNormal(ck);
      else
	{
	  _team[_turn->getTeamTurn()]->checkPos();
	  _managePlayer->playAnimation(_team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()], _map, _team, _drop);
	  if (_managePlayer->getPrior() == false)
	    {
	      _prior = false;
	      std::clock_t	cl;
	      cl = clock();
	      _turn->endPause(cl);
	      _clock.endPause(cl);
	      epurTeam();
	      if (checkEnd() == true)
		return;
	      passTurn();
	    }
	  ck = true;
	}
      _turn->checkJump(_team, _map);
      if (_loc == "normal" || _loc == "inventory")
	{
	  checkGravityPlayer(ck);
	  _drop->gravity(_map, ck);
	  _drop->take(_team);
	  epurTeam();
	}
      if (checkEnd() == true)
	return;
    }
  catch (CatchIt<bool> &b)
    {
      _event = 0;
      if (b.getIt() == false)
	throw CatchIt<bool> (false);
      ck = true;
    }
  if (ck == true || _init == true)
    {
      _view->setDrop(_drop->getDrops());
      _view->setWind(_wind);
      _view->setLocation(_loc);
      if (_init == true)
	_view->setEdited(true);
      _init = false;
      _view->setTeam(_team);
      if (_sel >= (int)_team.size())
	_sel = 0;
      _view->setData("< " + _team[_sel]->getName() + " -> life : " + _team[_sel]->getLifes() + "%, " + conv.toString(_team[_sel]->getPlayers().size()) + " worms >");
      while (_view->getEditIt() == true);
      if (_observe == true && _location != "menu")
	_observer->notifier(_view);
    }
}

void			Worms::manageMenu()
{
  if (_event == MMAJ_K)
    {
      _location = "menu";
      throw CatchIt<bool> (true);
    }
}

void			Worms::manageInventory()
{
  if (_converter.find(_event) != _converter.end() && _converter[_event] == "INVENTORY")
    {
      _loc = "inventory";
      _view->setInventory(_team[_turn->getTeamTurn()]->getInventory());
      _view->setInvPos(_inventory.getPos());
      throw CatchIt<bool> (true);
    }
}

void			Worms::normal()
{
  manageNextTurn();
  manageInventory();
  try
    {
      _team[_turn->getTeamTurn()]->checkPos();
      _managePlayer->playTurn(_team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()], _map, _event, _wind, _team);
      try
	{
	  _turn->playTurn(_team, _map, _event);
	}
      catch (CatchIt<bool> &b)
	{
	  if (_turn->getMove() == true && _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->getFall() == 0)
	    _view->setEffect(WALK);
	  else
	    if (_turn->getJump() == true && _team[_turn->getTeamTurn()]->getPlayers()[_team[_turn->getTeamTurn()]->getPos()]->getFall() == 0)
	      _view->setEffect(JUMP);
	  throw (CatchIt<bool> (b.getIt()));
	}
    }
  catch (CatchIt<bool> &b)
    {
      if (_managePlayer->getTurn() == true)
	passTurn();
      else
	if (_managePlayer->getPrior() == true)
	  {
	    std::clock_t clo;
	    clo = clock();
	    _turn->startPause(clo);
	    _clock.startPause(clo);
	    _prior = true;
	  }
      throw (CatchIt<bool> (b.getIt()));
    }
}

void			Worms::inventory()
{
  try
    {
      _inventory.manageEvent(_event, _team[_turn->getTeamTurn()]);
    }
  catch (CatchIt<bool> const &e)
    {
      if (e.getIt() == true)
	{
	  _loc = _inventory.getLoc();
	  if (_loc == "inventory")
	    {
	      _view->setInventory(_team[_turn->getTeamTurn()]->getInventory());
	      _view->setInvPos(_inventory.getPos());
	      _team[_turn->getTeamTurn()]->setSelectInv(_inventory.getPos());
	    }
	  else
	    _team[_turn->getTeamTurn()]->setWeapon(_inventory.getPos());
	  if (_inventory.getEffect().size() != 0)
	    _view->setEffect(_inventory.getEffect());
	}
      throw CatchIt<bool> (e.getIt());
    }
}

void			Worms::pause()
{
  std::string		tmp;
  int			pos;
  std::clock_t		cl;

  pos = _pause.getPos();
  tmp = _pause.manageEvent(_event);
  if (tmp == "menu")
    _location = "menu";
  else
    if (tmp == "normal")
      {
	_loc = "normal";
	cl = clock();
	_turn->endPause(cl);
	_clock.endPause(cl);
	_view->setEffect(SEL);
      }
  _view->setPosPause(_pause.getPos());
  if (pos != _pause.getPos() || _location != "pause")
    {
      if (_view->getEffect().size() == 0)
	_view->setEffect(SWI);	
      throw CatchIt<bool> (true);
    }
}

void			Worms::manageSwitch()
{
  Convert<size_t>		conv;

  if (_converter.find(_event) != _converter.end() && (_converter[_event] == "PREVDATA" || _converter[_event] == "NEXTDATA"))
    {
      if (_converter[_event] == "PREVDATA")
	_sel--;
      else
	_sel++;
      if (_sel < 0)
	_sel = _team.size() - 1;
      else
	if (_sel >= (int)_team.size())
	  _sel = 0;
      _view->setEffect(SWI);
      throw CatchIt<bool> (true);
    }
}

void			Worms::manageNextTurn()
{
  if (_converter.find(_event) != _converter.end() && _converter[_event] == "NEXTTURN")
    {
      passTurn();
      throw CatchIt<bool> (true);
    }
}

void                    Worms::managePause()
{
  std::clock_t		clo;

  if (_event == PMAJ_K)
    {
      clo = clock();
      _loc = "pause";
      _turn->startPause(clo);
      _clock.startPause(clo);
      _view->setEffect(SEL);
      throw CatchIt<bool> (true);
    }
}

void			Worms::writeCnf()
{
  File			f(".cfg/.cnf");
  std::string		tmp;
  Convert<int>		conv;
  Position		pos;
  size_t		i;
  size_t		size;
  size_t		x;
  size_t		size2;
  bool			ck;
  std::vector<Player *>	pla;
  int			ti;

  if (_turn != NULL)
    ti = _turn->getTeamTurn();
  else
    ti = 0;
  tmp = "WormsPerTeam " + conv.toString(_wormsPerTeam) + "\nMode " + conv.toString(_mode) + "\nTurn " + conv.toString(ti) + "\nTime " + conv.toString(_clock.getMin()) + ":" + conv.toString(_clock.getSec()) + " " + conv.toString(_time - _view->getTime()) + "\nWaterLvl ";
  if (_view->getWaterLvl() + _view->getWaterMin() > 0)
    tmp += conv.toString(_view->getWaterLvl() + _view->getWaterMin() + 1) + "\n" + _back + "\n";
  else
    tmp += "0\n" + _back + "\n";
  i = 0;
  size = _team.size();
  while (i < size)
    {
      ck = false;
      std::string team;
      team = _team[i]->getName() + " " + conv.toString(_team[i]->getPos()) + " " +
	conv.toString(_team[i]->getMun("Pistol")) + " " + conv.toString(_team[i]->getMun("Teleporter")) + ";";
      pla = _team[i]->getPlayers();
      x = 0;
      size2 = pla.size();
      while (x < size2)
	{
	  team += conv.toString(pla[x]->getLife()) + " " + conv.toString(pla[x]->getPosition().getX()) + " " + conv.toString(pla[x]->getPosition().getY());
	  if (pla[x]->getLife() != 0)
	    ck = true;
	  x++;
	  if ((int)x < _wormsPerTeam)
	    team += ";";
	}
      while ((int)x < _wormsPerTeam)
	{
	  team += "0 0 0";
	  x++;
	  if ((int)x < _wormsPerTeam)
	    team += ";";
	}
      team += "\n";
      if (ck == true)
	tmp += team;
      i++;
    }
  f.writeTronc(tmp);
}

bool		Worms::getGameCnf() const
{
  return (_gameCnf);
}

bool		Worms::getGameMap() const
{
  return (_gameMap);
}

void		Worms::setGameCnf(const bool &c)
{
  _gameCnf = c;
}

void		Worms::setGameMap(const bool &c)
{
  _gameMap = c;
}

void		Worms::saveMap()
{
  bool		ret;

  if (_location == "game" && _map != NULL)
    {
      ret = _map->save(_view->getEditIt());
      if (ret == true)
	_view->setEdited(ret);
      if (_view->edited() == true)
	_view->setEditIt(false);
    }
}

void		Worms::destroyerMap()
{
  if (_map != NULL)
    _map->destroyMapWater(_view->getWaterLvl() + _view->getWaterMin());
}

bool				Worms::checkIntegrity(const std::string &tmp)
{
  int                   i;
  int                   cpt;

  i = 0;
  cpt = 0;
  while (tmp[i])
    {
      if (tmp[i] == ' ')
        cpt++;
      if (cpt != 0 && (tmp[i] > '9' || tmp[i] < '0') && tmp[i] != ' ')
	return (false);
      i++;
    }
  if (cpt != 3)
    return (false);
  if (tmp.substr(0, 3) == "IA_")
    return (false);
  return (true);
}

void				Worms::editScores()
{
  std::vector<std::string>	team;
  std::vector<int>		win;
  std::vector<int>		def;
  std::vector<int>		scores;
  File				file(".cfg/scores");
  Convert<int>			conv;
  std::vector<std::string>	lines;
  Vector			vec;
  size_t			i;
  size_t			size;
  size_t			x;
  size_t			size2;

  try
    {
      lines = vec.getVector(file.read(), '\n');
    }
  catch (AError const &e) {}
  i = 0;
  size = lines.size();
  while (i < size)
    {
      std::vector<std::string>	elem;
      if (checkIntegrity(lines[i]) == true)
	{
	  elem = vec.getVector(lines[i], ' ');
	  team.push_back(elem[0]);
	  win.push_back(conv.toNumber(elem[1]));
	  def.push_back(conv.toNumber(elem[2]));
	  scores.push_back(conv.toNumber(elem[3]));
	}
      i++;
    }
  i = 0;
  size = _teamSave.size();
  size2 = team.size();
  while (i < size)
    {
      x = 0;
      while (x < size2)
	{
	  if (team[x] == _teamSave[i]->getName() && team[x] != _team[0]->getName())
	    {
	      def[x] += 1;
	      size--;
	      _teamSave.erase(_teamSave.begin() + i);
	      i--;
	      break;
	    }
	  x++;
	}
      i++;
    }
  while (_teamSave.size())
    {
      if (_teamSave[0]->getName() != _team[0]->getName())
	{
	  team.push_back(_teamSave[0]->getName());
	  win.push_back(0);
	  def.push_back(0);
	  scores.push_back(0);
	}
      _teamSave.erase(_teamSave.begin());
    }
  x = 0;
  size = team.size();
  bool	ck;
  ck = false;
  while (x < size)
    {
      if (team[x] == _team[0]->getName())
	{
	  ck = true;
	  win[x] += 1;
	  scores[x] += _team[0]->getScores();
	}
      x++;
    }
  if (ck == false)
    {
      team.push_back(_team[0]->getName());
      win.push_back(1);
      def.push_back(0);
      scores.push_back(_team[0]->getScores());
    }
  std::string	res;

  i = 0;
  size = team.size();
  while (i < size)
    {
      res += team[i] + " " + conv.toString(win[i]) + " " + conv.toString(def[i]) + " " + conv.toString(scores[i]) + "\n";
      i++;
    }
  try
    {
      file.writeTronc(res);
    }
  catch (AError const &e) {}
}

bool		Worms::wormsIn(const Position &p)
{
  size_t	i;
  size_t	size;
  size_t	x;
  size_t	size2;
  std::vector<Player *>	players;

  i = 0;
  size = _team.size();
  while (i < size)
    {
      x = 0;
      players = _team[i]->getPlayers();
      size2 = players.size();
      while (x < size2)
	{
	  if (p.getX() == players[x]->getPosition().getX() && p.getY() == players[x]->getPosition().getY())
	    return (true);
	  x++;
	}
      i++;
    }
  return (false);
}

bool		Worms::wormsIn(const int &x, const int &y)
{
  return (wormsIn(Position(x, y)));
}

void		Worms::writeDrop()
{
  _drop->write();
}
