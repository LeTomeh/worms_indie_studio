//
// MapCreator.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:12:38 2016 Maxime LECOQ
// Last update Sat May 28 14:01:00 2016 Maxime LECOQ
//

#include	"MapCreator.hh"

MapCreator::MapCreator()
{
  File				dest(".cfg/map.png");
  File				src(".cfg/map/template.png");
  File				cnf(".cfg/.cnf");
  std::vector<std::string>	world;
  struct timeval		time;
  Vector			vec;

  gettimeofday(&time, NULL);
  srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
  world.push_back("world1");
  world.push_back("world2");
  world.push_back("world3");
  _link = ".cfg/map/" + world[rand() % world.size()] + "/";
  try
    {
      cnf.writeAppend(_link + "back.png" + "\n");
    }
  catch (AError const &e)
    {
      e.notCriticalError();
      throw ThrowErrorData(vec.getVector("MapCreator\ncan't write in\ncnf file the background", '\n'));
    }
  try
    {
      dest.writeTronc(src.read());
      png = new Png(".cfg/map.png");
      _width = png->getWidth();
      _height = png->getHeight();
      _rowPointers = png->getRowPointers();
      makeGround();
      makeVolatile();
      png->setRowPointers(_rowPointers);
      png->save();
      png->clean();
      delete (png);
    }
  catch (AError const &e)
    {
      e.notCriticalError();
      throw ThrowErrorData(vec.getVector("MapCreator\ncan't find template.png", '\n'));
    }
}

MapCreator::~MapCreator()
{
}

MapCreator::MapCreator(const MapCreator &c) : _rowPointers(c._rowPointers), _width(c._width), _height(c._height), _elem(c._elem), _link(c._link) {}

MapCreator	&MapCreator::operator=(const MapCreator &c)
{
  if (this != &c)
    {
      _rowPointers = c._rowPointers;
      _width = c._width;
      _height = c._height;
      _elem = c._elem;
      _link = c._link;
    }
  return (*this);
}

void		MapCreator::setRowPointers()
{
  png->setRowPointers(_rowPointers);
}

void		MapCreator::loadPng(const std::string &f)
{
  try
    {
      Png png(f);

      _elem.push_back(png);
    }
  catch (AError const &e) {}
}

void		MapCreator::copyBlock(const int &x, const int &y, Png form)
{
  int		i;
  int		i2;
  int		wi;
  int		he;
  png_bytep	*img;
  png_byte	*rowi;
  png_byte	*ptri;
  png_byte	*row;
  png_byte	*ptr;

  wi = form.getWidth();
  he = form.getHeight();
  img = form.getRowPointers();
  i = 0;
  i2 = 0;
  while (i < he && (i + y) < _height)
    {
      i2 = 0;
      rowi = img[i];
      row = _rowPointers[y + i];
      while (i2 < wi && (i2 + x) < _width)
	{
	  ptri = &(rowi[i2 * 4]);
	  ptr = &(row[(i2 + x) * 4]);
	  if (ptr[3] == 0)
	    {
	      ptr[0] = ptri[0];
	      ptr[1] = ptri[1];
	      ptr[2] = ptri[2];
	      ptr[3] = ptri[3];
	    }
	  i2++;
	}
      i++;
    }
}

void		MapCreator::copyBlock(const int &x, const int &y, Png *form)
{
  int		i;
  int		i2;
  int		wi;
  int		he;
  png_bytep	*img;
  png_byte	*rowi;
  png_byte	*ptri;
  png_byte	*row;
  png_byte	*ptr;

  wi = form->getWidth();
  he = form->getHeight();
  img = form->getRowPointers();
  i = 0;
  i2 = 0;
  while (i < he && (i + y) < _height)
    {
      i2 = 0;
      rowi = img[i];
      row = _rowPointers[y + i];
      while (i2 < wi && (i2 + x) < _width)
	{
	  ptri = &(rowi[i2 * 4]);
	  ptr = &(row[(i2 + x) * 4]);
	  if (ptr[3] == 0)
	    {
	      ptr[0] = ptri[0];
	      ptr[1] = ptri[1];
	      ptr[2] = ptri[2];
	      ptr[3] = ptri[3];
	    }
	  i2++;
	}
      i++;
    }
}

void		MapCreator::makeGround()
{
  int				sel;

  loadPng(_link + "map1.png");
  loadPng(_link + "map2.png");
  loadPng(_link + "map3.png");
  loadPng(_link + "map4.png");
  loadPng(_link + "map5.png");
  loadPng(_link + "map6.png");
  sel = rand() % _elem.size();
  copyBlock(0, 0, _elem[sel]);
  while (_elem.size() != 0)
    {
      _elem[0].clean();
      _elem.erase(_elem.begin());
    }
}

void		MapCreator::makeVolatile()
{
  size_t		i;
  int			sel;
  int			x;
  int			y;
  int			mod;

  loadPng(_link + "block1.png");
  loadPng(_link + "block2.png");
  loadPng(_link + "block3.png");
  loadPng(_link + "block4.png");
  i = 0;
  mod = _width / 3;
  while (i < 3 && _elem.size() != 0)
    {
      x = (rand() % ((_width / 4) - 100)) + (i * (_width / 3)) + 50 - (i * 20);
      y = (rand() % ((_height / 3) - 150)) + 30;
      sel = rand() % _elem.size();
      copyBlock(x, y, _elem[sel]);
      _elem.erase(_elem.begin() + sel);
      i++;
      mod += (_width / 3);
    }
  while (_elem.size() != 0)
    {
      _elem[0].clean();
      _elem.erase(_elem.begin());
    }
}

void		MapCreator::deletePoint(const int &x, const int &y, png_bytep *rowPointers)
{
  png_byte			*row;
  png_byte			*ptr;

  row = rowPointers[y];
  ptr = &(row[x*4]);
  ptr[0] = 0;
  ptr[1] = 0;
  ptr[2] = 0;
  ptr[3] = 0;
}

void		MapCreator::firstDelete(const int &width, const int &height, png_bytep *rowPointers)
{
  int		y;
  int		x;

  y = 0;
  while (y < 40)
    {
      x = 0;
      while (x < width)
	deletePoint(x++, y, rowPointers);
      y++; 
    }
  y = height - 20;
  while (y < height)
    {
      x = 0;
      while (x < width)
	deletePoint(x++, y, rowPointers);
      y++; 
    }
  x = 0;
  while (x < 20)
    {
      y = 0;
      while (y < height)
	deletePoint(x, y++, rowPointers);
      x++;
	}
  x = width - 20;
  while (x < width)
    {
      y = 0;
      while (y < height)
	deletePoint(x, y++, rowPointers);
      x++;
    }
}

