//
// Player.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:22:18 2016 Maxime LECOQ
// Last update Sun Jun  5 04:07:03 2016 Maxime LECOQ
//

#include	"Player.hh"

Player::Player(const std::string &n, const MemberData *d, const Inventory *inv) :_name(n)
{
  _inventory = (Inventory *)inv;
  _life = d->getLife();
  _pos = d->getPos();
  _fall = 0;
  _sprite = FRONT;
  _jump = "";
  _cptJ = 0;
  _saveY = 0;
  _saveCpt = 0;
  _current = NULL;
  _cursor = new Cursor;
  _tele = false;
  _current = _inventory->getWeapon("Bazooka");
  _myTurn = false;
}

Player::~Player()
{
  delete (_cursor);
}

Player::Player(const Player &c) : _name(c._name), _life(c._life), _pos(c._pos), _fall(c._fall), _sprite(c._sprite), _jump(c._jump), _cptJ(c._cptJ), _saveY(c._saveY), _saveCpt(c._saveCpt), _inventory(c._inventory), _current(c._current), _cursor(c._cursor), _dir(c._dir), _tele(c._tele), _myTurn(c._myTurn) {}

Player	&Player::operator=(const Player &c)
{
  if (this != &c)
    {
      _name = c._name;
      _life = c._life;
      _pos = c._pos;
      _fall = c._fall;
      _sprite = c._sprite;
      _jump = c._jump;
      _cptJ = c._cptJ;
      _saveY = c._saveY;
      _saveCpt = c._saveCpt;
      _inventory = c._inventory;
      _current = c._current;
      _cursor = c._cursor;
      _dir = c._dir;
      _tele = c._tele;
      _myTurn = c._myTurn;
    }
  return (*this);
}

void		Player::setLife(const int &l)
{
  _life = l;
}

int		Player::getLife() const
{
  return (_life);
}

const std::string	&Player::getTeamName() const
{
  return (_name);
}

Position		Player::getPosition() const
{
  return (_pos);
}

void			Player::setPosition(const Position &p)
{
  _pos = p;
}

void			Player::fall()
{
  _fall++;
  if (_sprite != FRONT)
    changeDir(_dir);
}

void			Player::endFall()
{
  if (_saveCpt != 0)
    {
      _fall -= _saveCpt;
      if (_fall < 0)
	_fall = 0;
    }
  _life -= (_fall / 20);
  _fall = 0;
  _saveY = 0;
  _saveCpt = 0;
}

int			Player::getFall() const
{
  return (_fall);
}

void			Player::setSprite(const std::string &c)
{
  if (_sprite == FRONT)
    {
      _cursor->setActive(false);
      setTele(false);
      take(false);
    }
  _sprite = c;
}

const std::string	&Player::getSprite() const
{
  return (_sprite);
}

void			Player::setJump(const std::string &j)
{
  if (j == "LEFT" || j == "RIGHT")
    {
      _jump = j;
      _saveY = _pos.getY();
      _saveCpt = 0;
    }
  else
    _jump = "";
  _cptJ = 0;
}

void			Player::endJump()
{
  _cptJ = 0;
  _jump = "";
}

const std::string	&Player::getJump() const
{
  return (_jump);
}

void			Player::jump()
{
  if (_saveY > _pos.getY())
    {
      _saveCpt += (_saveY - _pos.getY());
      _saveY = _pos.getY();
    }
  _cptJ++;
}

int			Player::getJumpCpt() const
{
  return (_cptJ);
}

void			Player::setWeapon(IWeapon *w)
{
  int			y;

  _current = w;
  if (_current != NULL)
    {
      y = _pos.getY() - 15;
      if (y < 0)
	y = 0;
      if (_dir == true)
	{
	  setSprite(_current->getRightSprite());
	  _cursor->setPosition(Position(_pos.getX() + 40, y));
	}
      else
	{
	  setSprite(_current->getLeftSprite());
	  _cursor->setPosition(Position(_pos.getX() - 40, y));
	}
      if (_current->getName() != "FirePunch")
	{
	  _cursor->setActive(true);
	  _cursor->setAngle(0);
	}
    }
  else
    {
      _cursor->setActive(false);
      _current = _inventory->getWeapon("Bazooka");
    }
}

void			Player::unsetWeapon()
{
  _current = _inventory->getWeapon("Bazooka");
  _cursor->setActive(false);
}

IWeapon			*Player::getCurrentWeapon() const
{
  return (_current);
}

void			Player::changeDir(const bool &dir)
{
  int			y;

  _dir = dir;
  if (_current != NULL && _current->getMun() != 0)
    {
      y = _pos.getY() - 15;
      if (y < 0)
	y = 0;
      if (dir == true)
	{
	  setSprite(_current->getRightSprite());
	  _cursor->setPosition(Position(_pos.getX() + 40, y));
	}
      else
	{
	  setSprite(_current->getLeftSprite());
	  _cursor->setPosition(Position(_pos.getX() - 40, y));
	}
      if (_current->getName() != "FirePunch")
	{
	  _cursor->setActive(true);
	  _cursor->setAngle(0);
	}
    }
  else
    {
      _cursor->setActive(false);
      if (dir == true)
	setSprite(MOVERIGHT);
      else
	setSprite(MOVELEFT);
    }
}

bool			Player::getDir() const
{
  return (_dir);
}

Cursor			*Player::getCursor() const
{
  return (_cursor);
}

void			Player::setTele(const bool &c)
{
  _tele = c;
}

bool			Player::teleUse() const
{
  return (_tele);
}

bool			Player::getOk() const
{
  return (_myTurn);
}

void			Player::take(const bool &c)
{
  _myTurn = c;
}

Inventory		*Player::getInventory() const
{
  return (_inventory);
}
