//
// View.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May  5 15:33:59 2016 Maxime LECOQ
// Last update Sun Jun  5 07:01:56 2016 Maxime LECOQ
//

#include	"View.hh"

View::View()
{
  _posPause = 0;
  _pause.push_back("continue");
  _pause.push_back("menu");
  _pause.push_back("quit");
  _location = "normal";
  _time = 0;
  _min = 0;
  _sec = 0;
  _waterLvl = 0;
  _waterMin = 0;
  _clock = "assets/clock.png";
  _clockAlert = "assets/clockAlert.png";
  _waves = "assets/waves.png";
  _pauseB = "assets/fond2.jpg";
  _edited = false;
  _sound = GAME;
  _soundOn = false;
  _effectOn = false;
  _pro = new Projectile;
  _editIt = false;
  _wind = 0;
  _meshStat = false;
  _clockDesc = clock();
}

View::~View()
{
  delete (_pro);
}

View::View(const View &c) : _back(c._back), _map(c._map), _team(c._team), _ctrl(c._ctrl), _ctrlList(c._ctrlList), _posPause(c._posPause), _pause(c._pause), _location(c._location), _data(c._data), _teamTurn(c._teamTurn), _time(c._time), _min(c._min), _sec(c._sec), _waterLvl(c._waterLvl), _waterMin(c._waterMin), _clock(c._clock), _clockAlert(c._clockAlert), _waves(c._waves), _pauseB(c._pauseB), _edited(c._edited), _sound(c._sound), _soundOn(c._soundOn), _effect(c._effect), _effectOn(c._effectOn), _inv(c._inv), _invPos(c._invPos), _pro(c._pro), _editIt(c._editIt), _wind(c._wind), _drops(c._drops), _meshStat(c._meshStat), _meshName(c._meshName), _meshTexture(c._meshTexture), _meshSound(c._meshSound), _desc(c._desc) {}

View &View::operator=(const View &c)
{
  if (this != &c)
    {
      _map = c._map;
      _back = c._back;
      _team = c._team;
      _ctrl = c._ctrl;
      _ctrlList = c._ctrlList;
      _posPause = c._posPause;
      _pause = c._pause;
      _location = c._location;
      _data = c._data;
      _teamTurn = c._teamTurn;
      _time = c._time;
      _min = c._min;
      _sec = c._sec;
      _waterLvl = c._waterLvl;
      _waterMin = c._waterMin;
      _clock = c._clock;
      _clockAlert = c._clockAlert;
      _waves = c._waves;
      _pauseB = c._pauseB;
      _edited = c._edited;
      _sound = c._sound;
      _soundOn = c._soundOn;
      _effect = c._effect;
      _effectOn = c._effectOn;
      _inv = c._inv;
      _invPos = c._invPos;
      _pro = c._pro;
      _editIt = c._editIt;
      _wind = c._wind;
      _drops = c._drops;
      _meshStat = c._meshStat;
      _meshName = c._meshName;
      _meshTexture = c._meshTexture;
      _meshSound = c._meshSound;
      _desc = c._desc;
    }
  return (*this);
}

void		View::setPosPause(const int &x)
{
  _posPause = x;
}

int		View::getPosPause() const
{
  return (_posPause);
}

std::vector<std::string>	View::getPause() const
{
  return (_pause);
}

void		View::setBack(const std::string &b)
{
  _back = b;
}

void		View::setMap(const std::string &m)
{
  _map = m;
}

const std::string		&View::getBack() const
{
  return (_back);
}

const std::string		&View::getMap() const
{
  return (_map);
}

void				View::setTeam(const std::vector<Team *> &t)
{
  _team = t;
}

std::vector<Team *>		View::getTeam() const
{
  return (_team);
}

void            View::setCtrlList(const std::vector<std::string> &v)
{
  _ctrlList = v;
}

std::vector<std::string>        View::getCtrlList() const
{
  return (_ctrlList);
}

void            View::setCtrl(std::map<std::string, int> v)
{
  _ctrl = v;
}

std::map<std::string, int>      View::getCtrl()
{
  return (_ctrl);
}

void			View::setLocation(const std::string &c)
{
  _location = c;
}

const std::string	&View::getLocation() const
{
  return (_location);
}

void			View::setData(const std::string &c)
{
  _data = c;
}

const std::string	&View::getData() const
{
  return (_data);
}

void			View::setTeamTurn(const std::string &c)
{
  _teamTurn = c;
}

const std::string	&View::getTeamTurn() const
{
  return (_teamTurn);
}

const std::string	&View::getClock() const
{
  return (_clock);
}

const std::string	&View::getClockAlert() const
{
  return (_clockAlert);
}

const std::string	&View::getWaves() const
{
  return (_waves);
}

const std::string	&View::getPauseB() const
{
  return (_pauseB);
}

int			View::getTime() const
{
  return (_time);
}

void			View::setTime(const int &t)
{
  _time = t;
}

int			View::getMin() const
{
  return (_min);
}

void			View::setMin(const int &t)
{
  _min = t;
}

int			View::getSec() const
{
  return (_sec);
}

void			View::setSec(const int &t)
{
  _sec = t;
}

int			View::getWaterLvl() const
{
  return (_waterLvl);
}

void			View::setWaterLvl(const int &t)
{
  _waterLvl = t;
}

int			View::getWaterMin() const
{
  return (_waterMin);
}

void			View::setWaterMin(const int &t)
{
  _waterMin = t;
}

bool			View::edited() const
{
  return (_edited);
}

void			View::setEdited(const bool &c)
{
  _edited = c;
}

bool			View::getEditIt() const
{
  return (_editIt);
}

void			View::setEditIt(const bool &c)
{
  _editIt = c;
}

const std::string             &View::getSound() const
{
  return (_sound);
}

bool                          View::soundIsOn() const
{
  return (_soundOn);
}

std::string			View::getEffect()
{
  std::string			tmp;

  if (_effect.size() != 0)
    {
      tmp = _effect[0];
      _effect.erase(_effect.begin());
      return (tmp);
    }
  else
    return ("");
}

bool                          View::effectIsOn() const
{
  return (_effectOn);
}

void                          View::setEffectOn(const bool &c)
{
  _effectOn = c;
}

void                          View::setEffect(const std::string &c)
{
  _effect.push_back(c);
}

void                          View::setSound(const bool &c)
{
  _soundOn = c;
}

void			View::setInventory(Inventory *i)
{
  _inv = i;
}

Inventory		*View::getInventory() const
{
  return (_inv);
}

void			View::setInvPos(const int &c)
{
  _invPos = c;
}

int			View::getInvPos() const
{
  return (_invPos);
}

void			View::setProjectile(Projectile *p)
{
  _pro = p;
}

Projectile		*View::getProjectile() const
{
  return (_pro);
}

int			View::getWind() const
{
  return (_wind);
}

void			View::setWind(const int &c)
{
  _wind = c;
}

void			View::setDrop(const std::vector<Drop> &d)
{
  _drops = d;
}

const std::vector<Drop>	&View::getDrop() const
{
  return (_drops);
}

void			View::setMeshStatus(const bool &c)
{
  _meshStat = c;
}

void			View::setMeshName(const std::string &c)
{
  _meshName = c;
}

void			View::setMeshTexture(const std::string &c)
{
  _meshTexture = c;
}

void			View::setMeshSound(const std::string &c)
{
  _meshSound = c;
}

bool			View::getMeshStatus() const
{
  return (_meshStat);
}

const std::string	&View::getMeshName() const
{
  return (_meshName);
}

const std::string	&View::getMeshTexture() const
{
  return (_meshTexture);
}

const std::string	&View::getMeshSound() const
{
  return (_meshSound);
}

void			View::setDesc(const std::string &c)
{
  _desc = c;
  _clockDesc =  clock();
}

const std::string	&View::getDesc()
{
  if (((double)clock() - (double)_clockDesc) / CLOCKS_PER_SEC > 3)
    _desc = "";
  return (_desc);
}
