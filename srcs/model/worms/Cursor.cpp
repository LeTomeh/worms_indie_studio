//
// Cursor.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:53:39 2016 Maxime LECOQ
// Last update Mon May 30 17:17:11 2016 Maxime LECOQ
//

#include	"Cursor.hh"

Cursor::Cursor() : _pos(Position(0, 0)), _sprite(CURSOR), _active(false) {}

Cursor::~Cursor() {}

Cursor::Cursor(const Cursor &c) : _pos(c._pos), _sprite(c._sprite), _active(c._active), _angle(c._angle) {}

Cursor	&Cursor::operator=(const Cursor &c)
{
  if (this != &c)
    {
      _pos = c._pos;
      _sprite = c._sprite;
      _active = c._active;
      _angle = c._angle;
   }
  return (*this);
}

void		Cursor::setSprite(const std::string &s)
{
  _sprite = s;
}

const std::string &Cursor::getSprite() const
{
  return (_sprite);
}

void		Cursor::setX(const int &x)
{
  _pos.setX(x);
}

void		Cursor::setY(const int &y)
{
  _pos.setY(y);
}

int		Cursor::getX() const
{
  return (_pos.getX());
}

int		Cursor::getY() const
{
  return (_pos.getY());
}

void		Cursor::setPosition(const Position &c)
{
  _pos = c;
}

Position	Cursor::getPosition() const
{
  return (_pos);
}

void		Cursor::setActive(const bool &c)
{
  _active = c;
}

bool		Cursor::getActive() const
{
  return (_active);
}

void		Cursor::setAngle(const int &x)
{
  _angle = x;
}

int		Cursor::getAngle() const
{
  return (_angle);
}
