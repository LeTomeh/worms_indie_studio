//
// Pause.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May 18 16:11:05 2016 Maxime LECOQ
// Last update Thu May 19 10:14:30 2016 Maxime LECOQ
//

#include	"Worms.hh"

Pause::Pause()
{
  _pos = 0;
}

Pause::~Pause() {}

Pause::Pause(const Pause &c) : _pos(c._pos) {}

Pause	&Pause::operator=(const Pause &c)
{
  if (this != &c)
    {
      _pos = c._pos;
    }
  return (*this);
}

std::string             Pause::manageEvent(const int &ev)
{
  std::string		tmp;

  tmp = "";
  manageSelect(ev);
  manageEnter(ev, tmp);
  return (tmp);
}

void			Pause::init()
{
  _pos = 0;
}

int			Pause::getPos() const
{
  return (_pos);
}

void			Pause::manageSelect(const int &ev)
{
  if (ev == UP_K)
    _pos--;
  else
    if (ev == DOWN_K)
      _pos++;
  if (_pos == -1)
    _pos = 2;
  else
    if (_pos == 3)
      _pos = 0;
}

void			Pause::manageEnter(const int &ev, std::string &t)
{
  if (ev == SPACE_K || ev == RETURN_K)
    {
      if (_pos == 2)
	throw CatchIt<bool> (false);
      else
	if (_pos == 1)
	  t = "menu";
	else
	  t = "normal";
    }
}
