//
// Png.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:12:38 2016 Maxime LECOQ
// Last update Sun Jun  5 02:59:49 2016 Maxime LECOQ
//

#include	"Png.hh"

Png::Png(const std::string &n)
{
  construct(n);
}

Png::Png(const char *n)
{
  std::string tmp(n);

  construct(tmp);
}

Png::~Png()
{
}

Png::Png(const Png &c) : _width(c._width), _height(c._height), _colorType(c._colorType), _bitDepth(c._bitDepth), _numberOfPasses(c._numberOfPasses), _rowPointers(c._rowPointers), _run(c._run), _edit(c._edit), _save(c._save), _name(c._name) {}

Png	&Png::operator=(const Png &c)
{
  if (this != &c)
    {
      _width = c._width;
      _height = c._height;
      _colorType = c._colorType;
      _bitDepth = c._bitDepth;
      _numberOfPasses = c._numberOfPasses;
      _rowPointers = c._rowPointers;
      _run = c._run;
      _edit = c._edit;
      _save = c._save;
      _name = c._name;
    }
  return (*this);
}

void		Png::save()
{
  FILE		*fp;
  png_structp   pngWrite;
  png_infop     infoPtr;

  fp  = fopen(".cfg/mapTmp.png", "wb");
  if (!fp)
    throw ErrorPng("cannot open map in writting mode");
  pngWrite = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!pngWrite)
    throw ErrorPng("cannot create write struct");
  infoPtr = png_create_info_struct(pngWrite);
  if (!infoPtr)
    throw ErrorPng("cannot create info struct");
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during init_io");
  png_init_io(pngWrite, fp);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during writing header");
  png_set_IHDR(pngWrite, infoPtr, _width, _height,
               _bitDepth, _colorType, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  png_write_info(pngWrite, infoPtr);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during writing bytes");
  png_write_image(pngWrite, _rowPointers);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during end of writing");
  png_write_end(pngWrite, NULL);
  png_destroy_write_struct(&pngWrite, &infoPtr);
  fclose(fp);
  _edit = false;
  _save = true;
  File map(".cfg/map.png");
  File src(".cfg/mapTmp.png");

  try
    {
      map.writeTronc(src.read());
    }
  catch (AError const &e) {}
  try
    {
      src.destroy();
    }
  catch (AError const &e) {}
}

void		Png::save(const std::string &file)
{
  FILE		*fp;
  png_structp   pngWrite;
  png_infop     infoPtr;

  fp  = fopen(file.c_str(), "wb");
  if (!fp)
    throw ErrorPng("cannot open map in writting mode");
  pngWrite = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!pngWrite)
    throw ErrorPng("cannot create write struct");
  infoPtr = png_create_info_struct(pngWrite);
  if (!infoPtr)
    throw ErrorPng("cannot create info struct");
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during init_io");
  png_init_io(pngWrite, fp);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during writing header");
  png_set_IHDR(pngWrite, infoPtr, _width, _height,
               _bitDepth, _colorType, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  png_write_info(pngWrite, infoPtr);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during writing bytes");
  png_write_image(pngWrite, _rowPointers);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during end of writing");
  png_write_end(pngWrite, NULL);
  png_destroy_write_struct(&pngWrite, &infoPtr);
  fclose(fp);
  clean();
}

void		Png::construct(const std::string &n)
{
  png_byte	header[8];
  int		y;
  FILE		*fp;
  png_structp   pngRead;
  png_infop     infoPtr;

  _name = n;
  fp = fopen(n.c_str(), "rb");
  if (!fp)
    throw ErrorPng("cannot open map in reading mode");
  fread(header, 1, 8, fp);
  if (png_sig_cmp(header, 0, 8))
    throw ErrorPng("Header file of map isn't correct");
  if (!(pngRead = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)))
    throw ErrorPng("cannot create read struct");
  infoPtr = png_create_info_struct(pngRead);
  if (!infoPtr)
    throw ErrorPng("cannot create info struct");
  if (setjmp(png_jmpbuf(pngRead)))
    throw ErrorPng("Error during init_io");
  png_init_io(pngRead, fp);
  png_set_sig_bytes(pngRead, 8);
  png_read_info(pngRead, infoPtr);
  _width = png_get_image_width(pngRead, infoPtr);
  _height = png_get_image_height(pngRead, infoPtr);
  _colorType = png_get_color_type(pngRead, infoPtr);
  _bitDepth = png_get_bit_depth(pngRead, infoPtr);
  _numberOfPasses = png_set_interlace_handling(pngRead);
  png_read_update_info(pngRead, infoPtr);
  if (setjmp(png_jmpbuf(pngRead)))
    throw ErrorPng("Error during read_image");
  _rowPointers = (png_bytep*)malloc(sizeof(png_bytep) * _height);
  y = 0;
  while (y < _height)
    _rowPointers[y++] = (png_byte*) malloc(png_get_rowbytes(pngRead, infoPtr));
  png_read_image(pngRead, _rowPointers);
  png_destroy_read_struct(&pngRead, &infoPtr, NULL);
  fclose(fp);
  _edit = false;
  _save = false;
}

int		Png::getWidth() const
{
  return (_width);
}

int		Png::getHeight() const
{
  return (_height);
}

png_bytep	*Png::getRowPointers() const
{
  return (_rowPointers);
}

void		Png::setRowPointers(png_bytep *r)
{
  _rowPointers = r;
}

png_byte	*Png::getPixels(const int &x, const int &y) const
{
  png_byte	*row;
  int		x2;

  x2 = x;
  if (x == 0)
    x2++;
  else
    if (x == _width)
      x2--;
  if (x2 < _width && y < _height && x2 > 0 && y > 0)
    {
      row = _rowPointers[y];
      return (&(row[x2 * 4]));
    }
  else
    return (NULL);
}

void		Png::setPixels(const int &x, const int &y, png_byte *p)
{
  png_byte	*row;
  png_byte	*ptr;

  if (x < _width && y < _height)
    {
      row = _rowPointers[y];
      ptr = &(row[x * 4]);
      ptr[0] = p[0];
      ptr[1] = p[1];
      ptr[2] = p[2];
      ptr[3] = p[3];
      _edit = true;
    }
}

void		Png::putOff()
{
  _run = false;
}

void		Png::putOn()
{
  _run = true;
}

bool		Png::run() const
{
  return (_run);
}

bool		Png::edit() const
{
  return (_edit);
}

bool		Png::isSave() const
{
  return (_save);
}

void		Png::setSave(const bool &c)
{
  _save = c;
}

void		Png::clean()
{
  int		y;

  y = 0;
  while (y < _height)
    {
      free(_rowPointers[y]);
      y++;
    }
    free(_rowPointers);
}

const std::string &Png::getName() const
{
  return (_name);
}

bool		Png::saveMap(const bool &ck)
{
  FILE		*fp;
  png_structp   pngWrite;
  png_infop     infoPtr;

  fp  = fopen(".cfg/mapTmp.png", "wb");
  if (!fp)
    throw ErrorPng("cannot open map in writting mode");
  pngWrite = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!pngWrite)
    throw ErrorPng("cannot create write struct");
  infoPtr = png_create_info_struct(pngWrite);
  if (!infoPtr)
    throw ErrorPng("cannot create info struct");
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during init_io");
  png_init_io(pngWrite, fp);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during writing header");
  png_set_IHDR(pngWrite, infoPtr, _width, _height,
               _bitDepth, _colorType, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  png_write_info(pngWrite, infoPtr);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during writing bytes");
  png_write_image(pngWrite, _rowPointers);
  if (setjmp(png_jmpbuf(pngWrite)))
    throw ErrorPng("Error during end of writing");
  png_write_end(pngWrite, NULL);
  png_destroy_write_struct(&pngWrite, &infoPtr);
  fclose(fp);
  _edit = false;
  _save = true;
  File map(".cfg/map.png");
  File src(".cfg/mapTmp.png");

  try
    {
      map.writeTronc(src.read());
    }
  catch (AError const &e) {}
  try
    {
      src.destroy();
    }
  catch (AError const &e) {}
  if (ck == true)
    return (true);
  else
    return (false);
}

void			Png::destroy(const int &x, const int &y, const std::string &pochoir)
{
  if (pochoir.size() == 0)
    return;
  try
    {
      Png		cp(pochoir);
      png_byte		*row;
      png_byte		*rowi;
      png_byte		*ptr;
      png_byte		*ptri;
      png_bytep		*rowPointers;
      int		i;
      int		i2;
      int		wi;
      int		he;

      rowPointers = cp.getRowPointers();
      i = 0;
      wi = cp.getWidth();
      he = cp.getHeight();
      while (i < he && (i + y - (he / 2)) < _height && (i + y - (he / 2)) > 0)
	{
	  i2 = 0;
	  rowi = rowPointers[i];
	  row = _rowPointers[i + y - (he / 2)];
	  while (i2 < wi && (i2 + x - (wi / 2)) < _width && (i2 + x - (wi / 2)) > 0)
	    {
	      ptri = &(rowi[i2 * 4]);
	      ptr = &(row[(i2 + x - (wi / 2)) * 4]);
	      if (ptri[3] == 0)
		{
		  ptr[0] = 0;
		  ptr[1] = 0;
		  ptr[2] = 0;
		  ptr[3] = 0;
		}
	      i2++;
	    }
	  i++;
	}
      cp.clean();
    }
  catch (AError const &e) {}
}
