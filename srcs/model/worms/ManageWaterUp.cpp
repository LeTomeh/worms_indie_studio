//
// ManageWaterUp.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:53:39 2016 Maxime LECOQ
// Last update Tue May 31 17:25:59 2016 Maxime LECOQ
//

#include	"ManageWaterUp.hh"

ManageWaterUp::ManageWaterUp(View *v) : _view(v) {}

ManageWaterUp::~ManageWaterUp() {}

ManageWaterUp::ManageWaterUp(const ManageWaterUp &c) : _view(c._view), _map(c._map), _turn(c._turn), _time(c._time) {}

ManageWaterUp	&ManageWaterUp::operator=(const ManageWaterUp &c)
{
  if (this != &c)
    {
      _view = c._view;
      _map = c._map;
      _turn = c._turn;
      _time = c._time;
   }
  return (*this);
}

void                    ManageWaterUp::deathPlayer(const int &x, const int &i, std::vector<Team *> &team)
{
  if (team[i]->deletePlayer(x) == true)
    {
      _view->setEffect(DEAD);
      _turn->nextTurn(team);
      _view->setTeamTurn(team[_turn->getTeamTurn()]->getName() + " turn");
      _view->setTime(_time);
      _turn->resetPause();
    }
}

void			ManageWaterUp::setter(Map *m, Turn *t, const int &time)
{
  _map = m;
  _turn = t;
  _time = time;
}

void                    ManageWaterUp::checkWaterDeath(std::vector<Team *> &team)
{
  size_t                i;
  size_t                x;
  std::vector<Player *> players;
  Position              tmp;

  i = 0;
  while (i < team.size())
    {
      x = 0;
      players = team[i]->getPlayers();
      while (x < players.size())
        {
          tmp = players[x]->getPosition();
          if (_map->getWaterAt(tmp, _view->getWaterMin() + _view->getWaterLvl()) == true)
            {
              deathPlayer(x, i, team);
              players = team[i]->getPlayers();
	      x = 0;
            }
          else
            x++;
        }
      i++;
    }
}
