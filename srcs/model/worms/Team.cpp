//
// Team.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:22:18 2016 Maxime LECOQ
// Last update Sun Jun  5 15:47:04 2016 Maxime LECOQ
//

#include	"Team.hh"

Team::Team(const TeamNameMaker *t, const int &m, const int &nb, const std::vector<MemberData*> &data) :_name(t->getName()), _mode(m), _status(t->getStatus()), _pos(t->getTurn())
{
  int		life;
  int		i;
  ManageInventory       inv;

  inv.addDefWeapon();
  _inventory = new Inventory(inv.getStuff());
  if (m == 0)
    life = 100;

  else
    life = 50;
  _lifeMax = nb * life;
  _inventory->getStuff<IWeapon *>("Pistol")->setMun(t->getPistol());
  _inventory->getStuff<IWeapon *>("Teleporter")->setMun(t->getMissile());
  _inventory->getStuff<IWeapon *>("Pistol")->setPictureOn(WPISTOLON);
  _inventory->getStuff<IWeapon *>("Pistol")->setDamage(30);
  _inventory->getStuff<IWeapon *>("Pistol")->setPictureOff(WPISTOLOFF);
  _inventory->getStuff<IWeapon *>("Pistol")->setDescription(DPISTOL);
  _inventory->getStuff<IWeapon *>("Teleporter")->setPictureOn(WTELEON);
  _inventory->getStuff<IWeapon *>("Teleporter")->setPictureOff(WTELEOFF);
  _inventory->getStuff<IWeapon *>("Teleporter")->setDescription(DTELE);
  _inventory->getStuff<IWeapon *>("Teleporter")->setLeftSprite(TELEL);
  _inventory->getStuff<IWeapon *>("Teleporter")->setRightSprite(TELER);
  _inventory->getStuff<IWeapon *>("Pistol")->setLeftSprite(PISTOLL);
  _inventory->getStuff<IWeapon *>("Pistol")->setRightSprite(PISTOLR);
  _inventory->getStuff<IWeapon *>("Pistol")->setBulletLeft(BULLETSHOT);
  _inventory->getStuff<IWeapon *>("Pistol")->setBulletRight(BULLETSHOTREV);
  _inventory->getStuff<IWeapon *>("Pistol")->setPow(PISTOLPOCHOIR);
  _inventory->getStuff<IWeapon *>("Teleporter")->setMesh(ANIMTELE);
  _inventory->getStuff<IWeapon *>("Pistol")->setMesh(ANIMPISTOL);
  _inventory->getStuff<IWeapon *>("Teleporter")->setTexture(TETELE);
  _inventory->getStuff<IWeapon *>("Pistol")->setTexture(TEPISTOL);
  _inventory->getStuff<IWeapon *>("Teleporter")->setSound(SOUNDTELE);
  _inventory->getStuff<IWeapon *>("Pistol")->setSound(SOUNDPISTOL);
  i = 0;
  while (i < nb)
    {
      if (data[i]->getLife() > 0)
	_player.push_back(new Player(t->getName(), data[i], _inventory));
      i++;
    }
  if (_pos >= (int)_player.size())
    _pos = 0;
}

Team::~Team()
{
  delete (_inventory);
}

Team::Team(const Team &c) : _name(c._name), _mode(c._mode), _status(c._status), _lifeMax(c._lifeMax), _player(c._player), _pos(c._pos), _inventory(c._inventory) {}

Team	&Team::operator=(const Team &c)
{
  if (this != &c)
    {
      _name = c._name;
      _mode = c._mode;
      _status = c._status;
      _lifeMax = c._lifeMax;
      _player = c._player;
      _pos = c._pos;
      _inventory = c._inventory;
    }
  return (*this);
}

void			Team::checkPlayers()
{
  size_t		i;
  size_t		size;

  i = 0;
  size = _player.size();
  while (i < size)
    {
      if (_player[i]->getLife() <= 0)
	{
	  _player.erase(_player.begin() + i);
	  if (_pos == (int)i)
	    nextPos();
	  size--;
	}
      else
	i++;
    }
}

std::vector<Player *>	Team::getPlayers() const
{
  return (_player);
}

const std::string	&Team::getName() const
{
  return (_name);
}

std::string		Team::getLifes() const
{
  size_t		i;
  size_t		size;
  size_t		life;
  Convert<double>	conv;
  std::string		tmp;

  size = _player.size();
  i = 0;
  life = 0;
  while (i < size)
    {
      life += _player[i]->getLife();
      i++;
    }
  tmp = conv.toString(((double)life / (double)_lifeMax) * 100);
  size = tmp.find(".");
  if (size < tmp.size())
    tmp = tmp.substr(0, size + 3);
  return (tmp);
}

void		Team::nextPos()
{
  _pos++;
  if (_pos >= (int)_player.size())
    _pos = 0;
}

int		Team::getPos() const
{
  return (_pos);
}

bool		Team::deletePlayer(const int &p)
{
  if (p < (int)_player.size() && p >= 0)
    _player.erase(_player.begin() + p);
  if (_pos == p)
    {
      nextPos();
      return (true);
    }
  else
    return (false);
}

void		Team::deleteMemberIn(const std::vector<Position> &pos)
{
  size_t	i;
  size_t	size;
  size_t	s;
  size_t	sMax;
  Position	tmp;

  i = 0;
  size = _player.size();
  sMax = pos.size();
  s = 0;
  while (s < sMax)
    {
      i = 0;
      while (i < size)
	{
	  tmp = _player[i]->getPosition();
	  if (tmp.getX() == pos[s].getX() && tmp.getY() == pos[s].getY())
	    {
	      _player.erase(_player.begin() + i);
	      size--;
	    }
	  else
	    i++;
	}
      s++;
    }
}

int		Team::getScores() const
{
  size_t		i;
  size_t		size;
  int			life;

  size = _player.size();
  i = 0;
  life = 0;
  while (i < size)
    {
      life += _player[i]->getLife();
      i++;
    }
  return (life);
}

bool		Team::getStatus() const
{
  return (_status);
}

int		Team::getMun(const std::string &s)
{
  IWeapon	*tmp;

  tmp = _inventory->getStuff<IWeapon *>(s);
  if (tmp == NULL)
    return (0);
  else
    return (tmp->getMun());
}

Inventory		*Team::getInventory() const
{
  return (_inventory);
}

void			Team::setWeapon(const int &pos)
{
  _player[_pos]->setWeapon(_inventory->getWeapon(_inventory->getWeapons()[pos]));
  _player[_pos]->changeDir(_player[_pos]->getDir());
}

void			Team::setSelectInv(const int &pos)
{
  _inventory->setPos(pos);
}
void			Team::checkPos()
{
  if (_pos < 0)
    _pos = _player.size() - 1;
  else
    if (_pos >= (int)_player.size())
      _pos = 0;
}

void			Team::checkArrow(const bool &ck)
{
  size_t		i;
  size_t		size;

  i = 0;
  size = _player.size();
  while (i < size)
    {
      if (ck == true && (int)i == _pos)
	_player[i]->take(true);
      else
	_player[i]->setSprite(FRONT);
      i++;
    }
}
