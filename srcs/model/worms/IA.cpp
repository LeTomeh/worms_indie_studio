//
// IA.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:53:39 2016 Maxime LECOQ
// Last update Sun Jun  5 15:36:42 2016 Maxime LECOQ
//

#include	"IA.hh"

IA::IA() : _next(0), _cl(clock())
{
  _L = luaL_newstate();
  luaL_openlibs(_L);
  _switchInv = false;
  _teleActif = false;
  _selTele = false;
  _last = NULL;
  _dbg = false;
  _launch = false;
}

IA::~IA() {}

IA::IA(const IA &c) : _next(c._next), _cl(c._cl), _L(c._L), _last(c._last), _switchInv(c._switchInv), _teleActif(c._teleActif), _selTele(c._selTele), _dbg(c._dbg), _launch(c._launch) {}

IA	&IA::operator=(const IA &c)
{
  if (this != &c)
    {
      _next = c._next;
      _cl = c._cl;
      _L = c._L;
      _last = c._last;
      _switchInv = c._switchInv;
      _teleActif = c._teleActif;
      _selTele = c._selTele;
      _dbg = c._dbg;
      _launch = c._launch;
    }
  return (*this);
}

int				IA::returnDif(const int &x, const int &x2)
{
  if (x > x2)
    return (x - x2);
  else
    return (x2 - x);
}

std::string			IA::playTurn(std::vector<Team *> &teams, const int &my, Map *map)
{
  Team                  *team;
  int			my_p;
  std::vector<Player*> 	players;
  Player		*player;
  Position		cur;
  Position		close;
  Position		tmp;
  size_t		i;
  size_t		size;
  size_t		i2;
  size_t		size2;
  int			xTmp;
  int			xClo;
  int			yTmp;
  int			yClo;

  if (my >= (int)teams.size())
    return ("NEXTTURN");
  team = teams[my];
  team->checkPos();
  my_p = team->getPos();
  player = team->getPlayers()[my_p];
  if (_last != player)
    {
      _next = 0;
      _last = player;
      _save = tmp;
      _switchInv = false;
      _teleActif = false;
      _selTele = false;
      _dbg = false;
      _launch = false;
    }
  if (_next != 0 && ((double)clock() - (double)_cl) / CLOCKS_PER_SEC < _next)
    return ("");
  _cl = clock();
  cur = player->getPosition();
  i = 0;
  size = teams.size();
  while (i < size)
    {
      i2 = 0;
      players = teams[i]->getPlayers();
      size2 = players.size();
      while (teams[i] != team && i2 < size2)
	{
	  tmp = players[i2]->getPosition();
	  xTmp = returnDif(tmp.getX(), cur.getX());
	  xClo = returnDif(close.getX(), cur.getX());
	  yTmp = returnDif(tmp.getY(), cur.getY());
	  yClo = returnDif(close.getY(), cur.getY());
	  if ((xTmp < xClo && yTmp < yClo) || (close.getX() == 0 && close.getY() == 0))
	    close = tmp;
	  i2++;
	}
      i++;
    }
  try
    {
      manageTeleporter(player, close);
    }
  catch (CatchIt<std::string> &b) { return (b.getIt()); }
  if (luaL_loadfile(_L, "LUA/move_script.lua"))
    return ("NEXTTURN");
  lua_pushnumber(_L, cur.getX());
  lua_setglobal(_L, "pos_x");

  lua_pushnumber(_L, close.getX());
  lua_setglobal(_L, "enemy_x");
  lua_pcall(_L, 0, LUA_MULTRET, 0);

  lua_getglobal(_L, "ret");
  int dir = lua_tonumber(_L, -1);
  if (dir == 0)
    _dir = "NONE";
  else
    if (dir == 1)
      {
	_dir = "LEFT";
	player->changeDir(false);
      }
    else
      {
	_dir = "RIGHT";
	player->changeDir(true);
      }
  xClo = returnDif(close.getX(), cur.getX());
  if ((_save.getX() == cur.getX() && _save.getY() == cur.getY()) || _dir == "NONE" || xClo < 20)
    {
      _next = 2.00;
      try
	{
	  checkShoot(player, map, close);
	  checkTp(player, close);
	  if (player->getCurrentWeapon()->getName() == "Teleporter")
	    player->unsetWeapon();
	  return ("USETOOL");
	}
      catch (CatchIt<std::string> &b) { return (b.getIt()); }
    }
  else
    {
      _next = 0.10;
      if (rand() % 6 > 4)
	_dir = "JUMP" + _dir;
      _save = cur;
      return (_dir);
    }
  return ("");
}

void			IA::checkShoot(Player *p,  Map *map, const Position &close)
{
  Position		pos;

  (void)close;
  pos = p->getPosition();
  if (p->getCurrentWeapon()->getName() == "Teleporter")
    return;
  if (p->getDir() == true)
    {
      if (map->ground(pos.getX() + 1, pos.getY()) == false && map->ground(pos.getX() + 1, pos.getY() - 1) == false
	  && map->ground(pos.getX() + 2, pos.getY()) == false && map->ground(pos.getX() + 2, pos.getY() - 1) == false 
	  && map->ground(pos.getX() + 3, pos.getY()) == false && map->ground(pos.getX() + 3, pos.getY() - 1) == false
	  && map->ground(pos.getX() + 4, pos.getY()) == false && map->ground(pos.getX() + 4, pos.getY() - 1) == false
	  && map->ground(pos.getX() + 1, pos.getY() - 2) == false && map->ground(pos.getX() + 2, pos.getY() - 2) == false
	  && map->ground(pos.getX() + 3, pos.getY() - 2) == false && map->ground(pos.getX() + 4, pos.getY() - 2) == false
	  && map->ground(pos.getX() + 1, pos.getY() - 3) == false && map->ground(pos.getX() + 2, pos.getY() - 3) == false
	  && map->ground(pos.getX() + 3, pos.getY() - 3) == false  && map->ground(pos.getX() + 4, pos.getY() - 3) == false)
	throw CatchIt<std::string> ("USETOOL");
    }
  else
    {
      if (map->ground(pos.getX() - 1, pos.getY()) == false && map->ground(pos.getX() - 1, pos.getY() - 1) == false
	  && map->ground(pos.getX() - 2, pos.getY()) == false && map->ground(pos.getX() - 2, pos.getY() - 1) == false 
	  && map->ground(pos.getX() - 3, pos.getY()) == false && map->ground(pos.getX() - 3, pos.getY() - 1) == false
	  && map->ground(pos.getX() - 4, pos.getY()) == false && map->ground(pos.getX() - 4, pos.getY() - 1) == false
	  && map->ground(pos.getX() - 1, pos.getY() - 2) == false && map->ground(pos.getX() - 2, pos.getY() - 2) == false
	  && map->ground(pos.getX() - 3, pos.getY() - 2) == false && map->ground(pos.getX() - 4, pos.getY() - 2) == false
	  && map->ground(pos.getX() - 1, pos.getY() - 3) == false && map->ground(pos.getX() - 2, pos.getY() - 3) == false
	  && map->ground(pos.getX() - 3, pos.getY() - 3) == false && map->ground(pos.getX() - 4, pos.getY() - 3) == false)
	throw CatchIt<std::string> ("USETOOL");
    }
}

void			IA::checkTp(Player *p, const Position &close)
{
  int			tmpX;
  int			tmpY;

  tmpX = returnDif(p->getPosition().getX(), close.getX());
  tmpY = returnDif(p->getPosition().getY(), close.getY());
  if (tmpX > 50 || tmpY > 50)
    {
      if (p->getCurrentWeapon()->getName() != "Teleporter")
	{
	  _switchInv = true;
	  _next = 1.00;
	  throw CatchIt<std::string> ("INVENTORY");
	}
      else
	{
	  _teleActif = true;
	  throw CatchIt<std::string> ("USETOOL");
	}
    }
}

void		IA::manageTeleporter(Player *p, const Position &close)
{
  if (p->getInventory()->getWeapon(p->getInventory()->getWeapons()[p->getInventory()->getPos()])->getName() != "Teleporter" && _switchInv == true)
    {
      _next = 1.00;
      throw CatchIt<std::string> ("RIGHT");
    }
  else
    {
      if (_switchInv == true)
	{
	  _switchInv = false;
	  _next = 2.00;
	  _selTele = true;
	  throw CatchIt<int> (RETURN_K);
	}
      else
	if (_selTele == true)
	  {
	    _switchInv = false;
	    _selTele = false;
	    _teleActif = true;
	    throw CatchIt<std::string> ("USETOOL");
	  }
	else
	  if (_teleActif == true)
	    {
	      if (p->getInventory()->getWeapon(p->getInventory()->getWeapons()[p->getInventory()->getPos()])->getName() == "Teleporter" 
		  && p->getInventory()->getWeapon(p->getInventory()->getWeapons()[p->getInventory()->getPos()]) ->getMun() == 0 && _dbg == false)
		{
		  _dbg = true;
		  throw CatchIt<std::string> ("RIGHT");
		}
	      else
		if (_dbg == true)
		  {
		    _dbg = false;
		    _launch = true;
		    throw CatchIt<int> (RETURN_K);
		  }
		else
		  if (_launch == true)
		    {
		      _launch = false;
		      throw CatchIt<std::string> ("USETOOL");
		    }
	      _next = 0;
	      if (p->getCursor()->getPosition().getX() < close.getX())
		throw CatchIt<std::string> ("RIGHT");
	      else
		if (p->getCursor()->getPosition().getX() > close.getX())
		  throw CatchIt<std::string> ("LEFT");
	      if (p->getCursor()->getPosition().getY() < close.getY())
		throw CatchIt<std::string> ("DOWN");
	      else
		if (p->getCursor()->getPosition().getY() > close.getY())
		  throw CatchIt<std::string> ("UP");
	      _teleActif = false;
	      throw CatchIt<std::string> ("USETOOL");
	    }
    }
}
