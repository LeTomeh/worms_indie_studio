//
// Pixels.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 17:22:18 2016 Maxime LECOQ
// Last update Tue May 17 18:48:42 2016 Maxime LECOQ
//

#include	"Pixels.hh"

Pixels::Pixels() {}

Pixels::~Pixels() {}

Pixels::Pixels(const Pixels &c) { (void)c; }

Pixels	&Pixels::operator=(const Pixels &c)
{
  if (this != &c)
    {
    }
  return (*this);
}

void		Pixels::change(Map *map, const Position &pos, const Color &col, int alpha)
{
  png_byte	*ptr;

  alpha = alpha % 256;
  ptr = map->getPixels(pos.getX(), pos.getY());
  if (ptr != NULL)
    {
      ptr[0] = col.getRed();
      ptr[1] = col.getGreen();
      ptr[2] = col.getBlue();
      ptr[3] = alpha;
      map->setPixels(pos.getX(), pos.getY(), ptr);
    }
}
