//
// Map.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu May 12 13:12:38 2016 Maxime LECOQ
// Last update Thu Jun  2 13:28:04 2016 Maxime LECOQ
//

#include	"Map.hh"

Map::Map(const std::string &map)
{
  Vector	vec;

  try
    {
      _png = new Png(map);
      _rowPointers = _png->getRowPointers();
      _width = _png->getWidth();
      _height = _png->getHeight();
    }
  catch (AError const &e)
    {
      e.notCriticalError();
      throw ThrowErrorData(vec.getVector("Map\ncan't load the map", '\n'));
    }
  try
    {
      _wa = new Png("assets/waves.png");
    }
  catch (AError const &e)
    {
      e.notCriticalError();
      throw ThrowErrorData(vec.getVector("Map\nwaves.png not found", '\n'));
    }

}

Map::~Map()
{
  _wa->clean();
  _png->clean();
  delete (_wa);
  delete (_png);
}

Map::Map(const Map &c) : _png(c._png), _pos(c._pos), _width(c._width), _height(c._height), _rowPointers(c._rowPointers), _wa(c._wa) {}

Map	&Map::operator=(const Map &c)
{
  if (this != &c)
    {
      _png = c._png;
      _pos = c._pos;
      _width = c._width;
      _height = c._height;
      _rowPointers = c._rowPointers;
      _wa = c._wa;
    }
  return (*this);
}

bool		Map::save(const bool &c)
{
  return (_png->saveMap(c));
}

png_byte	*Map::getPixels(const int &x, const int &y) const
{
  return (_png->getPixels(x, y));
}

void		Map::setPixels(const int &x, const int &y, png_byte *p)
{
  _png->setPixels(x, y, p);
}

bool		Map::ground(const int &x, const int &y) const
{
  png_byte	*tmp;

  tmp = _png->getPixels(x, y);
  if (tmp != NULL && tmp[3] != 0)
    return (true);
  else
    return (false);
}

int		Map::luminosity(const int &x, const int &y) const
{
  png_byte	*tmp;

  tmp = _png->getPixels(x, y);
  if (tmp != NULL)
    return ((int)tmp[3]);
  else
    return (0);
}

int		Map::getWidth() const
{
  return (_png->getWidth());
}

int		Map::getHeight() const
{
  return (_png->getHeight());
}

bool		Map::isSave() const
{
  return (_png->isSave());
}

bool		Map::isEdit() const
{
  return (_png->edit());
}

void		Map::setSave(const bool &c)
{
  _png->setSave(c);
}

void		Map::addPosition(const Position &p)
{
  _pos.push_back(p);
}

std::vector<Position> Map::getPosition() const
{
  return (_pos);
}

Position		Map::getPosition(const int &d) const
{
  if (d < (int)_pos.size())
    return (_pos[d]);
  return (Position(0, 0));
}

void			Map::deletePosition(const int &d)
{
  if (d < (int)_pos.size())
    _pos.erase(_pos.begin() + d);
}

void			Map::deletePosition(const Position &d)
{
  size_t		i;
  size_t		size;

  i = 0;
  size = _pos.size();
  while (i < size)
    {
      if (_pos[i].getX() == d.getX() && _pos[i].getY() == d.getY())
	{
	  _pos.erase(_pos.begin() + i);
	  size--;
	}
      else
	i++;
    }
}

void			Map::resetPosition(const int &i, const Position &d)
{
  if (i < (int)_pos.size())
    {
      _pos[i].setX(d.getX());
      _pos[i].setY(d.getY());
    }
}

void			Map::resetPosition(const Position &prev, const Position &d)
{
  size_t		i;
  size_t		size;

  i = 0;
  size = _pos.size();
  while (i < size)
    {
      if (_pos[i].getX() == prev.getX() && _pos[i].getY() == prev.getY())
	{
	  _pos[i].setX(d.getX());
	  _pos[i].setY(d.getY());
	}
      i++;
    }
}

void			Map::deletePosition(const int &y, const int &x)
{
  size_t		i;
  size_t		size;

  i = 0;
  size = _pos.size();
  while (i < size)
    {
      if (_pos[i].getX() == x && _pos[i].getY() == y)
	{
	  _pos.erase(_pos.begin() + i);
	  size--;
	}
      else
	i++;
    }
}

void			Map::destroyMapWater(const int &lim)
{
  int           he;
  int           wi;
  int           xImg;
  int           xMax;
  int           yImg;
  int           yMax;
  png_bytep     *rowPointersI;
  png_byte      *rowI;
  png_byte      *ptrI;
  png_byte      *row;
  png_byte      *ptr;

  if (lim <= 0)
    return;
  try
    {
      rowPointersI = _wa->getRowPointers();
      yImg = 0;
      yMax = _wa->getHeight();
      xMax = _wa->getWidth();
      he = _height - (lim * 2);
      while (he < _height && yImg < yMax)
	{
	  wi = 0;
	  xImg = 0;
	  rowI = rowPointersI[yImg];
	  row = _rowPointers[he];
	  while (wi < _width && xImg < xMax)
	    {
	      ptrI = &(rowI[xImg * 4]);
	      ptr = &(row[wi * 4]);
	      if (ptrI[3] > 0)
		{
		  ptr[0] = 0;
		  ptr[1] = 0;
		  ptr[2] = 0;
		  ptr[3] = 0;
		}
	      wi++;
	      xImg++;
	    }
	  yImg++;
	  he++;
	}
      _png->setRowPointers(_rowPointers);
    }
  catch (AError const &e) {}
}

bool		Map::getWaterAt(const Position &p, const int &lvl)
{
  png_byte	*px;

  if (_height - p.getY() > (lvl * 2))
    return (false);
  px = _wa->getPixels(p.getX(), lvl * 2 - _height + p.getY());
  if (px[3] > 0)
    return (true);
  else
    return (false);
}

void			Map::destroy(const int &x, const int &y, const Png &cp)
{
  png_byte          *row;
  png_byte          *rowi;
  png_byte          *ptr;
  png_byte          *ptri;
  png_bytep         *rowPointers;
  int               i;
  int               i2;
  int               wi;
  int               he;

  rowPointers = cp.getRowPointers();
  i = 0;
  wi = cp.getWidth();
  he = cp.getHeight();
  while (i < he)
    {
      if ((i + y - (he / 2)) < _height && (i + y - (he / 2)) > 0)
	{
	  i2 = 0;
	  rowi = rowPointers[i];
	  row = _rowPointers[i + y - (he / 2)];
	  while (i2 < wi)
	    {
	      if ((i2 + x - (wi / 2)) < _width && (i2 + x - (wi / 2)) > 0)
		{
		  ptri = &(rowi[i2 * 4]);
		  ptr = &(row[(i2 + x - (wi / 2)) * 4]);
		  if (ptri[3] == 0)
		    {
		      ptr[0] = 0;
		      ptr[1] = 0;
		      ptr[2] = 0;
		      ptr[3] = 0;
		    }
		}
	      i2++;
	    }
	}
      i++;	  
    }
}
