//
// Projectile.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:53:39 2016 Maxime LECOQ
// Last update Wed Jun  1 09:00:48 2016 Maxime LECOQ
//

#include	"Projectile.hh"

Projectile::Projectile() : _active(false) {}

Projectile::~Projectile() {}

Projectile::Projectile(const Projectile &c) : _pos(c._pos), _list(c._list), _active(c._active), _sprite(c._sprite) {}

Projectile	&Projectile::operator=(const Projectile &c)
{
  if (this != &c)
    {
      _pos = c._pos;
      _list = c._list;
      _active = c._active;
      _sprite = c._sprite;
   }
  return (*this);
}

void		Projectile::active(const bool &c)
{
  _active = c;
}

bool		Projectile::getActive() const
{
  return (_active);
}

Position	Projectile::getPosition() const
{
  return (_pos);
}

void		Projectile::freeProjectile()
{
  std::vector<Position>	tmp;

  _list = tmp;
}

void		Projectile::affectPos()
{
  if (_list.size() != 0)
    {
      _pos = _list[0];
      _list.erase(_list.begin());
    }
  else
    _active = false;
}

void		Projectile::setDirection(const Position &p, const int &x, const int &y, const int &xM, const int &yM, const int &mul, const int &max, const int &gr, const int &wind)
{
  Position	tmp;
  int		i;
  int		gravi;

  gravi = 0;
  tmp = p;
  i = 0;
  (void)mul;
  while (i < max && tmp.getX() > 0 && tmp.getY() > 0 && tmp.getX() < xM && tmp.getY() < yM)
    {
      _list.push_back(tmp);
      tmp.setX((tmp.getX() + ((x * mul) / 5)) + (gravi * wind));
      tmp.setY((tmp.getY() - (y / 5)) + gravi);
      i++;
      if (i % 20 == 0)
	gravi += gr;
    }
}

void		Projectile::setSprite(const std::string &s)
{
  _sprite = s;
}

const std::string	&Projectile::getSprite() const
{
  return (_sprite);
}

void		Projectile::setPosition(const Position &p)
{
  _pos = p;
}
