//
// ManagePlayer.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Mon May 30 09:53:39 2016 Maxime LECOQ
// Last update Sun Jun  5 06:15:29 2016 Maxime LECOQ
//

#include	"ManagePlayer.hh"

ManagePlayer::ManagePlayer(View *v) : _view(v)
{
  _dispatch["LEFT"] = &ManagePlayer::left;
  _dispatch["RIGHT"] = &ManagePlayer::right;
  _dispatch["UP"] = &ManagePlayer::up;
  _dispatch["DOWN"] = &ManagePlayer::down;
  _dispatch2["UP"] = &ManagePlayer::up2;
  _dispatch2["DOWN"] = &ManagePlayer::down2;
  _weap["Teleporter"] = &ManagePlayer::teleport;
  _weap["Shotgun"] = &ManagePlayer::shotgun;
  _weap["Pistol"] = &ManagePlayer::pistol;
  _weap["Grenade"] = &ManagePlayer::grenade;
  _weap["Bazooka"] = &ManagePlayer::bazooka;
  _weap["FirePunch"] = &ManagePlayer::firePunch;
  _cpt = 1;
  _cl = clock();
  _turn = false;
  _prior = false;
}

ManagePlayer::~ManagePlayer() {}

ManagePlayer::ManagePlayer(const ManagePlayer &c) : _view(c._view), _converter(c._converter), _dispatch(c._dispatch), _dispatch2(c._dispatch), _cl(c._cl), _last(c._last), _cpt(c._cpt), _turn(c._turn), _prior(c._prior) {}

ManagePlayer	&ManagePlayer::operator=(const ManagePlayer &c)
{
  if (this != &c)
    {
      _view = c._view;
      _converter = c._converter;
      _dispatch = c._dispatch;
      _dispatch2 = c._dispatch2;
      _cl = c._cl;
      _last = c._last;
      _cpt = c._cpt;
      _turn = c._turn;
      _prior = c._prior;
    }
  return (*this);
}

void			ManagePlayer::setTouch(std::map<int, std::string> c)
{
  _converter = c;
}

void			ManagePlayer::playTurn(Player *p, Map *map, const int &ev, const int &wind, std::vector<Team *> &team)
{
  _turn = false;
  _prior = false;
  if (p->getCurrentWeapon() == NULL)
    return;
  if (_converter.find(ev) != _converter.end())
    {
      if (_dispatch.find(_converter[ev]) != _dispatch.end()
	  && (p->getCurrentWeapon() != NULL && p->getCurrentWeapon()->getName() == "Teleporter") && p->teleUse() == true)
	(this->*_dispatch[_converter[ev]])(p, map);
      else
	if (_dispatch2.find(_converter[ev]) != _dispatch2.end()
	    && (p->getCurrentWeapon() != NULL && p->getCurrentWeapon()->getName() != "FirePunch"))
	  (this->*_dispatch2[_converter[ev]])(p, map);
	else
	  if (_converter[ev] == "USETOOL")
	    execute(p, map, wind, team);
    }
}

void			ManagePlayer::execute(Player *p, Map *map, const int &wind, std::vector<Team *> &team)
{
  if (_weap.find(p->getCurrentWeapon()->getName()) != _weap.end())
    (this->*_weap[p->getCurrentWeapon()->getName()])(p, map, wind, team);
}

void			ManagePlayer::left(Player *p, Map *map)
{
  if (_last == "LEFT" && ((double)clock() - (double)_cl) / CLOCKS_PER_SEC < 0.50)
    _cpt++;
  else
    _cpt = 1;
  p->getCursor()->setX(p->getCursor()->getX() - _cpt);
  if (p->getCursor()->getX() < 1)
    p->getCursor()->setX(1);
  if (map->ground(p->getCursor()->getPosition().getX(), p->getCursor()->getPosition().getY()) == true)
    p->getCursor()->setSprite(CURSORERR);
  else
    p->getCursor()->setSprite(CURSOR);
  _cl = clock();
  _last = "LEFT";
  throw CatchIt<bool> (true);
}

void			ManagePlayer::right(Player *p, Map *map)
{
  if (_last == "RIGHT" && ((double)clock() - (double)_cl) / CLOCKS_PER_SEC < 0.50)
    _cpt++;
  else
    _cpt = 1;
  p->getCursor()->setX(p->getCursor()->getX() + _cpt);
  if (p->getCursor()->getX() > map->getWidth())
    p->getCursor()->setX(map->getWidth());
  if (map->ground(p->getCursor()->getPosition().getX(), p->getCursor()->getPosition().getY()) == true)
    p->getCursor()->setSprite(CURSORERR);
  else
    p->getCursor()->setSprite(CURSOR);
  _cl = clock();
  _last = "RIGHT";
  throw CatchIt<bool> (true);
}

void			ManagePlayer::down(Player *p, Map *map)
{
  if (_last == "DOWN" && ((double)clock() - (double)_cl) / CLOCKS_PER_SEC < 0.50)
    _cpt++;
  else
    _cpt = 1;
  p->getCursor()->setY(p->getCursor()->getY() + _cpt);
  if (p->getCursor()->getY() > map->getHeight())
    p->getCursor()->setY(map->getHeight());
  if (map->ground(p->getCursor()->getPosition().getX(), p->getCursor()->getPosition().getY()) == true)
    p->getCursor()->setSprite(CURSORERR);
  else
    p->getCursor()->setSprite(CURSOR);
  _cl = clock();
  _last = "DOWN";
  (void)map;
  throw CatchIt<bool> (true);
}

void			ManagePlayer::up(Player *p, Map *map)
{
  if (_last == "UP" && ((double)clock() - (double)_cl) / CLOCKS_PER_SEC < 0.50)
    _cpt++;
  else
    _cpt = 1;
  p->getCursor()->setY(p->getCursor()->getY() - _cpt);
  if (p->getCursor()->getY() < 0)
    p->getCursor()->setY(0);
  if (map->ground(p->getCursor()->getPosition().getX(), p->getCursor()->getPosition().getY()) == true)
    p->getCursor()->setSprite(CURSORERR);
  else
    p->getCursor()->setSprite(CURSOR);
  _cl = clock();
  _last = "UP";
  (void)map;
  throw CatchIt<bool> (true);
}

void			ManagePlayer::teleport(Player *p, Map *map, const int &wind, std::vector<Team *> &t)
{
  (void)wind;
  (void)t;
  if (p->teleUse() == false)
    p->setTele(true);
  else
    {
      if (map->ground(p->getCursor()->getPosition().getX(), p->getCursor()->getPosition().getY()) == true)
	_view->setEffect(FAIL);
      else
	{
	  p->setPosition(p->getCursor()->getPosition());
	  p->getCurrentWeapon()->setMun(p->getCurrentWeapon()->getMun() - 1);
	  _view->setEffect(TELEPORTER);
	  if (p->getCurrentWeapon()->getMun() == 0 || p->getTeamName().substr(0, 3) == "IA_")
	    p->unsetWeapon();
	  _turn = true;
	}
    }
  throw CatchIt<bool> (true);
}

void			ManagePlayer::firePunch(Player *p, Map *m, const int &wind, std::vector<Team *> &team)
{
  Position		pos;
  Player		*player;
  int			i;
  int			i2;

  pos = p->getPosition();
  pos.setY(pos.getY() - 15);
  if (pos.getY() < 0)
    pos.setY(0);
  if (p->getDir() == true)
    pos.setX(pos.getX() + 15);
  else
    pos.setX(pos.getX() - 15);
  i = 0;
  player = NULL;
  while (i < 15)
    {
      if (i == 0)
	i2 = 1;
      else
	i2 = 0;
      while (i2 < 20)
	{
	  if (pos.getX() + i > 0 && pos.getY() - i2 > 0 && pos.getX() + i < m->getWidth() && pos.getY() - i2 < m->getHeight())
	    {
	      player = getPlayer(Position(pos.getX() + i, pos.getY() - i2), team);
	      if (player != NULL && player != p)
		{
		  i = 15;
		  i2 = 15;
		  break;
		}
	      if (pos.getX() + i > 0 && pos.getY() + i2 > 0 && pos.getX() + i < m->getWidth() && pos.getY() + i2 < m->getHeight())
		{
		  player = getPlayer(Position(pos.getX() + i, pos.getY() + i2), team);
		  if (player != NULL && player != p)
		    {
		      i = 15;
		      i2 = 15;
		      break;
		    }
		}
	      if (pos.getX() - i > 0 && pos.getY() + i2 > 0 && pos.getX() - i < m->getWidth() && pos.getY() + i2 < m->getHeight())
		{
		  player = getPlayer(Position(pos.getX() - i, pos.getY() + i2), team);
		  if (player != NULL && player != p)
		    {
		      i = 15;
		      i2 = 15;
		      break;
		    }
		}
	      if (pos.getX() - i > 0 && pos.getY() - i2 > 0 && pos.getX() - i < m->getWidth() && pos.getY() - i2 < m->getHeight())
		{
		  player = getPlayer(Position(pos.getX() - i, pos.getY() - i2), team);
		  if (player != NULL && player != p)
		    {
		      i = 15;
		      i2 = 15;
		      break;
		    }
		}
	    }
	  i2++;
	}
      i++;
    }
  if (player != NULL)
    {
      player->setLife(player->getLife() - p->getCurrentWeapon()->getDamage());
      if (player->getLife() < 0)
	player->setLife(0);
      _view->setEffect(FIREPUNCH);
    }
  else
    _view->setEffect(FAIL);
  _turn = true;
  (void)wind;
  throw CatchIt<bool> (true);
}

void			ManagePlayer::grenade(Player *p, Map *map, const int &wind, std::vector<Team *> &t)
{
  Projectile		*bul;
  Position		tmp;

  (void)t;
  bul = _view->getProjectile();
  bul->active(true);
  tmp = p->getPosition();
  tmp.setY(tmp.getY() - 15);
  if (tmp.getY() < 0)
    tmp.setY(0);
  if (p->getDir() == true)
    bul->setDirection(tmp, p->getCursor()->getPosition().getX() - p->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), 1, map->getWidth(), 3, 0);
  else
    bul->setDirection(tmp, p->getPosition().getX() - p->getCursor()->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), -1, map->getWidth(), 3, 0);
  bul->affectPos();
  if (p->getDir() == true)
    bul->setSprite(p->getCurrentWeapon()->getBulletRight());
  else
    bul->setSprite(p->getCurrentWeapon()->getBulletLeft());
  _view->setEffect(LAUNCH);
  _prior = true;
  _view->setProjectile(bul);
  (void)wind;
  throw CatchIt<bool> (true);
}

void			ManagePlayer::bazooka(Player *p, Map *map, const int &wind, std::vector<Team *> &t)
{
  Projectile		*bul;
  Position		tmp;

  (void)t;
  bul = _view->getProjectile();
  bul->active(true);
  tmp = p->getPosition();
  tmp.setY(tmp.getY() - 15);
  if (tmp.getY() < 0)
    tmp.setY(0);
  if (p->getDir() == true)
    bul->setDirection(tmp, p->getCursor()->getPosition().getX() - p->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), 1, map->getWidth(), 1, wind);
  else
    bul->setDirection(tmp, p->getPosition().getX() - p->getCursor()->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), -1, map->getWidth(), 1, wind);
  bul->affectPos();
  if (p->getDir() == true)
    bul->setSprite(p->getCurrentWeapon()->getBulletRight());
  else
    bul->setSprite(p->getCurrentWeapon()->getBulletLeft());
  _view->setEffect(ROLAU);
  _prior = true;
  _view->setProjectile(bul);
  throw CatchIt<bool> (true);
}

void			ManagePlayer::shotgun(Player *p, Map *map, const int &wind, std::vector<Team *> &t)
{
  Projectile		*bul;
  Position		tmp;

  (void)wind;
  (void)t;
  bul = _view->getProjectile();
  bul->active(true);
  tmp = p->getPosition();
  tmp.setY(tmp.getY() - 15);
  if (tmp.getY() < 0)
    tmp.setY(0);
  if (p->getDir() == true)
    bul->setDirection(tmp, p->getCursor()->getPosition().getX() - p->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), 1, 100, 0, 0);
  else
    bul->setDirection(tmp, p->getPosition().getX() - p->getCursor()->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), -1, 100, 0, 0);
  bul->affectPos();
  if (p->getDir() == true)
    bul->setSprite(p->getCurrentWeapon()->getBulletRight());
  else
    bul->setSprite(p->getCurrentWeapon()->getBulletLeft());
  _view->setEffect(SHOTGUN);
  _prior = true;
  _view->setProjectile(bul);
  throw CatchIt<bool> (true);
}

void			ManagePlayer::pistol(Player *p, Map *map, const int &wind, std::vector<Team *> &t)
{
  Projectile		*bul;
  Position		tmp;

  (void)wind;
  (void)t;
  bul = _view->getProjectile();
  bul->active(true);
  tmp = p->getPosition();
  tmp.setY(tmp.getY() - 15);
  if (tmp.getY() < 0)
    tmp.setY(0);
  if (p->getDir() == true)
    bul->setDirection(tmp, p->getCursor()->getPosition().getX() - p->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), 1, map->getWidth(), 0, 0);
  else
    bul->setDirection(tmp, p->getPosition().getX() - p->getCursor()->getPosition().getX(),
		      p->getPosition().getY() - p->getCursor()->getPosition().getY() - 15,
		      map->getWidth(), map->getHeight(), -1, map->getWidth(), 0, 0);
  bul->affectPos();
  if (p->getDir() == true)
    bul->setSprite(p->getCurrentWeapon()->getBulletRight());
  else
    bul->setSprite(p->getCurrentWeapon()->getBulletLeft());
  _view->setEffect(PISTOL);
  _prior = true;
  _view->setProjectile(bul);
  throw CatchIt<bool> (true);
}

void			ManagePlayer::down2(Player *p, Map *map)
{
  int			angle;
  int			absAngle;
  Position		pos;

  if (p->getCurrentWeapon()->getName() == "Teleporter")
    return;
  if (p->getCursor()->getActive() == true)
    {
      angle = p->getCursor()->getAngle() - 1;
      if (angle < -40)
	angle = -40;
      p->getCursor()->setAngle(angle);
      pos = p->getPosition();
      pos.setY(pos.getY() - 15);
      if (angle < 0)
	absAngle = -angle;
      else
	absAngle = angle;
      if (p->getDir() == true)
	{
	  pos.setX(pos.getX() + 40);
	  pos.setX(pos.getX() - absAngle);
	  pos.setY(pos.getY() - angle);
	}
      else
	{
	  pos.setX(pos.getX() - 40);
	  pos.setX(pos.getX() + absAngle);
	  pos.setY(pos.getY() - angle);
	}
      pos.reset(0, map->getWidth(), 0, map->getHeight());
      p->getCursor()->setPosition(pos);
    }
  throw CatchIt<bool> (true);
}

void			ManagePlayer::up2(Player *p, Map *map)
{
  int			angle;
  int			absAngle;
  Position		pos;

  if (p->getCurrentWeapon()->getName() == "Teleporter")
    return;
  if (p->getCursor()->getActive() == true)
    {
      angle = p->getCursor()->getAngle() + 1;
      if (angle > 40)
	angle = 40;
      p->getCursor()->setAngle(angle);
      pos = p->getPosition();
      pos.setY(pos.getY() - 15);
      if (angle < 0)
	absAngle = -angle;
      else
	absAngle = angle;
      if (p->getDir() == true)
	{
	  pos.setX(pos.getX() + 40);
	  pos.setX(pos.getX() - absAngle);
	  pos.setY(pos.getY() - angle);
	}
      else
	{
	  pos.setX(pos.getX() - 40);
	  pos.setX(pos.getX() + absAngle);
	  pos.setY(pos.getY() - angle);
	}
      pos.reset(0, map->getWidth(), 0, map->getHeight());
      p->getCursor()->setPosition(pos);
    }
  throw CatchIt<bool> (true);
}

bool			ManagePlayer::getTurn() const
{
  return (_turn);
}

bool			ManagePlayer::getPrior() const
{
  return (_prior);
}

void			ManagePlayer::checkColisionMap(Map *m, const int &x, const int &y, bool &ck)
{
  if (x < 0 || y < 0 || x > m->getWidth() || y > m->getHeight())
    return;
  else
    if (m->ground(x, y) == true)
      ck = true;
}

void			ManagePlayer::checkColisionDrop(const Position &pos, ManageDrop *drop, bool &ck)
{
  if (ck == true)
    return;
  else
    if (drop->checkIn(pos) == true)
      ck = true;
} 
void			ManagePlayer::checkColisionPlayerIn(Player *p, std::vector<Team *> &team, const int &x, const int &y, Map *m, bool &ck)
{
  Player		*player;
  int			i;
  int			i2;

  if (ck == true)
    return;
  i = 0;
  while (i < 15)
    {
      if (i == 0)
	i2 = 1;
      else
	i2 = 0;
      while (i2 < 20)
	{
	  if (x + i > 0 && y - i2 > 0 && x + i < m->getWidth() && y - i2 < m->getHeight())
	    {
	      player = getPlayer(Position(x + i, y - i2), team);
	      if (player != NULL && player != p)
		ck = true;
	      if (x + i > 0 && y + i2 > 0 && x + i < m->getWidth() && y + i2 < m->getHeight())
		{
		  player = getPlayer(Position(x + i, y + i2), team);
		  if (player != NULL && player != p)
		    ck = true;
		}
	      if (x - i > 0 && y + i2 > 0 && x - i < m->getWidth() && y + i2 < m->getHeight())
		{
		  player = getPlayer(Position(x - i, y + i2), team);
		  if (player != NULL && player != p)
		    ck = true;
		}
	      if (x - i > 0 && y - i2 > 0 && x - i < m->getWidth() && y - i2 < m->getHeight())
		{
		  player = getPlayer(Position(x - i, y - i2), team);
		  if (player != NULL && player != p)
		    ck = true;
		}
	    }
	  i2++;
	}
      i++;
    }
}

int			ManagePlayer::getDamage(const int &da, const int &xO, const int &yO, const int &xP, const int &yP)
{
  int		x;
  int		y;
  int		res;

  if (xP > xO)
    x = xP - xO;
  else
    x = xO - xP;
  if (yP > yO)
    y = yP - yO;
  else
    y = yO - yP;
  res = (x + y) / 10;
  res = da - res;
  if (res <= 0)
    res = 1;
  return (res);
}

void			ManagePlayer::kill(const int &x, const int &y, IWeapon *weap, std::vector<Team *> &team, Map *map)
{
  if (weap->getPow().size() == 0)
    return;
  else
    {
      try
	{
	  Png		    cp(weap->getPow());
	  png_byte          *row;
	  png_byte          *ptr;
	  png_bytep         *rowPointers;
	  int               i;
	  int               i2;
	  int               wi;
	  int               he;
	  Player	    *pl;

	  map->destroy(x, y, cp);
	  rowPointers = cp.getRowPointers();
	  i = 0;
	  wi = cp.getWidth();
	  he = cp.getHeight();
	  while (i < he)
	    {
	      if ((i + y - (he / 2)) < map->getHeight() && (i + y - (he / 2)) > 0)
		{
		  i2 = 0;
		  row = rowPointers[i];
		  while (i2 < wi)
		    {
		      if ((i2 + x - (wi / 2)) < map->getWidth() && (i2 + x - (wi / 2)) > 0)
			{
			  ptr = &(row[i2 * 4]);
			  if (ptr[3] == 0)
			    {
			      if ((pl = getPlayer(Position(i2 + x - (wi / 2), i + y - (he / 2)), team)) != NULL)
				{
				  pl->setLife(pl->getLife() - getDamage(weap->getDamage(), x, y, pl->getPosition().getX(), pl->getPosition().getY()));
				  if (pl->getLife() < 0)
				  pl->setLife(0);
				  (void)pl;
				}
			    }
			}
		      i2++;
		    }
		}
	      i++;
	    }
	  cp.clean();
	}
      catch (AError const &e) {}
    }
  (void)team;
}

void			ManagePlayer::playAnimation(Player *p, Map *map, std::vector<Team *> &team, ManageDrop *drop)
{
  int			i;
  Projectile		*bul;
  Position		tmp;
  bool			ck;

  i = 0;
  bul = _view->getProjectile();
  ck = false;
  while (i < 3 && bul->getActive() == true && ck == false)
    {
      tmp = bul->getPosition();
      checkColisionMap(map, tmp.getX(), tmp.getY(), ck);
      checkColisionPlayerIn(p, team, tmp.getX(), tmp.getY(), map, ck);
      checkColisionDrop(tmp, drop, ck);
      bul->affectPos();
      i++;
    }
  if (bul->getActive() == false || ck == true)
    {
      if (ck == true)
	{
	  p->getCurrentWeapon();
	  kill(tmp.getX(), tmp.getY(), p->getCurrentWeapon(), team, map);
	  _view->setEditIt(true);
	  if (p->getCurrentWeapon()->getName() == "Bazooka")
	    _view->setEffect(BAZOEXP);
	  else
	    if (p->getCurrentWeapon()->getName() == "Grenade")
	      {
		_view->setEffect(GRECOL);
		_view->setEffect(GREEXP);
	      }
	  bul->active(false);
	  bul->freeProjectile();
	  _view->setMeshStatus(true);
	  _view->setMeshName(p->getCurrentWeapon()->getMesh());
	  _view->setMeshTexture(p->getCurrentWeapon()->getTexture());
	  _view->setMeshSound(p->getCurrentWeapon()->getMeshSound());
	}
      _prior = false;
      if (p->getCurrentWeapon()->getName() == "Pistol")
	{
	  if (p->getCurrentWeapon()->getMun() > 0)
	    p->getCurrentWeapon()->setMun(p->getCurrentWeapon()->getMun() - 1);
	  if (p->getCurrentWeapon()->getMun() == 0)
	    p->unsetWeapon();
	}
    }
}

Player			*ManagePlayer::getPlayer(const Position &p, std::vector<Team *> &team)
{
  size_t        i;
  size_t        size;
  size_t        x;
  size_t        size2;
  std::vector<Player *> players;

  i = 0;
  size = team.size();
  while (i < size)
    {
      x = 0;
      players = team[i]->getPlayers();
      size2 = players.size();
      while (x < size2)
        {
          if (p.getX() == players[x]->getPosition().getX() && p.getY() == players[x]->getPosition().getY())
            return (players[x]);
          x++;
        }
      i++;
    }
  return (NULL);
}
