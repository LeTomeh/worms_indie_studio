//
// Grenade.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:52:44 2016 Maxence Gaumont
// Last update Fri Jun  3 09:09:26 2016 Maxime LECOQ
//

#include "Grenade.hh"

Grenade::Grenade(int damage, int munitions, const std::string &name) : AThrownWeapon(damage, munitions, name, ANIMGRENADE, TEGRENADE, SOUNDGRENADE)
{
  _on = WGRENADE;
  _off = WGRENADE;
  _description = DGRENADE;
  _leftSp = GRENADEL;
  _rightSp = GRENADER;
  _leftBul = BULLETGRESHOT;
  _rightBul = BULLETGREREV;
  _pow = GRENADEPOCHOIR;
}

Grenade::~Grenade()
{
}

AWeapon		*Grenade::clone() const
{
  return (new Grenade(*this));
}

void		Grenade::attack() const
{
  std::cout << _Name << ": cling... BOUUM" << std::endl;
}

void                    Grenade::proMode()
{
}
