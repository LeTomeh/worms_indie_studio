//
// FirePunch.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:52:44 2016 Maxence Gaumont
// Last update Fri Jun  3 09:09:59 2016 Maxime LECOQ
//

#include "FirePunch.hh"

FirePunch::FirePunch(int damage, int munitions, const std::string &name) : AMeleeWeapon(damage, munitions, name, ANIMFIREPUNCH, TEFIREPUNCH, SOUNDFIREPUNCH)
{
  _on = WFIREPUNCH;
  _off = WFIREPUNCH;
  _description = DFIREPUNCH;
  _leftSp = FIREL;
  _rightSp = FIRER;
}

FirePunch::~FirePunch()
{
}

AWeapon			*FirePunch::clone() const
{
  return (new FirePunch(*this));
}

void			FirePunch::attack() const
{
  std::cout << _Name << ": Fire... PUNCH !" << std::endl;
}

void                    FirePunch::proMode()
{
}
