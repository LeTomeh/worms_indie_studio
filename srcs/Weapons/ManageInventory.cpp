//
// ManageInventory.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed May  4 14:44:58 2016 Maxence Gaumont
// Last update Sun May 29 18:28:03 2016 Maxime LECOQ
//

#include "ManageInventory.hh"

ManageInventory::ManageInventory() : stuff()
{
}

ManageInventory::~ManageInventory()
{
}

void		ManageInventory::addDefWeapon()
{
  StuffFactory<AWeapon*>	*fac = new StuffFactory<AWeapon*>;

  fac->save("Bazooka", new Bazooka);
  fac->save("Grenade", new Grenade);
  fac->save("Shotgun", new Shotgun);
  fac->save("FirePunch", new FirePunch);

  stuff.fillStuff<IWeapon*>(fac->create("Bazooka"));
  stuff.fillStuff<IWeapon*>(fac->create("Grenade"));
  stuff.fillStuff<IWeapon*>(fac->create("Shotgun"));
  stuff.fillStuff<IWeapon*>(fac->create("FirePunch"));

  stuff.fillStuff<IWeapon*>(new DecoratorStat(*(fac->create("Bazooka")), 0, 3, "Teleporter"));
  stuff.fillStuff<IWeapon*>(new DecoratorStat(*(fac->create("Shotgun")), 25, 20, "Pistol"));
}

Inventory		ManageInventory::getStuff() const
{
  return (stuff);
}
