//
// Shotgun.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:52:44 2016 Maxence Gaumont
// Last update Fri Jun  3 09:10:24 2016 Maxime LECOQ
//

#include "Shotgun.hh"

Shotgun::Shotgun(int damage, int munitions, const std::string &name) : AFireWeapon(damage, munitions, name, ANIMSHOTGUN, TESHOTGUN, SOUNDSHOTGUN)
{
  _on = WSHOTGUN;
  _off = WSHOTGUN;
  _description = DSHOTGUN;
  _leftSp = SHOTGUNL;
  _rightSp = SHOTGUNR;
  _leftBul = BULLETSHOT;
  _rightBul = BULLETSHOTREV;
  _pow = SHOTGUNPOCHOIR;
}

Shotgun::~Shotgun()
{
}

AWeapon			*Shotgun::clone() const
{
  return (new Shotgun(*this));
}

void			Shotgun::attack() const
{
  std::cout << _Name << ": BOOM ! Crikcrik" << std::endl;
}

void                    Shotgun::proMode()
{
}
