//
// AMeleeWeapon.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 17:51:41 2016 Maxence Gaumont
// Last update Fri Jun  3 01:08:58 2016 Maxime LECOQ
//

#include "AMeleeWeapon.hh"

AMeleeWeapon::AMeleeWeapon(int damage, int munitions, const std::string &name, const std::string &m, const std::string &t, const std::string &s) : AWeapon(damage, munitions, name, m, t, s)
{
}

void	AMeleeWeapon::attack() const
{
  std::cout << "attack with a Melee Weapon" << std::endl;
}
