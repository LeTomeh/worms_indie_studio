//
// main.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:44:09 2016 Maxence Gaumont
// Last update Thu May  5 14:37:35 2016 thomas alexandre
//

#include "ManageInventory.hh"

int	main()
{
  ManageInventory	inv;

  inv.addDefWeapon();

  Inventory		*stuff = new Inventory(inv.getStuff());

  stuff->getStuff<IWeapon*>("Bazooka")->attack();
  stuff->getStuff<IWeapon*>("Grenade")->attack();
  stuff->getStuff<IWeapon*>("Shotgun")->attack();
  stuff->getStuff<IWeapon*>("FirePunch")->attack();
  stuff->getStuff<IWeapon*>("Homing Missile")->attack();
  stuff->getStuff<IWeapon*>("Pistol")->attack();

  delete (stuff);

  return (0);
}
