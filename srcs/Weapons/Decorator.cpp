//
// Decorator.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Mon May  2 16:15:51 2016 Maxence Gaumont
// Last update Fri Jun  3 01:07:00 2016 Maxime LECOQ
//

#include "Decorator.hh"

Decorator::Decorator(AWeapon &weapon, int damage, int munitions, const std::string &name) : AWeapon(damage, munitions, name, "", "", ""), m_base(weapon)
{
}

Decorator::~Decorator()
{
}
