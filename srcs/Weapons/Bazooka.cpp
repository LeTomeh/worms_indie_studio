//
// Bazooka.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Wed Apr 27 11:52:44 2016 Maxence Gaumont
// Last update Fri Jun  3 09:09:05 2016 Maxime LECOQ
//

#include "Bazooka.hh"

Bazooka::Bazooka(int damage, int munitions, const std::string &name) : ALaunchWeapon(damage, munitions, name, ANIMBAZOOKA, TEBAZOOKA, SOUNDBAZOOKA)
{
  _on = WBAZOOKA;
  _off = WBAZOOKA;
  _description = DBAZOOKA;
  _leftSp = BAZOOL;
  _rightSp = BAZOOR;
  _leftBul = BULLETBAZOOSHOT;
  _rightBul = BULLETBAZOOREV;
  _pow = BAZOOKAPOCHOIR;
}

Bazooka::~Bazooka()
{
}

AWeapon			*Bazooka::clone() const
{
  return (new Bazooka(*this));
}

void			Bazooka::attack() const
{
  std::cout << _Name << ": fshouu BAM !" << std::endl;
}

void			Bazooka::proMode()
{
}
