//
// AWeapon.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Thu Apr 28 10:30:14 2016 Maxence Gaumont
// Last update Sat Jun  4 09:28:58 2016 Maxime LECOQ
//

#include "AWeapon.hh"

AWeapon::AWeapon(int damage, int munitions, const std::string &name, const std::string &m, const std::string &t, const std::string &s)
{
  _Name = name;
  _Dmg = damage;
  _Mun = munitions;
  _pow = "";
  _mesh = m;
  _texture = t;
  _meshSound = s;
}

AWeapon::AWeapon(const AWeapon &o) : _Dmg(o._Dmg), _Mun(o._Mun), _Name(o._Name), _on(o._on), _off(o._off), _description(o._description), _leftSp(o._leftSp), _rightSp(o._rightSp), _leftBul(o._leftBul), _rightBul(o._rightBul), _pow(o._pow), _mesh(o._mesh), _texture(o._texture), _meshSound(o._meshSound) {}

AWeapon		&AWeapon::operator=(const AWeapon &o)
{
  if (&o)
    {
      _Name = o.getName();
      _Dmg = o.getDamage();
      _Mun = o.getMun();
      _on = o._on;
      _off = o._off;
      _description = o._description;
      _leftSp = o._leftSp;
      _rightSp = o._rightSp;
      _leftBul = o._leftBul;
      _rightBul = o._rightBul;
      _pow = o._pow;
      _mesh = o._mesh;
      _texture = o._texture;
      _meshSound = o._meshSound;
    }
  return *this;
}

const std::string	&AWeapon::getName() const
{
  return (_Name);
}

int		AWeapon::getDamage() const
{
  return (_Dmg);
}

int		AWeapon::getMun() const
{
  return (_Mun);
}

AWeapon		*AWeapon::clone()
{
  return (this);
}

const std::string	&AWeapon::getPictureOn() const
{
  return (_on);
}

const std::string	&AWeapon::getPictureOff() const
{
  return (_off);
}

const std::string	&AWeapon::getDescription() const
{
  return (_description);
}

void		AWeapon::setMun(const int &i)
{
  _Mun = i;
}

void		AWeapon::setPictureOn(const std::string &s)
{
  _on = s;
}

void		AWeapon::setPictureOff(const std::string &s)
{
  _off = s;
}

void		AWeapon::setDescription(const std::string &s)
{
  _description = s;
}

const std::string     &AWeapon::getLeftSprite() const
{
  return (_leftSp);
}

const std::string     &AWeapon::getRightSprite() const
{
  return (_rightSp);
}

void                  AWeapon::setLeftSprite(const std::string &s)
{
  _leftSp = s;
}

void                  AWeapon::setRightSprite(const std::string &s)
{
  _rightSp = s;
}

const std::string     &AWeapon::getBulletLeft() const
{
  return (_leftBul);
}

const std::string     &AWeapon::getBulletRight() const
{
  return (_rightBul);
}

void                  AWeapon::setBulletLeft(const std::string &s)
{
  _leftBul = s;
}

void                  AWeapon::setBulletRight(const std::string &s)
{
  _rightBul = s;
}

void			AWeapon::setPow(const std::string &c)
{
  _pow = c;
}

const std::string	&AWeapon::getPow() const
{
  return (_pow);
}

const std::string	&AWeapon::getMesh() const
{
  return (_mesh);
}

const std::string	&AWeapon::getTexture() const
{
  return (_texture);
}

const std::string	&AWeapon::getMeshSound() const
{
  return (_meshSound);
}

void			AWeapon::setMesh(const std::string &s)
{
  _mesh = s;
}

void			AWeapon::setTexture(const std::string &s)
{
  _texture = s;
}

void			AWeapon::setSound(const std::string &s)
{
  _meshSound = s;
}

void			AWeapon::setDamage(const int &c)
{
  _Dmg = c;
}
