//
// DecoratorStat.cpp for  in /home/gaumon_a/rendu/ProjetS2/cpp_indie_studio/srcs/Weapons
// 
// Made by Maxence Gaumont
// Login   <gaumon_a@epitech.net>
// 
// Started on  Mon May  2 17:29:05 2016 Maxence Gaumont
// Last update Sat May 28 18:52:17 2016 Maxime LECOQ
//

#include "DecoratorStat.hh"

DecoratorStat::DecoratorStat(AWeapon &weapon, int damage, int munitions, const std::string &name) : Decorator(weapon, damage, munitions, name)
{
}


IWeapon		*DecoratorStat::clone() const
{
  return (new Bazooka(_Dmg, _Mun, _Name));
}

void		DecoratorStat::attack() const
{
  std::cout << _Name << ": fshouu BAM !" << std::endl;
}

int		DecoratorStat::getDamage() const
{
  return (this->_Dmg);
}

int		DecoratorStat::getMun() const
{
  return (this->_Mun);
}

const std::string	&DecoratorStat::getName() const
{
  return (this->_Name);
}

void			DecoratorStat::proMode()
{
}
