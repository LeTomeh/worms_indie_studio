//
// Controller.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas Andr�
// Login   <andre_o@epitech.net>
// 
// Started on  Tue Apr 26 16:09:41 2016 Thomas Andr�
// Last update Sat Jun  4 05:43:49 2016 Thomas Andr�
//

#include	"Controller.hh"

Controller::Controller()
{
  _ev = new AThread;
  try
    {
      _event = new EventReceiver;
      _event->setObserver(this);
      _gui = new IrrlichtGUI((EventReceiver *)_event);
      _ev->run(runCatchingEvent, _event);
    }
  catch (AError const &e)
    {
      e.criticalError();
    }
  _model = new Model;
  _view = NULL;
}

Controller::~Controller()
{
  _event->unSetObserver();
  try
    {
      _ev->waitDeath();
    }
  catch (AError const &e) {}
  delete (_gui);
  delete (_model);
  delete (_event);
  delete (_ev);
}

Controller::Controller(const Controller &cop) : _gui(cop._gui), _th(cop._th), _ev(cop._ev),  _model(cop._model), _event(cop._event), _list(cop._list), _view(cop._view) {}

Controller		&Controller::operator=(const Controller &cop)
{
  if (this != &cop)
    {
      _gui = cop._gui;
      _th = cop._th;
      _ev = cop._ev;
      _event = cop._event;
      _model = cop._model;
      _list = cop._list;
      _view = cop._view;
    }
  return (*this);
}

int		Controller::getEvent()
{
  int		ev;

  if (_list.empty() == false)
    {
      ev = _list.front();
      _list.pop_front();
    }
  else
    ev = 0;
  return (ev);
}

void		Controller::start()
{
  int		ev;

  _model->setObserver(this);
  if (_model->getEffect() == true)
    _gui->sayHello();
  try
    {
      while (_gui->run() && (ev = getEvent()) != QUIT_K)
	{
	  try
	    {
	      _model->execute(ev);
	    }
	  catch (CatchIt<AMenuData *> const &data)
	    {
	      _gui->setMenu(data.getIt());
	    }
	  catch (CatchIt<TeamNameMaker *> const &data)
	    {
	      _gui->setTeamMaker(data.getIt());
	    }
	  catch (CatchIt<ErrorData *> const &data)
	    {
	      _gui->setError(data.getIt());
	    }
	  catch (CatchIt<GeneratorData *> const &data)
	    {
	      _gui->setGenerate(data.getIt());
	    }
	  if (_view != NULL)
	    {
	      _gui->setMap(_view);
	      _view = NULL;
	    }
	  _gui->runGUI();
	}
    }
  catch (CatchIt<bool> const &b) {}
  _gui->stopSounds();
  if (_model->getEffect() == true)
    _gui->sayByeBye();
}

void		Controller::notifier(const int &ev)
{
  _list.push_back(ev);
}

void		Controller::notifier(View *no)
{
  _view = no;
}

