//
// ManageDrop.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:28:57 2016 Maxime LECOQ
//

#include	"ManageDrop.hh"

ManageDrop::ManageDrop(View *v) : _cl(clock()), _cl2(clock()), _view(v), _cpt(0) {}

ManageDrop::ManageDrop(const ManageDrop &c) : _drops(c._drops), _fac(c._fac), _cl(c._cl), _cl2(c._cl2), _view(c._view), _cpt(c._cpt) {}

ManageDrop	&ManageDrop::operator=(const ManageDrop &c)
{
  if (this != &c)
    {
      _drops = c._drops;
      _fac = c._fac;
      _cl = c._cl;
      _cl2 = c._cl2;
      _view = c._view;
      _cpt = c._cpt;
    }
  return (*this);
}

ManageDrop::~ManageDrop() {}

void		ManageDrop::newDrop(const int &xM)
{
  Position	p;

  p.setX(rand() % xM);
  p.setY(0);
  _drops.push_back(Drop(_fac.get(), p));
}

void		ManageDrop::addDrop(const std::string &d, const Position &p)
{
  _drops.push_back(Drop(_fac.get(d), p));
}

void		ManageDrop::askDrop(const int &xM, const size_t &team)
{
  _cpt++;
  if (_cpt > team * 3 && ((double)clock() - (double)_cl2) / CLOCKS_PER_SEC > 15.00)
    {
      _cpt = 0;
      newDrop(xM);
      _cl2 = clock();
    }
}

const std::vector<Drop>	&ManageDrop::getDrops() const
{
  return (_drops);
}

void			ManageDrop::reset()
{
  while (_drops.empty() == false)
    _drops.erase(_drops.begin());
}

void			ManageDrop::gravity(Map *m, bool &ck)
{
  size_t		i;
  size_t		size;
  Position		tmp;

  if (((double)clock() - (double)_cl) / CLOCKS_PER_SEC < 0.2)
    return;
  _cl = clock();
  i = 0;
  size = _drops.size();
  while (i < size)
    {
      tmp = _drops[i].getPosition();
      tmp.setY(tmp.getY() + 4);
      if (tmp.getY() >= m->getHeight() && m->ground(tmp.getX(), tmp.getY()) == false)
	{
	  _drops.erase(_drops.begin() + i);
	  size--;
	}
      else
	{
	  if (m->ground(tmp.getX(), tmp.getY()) == false)
	    {
	      ck = true;
	      _drops[i].setPosition(tmp);
	    }
	  i++;
	}
    }
}

void			ManageDrop::take(std::vector<Team *> &team)
{
  size_t		i;
  size_t		size;
  std::vector<Player *>	players;
  size_t		i2;
  size_t		size2;
  size_t		i3;
  size_t		size3;

  i = 0;
  size = team.size();
  size3 = _drops.size();
  while (i < size)
    {
      players = team[i]->getPlayers();
      i2 = 0;
      size2 = players.size();
      while (i2 < size2)
	{
	  if (players[i2]->getLife() > 0)
	    {
	      i3 = 0;
	      while (i3 < size3)
		{
		  if (_drops[i3].in(players[i2]->getPosition()) == true)
		    {
		      _view->setEffect(_drops[i3].getContent()->getDescription());
		      _view->setDesc(_drops[i3].getContent()->getDesc());
		      _drops[i3].getContent()->doEffect(players[i2], team[i]);
		      _drops.erase(_drops.begin() + i3);
		      size3--;
		    }
		  else
		    i3++;
		}
	    }
	  i2++;
	}
      i++;
    }
}

bool		ManageDrop::checkIn(const Position &p)
{
  size_t		i;
  size_t		size;

  i = 0;
  size = _drops.size();
  while (i < size)
    {
      if (_drops[i].inBullet(p) == true)
	{
	  _drops.erase(_drops.begin() + i);
	  size--;
	  return (true);
	}
      else
	i++;
    }
  return (false);
}

void		ManageDrop::write()
{
  File		file(".cfg/.drop");
  std::string	tmp;
  size_t	i;
  size_t	size;
  Convert<int>	conv;

  i = 0;
  size = _drops.size();
  while (i < size)
    {
      tmp += _drops[i].getContent()->getName() + " " + conv.toString(_drops[i].getPosition().getX()) + " " + conv.toString(_drops[i].getPosition().getY()) + "\n";
      i++;
    }
  try
    {
      file.writeTronc(tmp);
    }
  catch (AError const &e) {}
}

void		ManageDrop::checkPos(Map *m)
{
  size_t	i;
  size_t	size;

  i = 0;
  size = _drops.size();
  while (i < size)
    {
      if (_drops[i].getPosition().getX() < 0 || _drops[i].getPosition().getX() > m->getWidth()
	  || _drops[i].getPosition().getY() < 0 || _drops[i].getPosition().getY() > m->getHeight())
	{
	  _drops.erase(_drops.begin() + i);
	  size--;
	}
      else
	i++;
    }
}
