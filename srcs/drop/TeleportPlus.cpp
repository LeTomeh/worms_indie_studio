//
// TeleportPlus.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:06:32 2016 Maxime LECOQ
//

#include	"TeleportPlus.hh"

TeleportPlus::TeleportPlus() : ADropable("TeleportPlus", SELWE, "You find 3 teleporters") {}

TeleportPlus::TeleportPlus(const TeleportPlus &c) : ADropable(c.getName(), c.getDescription(), c.getDesc()) {}

TeleportPlus	&TeleportPlus::operator=(const TeleportPlus &c)
{
  if (this != &c)
    {
      _name = c.getName();
      _description = c.getDescription();
    }
  return (*this);
}

TeleportPlus::~TeleportPlus() {}

void		TeleportPlus::doEffect(Player *p, Team *t)
{
  t->getInventory()->getWeapon("Teleporter")->setMun(t->getInventory()->getWeapon("Teleporter")->getMun() + 3);
  (void)p;
}
