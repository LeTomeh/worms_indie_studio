//
// PistolPlus.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:06:43 2016 Maxime LECOQ
//

#include	"PistolPlus.hh"

PistolPlus::PistolPlus() : ADropable("PistolPlus", SELWE, "You find 5 munition of pistol") {}

PistolPlus::PistolPlus(const PistolPlus &c) : ADropable(c.getName(), c.getDescription(), c.getDesc()) {}

PistolPlus	&PistolPlus::operator=(const PistolPlus &c)
{
  if (this != &c)
    {
      _name = c.getName();
      _description = c.getDescription();
    }
  return (*this);
}

PistolPlus::~PistolPlus() {}

void		PistolPlus::doEffect(Player *p, Team *t)
{
  t->getInventory()->getWeapon("Pistol")->setMun(t->getInventory()->getWeapon("Pistol")->getMun() + 5);
  (void)p;
}
