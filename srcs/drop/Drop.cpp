//
// Drop.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Thu Jun  2 20:26:16 2016 Maxime LECOQ
//

#include	"Drop.hh"

Drop::Drop(IDropable *con, const Position &p) : _content(con), _pos(p), _sprite(DROP) {}

Drop::Drop(const Drop &c) : _content(c._content), _pos(c._pos), _sprite(c._sprite) {}

Drop	&Drop::operator=(const Drop &c)
{
  if (this != &c)
    {
      _content = c._content;
      _pos = c._pos;
      _sprite = c._sprite;
    }
  return (*this);
}

Drop::~Drop()
{
}

IDropable		*Drop::getContent() const
{
  return (_content);
}

bool			Drop::in(const Position &p) const
{

  if (p.getX() >= _pos.getX() && p.getX() <= _pos.getX() + 19
      && p.getY() >= _pos.getY() && p.getY() <= _pos.getY() + 15)
    return (true);
  return (false);
}

bool			Drop::inBullet(const Position &p) const
{

  if (p.getX() >= _pos.getX() && p.getX() <= _pos.getX() + 19
      && p.getY() <= _pos.getY() && p.getY() >= _pos.getY() - 15)
    return (true);
  return (false);
}

Position		Drop::getPosition() const
{
  return (_pos);
}

const std::string	&Drop::getSprite() const
{
  return (_sprite);
}

void			Drop::setPosition(const Position &c)
{
  _pos = c;
}
