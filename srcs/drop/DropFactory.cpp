//
// DropFactory.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 23:21:28 2016 Maxime LECOQ
// Last update Thu Jun  2 23:36:43 2016 Maxime LECOQ
//

#include	"DropFactory.hh"

DropFactory::DropFactory()
{
  _facto["HealPlayer"] = &DropFactory::makeHealP;
  _facto["HealTeam"] = &DropFactory::makeHealT;
  _facto["PoisonPlayer"] = &DropFactory::makePoisonP;
  _facto["PoisonTeam"] = &DropFactory::makePoisonT;
  _facto["PistolPlus"] = &DropFactory::makePistolP;
  _facto["TeleportPlus"] = &DropFactory::makeTeleportP;
  _factoRand[0] = &DropFactory::makeHealP;
  _factoRand[1] = &DropFactory::makeHealT;
  _factoRand[2] = &DropFactory::makePoisonP;
  _factoRand[3] = &DropFactory::makePoisonT;
  _factoRand[4] = &DropFactory::makePistolP;
  _factoRand[5] = &DropFactory::makeTeleportP;
  srand(time(NULL));
}

DropFactory::~DropFactory() {}

DropFactory::DropFactory(const DropFactory &c) : _facto(c._facto), _factoRand(c._factoRand) {}

DropFactory	&DropFactory::operator=(const DropFactory &c)
{
  if (this != &c)
    {
      _facto = c._facto;
      _factoRand = c._factoRand;
    }
  return (*this);
}

IDropable	*DropFactory::get(const std::string &what)
{
  if (_facto.find(what) != _facto.end())
    return ((this->*_facto[what])());
  return (NULL);
}

IDropable	*DropFactory::get()
{
  int		x;
  
  x = rand() % 6;
  if (_factoRand.find(x) != _factoRand.end())
    return ((this->*_factoRand[x])());
  return (NULL);
}

IDropable	*DropFactory::makeHealP()
{
  return (new HealPlayer);
}

IDropable	*DropFactory::makeHealT()
{
  return (new HealTeam);
}

IDropable	*DropFactory::makePoisonP()
{
  return (new PoisonPlayer);
}

IDropable	*DropFactory::makePoisonT()
{
  return (new PoisonTeam);
}

IDropable	*DropFactory::makePistolP()
{
  return (new PistolPlus);
}

IDropable	*DropFactory::makeTeleportP()
{
  return (new TeleportPlus);
}
