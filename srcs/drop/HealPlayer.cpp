//
// HealPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:06:53 2016 Maxime LECOQ
//

#include	"HealPlayer.hh"

HealPlayer::HealPlayer() : ADropable("HealPlayer", HEAL, "Heal player of 50") {}

HealPlayer::HealPlayer(const HealPlayer &c) : ADropable(c.getName(), c.getDescription(), c.getDesc()) {}

HealPlayer	&HealPlayer::operator=(const HealPlayer &c)
{
  if (this != &c)
    {
      _name = c.getName();
      _description = c.getDescription();
    }
  return (*this);
}

HealPlayer::~HealPlayer() {}

void		HealPlayer::doEffect(Player *p, Team *t)
{
  if (p->getLife() > 0)
    p->setLife(p->getLife() + 50);
  (void)t;
}
