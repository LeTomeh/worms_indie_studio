//
// HealTeam.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:07:03 2016 Maxime LECOQ
//

#include	"HealTeam.hh"

HealTeam::HealTeam() : ADropable("HealTeam", HEAL, "Heal Team of 10") {}

HealTeam::HealTeam(const HealTeam &c) : ADropable(c.getName(), c.getDescription(), c.getDesc()) {}

HealTeam	&HealTeam::operator=(const HealTeam &c)
{
  if (this != &c)
    {
      _name = c.getName();
      _description = c.getDescription();
    }
  return (*this);
}

HealTeam::~HealTeam() {}

void		HealTeam::doEffect(Player *p, Team *t)
{
  size_t	i;
  size_t	size;
  std::vector<Player *> pl;

  i = 0;
  pl = t->getPlayers();
  size = pl.size();
  while (i < size)
    {
      if (pl[i]->getLife() > 0)
	pl[i]->setLife(pl[i]->getLife() + 10);
      i++;
    }
  (void)p;
}
