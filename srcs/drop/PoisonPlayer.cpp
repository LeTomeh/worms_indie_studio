//
// PoisonPlayer.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:07:35 2016 Maxime LECOQ
//

#include	"PoisonPlayer.hh"

PoisonPlayer::PoisonPlayer() : ADropable("PoisonPlayer", POISON, "Poison Player of 30") {}

PoisonPlayer::PoisonPlayer(const PoisonPlayer &c) : ADropable(c.getName(), c.getDescription(), c.getDesc()) {}

PoisonPlayer	&PoisonPlayer::operator=(const PoisonPlayer &c)
{
  if (this != &c)
    {
      _name = c.getName();
      _description = c.getDescription();
    }
  return (*this);
}

PoisonPlayer::~PoisonPlayer() {}

void		PoisonPlayer::doEffect(Player *p, Team *t)
{
  if (p->getLife() > 0)
    p->setLife(p->getLife() - 30);
  if (p->getLife() < 0)
    p->setLife(0);
  (void)t;
}
