//
// ADropable.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:01:54 2016 Maxime LECOQ
//

#include	"ADropable.hh"

ADropable::ADropable(const std::string &n, const std::string &d, const std::string &p) : _name(n), _description(d), _desc(p) {}

ADropable::ADropable(const ADropable &c) : _name(c._name), _description(c._description), _desc(c._desc) {}

ADropable	&ADropable::operator=(const ADropable &c)
{
  if (this != &c)
    {
      _name = c._name;
      _description = c._description;
      _desc = c._desc;
    }
  return (*this);
}

ADropable::~ADropable() {}

const std::string     &ADropable::getName() const
{
  return (_name);
}

const std::string     &ADropable::getDesc() const
{
  return (_desc);
}

const std::string     &ADropable::getDescription() const
{
  return (_description);
}
