//
// PoisonTeam.hh for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Thu Jun  2 16:14:49 2016 Maxime LECOQ
// Last update Fri Jun  3 14:07:19 2016 Maxime LECOQ
//

#include	"PoisonTeam.hh"

PoisonTeam::PoisonTeam() : ADropable("PoisonTeam", POISON, "Poison Team of 5") {}

PoisonTeam::PoisonTeam(const PoisonTeam &c) : ADropable(c.getName(), c.getDescription(), c.getDesc()) {}

PoisonTeam	&PoisonTeam::operator=(const PoisonTeam &c)
{
  if (this != &c)
    {
      _name = c.getName();
      _description = c.getDescription();
    }
  return (*this);
}

PoisonTeam::~PoisonTeam() {}

void		PoisonTeam::doEffect(Player *p, Team *t)
{
  size_t	i;
  size_t	size;
  std::vector<Player *> pl;

  i = 0;
  pl = t->getPlayers();
  size = pl.size();
  while (i < size)
    {
      if (pl[i]->getLife() > 0)
	pl[i]->setLife(pl[i]->getLife() - 5);
      if (pl[i]->getLife() <= 0)
	{
	  pl[i]->setLife(0);
	  t->deletePlayer((int)i);
	  size--;
	}
      else
	i++;
    }
  (void)p;
}
