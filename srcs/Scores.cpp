//
// Scores.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue May  3 13:07:03 2016 Maxime LECOQ
// Last update Wed Jun  1 09:09:52 2016 Maxime LECOQ
//

#include	"Scores.hh"

Scores::Scores() {}

Scores::~Scores() {}

Scores::Scores(const Scores &c) : _nameWin(c._nameWin), _nameDef(c._nameDef), _nameScores(c._nameScores), _nameRatio(c._nameRatio), _win(c._win), _def(c._def), _scores(c._scores), _ratio(c._ratio) {}

Scores		&Scores::operator=(const Scores &c)
{
  if (this != &c)
    {
      _nameWin = c._nameWin;
      _nameDef = c._nameDef;
      _nameScores = c._nameScores;
      _nameRatio = c._nameRatio;
      _win = c._win;
      _def = c._def;
      _scores = c._scores;
      _ratio = c._ratio;
    }
  return (*this);
}

void			Scores::setScores(std::vector<std::string> &v)
{
  size_t		i;
  size_t		size;
  Epur			clean;
  String		str;
  Convert<int>		conv;
  Convert<float>	conv2;
  std::string		tmp;
  size_t		found;
  std::string		name;
  std::vector<std::string> tmpName;
  std::string		cut;

  i = 0;
  size = v.size();
  while (i < size)
    {
      v[i] = str.replace(v[i], '\t', ' ');
      v[i] = clean.epur(v[i], ' ');
      if (checkIntegrity(v[i]) == false)
	{
	  v.erase(v.begin() + i);
	  size--;
	}
      else
	i++;
    }
  i = 0;
  while (i < size)
    {
      tmp = v[i];
      found = tmp.find(' ');
      name = tmp.substr(0, found);
      tmpName.push_back(name);
      tmp = tmp.substr(found + 1, tmp.size());
      found = tmp.find(' ');
      _win[name] = conv.toNumber(tmp.substr(0, found));
      tmp = tmp.substr(found + 1, tmp.size());
      found = tmp.find(' ');
      _def[name] = conv.toNumber(tmp.substr(0, found));
      tmp = tmp.substr(found + 1, tmp.size());
      found = tmp.find(' ');
      _scores[name] = conv.toNumber(tmp.substr(0, found));
      if (_def[name] != 0)
	{
	  _ratio[name] = (float)_win[name] / (float)_def[name];
	  cut = conv2.toString(_ratio[name]);
	  if (cut.find(".") != cut.size())
	    {
	      cut = cut.substr(0, cut.find(".") + 3);
	      _ratio[name] = conv2.toNumber(cut);
	    }
	}
      else
	_ratio[name] = _win[name];
      i++;
    }
  setScoresSnd(tmpName);
}

void			Scores::setScoresSnd(std::vector<std::string> &name)
{
  size_t		i;
  size_t		x;
  size_t		size;
  
  i = 0;
  size = name.size();
  while (i < size)
    {
      x =  i + 1;
      while (x < size)
	{
	  if (name[i] == name[x])
	    {
	      name.erase(name.begin() + x);
	      size--;
	    }
	  else
	    x++;
	}
      i++;
    }
  _nameWin = best(_win, name);
  _nameDef = best(_def, name);
  _nameScores = best(_scores, name);
  _nameRatio = bestPlus(_ratio, name);
}

std::vector<std::string>	Scores::best(std::map<std::string, int> map, std::vector<std::string> name)
{
  std::vector<std::string>	ret;
  size_t			i;
  size_t			size;
  std::string			tmp;
  size_t			pos;

  i = 0;
  size = name.size();
  if (size > 0)
    tmp = name[0];
  pos = 0;
  while (size > 0 && ret.size() < 10)
    {
      if (map[tmp] < map[name[i]])
	{
	  tmp = name[i];
	  pos = i;
	}
      i++;
      if (i >= size)
	{
	  i = 0;
	  size--;
	  ret.push_back(tmp);
	  name.erase(name.begin() + pos);
	  pos = 0;
	  if (size > 0)
	    tmp = name[0];
	}
    }
  return (ret);
}

std::vector<std::string>	Scores::bestPlus(std::map<std::string, float> map, std::vector<std::string> name)
{
  std::vector<std::string>	ret;
  size_t			i;
  size_t			size;
  std::string			tmp;
  size_t			pos;

  i = 0;
  size = name.size();
  if (size > 0)
    tmp = name[0];
  pos = 0;
  while (size > 0 && ret.size() < 10)
    {
      if (map[tmp] < map[name[i]])
	{
	  tmp = name[i];
	  pos = i;
	}
      i++;
      if (i >= size)
	{
	  i = 0;
	  size--;
	  ret.push_back(tmp);
	  name.erase(name.begin() + pos);
	  pos = 0;
	  if (size > 0)
	    tmp = name[0];
	}
    }
  return (ret);
}

bool			Scores::checkIntegrity(const std::string &tmp)
{
  int			i;
  int			cpt;

  i = 0;
  cpt = 0;
  while (tmp[i])
    {
      if (tmp[i] == ' ')
	cpt++;
      if (cpt != 0 && (tmp[i] > '9' || tmp[i] < '0') && tmp[i] != ' ')
	return (false);
      i++;
    }
  if (cpt != 3)
    return (false);
  if (tmp.substr(0, 3) == "IA_")
    return (false);
  return (true);
}

std::vector<std::string>	Scores::getNameWin() const
{
  return (_nameWin);
}

std::vector<std::string>	Scores::getNameDefeat() const
{
  return (_nameDef);
}

std::vector<std::string>	Scores::getNameScores() const
{
  return (_nameScores);
}

std::vector<std::string>	Scores::getNameRatio() const
{
  return (_nameRatio);
}

std::map<std::string, int>	Scores::getWin() const
{
  return (_win);
}

std::map<std::string, int>	Scores::getDefeat() const
{
  return (_def);
}

std::map<std::string, int>	Scores::getScores() const
{
  return (_scores);
}

std::map<std::string, float>	Scores::getRatio() const
{
  return (_ratio);
}

std::vector<std::string>	Scores::getNameSc(const int &el) const
{
  if (el == 0)
    return (_nameWin);
  else
    {
      if (el == 1)
	return (_nameDef);
    }
  return (_nameScores);
}
