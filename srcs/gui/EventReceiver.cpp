//
// EventReceiver.cpp for EventReceiver in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <andre_o@epitech.eu>
// 
// Started on  Wed Apr 27 18:34:42 2016 Thomas André
// Last update Wed Jun  1 18:50:32 2016 Maxime LECOQ
//

#include "EventReceiver.hh"

EventReceiver::EventReceiver()
{
  irr::u32 i = 0;

  _observe = false;
  _vec.push_back(irr::KEY_BACK);
  _serial[irr::KEY_BACK] = DEL_K;
  _vec.push_back(irr::KEY_TAB);
  _serial[irr::KEY_TAB] = TAB_K;
  _vec.push_back(irr::KEY_RETURN);
  _serial[irr::KEY_RETURN] = RETURN_K;
  _vec.push_back(irr::KEY_PAUSE);
  _serial[irr::KEY_PAUSE] = PAUSE_K;
  _vec.push_back(irr::KEY_ESCAPE);
  _serial[irr::KEY_ESCAPE] = QUIT_K;
  _vec.push_back(irr::KEY_SPACE);
  _serial[irr::KEY_SPACE] = SPACE_K;
  _vec.push_back(irr::KEY_PRIOR);
  _serial[irr::KEY_PRIOR] = PRIOR_K;
  _vec.push_back(irr::KEY_NEXT);
  _serial[irr::KEY_NEXT] = PREV_K;
  _vec.push_back(irr::KEY_END);
  _serial[irr::KEY_END] = END_K;
  _vec.push_back(irr::KEY_LEFT);
  _serial[irr::KEY_LEFT] = LEFT_K;
  _vec.push_back(irr::KEY_UP);
  _serial[irr::KEY_UP] = UP_K;
  _vec.push_back(irr::KEY_RIGHT);
  _serial[irr::KEY_RIGHT] = RIGHT_K;
  _vec.push_back(irr::KEY_DOWN);
  _serial[irr::KEY_DOWN] = DOWN_K;
  _vec.push_back(irr::KEY_INSERT);
  _serial[irr::KEY_INSERT] = INSERT_K;
  _vec.push_back(irr::KEY_DELETE);
  _serial[irr::KEY_DELETE] = DELETE_K;
  _vec.push_back(irr::KEY_CAPITAL);
  _serial[irr::KEY_CAPITAL] = CAPITAL_K;
  _vec.push_back(irr::KEY_KEY_A);
  _serial[irr::KEY_KEY_A] = A_K;
  _vec.push_back(irr::KEY_KEY_B);
  _serial[irr::KEY_KEY_B] = B_K;
  _vec.push_back(irr::KEY_KEY_C);
  _serial[irr::KEY_KEY_C] = C_K;
  _vec.push_back(irr::KEY_KEY_D);
  _serial[irr::KEY_KEY_D] = D_K;
  _vec.push_back(irr::KEY_KEY_E);
  _serial[irr::KEY_KEY_E] = E_K;
  _vec.push_back(irr::KEY_KEY_F);
  _serial[irr::KEY_KEY_F] = F_K;
  _vec.push_back(irr::KEY_KEY_G);
  _serial[irr::KEY_KEY_G] = G_K;
  _vec.push_back(irr::KEY_KEY_H);
  _serial[irr::KEY_KEY_H] = H_K;
  _vec.push_back(irr::KEY_KEY_I);
  _serial[irr::KEY_KEY_I] = I_K;
  _vec.push_back(irr::KEY_KEY_J);
  _serial[irr::KEY_KEY_J] = J_K;
  _vec.push_back(irr::KEY_KEY_K);
  _serial[irr::KEY_KEY_K] = K_K;
  _vec.push_back(irr::KEY_KEY_L);
  _serial[irr::KEY_KEY_L] = L_K;
  _vec.push_back(irr::KEY_KEY_M);
  _serial[irr::KEY_KEY_M] = M_K;
  _vec.push_back(irr::KEY_KEY_N);
  _serial[irr::KEY_KEY_N] = N_K;
  _vec.push_back(irr::KEY_KEY_O);
  _serial[irr::KEY_KEY_O] = O_K;
  _vec.push_back(irr::KEY_KEY_P);
  _serial[irr::KEY_KEY_P] = P_K;
  _vec.push_back(irr::KEY_KEY_Q);
  _serial[irr::KEY_KEY_Q] = Q_K;
  _vec.push_back(irr::KEY_KEY_R);
  _serial[irr::KEY_KEY_R] = R_K;
  _vec.push_back(irr::KEY_KEY_S);
  _serial[irr::KEY_KEY_S] = S_K;
  _vec.push_back(irr::KEY_KEY_T);
  _serial[irr::KEY_KEY_T] = T_K;
  _vec.push_back(irr::KEY_KEY_U);
  _serial[irr::KEY_KEY_U] = U_K;
  _vec.push_back(irr::KEY_KEY_V);
  _serial[irr::KEY_KEY_V] = V_K;
  _vec.push_back(irr::KEY_KEY_W);
  _serial[irr::KEY_KEY_W] = W_K;
  _vec.push_back(irr::KEY_KEY_X);
  _serial[irr::KEY_KEY_X] = X_K;
  _vec.push_back(irr::KEY_KEY_Y);
  _serial[irr::KEY_KEY_Y] = Y_K;
  _vec.push_back(irr::KEY_KEY_Z);
  _serial[irr::KEY_KEY_Z] = Z_K;
  _vec.push_back(irr::KEY_F1);
  _serial[irr::KEY_F1] = F1_K;
  _vec.push_back(irr::KEY_F2);
  _serial[irr::KEY_F2] = F2_K;
  _vec.push_back(irr::KEY_F3);
  _serial[irr::KEY_F3] = F3_K;
  _vec.push_back(irr::KEY_F4);
  _serial[irr::KEY_F4] = F4_K;
  _vec.push_back(irr::KEY_F5);
  _serial[irr::KEY_F5] = F5_K;
  _vec.push_back(irr::KEY_F6);
  _serial[irr::KEY_F6] = F6_K;
  _vec.push_back(irr::KEY_F7);
  _serial[irr::KEY_F7] = F7_K;
  _vec.push_back(irr::KEY_F8);
  _serial[irr::KEY_F8] = F8_K;
  _vec.push_back(irr::KEY_F9);
  _serial[irr::KEY_F9] = F9_K;
  _vec.push_back(irr::KEY_F10);
  _serial[irr::KEY_F10] = F10_K;
  _vec.push_back(irr::KEY_F11);
  _serial[irr::KEY_F11] = F11_K;
  _vec.push_back(irr::KEY_F12);
  _serial[irr::KEY_F12] = F12_K;
  _vec.push_back(irr::KEY_PLUS);
  _serial[irr::KEY_PLUS] = PLUS_K;
  _vec.push_back(irr::KEY_COMMA);
  _serial[irr::KEY_COMMA] = VI_K;
  _vec.push_back(irr::KEY_MINUS);
  _serial[irr::KEY_MINUS] = MINUS_K;
  while (i < irr::KEY_KEY_CODES_COUNT)
    {
      _keyIsDown[i] = false;
      _keyIsShift[i] = false;
      _keyIsControl[i] = false;
      i++;
    }
}

EventReceiver::~EventReceiver()
{}

EventReceiver::EventReceiver(const EventReceiver &o) : _vec(o._vec), _serial(o._serial), _observe(o._observe), _observer(o._observer) {}

EventReceiver	&EventReceiver::operator=(const EventReceiver &o)
{
  if (this != &o)
    {
      _serial = o._serial;
      _vec = o._vec;
      _observe = o._observe;
      _observer = o._observer;
    }
  return (*this);
}

bool	EventReceiver::OnEvent(const irr::SEvent &event)
{
  if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
      _keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
      _keyIsShift[event.KeyInput.Key] = event.KeyInput.Shift;
      _keyIsControl[event.KeyInput.Key] = event.KeyInput.Control;
    }
  return false;
}

bool	EventReceiver::isKeyDown(irr::EKEY_CODE keyCode) const
{
  return _keyIsDown[keyCode];
}

bool	EventReceiver::isKeyShift(irr::EKEY_CODE keyCode) const
{
  return _keyIsShift[keyCode];
}

bool	EventReceiver::isKeyControl(irr::EKEY_CODE keyCode) const
{
  return _keyIsControl[keyCode];
}

void	EventReceiver::reset(irr::EKEY_CODE keyCode)
{
  _keyIsDown[keyCode] = false;
  _keyIsShift[keyCode] = false;
  _keyIsControl[keyCode] = false;
}

void		EventReceiver::setObserver(IObserver *obs)
{
  _observer = obs;
  _observe = true;
}

void		EventReceiver::unSetObserver()
{
  _observe = false;
  _observer = NULL;
}

void		EventReceiver::runObservable()
{
  unsigned int  i;
  unsigned int  max;
  int           ret;

  max = _vec.size();
  while (_observe == true)
    {
      i = 0;
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      while (i < max)
	{
	  if (isKeyDown(_vec[i]) && _serial.find(_vec[i]) != _serial.end())
	    {
	      if (_serial[_vec[i]] >= 7 && _serial[_vec[i]] <= 32 && isKeyShift(_vec[i]) == true)
		ret = _serial[_vec[i]] + 51;
	      else
		if (_serial[_vec[i]] >= 3 && _serial[_vec[i]] <= 6
		    && (isKeyShift(_vec[i]) == true || isKeyControl(_vec[i]) == true))
		  ret = _serial[_vec[i]] + 81;
		else
		  ret = _serial[_vec[i]];
	      reset(_vec[i]);
	      _observer->notifier(ret);
	      ret = 0;
	    }
	  i++;
	}
    }
}

void			EventReceiver::resetAll()
{
  size_t		i;
  size_t		size;

  i = 0;
  size = _vec.size();
  while (i < size)
    {
      reset(_vec[i]);
      i++;
    }
}
