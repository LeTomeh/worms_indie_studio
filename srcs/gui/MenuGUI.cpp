//
// MenuGUI.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:21:20 2016 Maxime LECOQ
// Last update Sat Jun  4 09:45:42 2016 Maxime LECOQ
//

#include	"MenuGUI.hh"

MenuGUI::MenuGUI(irr::IrrlichtDevice *win, irr::video::IVideoDriver *dri, irr::scene::ISceneManager *smgr, irr::gui::IGUIEnvironment *env, TextDisplayer *text, ImageDisplayer *img, const int &wi, const int &he, Sound *s) : AViewGUI(win, dri, smgr, env, text, img, wi, he, s)
{
  _menuElement[Element::LINK] = &MenuGUI::linkElement;
  _menuElement[Element::CONTROLLER] = &MenuGUI::controllerElement;
  _menuElement[Element::SELECTOR] = &MenuGUI::selectorElement;
  _menuElement[Element::BUTTON] = &MenuGUI::buttonElement;
  _menuElement[Element::VALUE] = &MenuGUI::valueElement;
  _menuElement[Element::SCORES] = &MenuGUI::scoresElement;
  _menuElement[Element::DELETE] = &MenuGUI::deleteElement;
  _conv[SPACE_K] = "SPACE";
  _conv[DOWN_PK] = "DOWN";
  _conv[UP_PK] = "UP";
  _conv[RIGHT_PK] = "RIGHT";
  _conv[LEFT_PK] = "LEFT";
  _conv[DOWN_K] = "DOWN";
  _conv[UP_K] = "UP";
  _conv[RIGHT_K] = "RIGHT";
  _conv[LEFT_K] = "LEFT";
  _conv[A_K] = "A";
  _conv[B_K] = "B";
  _conv[C_K] = "C";
  _conv[D_K] = "D";
  _conv[E_K] = "E";
  _conv[F_K] = "F";
  _conv[G_K] = "G";
  _conv[H_K] = "H";
  _conv[I_K] = "I";
  _conv[J_K] = "J";
  _conv[K_K] = "K";
  _conv[L_K] = "L";
  _conv[M_K] = "M";
  _conv[N_K] = "N";
  _conv[O_K] = "O";
  _conv[P_K] = "P";
  _conv[Q_K] = "Q";
  _conv[R_K] = "R";
  _conv[S_K] = "S";
  _conv[T_K] = "T";
  _conv[U_K] = "U";
  _conv[V_K] = "V";
  _conv[W_K] = "W";
  _conv[X_K] = "X";
  _conv[Y_K] = "Y";
  _conv[Z_K] = "Z";
}

MenuGUI::~MenuGUI() {}

MenuGUI::MenuGUI(const MenuGUI &o) : AViewGUI(o._window, o._driver, o._smgr, o._env, o._text, o._img, o._width, o._height, o._sound)
{
  _menuElement = o._menuElement;
  _conv = o._conv;
  _data = o._data;
}

MenuGUI	&MenuGUI::operator=(const MenuGUI &o)
{
  if (this != &o)
    {
      _window = o._window;
      _driver = o._driver;
      _smgr = o._smgr;
      _env = o._env;
      _width = o._width;
      _text = o._text;
      _img = o._img;
      _menuElement = o._menuElement;
      _height = o._height;
      _conv = o._conv;
      _data = o._data;
      _sound = o._sound;
    }
  return (*this);
}

void		MenuGUI::setMenuData(AMenuData *me)
{
  _data = me;
}

void		MenuGUI::menu()
{
  std::vector<Element>  elem;
  size_t                i;
  size_t                size;
  int                   y;
  std::string		tmp;
  int			save;

  i = 0;
  elem = _data->getElement();
  size = elem.size();
  _sound->stopSound(GAME);
  _sound->stopSound(CLOCKTURN);
  if (_data->soundIsOn() == true)
    _sound->play(_data->getSound(), true);
  else
    _sound->stopSound(_data->getSound());
  if (_data->effectIsOn() == true)
    {
      while ((tmp = _data->getEffect()) != "")
	_sound->play(tmp, false, true);
    }
  _driver->beginScene(true, true, irr::video::SColor(0,255,255,255));
  _img->print(_data->getBack(), 0, 0, _width, _height);
  textModule(_data->getPageName(), Rectangle(0, 50, _width / 2, _height / 2, true), Color(255, 0, 0), 15, false);
  msgBox(_data->getElement()[_data->getPos()].getMsg());
  y = 230;
  if (_data->getWarning() == true)
    {
      textModule("Warning : All key aren't set", Rectangle(0, y, _width / 2, _height / 2), 4, false);
      y += 60;
    }
  while (i < size)
    {
      save = y;
      if ((unsigned int)_data->getPos() == i)
        (this->*_menuElement[elem[i].getType()])(elem[i], y, true);
      else
        (this->*_menuElement[elem[i].getType()])(elem[i], y, false);
      if (save == y)
	y += 130;
      i++;
    }
  _driver->endScene();
}

void                          MenuGUI::linkElement(Element &elem, int &y, const bool &ck)
{
  if (elem.getValue() != "Continue")
    textModule(elem.getValue(), Rectangle(0, y, _width / 2, _height / 2), 10, ck);
  else
    textModule(elem.getValue(), Rectangle(0, _height - 70, _width / 2, _height / 2, true), 5, ck);
}

void                          MenuGUI::deleteElement(Element &elem, int &y, const bool &ck)
{
  textModule(elem.getValue(), Rectangle(0, y, _width / 2, _height / 2), 10, ck);
}

void                          MenuGUI::controllerElement(Element &elem, int &y, const bool &ck)
{
  Convert<int>          dd;

  textModule(elem.getValue() + " " + _conv[elem.getControllerValue()], Rectangle(0, y, _width / 2, _height / 2), 6, ck);
  y += 65;
}

void                          MenuGUI::selectorElement(Element &elem, int &y, const bool &ck)
{
  std::vector<std::string>      t;

  t = elem.getSelector();
  textModule(elem.getValue() + "   :   < " + t[elem.getPosSelector()] + " > ", Rectangle(0, y, _width / 2, _height / 2), 6, ck);
}

void                          MenuGUI::buttonElement(Element &elem, int &y, const bool &ck)
{
  if (elem.getValue() == "Reset")
    textModule(elem.getValue(), Rectangle(0, _height - 70, _width / 2, _height / 2, true), 5, ck);
  else
    textModule(elem.getValue(), Rectangle(0, _height - 120, _width / 2, _height / 2, true), 5, ck);
  (void)y;
}

void                          MenuGUI::valueElement(Element &elem, int &y, const bool &ck)
{
  Convert<int>                  conv;

  textModule(elem.getValue() + " " + conv.toString(elem.getValueNb()), Rectangle(0, y, _width / 2, _height / 2), 7, ck);
}

void                            MenuGUI::scoresElement(Element &elem, int &y, const bool &ck)
{
  Scores                        sc;
  std::vector<std::string>      t;
  std::map<std::string, int>    map2;
  size_t                        i;
  size_t                        size;
  Convert<float>                conv1;
  Convert<int>                  conv2;

  sc = elem.getScores();
  t = elem.getSelector();
  textModule("< " + t[elem.getPosSelector()] + " > ", Rectangle(0, y, _width / 2, _height / 2), 8, ck);
  i = 0;
  if (elem.getPosSelector() == 2)
    t = sc.getNameRatio();
  else
    t = sc.getNameSc(elem.getPosSelector());
  size = t.size();
  y += 90;
  while (i < size)
    {
      if (i == 0)
        {
          textModule(t[i] + " : " + conv2.toString(sc.getWin()[t[i]]) + " w | " + conv2.toString(sc.getDefeat()[t[i]]) + " d | " + conv1.toString(sc.getRatio()[t[i]]) + " r | " + conv2.toString(sc.getScores()[t[i]]) + " s", Rectangle(0, y, _width / 2, _height), Color(255, 0, 0), 8, false);
          y += 20;
        }
      else
        textModule(t[i] + " : " + conv2.toString(sc.getWin()[t[i]]) + " w | " + conv2.toString(sc.getDefeat()[t[i]]) + " d | " + conv1.toString(sc.getRatio()[t[i]]) + " r | " + conv2.toString(sc.getScores()[t[i]]) + " s", Rectangle(0, y, _width / 2, _height), 8, false);
      i++;
      y += 50;
    }
  if (i == 0)
    textModule("No scores set", Rectangle(0, y + 30, _width / 2, _height), 8, false);
}
