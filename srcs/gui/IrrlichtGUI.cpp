//
// IrrlichtGUI.cpp for cpp_indie_studio in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <thomas@epitech.net>
// 
// Started on  Wed Apr 27 17:09:12 2016 Thomas André
// Last update Sun Jun  5 23:27:17 2016 Maxime LECOQ
//

#include	"IrrlichtGUI.hh"

IrrlichtGUI::IrrlichtGUI(EventReceiver *ev)
{
  _status = "introduction";

  irr::IrrlichtDevice *nulldevice = irr::createDevice(irr::video::EDT_NULL);

  _deskres = nulldevice->getVideoModeList()->getDesktopResolution();
  nulldevice -> drop();
  irr::core::dimension2d<irr::u32> val(1920, 1080);
  _width = (int)val.Width;
  _height = (int)val.Height;
  _window = irr::createDevice(irr::video::EDT_SOFTWARE, val, 32, true, true, true, ev);
  if (!_window)
    throw ErrorWindow("create device failed");
  _window->getCursorControl()->setVisible(false); 
  _window->setResizable(false);
 _window->setWindowCaption(L"cpp_indie_studio : Worms");
 _driver = _window->getVideoDriver();
 _smgr = _window->getSceneManager();
 _env = _window->getGUIEnvironment();
 _sound = new Sound;
 _text = new TextDisplayer(_window);
 _text->addFont("assets/font/sf10.xml");
 _text->addFont("assets/font/sf12.xml");
 _text->addFont("assets/font/sf14.xml");
 _text->addFont("assets/font/sf16.xml");
 _text->addFont("assets/font/sf18.xml");
 _text->addFont("assets/font/sf20.xml");
 _text->addFont("assets/font/sf24.xml");
 _text->addFont("assets/font/sf28.xml");
 _text->addFont("assets/font/sf36.xml");
 _text->addFont("assets/font/sf48.xml");
 _text->addFont("assets/font/sf56.xml");
 _text->addFont("assets/font/sf68.xml");
 _text->addFont("assets/font/sf72.xml");
 _img = new ImageDisplayer(_driver, _smgr);
 _execute["introduction"] = &IrrlichtGUI::introduction;
 _execute["menu"] = &IrrlichtGUI::menu;
 _execute["launcher"] = &IrrlichtGUI::launcher;
 _execute["map"] = &IrrlichtGUI::map;
 _execute["error"] = &IrrlichtGUI::error;
 _execute["generate"] = &IrrlichtGUI::generate;
 _menu = new MenuGUI(_window, _driver, _smgr, _env, _text, _img, _width, _height, _sound);
 _launch = new LauncherGUI(_window, _driver, _smgr, _env, _text, _img, _width, _height, _sound);
 _worms = new WormsGUI(_window, _driver, _smgr, _env, _text, _img, _width, _height, _sound);
 _err = new ErrorGUI(_window, _driver, _smgr, _env, _text, _img, _width, _height, _sound);
 _generator = new GeneratorGUI(_window, _driver, _smgr, _env, _text, _img, _width, _height, _sound);
 _quit = false;
 _reset = false;
}

IrrlichtGUI::~IrrlichtGUI()
{
  delete (_img);
  delete (_text);
  delete (_menu);
  delete (_worms);
  delete (_err);
  delete (_launch);
  delete (_generator);
  _window->drop();
  irr::IrrlichtDevice *device = irr::createDevice(irr::video::EDT_SOFTWARE, _deskres, 32, true, false, true, NULL);
  (void)device;
  delete (_sound);
}

IrrlichtGUI::IrrlichtGUI(const IrrlichtGUI &o)
{
  _window = o._window;
  _driver = o._driver;
  _smgr = o._smgr;
  _env = o._env;
  _width = o._width;
  _height = o._height;
  _deskres = o._deskres;
  _status = o._status;
  _execute = o._execute;
  _launch = o._launch;
  _worms = o._worms;
  _err = o._err;
  _reset = o._reset;
  _quit = o._quit;
  _generator = o._generator;
  _sound = o._sound;
}

IrrlichtGUI&	IrrlichtGUI::operator=(const IrrlichtGUI &o)
{
  if (this != &o)
    {
      _window = o._window;
      _driver = o._driver;
      _smgr = o._smgr;
      _env = o._env;
      _width = o._width;
      _height = o._height;
      _deskres = o._deskres;
      _status = o._status;
      _execute = o._execute;
      _launch = o._launch;
      _worms = o._worms;
      _err = o._err;
      _reset = o._reset;
      _quit = o._quit;
      _generator = o._generator;
      _sound = o._sound;
    }
  return *this;
}

void		IrrlichtGUI::clear()
{
  _env->clear();
  _smgr->clear();
}

void		IrrlichtGUI::setFont(const char *path)
{
  _text->addFont(path);
}

void		IrrlichtGUI::refresh()
{

}

void		IrrlichtGUI::quit()
{
  _window->drop();
}

bool		IrrlichtGUI::run() const
{
  return _window->run();
}

bool		IrrlichtGUI::getQuit() const
{
  return (_quit);
}

bool		IrrlichtGUI::getReset() const
{
  return (_reset);
}

void		IrrlichtGUI::runGUI()
{
  if (_reset == true)
    {
      (this->*_execute[_status])();
      _reset = false;
    }
}

void		IrrlichtGUI::setMenu(AMenuData *data)
{
  _menu->setMenuData(data);
  _status = "menu";
  _reset = true;
}

void		IrrlichtGUI::menu()
{
  clear();
  _menu->menu();
}

void			IrrlichtGUI::introduction()
{
}

void			IrrlichtGUI::setTeamMaker(TeamNameMaker *data)
{
  _launch->setTeamNameMaker(data);
  _status = "launcher";
  _reset = true;
}

void			IrrlichtGUI::launcher()
{
  clear();
  _launch->launch();
}

void			IrrlichtGUI::setMap(View *view)
{
  _worms->setView(view);
  _status = "map";
  _reset = true;
}

void			IrrlichtGUI::map()
{
  _worms->map();
}

void			IrrlichtGUI::putOff()
{
  _quit = true;
}

void			IrrlichtGUI::setError(ErrorData *d)
{
  _err->setData(d);
  _reset = true;
  _status = "error";
}

void			IrrlichtGUI::error()
{
  _err->error();
}

void			IrrlichtGUI::setGenerate(GeneratorData *data)
{
  _status = "generate";
  _reset = true;
  _generator->setGenerate(data);
}

void			IrrlichtGUI::generate()
{
  _generator->generate();
}

void			IrrlichtGUI::sayByeBye()
{
  _sound->play(BYEBYE);
  while (_sound->isPlaying(BYEBYE) == true);
}

void			IrrlichtGUI::sayHello()
{
  _sound->play(HELLO);
}

void			IrrlichtGUI::stopSounds()
{
  _sound->stopAllSound();
}
