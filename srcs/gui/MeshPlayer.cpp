//
// MeshPlayer.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas Andr�
// Login   <andre_o@epitech.net>
// 
// Started on  Thu Jun  2 23:59:16 2016 Thomas Andr�
// Last update Fri Jun  3 09:28:41 2016 Maxime LECOQ
//

#include	"MeshPlayer.hh"

MeshPlayer::MeshPlayer(const std::string &name, irr::video::IVideoDriver *driver,
                       irr::scene::ISceneManager *scenemgr)
{
  _driver = driver;
  _scenemgr = scenemgr;
  add_camera();
  node = _scenemgr->addAnimatedMeshSceneNode(_scenemgr->getMesh(name.c_str()), 0, -1,
					     irr::core::vector3df(0.0f,
								  0.0f,
                                                                  30.0f));
}

MeshPlayer::MeshPlayer(const char *name, irr::video::IVideoDriver *driver,
                       irr::scene::ISceneManager *scenemgr)
{
  _driver = driver;
  _scenemgr = scenemgr;
  add_camera();
  node = _scenemgr->addAnimatedMeshSceneNode(_scenemgr->getMesh(name), 0, -1,
					     irr::core::vector3df(0.0f,
								  0.0f,
								  30.0f));
}

MeshPlayer::MeshPlayer(const MeshPlayer &c) : _driver(c._driver), _scenemgr(c._scenemgr), node(c.node) {}

MeshPlayer		&MeshPlayer::operator=(const MeshPlayer &c)
{
  if (this != &c)
    {
      _driver = c._driver;
      _scenemgr = c._scenemgr;
      node = c.node;
    }
  return (*this);
}

void            MeshPlayer::add_camera()
{
  irr::f32     rotateSpeed = 50.0f;
  irr::f32     moveSpeed = 0.02f;
  irr::f32     jumpSpeed = 0.5f;

  irr::SKeyMap keyMap[5];
  keyMap[0].Action = irr::EKA_MOVE_FORWARD;
  keyMap[0].KeyCode = irr::KEY_UP;
  keyMap[1].Action = irr::EKA_MOVE_BACKWARD;
  keyMap[1].KeyCode = irr::KEY_DOWN;
  keyMap[2].Action = irr::EKA_STRAFE_LEFT;
  keyMap[2].KeyCode = irr::KEY_LEFT;
  keyMap[3].Action = irr::EKA_STRAFE_RIGHT;
  keyMap[3].KeyCode = irr::KEY_RIGHT;
  keyMap[4].Action = irr::EKA_JUMP_UP;
  keyMap[4].KeyCode = irr::KEY_SPACE;
  _scenemgr->addCameraSceneNodeFPS(0, rotateSpeed, moveSpeed, -1, keyMap, 5, true, jumpSpeed, false, true);
}

void            MeshPlayer::play(irr::IrrlichtDevice *win)
{
  irr::f32	current;

  if (node)
    {
      node->setFrameLoop(0, 48);
      node->getLoopMode();
      while (win->run() && _driver)
	{
	  _driver->beginScene(true, true, irr::video::SColor(0, 255, 255, 255));
	  current = node->getFrameNr();
	  if (current + 1 > node->getEndFrame())
	    {
	      _scenemgr->drawAll();
	      _driver->endScene();
	      return;
	    }
	  _scenemgr->drawAll();
	  _driver->endScene();
	}
    }
}

void            MeshPlayer::loadTexture(const std::string &name)
{
  if (node)
    {
      node->setMaterialTexture(0, _driver->getTexture(name.c_str()));
      node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    }
}

void            MeshPlayer::loadTexture(const char *name)
{
  if (node)
    {
      node->setMaterialTexture(0, _driver->getTexture(name));
      node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    }
}

void		MeshPlayer::deleteIt()
{
  if (node)
    node->remove();
}
