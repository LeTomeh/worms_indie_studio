//
// ErrorGUI.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:21:20 2016 Maxime LECOQ
// Last update Sat May 28 14:10:18 2016 Maxime LECOQ
//

#include	"GeneratorGUI.hh"

GeneratorGUI::GeneratorGUI(irr::IrrlichtDevice *win, irr::video::IVideoDriver *dri, irr::scene::ISceneManager *smgr, irr::gui::IGUIEnvironment *env, TextDisplayer *text, ImageDisplayer *img, const int &wi, const int &he, Sound *s) : AViewGUI(win, dri, smgr, env, text, img, wi, he, s)
{
}

GeneratorGUI::~GeneratorGUI()
{
}

GeneratorGUI::GeneratorGUI(const GeneratorGUI &o) : AViewGUI(o._window, o._driver, o._smgr, o._env, o._text, o._img, o._width, o._height, o._sound)
{
  _data = o._data;
}

GeneratorGUI	&GeneratorGUI::operator=(const GeneratorGUI &o)
{
  if (this != &o)
    {
      _window = o._window;
      _driver = o._driver;
      _smgr = o._smgr;
      _env = o._env;
      _width = o._width;
      _text = o._text;
      _img = o._img;
      _height = o._height;
      _data = o._data;
      _sound = o._sound;
    }
  return (*this);
}

void		GeneratorGUI::setGenerate(GeneratorData *data)
{
  _data = data;
}

void		GeneratorGUI::show()
{
  if (_data->getReload() == true)
    {
      _img->destroy(".cfg/perso.png");
      _data->reload(false);
    }
  _img->print(".cfg/perso.png", 0, 0, _width, _height);
  _img->print(_data->getNamePng(), _data->getX(), _data->getY(), _width, _height);
}

void		GeneratorGUI::generate()
{
  if (_data->effectIsOn() == true && _data->getEffect().size() != 0)
    {
      _sound->play(_data->getEffect(), false);
      _data->setEffect("");
    }
  _driver->beginScene(true, true, irr::video::SColor(0,255,255,255));
  _img->print("assets/back.png", 0, 0, _width, _height);
  show();
  textModule(COMMAND_G1, Rectangle(100, 100, _width, _height), Color(255, 0, 0), 5, false);
  textModule(COMMAND_G2, Rectangle(100, 125, _width, _height), Color(255, 0, 0), 5, false);
  textModule(COMMAND_G3, Rectangle(100, 150, _width, _height), Color(255, 0, 0), 5, false);
  textModule(COMMAND_G4, Rectangle(100, 175, _width, _height), Color(255, 0, 0), 5, false);
  _driver->endScene();
}
