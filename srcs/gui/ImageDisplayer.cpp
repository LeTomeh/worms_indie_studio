//
// ImageDisplayer.cpp for ImageDisplayer in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <andre_o@epitech.eu>
// 
// Started on  Thu Apr 28 19:53:40 2016 Thomas André
// Last update Fri Jun  3 22:35:36 2016 Maxime LECOQ
//

#include	"ImageDisplayer.hh"

ImageDisplayer::ImageDisplayer(irr::video::IVideoDriver *dri, irr::scene::ISceneManager *smgr) : _driver(dri), _smgr(smgr) {}

ImageDisplayer::~ImageDisplayer() {}

ImageDisplayer::ImageDisplayer(const ImageDisplayer &o)
{
  _driver = o._driver;
  _smgr = o._smgr;
}

ImageDisplayer&	ImageDisplayer::operator=(const ImageDisplayer &o)
{
  if (this != &o)
    {
      _driver = o._driver;
      _smgr = o._smgr;
    }
  return *this;
}

irr::video::ITexture		*ImageDisplayer::print(const std::string &s, const int &x, const int &y, const int &x2, const int &y2)
{
  irr::video::ITexture* images = _driver->getTexture(s.c_str());

  _driver->draw2DImage(images, irr::core::position2d<irr::s32>(x, y),
                       irr::core::rect<irr::s32>(0, 0, x2, y2), 0,
                       irr::video::SColor(255,255,255,255), true);
  return (images);
}

void				ImageDisplayer::destroy(const std::string &s)
{
  irr::video::ITexture* images = _driver->getTexture(s.c_str());
  _driver->removeTexture(images);
}

void				ImageDisplayer::destroy(const char *s)
{
  irr::video::ITexture* images = _driver->getTexture(s);
  _driver->removeTexture(images);
}

irr::video::ITexture		*ImageDisplayer::print(const char *s, const int &x, const int &y, const int &x2, const int &y2)
{
  irr::video::ITexture* images = _driver->getTexture(s);

  _driver->draw2DImage(images, irr::core::position2d<irr::s32>(x, y),
                       irr::core::rect<irr::s32>(0, 0, x2, y2), 0,
                       irr::video::SColor(255,255,255,255), true);
  return (images);
}

void				ImageDisplayer::moveNode(irr::scene::ISceneNode *node, const int &x, const int &y)
{
  float				x2;
  float				y2;
  int				xTmp;

  if (x < 1920 / 2)
    x2 = - 1.00 + (float)(x / (1920.00 / 2.00));
  else
    {
      xTmp = x - (1920 / 2);
      x2 = (float)xTmp / (1920.00 / 2.00);
    }
  y2 = 1 - (float)(y / (1080.00 / 2.00));
  if (node)
    node->setPosition(irr::core::vector3df(x2, y2, 1));
}
