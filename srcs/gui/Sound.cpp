//
// Sound.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sat May 21 13:03:15 2016 Maxime LECOQ
// Last update Sat Jun  4 01:19:50 2016 Maxime LECOQ
//

#include	"Sound.hh"

Sound::Sound()
{
  _engine = irrklang::createIrrKlangDevice();
  if (!_engine)
    throw ErrorSound("create device sound failed");
}

Sound::~Sound()
{
  if (!_engine)
    _engine->drop();
}

Sound::Sound(const Sound &c) : _engine(c._engine) {}

Sound &Sound::operator=(const Sound &c)
{
  if (this != &c)
    {
      _engine = c._engine;
    }
  return (*this);
}

void		Sound::play(const std::string &sound, const bool &ck, const bool &replay)
{
  if (_engine->isCurrentlyPlaying(sound.c_str()) == false || replay == true)
    _engine->play2D(sound.c_str(), ck, false, false, irrklang::ESM_AUTO_DETECT);
}

void		Sound::stopSound(const std::string &s)
{
  if (_engine->isCurrentlyPlaying(s.c_str()) == true)
  _engine->removeSoundSource(s.c_str());
}

void		Sound::stopAllSound()
{
  _engine->removeAllSoundSources();
}

bool		Sound::isPlaying(const std::string &sound)
{
  return (_engine->isCurrentlyPlaying(sound.c_str()));
}
