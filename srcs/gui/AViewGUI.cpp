//
// AViewGUI.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:21:20 2016 Maxime LECOQ
// Last update Sat Jun  4 09:47:24 2016 Maxime LECOQ
//

#include	"AViewGUI.hh"

AViewGUI::AViewGUI(irr::IrrlichtDevice *win, irr::video::IVideoDriver *dri, irr::scene::ISceneManager *smgr, irr::gui::IGUIEnvironment *env, TextDisplayer *text, ImageDisplayer *img, const int &wi, const int &he, Sound *s) : _window(win), _driver(dri), _smgr(smgr), _env(env), _text(text), _img(img), _width(wi), _height(he), _sound(s) {}

AViewGUI::~AViewGUI() {}

AViewGUI::AViewGUI(const AViewGUI &o)
{
  _window = o._window;
  _driver = o._driver;
  _smgr = o._smgr;
  _env = o._env;
  _width = o._width;
  _height = o._height;
  _text = o._text;
  _img = o._img;
  _sound = o._sound;
}

AViewGUI	&AViewGUI::operator=(const AViewGUI &o)
{
  if (this != &o)
    {
      _window = o._window;
      _driver = o._driver;
      _smgr = o._smgr;
      _env = o._env;
      _width = o._width;
      _height = o._height;
      _text = o._text;
      _img = o._img;
      _sound = o._sound;
    }
  return (*this);
}

void            AViewGUI::textModule(const std::string &msg, const Rectangle &rec, const Color &col, const unsigned int &size, const bool &ck)
{
  std::wstring                  tmp;
  irr::core::rect<irr::s32>     pos(rec.getX(), rec.getY(), rec.getXEnd(), rec.getYEnd());
  irr::video::SColor color(255, col.getRed(), col.getGreen(), col.getBlue());

  tmp.assign(msg.begin(), msg.end());
  _text->print(tmp.c_str(), pos, rec.getCenter(), color, size + (int)ck);
}

void            AViewGUI::textModule(const std::string &msg, const Rectangle &rec, const unsigned int &size, const bool &ck)
{
  std::wstring                  tmp;
  irr::core::rect<irr::s32>     pos(rec.getX(), rec.getY(), rec.getXEnd(), rec.getYEnd());
  Color                         col(255, 255, 255);
  Color                         colSel(143, 153, 243);

  if (ck == true)
    col = colSel;
  irr::video::SColor color(255, col.getRed(), col.getGreen(), col.getBlue());
  tmp.assign(msg.begin(), msg.end());
  _text->print(tmp.c_str(), pos, rec.getCenter(), color, size + (int)ck);
}

void                            AViewGUI::msgBox(const std::vector<std::string> &v)
{
  size_t                        i;
  size_t                        size;
  int                           y;

  y = 520;
  i = 0;
  size = v.size();
  if (size > 2)
    y -= ((size - 2) * 10);
  while (i < size)
    {
      textModule(v[i], Rectangle(1480, y, _width, 500), Color(143, 153, 243), 7, false);
      i++;
      y += 35;
    }
}
