//
// ErrorGUI.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:21:20 2016 Maxime LECOQ
// Last update Sat Jun  4 04:38:36 2016 Maxime LECOQ
//

#include	"ErrorGUI.hh"

ErrorGUI::ErrorGUI(irr::IrrlichtDevice *win, irr::video::IVideoDriver *dri, irr::scene::ISceneManager *smgr, irr::gui::IGUIEnvironment *env, TextDisplayer *text, ImageDisplayer *img, const int &wi, const int &he, Sound *s) : AViewGUI(win, dri, smgr, env, text, img, wi, he, s)
{
}

ErrorGUI::~ErrorGUI() {}

ErrorGUI::ErrorGUI(const ErrorGUI &o) : AViewGUI(o._window, o._driver, o._smgr, o._env, o._text, o._img, o._width, o._height, o._sound), _data(o._data)
{
}

ErrorGUI	&ErrorGUI::operator=(const ErrorGUI &o)
{
  if (this != &o)
    {
      _window = o._window;
      _driver = o._driver;
      _smgr = o._smgr;
      _env = o._env;
      _width = o._width;
      _text = o._text;
      _img = o._img;
      _height = o._height;
      _sound = o._sound;
      _data = o._data;
    }
  return (*this);
}

void				ErrorGUI::error()
{
  std::vector<std::string>	err;
  size_t			i;
  size_t			size;
  int				y;

  _sound->stopSound(MENU);
  _sound->stopSound(CLOCKTURN);
  _sound->stopSound(GAME);
  _sound->stopSound(ENDGAME);
  if (_data->getSoundOn() == true && _data->getSound().size() != 0)
    {
      _sound->play(_data->getSound(), false);
      _data->setSound("");
    }
  _driver->beginScene(true, true, irr::video::SColor(0,255,255,255));
  _img->print("assets/error.png", 0, 0, _width, _height);
  err = _data->getError();
  i = 0;
  size = err.size();
  if (size == 2)
    y = 440;
  else
    y = 415;
  while (i < size)
    {
      textModule(_data->getError()[i], Rectangle(0, y, _width, _height), Color(255, 0, 0), 8, false);
      i++;
      y += 75;
    }
  _driver->endScene();
}

void			ErrorGUI::setData(ErrorData *d)
{
  _data = d;
}
