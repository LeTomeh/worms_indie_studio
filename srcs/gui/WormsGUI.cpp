//
// WormsGUI.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:21:20 2016 Maxime LECOQ
// Last update Sun Jun  5 04:38:48 2016 Maxime LECOQ
//

#include	"WormsGUI.hh"

WormsGUI::WormsGUI(irr::IrrlichtDevice *win, irr::video::IVideoDriver *dri, irr::scene::ISceneManager *smgr, irr::gui::IGUIEnvironment *env, TextDisplayer *text, ImageDisplayer *img, const int &wi, const int &he, Sound *s) : AViewGUI(win, dri, smgr, env, text, img, wi, he, s)
{
  _cmd[SPACE_K] = irr::KEY_SPACE;
  _cmd[A_K] = irr::KEY_KEY_A;
  _cmd[B_K] = irr::KEY_KEY_B;
  _cmd[C_K] = irr::KEY_KEY_C;
  _cmd[D_K] = irr::KEY_KEY_D;
  _cmd[E_K] = irr::KEY_KEY_E;
  _cmd[F_K] = irr::KEY_KEY_F;
  _cmd[G_K] = irr::KEY_KEY_G;
  _cmd[H_K] = irr::KEY_KEY_H;
  _cmd[I_K] = irr::KEY_KEY_I;
  _cmd[J_K] = irr::KEY_KEY_J;
  _cmd[K_K] = irr::KEY_KEY_K;
  _cmd[L_K] = irr::KEY_KEY_L;
  _cmd[M_K] = irr::KEY_KEY_M;
  _cmd[N_K] = irr::KEY_KEY_N;
  _cmd[O_K] = irr::KEY_KEY_O;
  _cmd[P_K] = irr::KEY_KEY_P;
  _cmd[Q_K] = irr::KEY_KEY_Q;
  _cmd[R_K] = irr::KEY_KEY_R;
  _cmd[S_K] = irr::KEY_KEY_S;
  _cmd[T_K] = irr::KEY_KEY_T;
  _cmd[U_K] = irr::KEY_KEY_U;
  _cmd[V_K] = irr::KEY_KEY_V;
  _cmd[W_K] = irr::KEY_KEY_W;
  _cmd[X_K] = irr::KEY_KEY_X;
  _cmd[Y_K] = irr::KEY_KEY_Y;
  _cmd[Z_K] = irr::KEY_KEY_Z;
  _cmd[DOWN_K] = irr::KEY_DOWN;
  _cmd[UP_K] = irr::KEY_UP;
  _cmd[RIGHT_K] = irr::KEY_RIGHT;
  _cmd[LEFT_K] = irr::KEY_LEFT;
  _displayMode["normal"] = &WormsGUI::normal;
  _displayMode["pause"] = &WormsGUI::pause;
  _displayMode["inventory"] = &WormsGUI::inventory;
  _wind[-2] = WINDL2;
  _wind[-1] = WINDL;
  _wind[0] = NOWIND;
  _wind[1] = WINDR;
  _wind[2] = WINDR;
}

WormsGUI::~WormsGUI() {}

WormsGUI::WormsGUI(const WormsGUI &o) : AViewGUI(o._window, o._driver, o._smgr, o._env, o._text, o._img, o._width, o._height, o._sound), _view(o._view), _cmd(o._cmd), _displayMode(o._displayMode), _wind(o._wind)
{
}

WormsGUI	&WormsGUI::operator=(const WormsGUI &o)
{
  if (this != &o)
    {
      _window = o._window;
      _driver = o._driver;
      _smgr = o._smgr;
      _env = o._env;
      _width = o._width;
      _text = o._text;
      _img = o._img;
      _height = o._height;
      _view = o._view;
      _cmd = o._cmd;
      _displayMode = o._displayMode;
      _sound = o._sound;
      _wind = o._wind;
    }
  return (*this);
}

void		WormsGUI::setView(View *vi)
{
  _view = vi;
}

void		WormsGUI::map()
{
  irr::video::ITexture	*map;
  std::string		tmp;

  _sound->stopSound(MENU);
  _sound->stopSound(ENDGAME);
  if (_view->soundIsOn() == true)
    _sound->play(_view->getSound(), true);
  else
    _sound->stopSound(_view->getSound());
  if (_view->effectIsOn() == true)
    {
      while ((tmp = _view->getEffect()) != "")
	{
	  if (tmp != SEL && tmp != SWI)
	    _sound->play(tmp, false);
	  else
	    _sound->play(tmp, false, true);
	}
    }
  _driver->beginScene(true, true, irr::video::SColor(0,255,255,255));
  _img->print(_view->getBack(), 0, 0, _width, _height);
  if (_view->getLocation() != "pause")
    {
      if (_view->edited() == true)
	{
	  _img->destroy(_view->getMap());
	  _view->setEdited(false);
	}
      map = _img->print(_view->getMap(), 0, 0, _width, _height);
      if (map == NULL)
	map = _img->print(_view->getMap(), 0, 0, _width, _height);
    }
  else
    _img->print(_view->getPauseB(), 0, 0, _width, _height);

  (this->*_displayMode[_view->getLocation()])();
  _driver->endScene();
}

void			WormsGUI::normal()
{
  size_t	i;
  size_t	size;
  size_t	x;
  size_t	elem;
  std::vector<Team *>	team;
  std::vector<Player *>	players;
  Convert<int>		conv;
  std::string		itmp;
  std::vector<Drop>	drops;
  std::string		tmp;

  team = _view->getTeam();
  if (_view->getWaterLvl() + _view->getWaterMin() > 0 && _view->getWaterLvl() + _view->getWaterMin() <= 3)
    textModule("Times UP ! Water comes up !", Rectangle(0, _height - 100, _width, _height, true), Color(255, 0, 0), 5, false);   
  _img->print(_view->getWaves(), 0, _height - ((_view->getWaterLvl() + _view->getWaterMin()) * 2), _width, _height);
  textModule(_view->getData(), Rectangle(10, 5, _width, _height, false), Color(143, 153, 243), 2, false);
  textModule(_view->getTeamTurn(), Rectangle(0, _height - 40, _width, _height, true), Color(143, 153, 243), 3, false);
  _img->print(_wind[_view->getWind()], 5, 35, _width, _height);
  if (_view->getMin() < 10)
    itmp = "0";
  itmp += conv.toString(_view->getMin()) + ":";
  if (_view->getSec() < 10)
    itmp += "0";
  itmp += conv.toString(_view->getSec());
  if (_view->getTime() <= 10)
    {
      _img->print(_view->getClockAlert(), _width - 75, 5, _width - 5, _height);
      textModule(conv.toString(_view->getTime()), Rectangle(_width - 78, 33, _width, _height, true), Color(255, 0, 0), 4, false);
      _sound->play(CLOCKTURN, true);
    }
  else
    {
      _sound->stopSound(CLOCKTURN);
      _img->print(_view->getClock(), _width - 75, 5, _width - 5, _height);
      textModule(conv.toString(_view->getTime()), Rectangle(_width - 78, 33, _width, _height, true), 4, false);
    }
  textModule(itmp, Rectangle(_width - 78, 20, _width, _height, true), 1, false);
  if ((tmp = _view->getDesc()).size() > 0)
    textModule(tmp, Rectangle(0, 20, _width, _height, true), Color(255, 0, 0), 4, false);
  drops = _view->getDrop();
  size = drops.size();
  i = 0;
  while (i < size)
    {
      _img->print(drops[i].getSprite(), drops[i].getPosition().getX(), drops[i].getPosition().getY() - 15, _width, _height);
      i++;
    }
  size = team.size();
  i = 0;
  while (i < size)
    {
      std::string tmp;
      tmp += team[i]->getName() + " ";
      players = team[i]->getPlayers();
      elem = players.size();
      x = 0;
      while (x < elem)
	{
	  std::string tmp2;
	  tmp2 += tmp + conv.toString(players[x]->getLife());
	  textModule(tmp2, Rectangle(players[x]->getPosition().getX() - 50, players[x]->getPosition().getY() - 45,
				     players[x]->getPosition().getX() + 50, players[x]->getPosition().getY()), Color(255, 0, 0), 1, false);
	  _img->print(players[x]->getSprite(), players[x]->getPosition().getX() - 10, players[x]->getPosition().getY() - 25, _width, _height);
	  if (players[x]->getCursor()->getActive() == true)
	    {
	      _img->print(players[x]->getCursor()->getSprite(), players[x]->getCursor()->getX() - 4, players[x]->getCursor()->getY() - 4, 9, 9);
	    }
	  if (players[x]->getOk() == true)
	    _img->print(POS, players[x]->getPosition().getX() - 10, players[x]->getPosition().getY() - 72, _width, _height);
	  x++;
	}
      i++;
    }
  if (_view->getProjectile()->getActive() == true)
    {
      _img->print(_view->getProjectile()->getSprite(), _view->getProjectile()->getPosition().getX(), _view->getProjectile()->getPosition().getY(), _width, _height);
    }
}

void			WormsGUI::pause()
{
  int			height;
  size_t		i;
  size_t		size;
  std::vector<std::string>	ve;

  height = 140;
  _sound->stopSound(CLOCKTURN);
  textModule("PAUSE", Rectangle(_width / 2, height, _width, (_height / 4) * 3), Color(255, 0, 0), 11, false);
  height += 200;
  ve = _view->getPause();
  size = ve.size();
  i = 0;
  while (i < size)
    {
      if ((int)i == _view->getPosPause())
	textModule(ve[i], Rectangle(_width / 2, height, _width, (_height / 4) * 3), Color(143, 153, 243), 12, false);
      else
	textModule(ve[i], Rectangle(_width / 2, height, _width, (_height / 4) * 3), Color(255, 255, 255), 11, false);
      i++;
      height += 150;
    }
}

void				WormsGUI::inventory()
{
  size_t			i;
  size_t			size;
  size_t			x;
  size_t			elem;
  std::vector<Team *>		team;
  std::vector<Player *>		players;
  Convert<int>			conv;
  std::string			itmp;
  Inventory			*inv;
  std::vector<std::string>	weap;
  std::vector<Drop>		drops;

  inv = _view->getInventory();
  weap = inv->getWeapons();
  team = _view->getTeam();
  _img->print(_view->getWaves(), 0, _height - ((_view->getWaterLvl() + _view->getWaterMin()) * 2), _width, _height);
  textModule(_view->getData(), Rectangle(5, 5, _width, _height, false), Color(143, 153, 243), 2, false);
  textModule(_view->getTeamTurn(), Rectangle(0, _height - 40, _width, _height, true), Color(143, 153, 243), 3, false);
  _img->print(_wind[_view->getWind()], 10, 35, _width, _height);
  if (_view->getMin() < 10)
    itmp = "0";
  itmp += conv.toString(_view->getMin()) + ":";
  if (_view->getSec() < 10)
    itmp += "0";
  itmp += conv.toString(_view->getSec());
  if (_view->getTime() <= 10)
    {
      _img->print(_view->getClockAlert(), _width - 75, 5, _width - 5, _height);
      textModule(conv.toString(_view->getTime()), Rectangle(_width - 78, 33, _width, _height, true), Color(255, 0, 0), 4, false);
      _sound->play(CLOCKTURN, true);
    }
  else
    {
      _sound->stopSound(CLOCKTURN);
      _img->print(_view->getClock(), _width - 75, 5, _width - 5, _height);
      textModule(conv.toString(_view->getTime()), Rectangle(_width - 78, 33, _width, _height, true), 4, false);
    }
  textModule(itmp, Rectangle(_width - 78, 20, _width, _height, true), 1, false);
  drops = _view->getDrop();
  size = drops.size();
  i = 0;
  while (i < size)
    {
      _img->print(drops[i].getSprite(), drops[i].getPosition().getX(), drops[i].getPosition().getY() - 15, _width, _height);
      i++;
    }
  size = team.size();
  i = 0;
  while (i < size)
    {
      std::string tmp;
      tmp += team[i]->getName() + " ";
      players = team[i]->getPlayers();
      elem = players.size();
      x = 0;
      while (x < elem)
	{
	  std::string tmp2;
	  tmp2 += tmp + conv.toString(players[x]->getLife());
	  textModule(tmp2, Rectangle(players[x]->getPosition().getX() - 50, players[x]->getPosition().getY() - 45,
				     players[x]->getPosition().getX() + 50, players[x]->getPosition().getY()), Color(255, 0, 0), 1, false);
	  _img->print(players[x]->getSprite(), players[x]->getPosition().getX() - 10, players[x]->getPosition().getY() - 25, _width, _height);
	  if (players[x]->getOk() == true)
	    _img->print(POS, players[x]->getPosition().getX() - 10, players[x]->getPosition().getY() - 72, _width, _height);
	  x++;
	}
      i++;
    }
  i = 0;
  size = weap.size();
  x = 673;
  while (i < size)
    {
      if (inv->getWeapon(weap[i])->getMun() != 0)
	_img->print(inv->getWeapon(weap[i])->getPictureOn(), x, 385, 75, 75);
      else
	_img->print(inv->getWeapon(weap[i])->getPictureOff(), x, 385, 75, 75);
      if ((int)i == _view->getInvPos())
	_img->print(WSEL, x, 385, 75, 75);
      if (inv->getWeapon(weap[i])->getMun() > 0)
	textModule("x" + conv.toString(inv->getWeapon(weap[i])->getMun()), Rectangle(x + 5, 435, x + 75, 435, false), Color(255, 255, 255), 2, false);
      x += 100;
      i++;
    }
  textModule(inv->getWeapon(weap[_view->getInvPos()])->getDescription(), Rectangle(673, 460, x - 25, 460, true), Color(143, 152, 243), 4, false);
}

