//
// TextDisplayer.cpp for TextDisplayer in /home/thomas/rendu/cpp_indie_studio
// 
// Made by Thomas André
// Login   <andre_o@epitech.eu>
// 
// Started on  Thu Apr 28 19:53:40 2016 Thomas André
// Last update Sat Jun  4 09:54:55 2016 Maxime LECOQ
//

#include	"TextDisplayer.hh"

TextDisplayer::TextDisplayer(irr::IrrlichtDevice *win)
{
  _env = win->getGUIEnvironment();
  _skin = _env->getSkin();
  _save = 100;
}

TextDisplayer::~TextDisplayer() {}

TextDisplayer::TextDisplayer(const TextDisplayer &o)
{
  _env = o._env;
  _font = o._font;
  _skin = o._skin;
  _save = o._save;
}

TextDisplayer&	TextDisplayer::operator=(const TextDisplayer &o)
{
  if (this != &o)
    {
      _env = o._env;
      _font = o._font;
      _skin = o._skin;
      _save = o._save;
    }
  return *this;
}

void		TextDisplayer::addFont(const char *path)
{
  irr::gui::IGUIFont* el= _env->getFont(path);

  if (el != NULL)
    _font.push_back(el);
}

void		TextDisplayer::addFont(const std::string &path)
{
  irr::gui::IGUIFont * el= _env->getFont(path.c_str());

  if (el != NULL)
    _font.push_back(el);
}

void		TextDisplayer::print(const wchar_t *t, const irr::core::rect<irr::s32> &rec, const bool &center, const irr::video::SColor col, const unsigned int &ft)
{
  unsigned int	font;

  if (ft < _font.size())
    font = ft;
  else
    font = _font.size() - 1;
  if (_font.size() != 0)
    {
      if (font != _save)
	{
	  _skin->setFont(_font[font]);
	  _skin->setFont(_env->getBuiltInFont(), irr::gui::EGDF_TOOLTIP);
	  _save = font;
	}
      _font[font]->draw(t, rec, col, center, false);
    }
}
