//
// LauncherGUI.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Wed May  4 19:21:20 2016 Maxime LECOQ
// Last update Sat Jun  4 04:45:00 2016 Maxime LECOQ
//

#include	"LauncherGUI.hh"

LauncherGUI::LauncherGUI(irr::IrrlichtDevice *win, irr::video::IVideoDriver *dri, irr::scene::ISceneManager *smgr, irr::gui::IGUIEnvironment *env, TextDisplayer *text, ImageDisplayer *img, const int &wi, const int &he, Sound *s) : AViewGUI(win, dri, smgr, env, text, img, wi, he, s) {}

LauncherGUI::~LauncherGUI() {}

LauncherGUI::LauncherGUI(const LauncherGUI &o) : AViewGUI(o._window, o._driver, o._smgr, o._env, o._text, o._img, o._width, o._height, o._sound)
{
  _team = o._team;
}

LauncherGUI	&LauncherGUI::operator=(const LauncherGUI &o)
{
  if (this != &o)
    {
      _window = o._window;
      _driver = o._driver;
      _smgr = o._smgr;
      _env = o._env;
      _width = o._width;
      _height = o._height;
      _team = o._team;
      _text = o._text;
      _img = o._img;
      _sound = o._sound;
    }
  return (*this);
}

void		LauncherGUI::setTeamNameMaker(TeamNameMaker *te)
{
  _team = te;
}

void		LauncherGUI::launch()
{
  Vector                vec;
  std::string           tmp;

  if (_team->effectIsOn() == true && _team->getEffect().size() != 0)
    {
      _sound->play(_team->getEffect(), false, true);
      _team->setEffect("");
    }
  _driver->beginScene(true, true, irr::video::SColor(0,255,255,255));
  _img->print("assets/fond.jpg", 0, 0, _width, _height);
  textModule("Enter your team name", Rectangle(0, 130, _width / 2, _height / 2, true), Color(255, 0, 0), 9, false);
  if (_team->getMax() != 1)
    tmp = "< " + _team->getBasic() + " >";
  else
    tmp = _team->getBasic();
  if (_team->getPos() == 0)
    textModule(tmp,  Rectangle(0, 250, _width / 2, _height / 2, true), 7, true);
  else
    textModule(tmp,  Rectangle(0, 250, _width / 2, _height / 2, true), 7, false);
  textModule("Team Name : " + _team->getName(),  Rectangle(0 , 330, _width / 2, _height / 2, true), 7, false);
  msgBox(_team->getMap()[_team->getPos()]);
  drawMenu(_team->getPos());
  drawContinue(_team->getPos());
  _driver->endScene();
}

void            LauncherGUI::drawMenu(const int &i)
{
  if (i == 1)
    textModule("Menu", Rectangle(0, _height - 120, _width / 2, _height / 2, true), 4, true);
  else
    textModule("Menu", Rectangle(0, _height - 120, _width / 2, _height / 2, true), 4, false);
}

void            LauncherGUI::drawContinue(const int &i)
{
  if (i == 2)
    textModule("Continue", Rectangle(0, _height - 70, _width / 2, _height / 2, true), 4 , true);
  else
    textModule("Continue", Rectangle(0, _height - 70, _width / 2, _height / 2, true), 4, false);
}
