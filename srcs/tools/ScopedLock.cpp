//
// ScopedLock.cpp for  in /home/lecoq_m/cpp_plazza
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sat Apr 23 15:31:18 2016 Maxime LECOQ
// Last update Sat Apr 23 15:41:06 2016 Maxime LECOQ
//

#include	"ScopedLock.hh"

ScopedLock::ScopedLock(Mutex & m) : _mu(m)
{
  _mu.lock();
}

ScopedLock::ScopedLock(const ScopedLock &cop) : _mu(cop._mu) {}

ScopedLock		&ScopedLock::operator=(const ScopedLock &cop)
{
  if (this != &cop)
    _mu = cop._mu;
  return (*this);
}

ScopedLock::~ScopedLock()
{
  _mu.unlock();
}
