//
// AThread.cpp for AThread in /home/lecoq_m/cpp_plazza/bootstrap
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@lecoq-Epitech>
// 
// Started on  Wed Apr 13 10:50:55 2016 Maxime LECOQ
// Last update Fri Jun  3 10:37:27 2016 Maxime LECOQ
//

#include	"AThread.hh"

AThread::AThread() : _status(IThread::WAIT) {}

AThread::AThread(const AThread &cop) : _status(cop.getStatus()) {}

AThread::~AThread() {}

AThread		&AThread::operator=(const AThread &cop)
{
  if (this != &cop)
    {
      _status = cop.getStatus();
    }
  return (*this);
}

IThread::ThreadStatus		AThread::getStatus() const
{
  return (_status);
}

void				AThread::run(void *(*f)(void *), void *arg)
{
  _thread = new std::thread(f, arg);
  _status = IThread::RUN;
}

template<class T>
void				AThread::run(void *(T::*f)(void *), void *arg)
{
  _thread = new std::thread(f, arg);
  _status = IThread::RUN;
}
           
void				AThread::waitDeath()
{
  if (_thread->joinable() == true)
    {
      _thread->join();
      delete (_thread);
    }
  _status = IThread::WAIT;
}

void				AThread::setStatus(const IThread::ThreadStatus &st)
{
  _status = st;
}
