//
// Mutex.cpp for  in /home/lecoq_m/cpp_plazza
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Sat Apr 23 15:34:12 2016 Maxime LECOQ
// Last update Sat Apr 23 15:40:18 2016 Maxime LECOQ
//

#include	"Mutex.hh"

Mutex::Mutex()
{
  pthread_mutex_init(&_mu, NULL);
}

Mutex::Mutex(const Mutex &cop) : _mu(cop._mu) {}

Mutex		&Mutex::operator=(const Mutex &cop)
{
  if (this != &cop)
    _mu = cop._mu;
  return (*this);
}

void	Mutex::lock()
{
  pthread_mutex_lock(&_mu);
}

void	Mutex::unlock()
{
  pthread_mutex_unlock(&_mu);
}

bool	Mutex::trylock()
{
  return ((bool)pthread_mutex_trylock(&_mu));
}

Mutex::~Mutex()
{
}
