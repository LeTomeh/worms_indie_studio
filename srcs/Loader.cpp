//
// Loader.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas Andr�
// Login   <andre_o@epitech.net>
// 
// Started on  Tue Apr 26 17:17:17 2016 Thomas Andr�
// Last update Fri Jun  3 22:34:50 2016 Maxime LECOQ
//

#include	"Loader.hh"

Loader::Loader()
{
  _controls.push_back("RIGHT");
  _controls.push_back("LEFT");
  _controls.push_back("UP");
  _controls.push_back("DOWN");
  _controls.push_back("PREVDATA");
  _controls.push_back("NEXTDATA");
  _controls.push_back("NEXTTURN");
  _controls.push_back("JUMPLEFT");
  _controls.push_back("JUMPRIGHT");
  _controls.push_back("INVENTORY");
  _controls.push_back("USETOOL");
  _controlsDefault["RIGHT"] = 6;
  _controlsDefault["LEFT"] = 5;
  _controlsDefault["UP"] = 4;
  _controlsDefault["DOWN"] = 3;
  _controlsDefault["PREVDATA"] = 7;
  _controlsDefault["NEXTDATA"] = 32;
  _controlsDefault["NEXTTURN"] = 20;
  _controlsDefault["JUMPLEFT"] = 23;
  _controlsDefault["JUMPRIGHT"] = 25;
  _controlsDefault["INVENTORY"] = 15;
  _controlsDefault["USETOOL"] = 2;
}

Loader::~Loader() {}

Loader::Loader(const Loader &cop)
{
  (void)cop;
}

Loader	&Loader::operator=(const Loader &cop)
{
  (void)cop;
  return (*this);
}

Data		Loader::load()
{
  File		file(".cfg/music");
  std::string	tmp;
  Epur		epur;
  Convert<bool> t;
  Convert<int>	conv;
  String	st;

  try
    {
      tmp = file.read();
      tmp = epur.epur(tmp, '\n');
      if (tmp.empty() == true || st.number(tmp) == false)
	_data.setMusic(true);
      else
	_data.setMusic(t.toNumber(tmp));
    }
  catch (AError const &e) { _data.setMusic(true); }
  _data.setAmbiance(false);	
  try
    {
      Png p(".cfg/perso.png");
      if (p.getHeight() != 1080 || p.getWidth() != 1920)
	_data.setPerso(false);
      else
	_data.setPerso(true);
      p.clean();
    }
  catch (AError const &e) { _data.setPerso(false); }
  try
    {
      file.setFile(".cfg/effect");
      tmp = file.read();
      tmp = epur.epur(tmp, '\n');
      if (tmp.empty() == true || st.number(tmp) == false)
	_data.setEffect(true);
      else
	_data.setEffect(t.toNumber(tmp));
    }
  catch (AError const &e) { _data.setEffect(true); }
  try
    {
      file.setFile(".cfg/turnP");
      tmp = file.read();
      tmp = epur.epur(tmp, '\n');
      if (tmp.empty() == false && st.number(tmp) == true)
	_data.setTurnP(conv.toNumber(tmp));
    }
  catch (AError const &e) { _data.setTurnP(30); }
  try
    {
      file.setFile(".cfg/turnN");
      tmp = file.read();
      tmp = epur.epur(tmp, '\n');
      if (tmp.empty() == false && st.number(tmp) == true)
	_data.setTurnN(conv.toNumber(tmp));
    }
  catch (AError const &e) { _data.setTurnN(45); }
  try
    {
      file.setFile(".cfg/partyP");
      tmp = file.read();
      tmp = epur.epur(tmp, '\n');
      if (tmp.empty() == false && st.number(tmp) == true)
	_data.setPartyP(conv.toNumber(tmp));
    }
  catch (AError const &e) { _data.setPartyP(20); }
  try
    {
      file.setFile(".cfg/partyN");
      tmp = file.read();
      tmp = epur.epur(tmp, '\n');
      if (tmp.empty() == false && st.number(tmp) == true)
	_data.setPartyN(conv.toNumber(tmp));
    }
  catch (AError const &e) { _data.setPartyN(10); }
  try
    {
      manageSave();
    }
  catch (AError const &e) { _data.setSave(false); }
  try
    {
      file.setFile(".cfg/.drop");
      _data.setDrop(file.read());
    }
  catch (AError const &e) { _data.setDrop(""); }
  try
    {
      loadControl();
    }
  catch (const AError &e)
    {
      _data.setControl(_controlsDefault);
    }
  _data.setControlDefault(_controlsDefault);
  _data.setControllerList(_controls);
  return (_data);
}

void				Loader::loadControl()
{
  File				file(".cfg/control");
  std::string			t;
  Epur				epur;
  Vector			vectorize;
  std::vector<std::string>	v;
  std::map<std::string, int>	controls;
  size_t			i;
  size_t			size;
  size_t			found;
  Convert<int>			conv;

  t = file.read();
  t = epur.epur(t, '\n');
  v = vectorize.getVector(t, '\n');
  i = 0;
  size = v.size();
  while (i < size)
    {
      found = v[i].find(':');
      if (found < v[i].size() && v[i].substr(found + 1, v[i].size()).empty() == false
	  && _controlsDefault.find(v[i].substr(0, found)) != _controlsDefault.end()
	  && v[i].substr(found + 1, v[i].size()) != "0")
	controls[v[i].substr(0, found)] = conv.toNumber(v[i].substr(found + 1, v[i].size()));
      i++;
    }
  i = 0;
  size = _controls.size();
  while (i < size)
    {
      if (controls.find(_controls[i]) == controls.end())
	controls[_controls[i]] = _controlsDefault[_controls[i]];
      i++;
    }
  checkControls(controls);
}

void		Loader::checkControls(std::map<std::string, int> &controls)
{
  size_t	i;
  size_t	size;
  size_t	x;
  bool		ok;

  i = 0;
  ok = true;
  size = _controls.size();
  while (i < size)
    {
      x = i + 1;
      while (x < size)
	{
	  if (controls[_controls[i]] == controls[_controls[x]])
	    {
	      if (controls[_controls[i]] != _controlsDefault[_controls[i]])
		controls[_controls[i]] = _controlsDefault[_controls[i]];
	      else
		controls[_controls[i]] = _controlsDefault[_controls[x]];
	      checkControls(controls);
	      ok = false;
	      i = size;
	      x = size;
	    }
	  x++;
	}
      i++;
    }
  if (ok == true)
      _data.setControl(controls);
}

std::vector<std::string>		Loader::getControls() const
{
  return (_controls);
}

Scores					Loader::loadScores()
{
  Scores				ret;
  File					file(".cfg/scores");
  std::string				tmp;
  Vector				vec;
  std::vector<std::string>		scores;

  try
    {
      tmp = file.read();
    }
  catch (AError const &e)
    {
      tmp = "";
    }
  scores = vec.getVector(tmp, '\n');
  ret.setScores(scores);
  return (ret);
}

void					Loader::manageSave()
{
  File					file(".cfg/.cnf");
  std::string				tmp;
  Vector				vec;
  std::vector<std::string>		line;

  try
    {
      Png					png(".cfg/map.png");

      png.clean();
      tmp = file.read();
      (void)png;
      line = vec.getVector(tmp, '\n');
      if (line.size() < 8 || line[0].substr(0, 13) != "WormsPerTeam "
	  || line[1].substr(0, 5) != "Mode " || line[2].substr(0, 5) != "Turn "
	  || line[3].substr(0, 5) != "Time " || line[4].substr(0, 9) != "WaterLvl " 
	  || (line[5].substr(0, 14) != ".cfg/map/world" && line[5] != "assets/back.png"))
	throw ErrorLoader("");
      _data.setSave(true);
    }
  catch (AError const &e)
    {
      _data.setSave(false);
    }
}
