//
// Data.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Maxime LECOQ
// Login   <lecoq_m@epitech.net>
// 
// Started on  Tue Apr 26 17:46:37 2016 Maxime LECOQ
// Last update Thu Jun  2 23:16:42 2016 Maxime LECOQ
//

#include	"Data.hh"

Data::Data() : _music(true), _ambiance(true), _effect(true), _save(true), _tP(30), _tN(45), _pP(10), _pN(20), _perso(false), _drop("") {}

Data::~Data() {}

Data::Data(const Data &c) : _music(c._music), _ambiance(c._ambiance), _effect(c._effect), _save(c._save), _control(c._control), _controlDefault(c._controlDefault), _controllerList(c._controllerList), _tP(c._tP), _tN(c._tN), _pP(c._pP), _pN(c._pN), _perso(c._perso), _drop(c._drop) {}

Data		&Data::operator=(const Data &c)
{
  if (this != &c)
    {
      _music = c._music;
      _ambiance = c._ambiance;
      _effect = c._effect;
      _save = c._save;
      _control = c._control;
      _controlDefault = c._controlDefault;
      _controllerList = c._controllerList;
      _tP = c._tP;
      _tN = c._tN;
      _pP = c._pP;
      _pN = c._pN;
      _perso = c._perso;
      _drop = c._drop;
    }
  return (*this);
}

void		Data::setMusic(const bool &c)
{
  File		f(".cfg/music");
  Convert<bool>	conv;

  try
    {
      f.writeTronc(conv.toString(c));
    }
  catch (AError const &e) { e.notCriticalError(); }
  _music = c;
}

void		Data::setAmbiance(const bool &c)
{
  (void)c;
}

void		Data::setEffect(const bool &c)
{
  File		f(".cfg/effect");
  Convert<bool>	conv;

  try
    {
      f.writeTronc(conv.toString(c));
    }
  catch (AError const &e) { e.notCriticalError(); }
  _effect = c;
}

void		Data::setSave(const bool &c)
{
  _save = c;
}

void		Data::setControl(const std::map<std::string, int> &c)
{
  std::string	tmp;
  File		file(".cfg/control");
  size_t	i;
  size_t	size;
  Convert<int>	conv;

  i = 0;
  size = _controllerList.size();
  _control = c;
  while (i < size)
    {
      tmp += _controllerList[i] + ':' + conv.toString(_control[_controllerList[i]]) + '\n';
      i++;
    }
  try
    {
      file.writeTronc(tmp);
    }
  catch (AError const &e) { e.notCriticalError(); }
}

void		Data::setTurnP(const int &c)
{
  File		f(".cfg/turnP");
  Convert<int>	conv;

  try
    {
      f.writeTronc(conv.toString(c));
    }
  catch (AError const &e) {}
  _tP = c;
}

void		Data::setTurnN(const int &c)
{
  File		f(".cfg/turnN");
  Convert<int>	conv;

  try
    {
      f.writeTronc(conv.toString(c));
    }
  catch (AError const &e) {}
  _tN = c;
}

void		Data::setPartyP(const int &c)
{
  File		f(".cfg/partyP");
  Convert<int>	conv;

  try
    {
      f.writeTronc(conv.toString(c));
    }
  catch (AError const &e) {}
  _pP = c;
}

void		Data::setPartyN(const int &c)
{
  File		f(".cfg/partyN");
  Convert<int>	conv;

  try
    {
      f.writeTronc(conv.toString(c));
    }
  catch (AError const &e) {}
  _pN = c;
}

int		Data::getPartyP() const
{
  return (_pP);
}

int		Data::getPartyN() const
{
  return (_pN);
}

int		Data::getTurnN() const
{
  return (_tN);
}

int		Data::getTurnP() const
{
  return (_tP);
}

void		Data::setControlDefault(const std::map<std::string, int> &c)
{
  _controlDefault = c;
}

void		Data::setControllerList(const std::vector<std::string> &c)
{
  _controllerList = c;
}

bool		Data::getMusic() const
{
  return (_music);
}

bool		Data::getAmbiance() const
{
  return (_ambiance);
}

bool		Data::getEffect() const
{
  return (_effect);
}

bool		Data::getSave() const
{
  return (_save);
}

std::map<std::string, int>	Data::getControl()
{
  return (_control);
}

std::map<std::string, int>	Data::getControlDefault()
{
  return (_controlDefault);
}

std::vector<std::string>	Data::getControllerList()
{
  return (_controllerList);
}

void				Data::setPerso(const bool &c)
{
  _perso = c;
}

bool				Data::getPerso() const
{
  return (_perso);
}

void				Data::setDrop(const std::string &c)
{
  _drop = c;
}

const std::string		&Data::getDrop() const
{
  return (_drop);
}
