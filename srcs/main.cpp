//
// main.cpp for  in /home/lecoq_m/cpp_indie_studio
// 
// Made by Thomas Andr�
// Login   <andre_o@epitech.net>
// 
// Started on  Tue Apr 26 16:04:05 2016 Thomas Andr�
// Last update Sat Jun  4 00:59:43 2016 Maxime LECOQ
//

#include	"Controller.hh"

void		*runCatchingEvent(void *arg)
{
  EventReceiver	*ev;

  ev = (EventReceiver *)arg;
  ev->runObservable();
  return (NULL);
}

void            introduction()
{
  std::system("cvlc -f -q --no-video-title-show --play-and-exit --no-loop --video-on-top --global-key-quit q assets/intro.avi");
}

void            *runGame(void *arg)
{
  Worms		*worms;

  worms = (Worms *)arg;
  worms->runObservable();
  return (arg);
}

void		*runCnf(void *arg)
{
  Worms		*worms;

  worms = (Worms *)arg;
  while (worms->getGameCnf() == true)
    try
      {
	std::this_thread::sleep_for(std::chrono::seconds(1));
	worms->writeCnf();
      }
    catch (AError const &e) {}
  return (arg);
}

void		*runMap(void *arg)
{
  Worms		*worms;

  worms = (Worms *)arg;
  while (worms->getGameMap() == true)
    try
      {
	worms->saveMap();
      }
    catch (AError const &e) {}
  return (arg);
}

void		*runDestroy(void *arg)
{
  Worms		*worms;

  worms = (Worms *)arg;
  while (worms->getGameMap() == true)
    try
      {
	std::this_thread::sleep_for (std::chrono::seconds(1));
	worms->destroyerMap();
      }
    catch (AError const &e) {}
  return (arg);
}

void		*runDrop(void *arg)
{
  Worms		*worms;

  worms = (Worms *)arg;
  while (worms->getGameMap() == true)
    try
      {
	std::this_thread::sleep_for (std::chrono::seconds(1));
	worms->writeDrop();
      }
    catch (AError const &e) {}
  return (arg);
}

void		start()
{
  Controller	worms;

  worms.start();
}

int		main(int ac, char **av)
{
  (void)av;
  if (ac != 1)
    std::cerr << "No param is wait to launch the project." << std::endl;
  introduction();
  start();
  return (0);
}
